<?php

return [
    'adminEmail' => 'info@geesstudios.com',
	'appName' => 'Resto POS',
	// 'baseURL' => 'http://localhost/fnb/',
	'loginURL' => 'https://projects.geesstudios.com/gees-resto/',
	'baseURL' => 'https://projects.geesstudios.com/gees-resto/',
	 // 'baseURL' => 'http://192.168.1.99/farmsso/',
    'maskMoneyOptions' => [
        'prefix' => 'Rp ',
        'suffix' => '',
        'affixesStay' => true,
        'thousands' => ',',
        'decimal' => '.',
        'precision' => 0,
        'allowZero' => true,
        'allowNegative' => false,
    ]
];
