<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'name' => 'Gees Resto',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'gii' => 'yii\gii\Module',
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableUnconfirmedLogin' => true,
            'confirmWithin' => 21600,
            'admins' => ['ademahendra', 'richfein71'],
            'controllerMap' => [
                'security' => 'app\controllers\user\SecurityController'
            ],
            'modelMap' => [
                'User' => 'app\models\override\User',
            ],
        ],
        'dynagrid' => [
            'class' => '\kartik\dynagrid\Module',
            // other module settings
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module'
            // enter optional module parameters below - only if you need to
            // use your own export download action or custom translation
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ],
        'purchasing' => [
            'class' => 'app\modules\purchasing\Purchasing',
        ],
        'master' => [
            'class' => 'app\modules\master\Master',
        ],
        'pembelian' => [
            'class' => 'app\modules\pembelian\Pembelian',
        ],
		
		'order' =>[
			'class' => 'app\modules\order\Order',
		]
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '6NDRCWPcEYnHINkYoilyYNDcFfD5mhMJ',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'dektrium\user\models\User',
            'identityCookie' => [
                'name' => '_gees_resto',
                'path' => '/gees-resto'  // correct path for the basictest1 app.
            ]
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'rules' => array(
                '' => 'site/index',
                'index' => 'site/index',
                'calendar' => 'reservation-date',
                'about' => 'site/about',
                'contact' => 'site/contact',
                'user/logout' => 'user/security/logout',
                'user/login' => 'user/security/login',
                'site/logout' => 'user/security/logout',
                'site/login' => 'user/security/login',
                'pos/<action:\w+>' => 'fnb-sale/<action>',
                'menu/<action:\w+>' => 'fnb-ingredients/<action>',
//		            'menu/<action:\w+>/<id:\d+>' => 'fnb-ingredients/<action>',
                'stock/<action:\w+>' => 'fnb-stock/<action>',
				
                'purchasing' => 'purchasing',
                'purchasing/<action:\w+>' => 'purchasing/default/<action>',
                
				'pembelian' => 'pembelian',
                'pembelian/<action:\w+>' => 'pembelian/default/<action>',
                
				'order' => 'order',
				'order/<action:\w+>' => 'order/default/<action>',
				
				'ingredients' => 'fnb-ingredients',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ],
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js' => []
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js' => []
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [],
                ],
                'yii\jui\JuiAsset' => [
                    'css' => [],
                    'js' => []
                ]
            ],
            'linkAssets' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@app/mailer',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.zoho.com',
                'username' => 'info@geesstudios.com',
                'password' => 'OpenMe!!',
                'port' => '465',
                'encryption' => 'ssl',
            ],
        ],
        'view' => [
            /*             'theme' => [
                            'basePath' => '@app/web/theme',
                            'baseUrl' => '@web/web/theme',
                            'pathMap' => [
                                '@app/views' => '@app/web/theme',
                                '@dektrium/user/views/security' => '@app/web/theme/views',
                                // '@dektrium/user/views/registration' => '@app/web/admin/views',
                            ],
                        ], */
            'theme' => [
                'basePath' => '@app/web/metronic',
                'baseUrl' => '@web/web/metronic',
                'pathMap' => [
                    '@app/views' => '@app/web/metronic',
                    '@dektrium/user/views/security' => '@app/web/metronic/views',
                    // '@dektrium/user/views/registration' => '@app/web/admin/views',
                ],
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'generators' => [ //here
            'crud' => [ // generator name
                'class' => 'yii\gii\generators\crud\Generator', // generator class
                'templates' => [ //setting for out templates
                    'myCrud' => '@app/web/gii/default', // template name => path to template
                ]
            ]
        ],
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1', '*'],
    ];
}

return $config;
