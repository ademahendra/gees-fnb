<?php

use yii\helpers\Html;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PembelianSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar Pembelian';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card bg-white">
    <div class="portlet mt-element-ribbon light portlet-fit bordered">
        <div class="ribbon ribbon-left ribbon-clip ribbon-shadow ribbon-border-dash-hor ribbon-color-success green-jungle uppercase">
            <div class="ribbon-sub ribbon-clip ribbon-left"></div>
            <i class="icon-notebook font-white"></i> <?= $this->title ?>
        </div>
        <div class="portlet-title">
            <div class="actions">
                <div class="btn-group btn-group-devided">

                </div>
            </div>
        </div>
        <div class="portlet-body">
            <div class="widget-content padding">

                <div class="pembelian-index">
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                    <p>
                        <?= Html::a('Tambah Pembelian', ['create'], ['class' => 'btn btn-success']) ?>
                    </p>


                    <?php
                    $columns = [
                        ['class' => 'kartik\grid\SerialColumn', 'order' => DynaGrid::ORDER_FIX_LEFT],
                        [
                            'class' => 'kartik\grid\ExpandRowColumn',
                            'expandAllTitle' => 'Expand All',
                            'collapseTitle' => 'Collapse All',
                            'expandTitle' => 'Expand',
                            'expandIcon' => '<span class="fa fa-plus"></span>',
                            'collapseIcon' => '<span class="fa fa-minus"></span>',
                            'value' => function ($model, $key, $index) {
                                return GridView::ROW_COLLAPSED;
                            },
                            'detailRowCssClass' => GridView::TYPE_DEFAULT,
                            'pageSummary' => false,
                            'detailUrl' => Url::to('pembelian/detail'),
                            'detailAnimationDuration' => 100,
                            'expandOneOnly' => true,
                            'enableCache' => false
                        ],

                        'no_faktur',
//            'no_surat_jalan',

                        [
                            'attribute' => 'tanggal_faktur',
                            'format' => 'raw',
                            'filterType' => GridView::FILTER_DATE,
                            'value' => function ($data) {
                                return date('d M Y', strtotime($data->tanggal_faktur));
                            }
                        ],
                        'no_faktur',
//			[
//				'attribute' => 'harga_beli',
//				'format' => 'raw',
//				'value' => function($data){
//					return number_format($data->harga_beli, 2, ',','.');
//				}
//			],
//			[
//				'attribute' => 'diskon',
//				'format' => 'raw',
//				'value' => function($data){
//					return number_format($data->diskon, 2, ',','.');
//				}
//			],
                        [
                            'attribute' => 'harga_kena_pajak',
                            'format' => 'raw',
                            'label' => 'Total',
                            'value' => function ($data) {
                                return number_format($data->harga_kena_pajak, 2, ',', '.');
                            }
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{edit} {delete}',
                            'buttons' => [
                                'edit' => function ($url, $model, $key) {     // render your custom button
                                    return Html::a('<span class="fa fa-pencil"></span>', ['update', 'id' => $model->id], ['class' => 'btn btn-success btn-xs', 'id' => 'multipleButton']);
                                },
                                'delete' => function ($url, $model, $key) {     // render your custom button
                                    return Html::a('<span class="fa fa-trash"></span>', ['delete', 'id' => $model->id], ['data-method' => 'post', 'class' => 'btn btn-primary btn-xs', 'id' => 'printButton']);
                                }
                            ],
                        ],
//			[
//				'class'=>'kartik\grid\ActionColumn',
//				'dropdown'=>false,
//				'order'=>DynaGrid::ORDER_FIX_RIGHT
//			],
                        // ['class'=>'kartik\grid\CheckboxColumn', 'order'=>DynaGrid::ORDER_FIX_RIGHT],
                    ];

                    echo DynaGrid::widget([
                        'columns' => $columns,
                        'storage' => DynaGrid::TYPE_COOKIE,
                        'theme' => 'panel-success',
                        'gridOptions' => [
                            'dataProvider' => $dataProvider,
//                            'filterModel' => $searchModel,
                            'panel' => ['heading' => '<h3 class="panel-title">Pembelian</h3>'],
                            // 'enableRowClick' => true,
                        ],
                        'options' => [
                            'id' => 'dynagrid-1',
                            'storage' => DynaGrid::TYPE_SESSION
                        ],

                    ]);
                    ?>


                </div>
            </div>
        </div>
    </div>
</div>
