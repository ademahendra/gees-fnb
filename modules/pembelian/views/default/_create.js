/**
 * Created by SERVER on 23/09/2017.
 */
var body = $('body');
var onXHR = false;

body.on('keyup', '#diskon_persen', function (e) {
    e.preventDefault();
    calculateDiscount();
});

body.on('keyup', '#diskon', function (e) {
    e.preventDefault();
    calculateDiscount(false);
});

body.on('blur', '#qty', function (e) {
    e.preventDefault();
    var receive = parseFloat($('#qty').val());
    var itemID = $('#barangList').val();
    var remainOrderQTY = 0;
    $('.idItem').each(function (i, val) {
        if(itemID == $(this).val()){
            remainOrderQTY += parseFloat($(this).parent().find('.orderQtyItem').val());
        }
    });

    $('.idReceive').each(function (i, val) {
        if(itemID == $(this).val()){
            receive += parseFloat($(this).parent().find('.qtyReceive').val());
        }
    });
    console.log(receive+" - "+remainOrderQTY);
    if(receive >  remainOrderQTY){
        bootbox.alert('Jumlah Terima Lebih Besar dari Jumlah Order. Anda Yakin?');
    }
    calculateDiscount(false);
});

body.on('keyup', '#diskonGlobalPercent', function (e) {
    e.preventDefault();
    calculateDiscountGlobal();
});

body.on('keyup', '#diskonGlobal', function (e) {
    e.preventDefault();
    calculateDiscountGlobal(false);
});

body.on('change', 'select#barangList', function () {
    var barangList = $('select#barangList').val();
    $.get(baseUrl + 'purchasing/detailbarang?id=' + barangList, function (jsonData) {
        var json = jQuery.parseJSON(jsonData);
        if (json.status) {
            $('#satuanBarang').html(json.satuan);
        }
    });
});

var newBarangDialog;
body.on('click', '#newItem', function (e) {
    e.preventDefault();
    newBarangDialog = BootstrapDialog.show({
        message: function (dialog) {
            var message = $('<div></div>');
            var pageToLoad = dialog.getData('pageToLoad');
            message.load(pageToLoad);
            return message;
        },
        title: 'New Item',
        data: {
            'pageToLoad': baseUrl + 'barang/new'
        },
        size: BootstrapDialog.SIZE_LARGE
    });
});

var addpenjualan = [];
function print(url) {
    var _this = this;
    var iframeId = 'printFrame';
    var iframe = $('iframe#printFrame');
    iframe.attr('src', url);

    iframe.load(function () {
        callPrint(iframeId);
    });
}

function calculateDiscount(percent) {
    var percent = (typeof(percent) != 'undefined')?percent:true;
    var discPercent = $('#diskon_persen').val();
    var discount = $('#diskon').val();
    var hpp = $('#hargaBeli').val();
    var ppn = parseInt($('#ppn').val());

    if ((discount !== '' || discPercent !== '') && hpp !== '') {
        if (discPercent !== '') {
            if (percent) {
                var discRate = (discPercent / 100) * hpp;
                $('#diskon').val(parseInt(discRate).toFixed(0));
                $('#diskon-disp').val(parseInt(discRate).toFixed(2));
            }
        }

        if (discount !== '') {
            if (percent == false) {
                var discPerc = discount / hpp * 100;
                // console.log('I am here: '+discPerc);
                $('#diskon_persen').val(parseInt(discPerc).toFixed(0));
            }
        }

    }
    discountRate = $('#diskon').val();
    if (hpp !== '' && ppn !== '') {
        if (ppn == 10) {
            var hsp = ((parseInt(hpp) - parseInt(discountRate)) + ((parseInt(hpp) - parseInt(discountRate)) * ppn / 100)).toFixed(0);
        } else {
            var hsp = (parseInt(hpp - parseInt(discountRate))).toFixed(0);
        }
        $('#harga_kena_pajak').val(hsp);
        $('#harga_kena_pajak-disp').val(parseInt(hsp).toFixed(2));
    }
}

function calculateDiscountGlobal(percent) {
    var percent = (typeof(percent) != 'undefined')?percent:true;
    var discPercent = $('#diskonGlobalPercent').val();
    var discount = $('#diskonGlobal').val();
    var hpp = $('#hargaTotal').val();
    var ppn = parseInt($('#ppn').val());
    ;

    if ((discount !== '' || discPercent !== '') && hpp !== '') {
        if (discPercent !== '') {
            if (percent) {
                var discRate = (discPercent / 100) * hpp;
                $('#diskonGlobal').val(parseInt(discRate).toFixed(0));
                //update harga setelah pajak
            }
        }

        if (discount !== '') {
            if (percent == false) {
                var discPerc = discount / hpp * 100;
                // console.log('I am here GLOBAL : '+discPerc);
                $('#diskonGlobalPercent').val(parseInt(discPerc).toFixed(0));
                //update harga setelah pajak
            }
        }
    }
    discountRate = $('#diskonGlobal').val();
    var hsp = 0;
    $('.totalHargaItemPPN').each(function (i, val) {
        hsp += parseFloat($(this).val());
    });
    $('#hargaTotalPajak').val(hsp);
    $('#hargaTotalPajakFormat').val(humanizeNumber(hsp));
    // console.log(hpp+' ---- '+ppn);
    // if (hpp !== '' && ppn !== '') {
    //     if (ppn == 10) {
    //         var hsp = ((parseInt(hpp) - parseInt(discountRate)) + ((parseInt(hpp) - parseInt(discountRate)) * ppn / 100)).toFixed(0);
    //         // console.log('HSP '+hsp);
    //     } else {
    //         var hsp = (parseInt(hpp - parseInt(discountRate))).toFixed(0);
    //         // console.log('HP '+hsp);
    //     }
    //     $('#hargaTotalPajak').val(hsp);
    //     $('#hargaTotalFormat').val(formatCurrency(parseFloat($('#hargaTotal').val())));
    //     $('#hargaTotalPajakFormat').val(formatCurrency(parseFloat($('#hargaTotalPajak').val())));
    //
    //
    // }
}

function callPrint(iframeId) {
    var PDF = document.getElementById(iframeId);
    PDF.focus();
    PDF.contentWindow.print();
}
//$("#addbutton").attr('disabled',true);

$("body").on('click', '.remove', function (e) {
    $(this).closest('tr').remove();
    var removeItem = $(this).closest('tr').attr('id');

    calculateDiscountGlobal();
    delete addpenjualan[removeItem];

    //$("#addbutton").fadeIn();
});

body.on('keyup', '#hargaBeli', function () {
    calculateDiscount();
});


body.on('keyup', '#harga_kena_pajak', function () {
    // var hkp = $('#harga_kena_pajak').val();
    // var hpp = $('#hargaBeli').val();
    // var ppn =  $('#ppn').val();
    // if(hkp !== '' && ppn !== ''){
    // if(ppn === '10'){
    // var hsp = (parseInt(hkp) / 1.1).toFixed(0);	
    // } else {
    // var hsp = parseInt(hkp).toFixed(0);
    // }

    // $('#hargaBeli').val(hsp);
    // }
});


body.on('change', '#ppn', function () {
    var hkp = $('#harga_kena_pajak').val();
    var hpp = $('#hargaBeli').val();
    var ppn = $('#ppn').val();
    if (ppn !== '0' && ppn !== '10') {
        alert('PPN harus berisi 10 atau 0!');
    }
    calculateDiscount();
});

/* 	body.on('change', '#pembelian-harga_beli,#pembelian-persentase', function(){
 var hb = $('#pembelian-harga_beli').val();
 var persen =  $('#pembelian-persentase').val();
 if(hb !== '' && persen !== ''){
 var laba = hb * persen / 100;
 var total = parseFloat(laba) + parseFloat(hb);
 $('#pembelian-harga_jual').val(total.toFixed(2));	
 }	
 }); */

$("#addbutton").on('click', function (e) {
    e.preventDefault();
    var idBarang = $('#barangList').val();
    if (idBarang !== '') {
        var form = $('#paymentForm').serialize();
        $.ajax({
            type: "POST",
            url: baseUrl + 'pembelian/additem',
            dataType: 'json',
            cache: false,
            data: form,
            success: function (json) {
                if (json.status) {
                    $("#tbody").append(json.tr);
                    //$("#addbutton").attr('disabled',true);
                    $("#gudangList").attr('readonly', true);
                    $("#supplierList").attr('readonly', true);
                    $('#pembelian-batch_no').val('');
                    $('#pembelian-expired_date').val('');
                    $('#harga_jual_1').val('');
                    $('#harga_jual_2').val('');
                    $('#harga_jual_3').val('');
                    $('#harga_jual_4').val('');
                    $('#pembelian-jumlah').val('');
                    // $('#pembelian-no_faktur').attr('readonly',true); 
                    diskon = parseInt($('#diskon').val());
                    hargaTotal = parseInt($('#hargaTotal').val());
                    hargaTotalPajak = parseInt($('#hargaTotalPajak').val());
                    hargabeli = parseInt($('#hargaBeli').val());
                    hargapajak = parseInt($('#harga_kena_pajak').val());
                    qty = parseInt($('#qty').val());
                    console.log(hargaTotal + ' ==== ' + hargaTotalPajak);
                    hargaDiskon = hargabeli - diskon;
                    $('#hargaTotal').val(hargaDiskon * qty);
                    $('#hargaTotalPajak').val(hargaTotalPajak + (hargapajak * qty));

                    $('#hargaTotalFormat').val(formatCurrency(parseFloat($('#hargaTotal').val())));
                    $('#hargaTotalPajakFormat').val(formatCurrency(parseFloat($('#hargaTotalPajak').val())));

                    // $('#hargaTotalFormat').val(parseInt($('#hargaTotal').val()).toFixed(2));
                    // $('#hargaTotalPajakFormat').val(parseInt($('#hargaTotalPajak').val()).toFixed(2));

                    // $('#hargaBeli').val('');
                    // $('#harga_kena_pajak').val('');
                    // $('#diskon').val('0');
                    // $('#diskon_persen').val('0');
                    $('#qty').val('0');
                    $('#barangList').focus();
                } else {

                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }
});

function loadiFrame(src) {
    $("#iframeplaceholder").html("<iframe id='myiframe' name='myname' src='" + src + "' />");
}

$("#save").on('click', function (e) {
    e.preventDefault();
	if(!onXHR) {
		onXHR = true;
		$.ajax({
			type: "POST",
			url: baseUrl + 'pembelian/save',
			dataType: 'json',
			cache: false,
			data: $('#paymentForm').serialize(),
			success: function (json) {
				if (json.status) {
					onXHR = false;
					$('#paymentForm').each(function () {
						this.reset();
					});

					loadiFrame(baseUrl + 'pembelian/printreceipt?id=' + json.nopembelian);
					$("#myiframe").load(
						function () {
							window.frames['myname'].focus();
							window.frames['myname'].print();
						}
					);

					addpenjualan = [];
					$('#tbody').html('');
					bootbox.alert({
						size: "small",
						title: "Success",
						message: "Purchase Success to Save!",
						callback: function(){
							location.reload();
						}
					});
				} else {
					onXHR = false;
                    bootbox.alert('Purchase Failed!');
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				onXHR = false;
				console.log(xhr.status);
				//console.log(xhr.responseText);
				console.log(thrownError);
			}
		});
	}
});

function addDays(n){
    var t = new Date();
    t.setDate(t.getDate() + n);
    var month = "0"+(t.getMonth()+1);
    var date = "0"+t.getDate();
    month = month.slice(-2);
    date = date.slice(-2);
    var date = month+"/"+date+"/"+t.getFullYear();
    return date;
}

body.on('change', '#poList', function (e) {
    e.preventDefault();
    var curr = $(this);
    $.ajax({
        type: "GET",
        url: baseUrl + 'pembelian/getpo?id=' + curr.val(),
        dataType: 'json',
        cache: false,
        success: function (json) {
            if (json.status) {
                $("#tbodyorder").html(json.tr);
                $('#detailContent').show();

                $('#supplierWrapper').html(json.supplier);
                $('#pembelian-jatuh_tempo').val(addDays(json.top));

                $('#barangWrapper').html(json.barang);
                $('#barangWrapper').find('span.select2').remove();

                var select2setting = {
                    "allowClear": true,
                    "theme": "bootstrap",
                    "width": "100%",
                    "placeholder": "Pilih Barang",
                    "language": "en-US"
                };
                var cloneItem = $('#barangList');
                var dataOption = cloneItem.attr('data-s2-options');
                if (cloneItem.data('select2')) {
                    cloneItem.select2('destroy').enable(false);
                }
                jQuery.when(cloneItem.select2(select2setting)).done(initS2Loading('barangList', dataOption));

                var select2Supplier = {
                    "allowClear": true,
                    "theme": "bootstrap",
                    "width": "100%",
                    "placeholder": "Pilih Supplier",
                    "language": "en-US"
                };
                var cloneSupplier = $('#supplierList');
                var dataSupplier = cloneSupplier.attr('data-s2-options');
                if (cloneSupplier.data('select2')) {
                    cloneSupplier.select2('destroy');
                }
                jQuery.when(cloneSupplier.select2(select2Supplier)).done(initS2Loading('supplierList', dataSupplier));

                $('.kv-plugin-loading').hide();
            } else {

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
});