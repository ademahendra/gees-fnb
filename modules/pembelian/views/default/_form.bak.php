<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use kartik\growl\Growl;
use kartik\money\MaskMoney;
use kartik\select2\Select2;

use app\models\PurchaseOrder;
use app\models\StokGudang;
use app\models\Supplier;
use app\models\Barang;
use app\models\Gudang;
use app\models\DetailPembelian;

$this->title = Yii::t('app', 'Data Pembelian');
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
	if(Yii::$app->session->getFlash('status') == 'true'){
		echo Growl::widget([
			'type' => Growl::TYPE_SUCCESS,
			'icon' => 'glyphicon glyphicon-ok-sign',
			'title' => 'Notifications',
			'showSeparator' => true,
			'body' => 'Data Pembelian berhasil Ditambahkan',
			'pluginOptions' => [
				'showProgressbar' => false,
				'placement' => [
					'from' => 'bottom',
					'align' => 'right', 
				]
			]
		]);
	} else if(Yii::$app->session->getFlash('status') == 'false'){
		echo Growl::widget([
			'type' => Growl::TYPE_DANGER,
			'icon' => 'glyphicon glyphicon-ok-sign',
			'title' => 'Notifications',
			'showSeparator' => true,
			'body' => 'Data Pembelian gagal Ditambahkan',
			'pluginOptions' => [
				'showProgressbar' => false,
				'placement' => [
					'from' => 'bottom',
					'align' => 'right',
				]
			]
		]); 			
	}
    $model->isNewRecord ? $model->ppn = '10':$model->ppn = $model->ppn;	
?>
<div class="card bg-white">	
<div class="card-block">
<div class="row">
<div class="page-title">
	<div class="title"><?= $this->title ?></div>
		<!--<div class="sub-title">Page Description</div>-->
	</div>
<div class="widget">
	<div class="widget-header transparent">
		
		<div class="additional-btn">
			<a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
			<a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
		</div>
	</div>


	 <div class="widget-content padding">	
		<div class="detail-penjualan-form">
			<?php $form = ActiveForm::begin([
				'id' => 'paymentForm'
			]); ?>
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-4">
						<?php
							$data = ArrayHelper::map(PurchaseOrder::find()->orderBy('tanggal_faktur ASC')->all(), 'id', 'no_po');
							echo $form->field($model, 'id_po')->widget(Select2::classname(), [
									'data' => $data,
									'options' => ['placeholder' => '--Pilih PO --','id'=>'poList', 'class' => 'm-t'],
									'pluginOptions' => [
											'allowClear' => true
									],
							])->label('PO Number');
							?>
					</div>
				</div>
				<div class="row">
					<div class="col-md-2">
						<label class="control-label">Supplier</label>
						<?= Html::textInput('supplierName', '', ['class' => 'form-control input-sm', 'maxlength' => true,'id' => 'namaSupplier', 'readonly' => true]) ?>
						<?= Html::activeHiddenInput($model, 'id_supplier', ['class' => 'form-control input-sm', 'maxlength' => true, 'id' => 'idSupplier']) ?>   
					</div>
					<div class="col-md-2">
						<?= $form->field($model, 'no_faktur')->textInput(['maxlength' => true]) ?>
					</div>
					<div class="col-md-2">
						<?= $form->field($model, 'tanggal_faktur')->textInput(['maxlength' => true, 'class' => 'form-control datepicker-input', 'data-provide'=>'datepicker','data-format' => 'yyyy-mm-dd', ]) ?>
					</div>
					<div class="col-md-2">
						<?= $form->field($model, 'jatuh_tempo')->textInput(['maxlength' => true, 'class' => 'form-control datepicker-input', 'data-provide'=>'datepicker','data-format' => 'yyyy-mm-dd', ]) ?>
					</div>
                    <div class="form-group col-md-4">
                        <?php
                        $data = ArrayHelper::map(Gudang::findAll(['type' => 1]), 'id','nama');
                        echo $form->field($model, 'id_gudang')->widget(Select2::classname(), [
                            'data' => $data,
                            'options' => ['placeholder' => '--Pilih Gudang --','id'=>'namagudang'],
                            'readonly' => true,
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ])->label('Gudang');
                        ?>

                    </div>
				</div>	
			</div>

			<hr>
			<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>
									Nama Barang 
								</th>
								<th>
									Jumlah
								</th>
								<th>
									Harga (/Unit)
								</th>
								
								<th>
									Diskon  
								</th>
								<th>
									PPN
								</th>
								<th>
									Harga (/Unit) + PPN
								</th>
								<th>
									Total Harga + PPN
								</th>
<!--								<th class="col-md-1">-->
<!--								-->
<!--								</th>-->
							</tr>
						</thead>
						<tbody id="tbodyorder">
						</tbody>
					</table>
				</div>
			</div>
			</div>
			
			<div class="row">
			<div class="card-header">
                Tambah Pembelian
            </div>
			<div class="col-md-12">
				<div class="row">	
					<div class="col-md-2" id="barangWrapper">
						<?php
							$data = ArrayHelper::map(Barang::find()->orderBy('nama ASC')->all(),'id','nama');
							echo $form->field($detailModel, 'id_barang')->widget(Select2::classname(), [
							'data' => $data,
							'theme' => Select2::THEME_BOOTSTRAP,
							'options' => ['placeholder' => '--Pilih Barang--','id' => 'barangList'],
							'pluginOptions' => [
								'allowClear' => true
								],
							])->label('Barang');
						?>
					</div>
					<div class="col-md-1">
                                <div class="form-group">
                                    <label class="control-label">Satuan</label>
                                    <br />
                                    <label class="control-label" id="satuanBarang"></label>
                                </div>
                       </div>					
					<div class="col-md-1">
						<?= $form->field($detailModel, 'jumlah')->textInput(['id' => 'qty']) ?>
					</div>	
					<div class="col-md-2">
						<?= $form->field($detailModel, 'harga_beli')->widget(MaskMoney::classname(),
						['options' => 
							[
								'id' => 'hargaBeli',
							]
						])->label('Harga (/Unit)') ?>						
					</div>		
					
					<div class="col-md-1">
						<?= $form->field($detailModel, 'diskon_persen')->textInput(['id' => 'diskon_persen','value' => '0'])->label('Disk (%)') ?>
					</div>		
					<div class="col-md-2">
						<?= $form->field($detailModel, 'diskon')->widget(MaskMoney::classname(),
						['options' =>['id' => 'diskon','value' => '0']])->label('Diskon') ?>
					</div>	
					<div class="col-md-1">
						<?= $form->field($detailModel, 'ppn')->textInput(['id' => 'ppn','value' => '10'])->label('PPN (%)') ?>
					</div>		
					<div class="col-md-2">
						<?= $form->field($detailModel, 'harga_kena_pajak')->widget(MaskMoney::classname(),
						['options' =>['id' => 'harga_kena_pajak','value' => '0'],'readonly' => true]) ?>
					</div>
					<div class="col-md-2">
						<?= Html::label('Harga Retail',null,['class' => 'control-label']) ?>
									
						<div class="input-text">
							<?= Html::textInput('harga_retail', null,['class' => 'form-control' , 'id'=>'hargaRetail'])
							?>
						</div>					
					</div>
					<div class="col-md-2">
						<?= Html::label('Harga Agen',null,['class' => 'control-label']) ?>
									
						<div class="input-text">
							<?= Html::textInput('harga_agen', null,['class' => 'form-control' , 'id'=>'hargaAgen'])
							?>
						</div>					
					</div>
					
				</div>
				<div class="row">
				<div class="col-md-12">
					<label>Detail Barang</label>
				</div>
				<div class="col-md-12" id="detailBarang">

				</div>
			</div>
			</div>			

			<div class="col-md-12">
				<div class="form-group">
					<?= Html::a('Tambah', ['#'], ['class' => 'btn btn-warning' , 'id' => 'addbutton']) ?>
					
					<?= Html::a('Kembali', ['index'], ['class' => 'btn btn-danger']) ?>
					<br />
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>
									Nama Barang 
								</th>
								<th>
									Jumlah
								</th>
								<th>
									Harga (/Unit)
								</th>
								
								<th>
									Diskon  
								</th>
								<th>
									PPN
								</th>
								<th>
									Harga (/Unit) + PPN
								</th>
								<th>
									Total Harga + PPN
								</th>
								<th>
									Harga Retail
								</th>
								<th>
									Harga Agen
								</th>
								<th class="col-md-1">
								
								</th>
							</tr>
						</thead>
						<tbody id="tbody">
						</tbody>
					</table>
				</div>
			</div>
			<div class="col-md-12">
				<div class="pull-left">
                    <!--
					<div class="col-md-3">
						<?= $form->field($model, 'diskon_persen')->textInput(['id' => 'diskonGlobalPercent', 'class' => 'form-control', 'value' => '0']) ?>
					</div>
					<div class="col-md-6">
						<?= $form->field($model, 'diskon')->textInput(['id' => 'diskonGlobal', 'class' => 'form-control', 'value' => '0']) ?>
					</div>-->
				</div>
				<div class="pull-right">
                    <!--
					<div class="col-md-6">
					<label class="control-label">Total Harga</label>
						<?= Html::textInput('totalharga','0',['id' => 'hargaTotalFormat', 'class' => 'form-control','readonly' => true, 'value' => '0']) ?>
						<?= $form->field($model, 'harga_beli')->hiddenInput(['id' => 'hargaTotal', 'class' => 'form-control' ,'readonly' => true, 'value' => '0'])->label(''); ?>
					</div>
                    -->
					<div class="col-md-12">
						<label class="control-label">Total Harga + Diskon</label>
						<?= Html::textInput('totalhargaPajak','0',['id' => 'hargaTotalPajakFormat', 'class' => 'form-control','readonly' => true, 'value' => '0']) ?>
						<?= $form->field($model, 'harga_kena_pajak')->hiddenInput(['id' => 'hargaTotalPajak', 'class' => 'form-control','readonly' => true, 'value' => '0'])->label(''); ?>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="form-group">
					<div class="pull-left">
					<?= Html::submitButton($model->isNewRecord ? 'Save' : 'Perbaharui', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','id' => 'save']) ?>
					</div>
			<?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>
<div id="iframeplaceholder"  style="display:none;"></div>

<?php

$this->registerCssFile(Yii::$app->getUrlManager()->getBaseUrl()."/web/theme/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
$this->registerJsFile(Yii::$app->getUrlManager()->getBaseUrl()."/web/theme/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js");

$script = <<< JS
	$('body').on('keyup', '#diskon_persen', function(e){
		e.preventDefault();
		calculateDiscount();
	});
	
	$('body').on('keyup', '#diskon', function(e){
		e.preventDefault();
		calculateDiscount(false);
	});
	
	$('body').on('keyup', '#diskonGlobalPercent', function(e){
		e.preventDefault();
		calculateDiscountGlobal();
	});
	
	$('body').on('keyup', '#diskonGlobal', function(e){
		e.preventDefault();
		calculateDiscountGlobal(false);
	});
	
	$('body').on('change', 'select#barangList', function () {
		var barangList = $('select#barangList').val();
		$.get(baseUrl + 'rest/detailbarang?id=' + barangList, function (jsonData) {
			var json = jQuery.parseJSON(jsonData);
			if (json.status) {
				$('#satuanBarang').html(json.satuan);
			}
		});
	});

	var newBarangDialog;
	$('body').on('click', '#newItem', function (e) {
		e.preventDefault();
		newBarangDialog = BootstrapDialog.show({
            message: function(dialog) {
                var message = $('<div></div>');
                var pageToLoad = dialog.getData('pageToLoad');
                message.load(pageToLoad);
                return message;
            },
			title:'New Item',
            data: {
                'pageToLoad': baseUrl+'barang/new'
            },
			size:BootstrapDialog.SIZE_LARGE
        });
	});	
	
	var addpenjualan = [];	
	function print(url)
	{
		var _this = this;
		var iframeId = 'printFrame';
		var iframe = $('iframe#printFrame');
		iframe.attr('src', url);
	 
		iframe.load(function() {
			callPrint(iframeId);
		});
	}
	
	function calculateDiscount(percent = true){
		var discPercent = $('#diskon_persen').val();
		var discount = $('#diskon').val();
		var hpp = $('#hargaBeli').val();
		var ppn =  parseInt($('#ppn').val());
		
		if((discount !== '' || discPercent !== '') && hpp !== ''){
			if(discPercent !== ''){
				if(percent){
					var discRate = (discPercent/100) * hpp;
					$('#diskon').val(parseInt(discRate).toFixed(0));
					$('#diskon-disp').val(parseInt(discRate).toFixed(2));
				}
			} 
			
			if(discount !== ''){
				if(percent == false){
					var discPerc = discount/hpp * 100;
					// console.log('I am here: '+discPerc);
					$('#diskon_persen').val(parseInt(discPerc).toFixed(0));
				}
			}
				
		}
		discountRate = $('#diskon').val();
		if(hpp !== '' && ppn !== ''){
			if(ppn == 10){
				var hsp = ((parseInt(hpp) - parseInt(discountRate)) + ((parseInt(hpp) - parseInt(discountRate)) * ppn / 100)).toFixed(0);
			} else {
				var hsp = (parseInt(hpp - parseInt(discountRate))).toFixed(0);
			}
			$('#harga_kena_pajak').val(hsp);
			$('#harga_kena_pajak-disp').val(parseInt(hsp).toFixed(2));
		}
	}
	
	function calculateDiscountGlobal(percent = true){
		var discPercent = $('#diskonGlobalPercent').val();
		var discount = $('#diskonGlobal').val();
		var hpp = $('#hargaTotal').val();
		var ppn =  parseInt($('#ppn').val());;
		
		if((discount !== '' || discPercent !== '') && hpp !== ''){
			if(discPercent !== ''){
				if(percent){
					var discRate = (discPercent/100) * hpp;
					$('#diskonGlobal').val(parseInt(discRate).toFixed(0));
					//update harga setelah pajak
				}
			} 
			
			if(discount !== ''){
				if(percent == false){
					var discPerc = discount/hpp * 100;
					// console.log('I am here GLOBAL : '+discPerc);
					$('#diskonGlobalPercent').val(parseInt(discPerc).toFixed(0));
					//update harga setelah pajak
				}
			}	
		}
		discountRate = $('#diskonGlobal').val();
		// console.log(hpp+' ---- '+ppn);
		if(hpp !== '' && ppn !== ''){
			if(ppn == 10){
				var hsp = ((parseInt(hpp) - parseInt(discountRate)) + ((parseInt(hpp) - parseInt(discountRate)) * ppn / 100)).toFixed(0);
				// console.log('HSP '+hsp);
			} else {
				var hsp = (parseInt(hpp - parseInt(discountRate))).toFixed(0);
				// console.log('HP '+hsp);
			}
			$('#hargaTotalPajak').val(hsp);
			$('#hargaTotalFormat').val(formatCurrency(parseFloat($('#hargaTotal').val())));
            $('#hargaTotalPajakFormat').val(formatCurrency(parseFloat($('#hargaTotalPajak').val())));
						
			
		}
	}
	 
	function callPrint(iframeId) {
		  var PDF = document.getElementById(iframeId);
		  PDF.focus();
		  PDF.contentWindow.print();
	}
	//$("#addbutton").attr('disabled',true);
	
	$("body").on('click','.remove', function(e){
		$(this).closest('tr').remove();
		var removeItem = $(this).closest('tr').attr('id');

		$('#grand_total').val(grand_total);
		delete addpenjualan[removeItem];

		//$("#addbutton").fadeIn();
	});
	
	$('body').on('keyup', '#hargaBeli', function(){
		calculateDiscount();
	});
	
	
	$('body').on('keyup', '#harga_kena_pajak', function(){
		// var hkp = $('#harga_kena_pajak').val();
		// var hpp = $('#hargaBeli').val();
		// var ppn =  $('#ppn').val();
		// if(hkp !== '' && ppn !== ''){
			// if(ppn === '10'){
				// var hsp = (parseInt(hkp) / 1.1).toFixed(0);	
			// } else {
				// var hsp = parseInt(hkp).toFixed(0);
			// }
			
			// $('#hargaBeli').val(hsp);
		// }
	});
	
	
	$('body').on('change', '#ppn', function(){
		var hkp = $('#harga_kena_pajak').val();
		var hpp = $('#hargaBeli').val();
		var ppn =  $('#ppn').val();
		if(ppn !== '0' && ppn !== '10'){
			alert('PPN harus berisi 10 atau 0!');
		}
		calculateDiscount();			
	});
	
/* 	$('body').on('change', '#pembelian-harga_beli,#pembelian-persentase', function(){
		var hb = $('#pembelian-harga_beli').val();
		var persen =  $('#pembelian-persentase').val();
		if(hb !== '' && persen !== ''){
			var laba = hb * persen / 100;
			var total = parseFloat(laba) + parseFloat(hb);
			$('#pembelian-harga_jual').val(total.toFixed(2));	
		}	
	}); */
	
	$("#addbutton").on('click', function(e){
		e.preventDefault();
		var idBarang = $('#barangList').val();
		if(idBarang !== ''){
			var form = $('#paymentForm').serialize();
			$.ajax({
				type: "POST",
				url: baseUrl+'pembelian/additem',
				dataType: 'json',
				cache: false,
				data: form,
				success: function (json) {
					if(json.status){	
						$("#tbody").append(json.tr);
						//$("#addbutton").attr('disabled',true);
						$("#gudangList").attr('readonly',true);
						$("#supplierList").attr('readonly',true);
						$('#pembelian-batch_no').val('');
						$('#pembelian-expired_date').val('');
						$('#harga_jual_1').val('');
						$('#harga_jual_2').val('');
						$('#harga_jual_3').val('');
						$('#harga_jual_4').val('');
						$('#pembelian-jumlah').val('');
						// $('#pembelian-no_faktur').attr('readonly',true); 
						diskon = parseInt($('#diskon').val());
						hargaTotal = parseInt($('#hargaTotal').val());
						hargaTotalPajak = parseInt($('#hargaTotalPajak').val());
						hargabeli = parseInt($('#hargaBeli').val());
						hargapajak = parseInt($('#harga_kena_pajak').val());
						qty = parseInt($('#qty').val());
						console.log(hargaTotal+' ==== '+hargaTotalPajak);
						hargaDiskon = hargabeli - diskon;
						$('#hargaTotal').val(hargaDiskon * qty);
						$('#hargaTotalPajak').val(hargaTotalPajak + (hargapajak * qty));
						
						$('#hargaTotalFormat').val(formatCurrency(parseFloat($('#hargaTotal').val())));
                        $('#hargaTotalPajakFormat').val(formatCurrency(parseFloat($('#hargaTotalPajak').val())));
                        
						// $('#hargaTotalFormat').val(parseInt($('#hargaTotal').val()).toFixed(2));
						// $('#hargaTotalPajakFormat').val(parseInt($('#hargaTotalPajak').val()).toFixed(2));
						
						// $('#hargaBeli').val('');
						// $('#harga_kena_pajak').val('');
						// $('#diskon').val('0');
						// $('#diskon_persen').val('0');
						$('#qty').val('0');
					} else {
						
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log(xhr.status);
					console.log(xhr.responseText);
					console.log(thrownError);
				}
			});
		}
	});	
	
	function loadiFrame(src)
    {
        $("#iframeplaceholder").html("<iframe id='myiframe' name='myname' src='" + src + "' />");
    }
	
	$("#save").on('click', function(e){
		e.preventDefault();
		var form = $('#paymentForm').serialize();
		$.ajax({
			type: "POST",
			url: baseUrl+'pembelian/save',
			dataType: 'json',
			cache: false,
			data: form,
			success: function (json) {
				if(json.status){	
					$( '#paymentForm' ).each(function(){
						this.reset();
					});
					
					loadiFrame(baseUrl+'pembelian/printreceipt?id='+json.nopembelian); 
						$("#myiframe").load( 
							function() {
								window.frames['myname'].focus();
								window.frames['myname'].print();
							}
					);
						
					addpenjualan = [];	
					$('#tbody').html('');
				} else {
					
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log(xhr.status);
				console.log(xhr.responseText);
				console.log(thrownError);
			}
		});
	});	
	 
	$('body').on('change','#poList', function(e){
		e.preventDefault();
		var curr = $(this);
		$.ajax({
			type: "GET",
			url: baseUrl+'pembelian/getpo?id='+curr.val(),
			dataType: 'json',
			cache: false,
			success: function (json) {
				if(json.status){
					$("#tbodyorder").html(json.tr);
					//$("#addbutton").attr('disabled',true);
					// $('#hargaBeli').val('');
					// $('#harga_kena_pajak').val('');
					$('#namaSupplier').val(json.supplier);
					$('#idSupplier').val(json.idsupplier);

					$('#barangWrapper').html(json.barang);
                    $('#barangWrapper').find('span.select2').remove();

					var select2setting = {"allowClear":true,"theme":"bootstrap","width":"100%","placeholder":"Pilih Barang","language":"en-US"};
					var cloneItem = $('#barangList');
					var dataOption = cloneItem.attr('data-s2-options');
					if (cloneItem.data('select2')) { cloneItem.select2('destroy'); }
		            jQuery.when(cloneItem.select2(select2setting)).done(initS2Loading('barangList',dataOption));
                    $('.kv-plugin-loading').hide();
				} else {

				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log(xhr.status);
				console.log(xhr.responseText);
				console.log(thrownError);
			}
		});
	});

JS;
$this->registerJs($script, \yii\web\View::POS_READY);
?>