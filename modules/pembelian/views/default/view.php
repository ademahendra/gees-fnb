<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pembelian */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pembelian', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="page-heading">
	  <h1></i> <?= Html::encode($this->title) ?></h1>
</div>

<div class="widget">
	<div class="widget-header transparent">
		<div class="additional-btn">
			<a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
			<a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
			
		</div>
	</div>

	<div class="widget-content padding">

<div class="pembelian-view">

    <p>
        <?= Html::a('Perbaharui', ['update', 'id' => $model->id], ['class' => 'btn btn-blue-2']) ?>
		&nbsp;
        <?= Html::a('Hapus', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-red-1',
            'data' => [
                'confirm' => 'Apa anda yakin akan menghapus data ini?',
                'method' => 'post',
            ],
        ]) ?>
		&nbsp;
		<?= Html::a('Tambah Pembelian', ['create'], ['class' => 'btn btn-green-3']) ?>
		&nbsp;
		<?= Html::a('Data Pembelian', ['index'], ['class' => 'btn btn-info']) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'no_faktur',
			
            ['attribute'=>'id_obat',
			 'label'=>'Obat',
			 'value'=> $model->obat->nama],
			 
            'jumlah',
            'harga_beli',
            'harga_jual',
            //'tanggal',
            'admin_id',
        ],
    ]) ?>

</div>
</div>
</div>
</div>

