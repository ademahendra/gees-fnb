<?php
/**
 * Created by PhpStorm.
 * User: adema
 * Date: 29/04/2017
 * Time: 11.12
 */
use app\models\DetailPembelian;
use app\models\Pembelian;
use app\models\Merchant;

setlocale(LC_ALL , 'id');

$penerimaan = Pembelian::find()
    ->where([
        'id' => $id
    ])
    ->one();
$merchantInfo = Merchant::find()->one();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" media="print" href="<?= $this->theme->baseUrl ?>/styles/print.css">
    <link rel="stylesheet" media="print" href="<?= $this->theme->baseUrl ?>/vendor/bootstrap/dist/css/bootstrap.css">
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="2" align="left"><img src="<?=Yii::$app->params['baseURL'].$merchantInfo->logo?>" width="100" height="100"/></td>
	</tr>
	<tr>
        <td colspan="2" align="left"><h3><?= $merchantInfo->name?></h3></td>
		<td colspan="3" align="right"><h2>PENERIMAAN BARANG</h2></td>
    </tr>
	<tr>
		<td colspan="2" align="left"><small><?= $merchantInfo->website?></small></td>
	</tr>
	<tr>
        <td colspan="2" align="left"><small><?= $merchantInfo->alamat?><br /> Telp. <?= $merchantInfo->telepon?></small></td>
    </tr>
	<tr>
        <td width="18%">No. Faktur</td>
        <td width="3%" align="center">:</td>
        <td width="74%"><?= $penerimaan->no_faktur ?></td>
    </tr>
    <tr>
        <td width="18%">Tanggal Terima</td>
        <td width="3%" align="center">:</td>
        <td width="74%"><?= strftime('%A, %d %B %G', strtotime($penerimaan->tanggal_faktur)) ?></td>
    </tr>
    <tr>
        <td width="18%">Jatuh Tempo</td>
        <td width="3%" align="center">:</td>
        <td width="74%" ><?= strftime('%A, %d %B %G', strtotime($penerimaan->jatuh_tempo)) ?></td>
    </tr>
    <tr>
        <td width="18%">Supplier</td>
        <td width="3%" align="center">:</td>
        <td width="74%"><?= $penerimaan->supplier->nama ?></td>
    </tr>
</table>
<table width="100%" border="1" cellpadding="0" cellspacing="0" class="table table-bordered">
    <thead>
    <tr class="success">
        <th>No</th>
<!--        <th>SKU</th>-->
        <th>Produk</th>
        <th>Qty</th>
        <th>Harga</th>
        <th>Diskon</th>
        <th>PPN</th>
        <th>Harga</th>
        <th>Total</th>
    </tr>
    </thead>
    <tbody id="paddingLess">
    <!--loop-->
    <?php
    $detail = DetailPembelian::find()
        ->where([
            'id_pembelian' => $id
        ])
        ->all();
    $i = 0;
    foreach($detail as $row) {
        $i++;
        ?>
        <tr class="success">
            <td><?= $i ?></td>
            <td><?php // $row->barang->kode ?></td>
            <td style="text-align:center;"><?= $row->barang->name ?></td>
            <td style="text-align:right;"><?= number_format($row->jumlah,2,',','.') ?></td>
            <td style="text-align:right;"><?= number_format($row->harga_beli,2,',','.') ?></td>
            <td style="text-align:right;"><?= number_format($row->diskon,2,',','.') ?></td>
            <td style="text-align:right;"><?= number_format($row->ppn,2,',','.') ?></td>
            <td style="text-align:right;"><?= number_format($row->harga_kena_pajak,2,',','.') ?></td>
            <td style="text-align:right;"><?= number_format($row->total,2,',','.') ?></td>
        </tr>
        <?php
    }
    ?>
    <!--end loop-->
    <tr>
        <td colspan="8" style="text-align:right;">Total: </td>
        <td style="text-align:right;"><?= number_format($penerimaan->harga_kena_pajak, 2, ',','.') ?></td>
    </tr>
    </tbody>

</table>
<script src="<?= $this->theme->baseUrl ?>/vendor/jquery/dist/jquery.js"></script>
<script src="<?= $this->theme->baseUrl ?>/vendor/bootstrap/dist/js/bootstrap.js"></script>
</body>
</html>