/**
 * Created by SERVER on 19/09/2017.
 */
$(document).ready(function () {
    var onXHR = false;
	var body = $('body');
    body.on('keyup', '#diskon_persen', function (e) {
        e.preventDefault();
        calculateDiscount();
    });

    body.on('keyup', '#diskon', function (e) {
        e.preventDefault();
        calculateDiscount(false);
    });

    body.on('keyup', '#diskonGlobalPercent', function (e) {
        e.preventDefault();
        calculateDiscountGlobal();
    });

    body.on('keyup', '#diskonGlobal', function (e) {
        e.preventDefault();
        calculateDiscountGlobal(false);
    });

    body.on('change', 'select#barangList', function () {
        var barangList = $('select#barangList').val();
        $.get(baseUrl + 'rest/detailbarang?id=' + barangList, function (jsonData) {
            var json = jQuery.parseJSON(jsonData);
            if (json.status) {
                $('#satuanBarang').html(json.satuan);
            }
        });
    });

    var newBarangDialog;
    body.on('click', '#newItem', function (e) {
        e.preventDefault();
        newBarangDialog = BootstrapDialog.show({
            message: function (dialog) {
                var message = $('<div></div>');
                var pageToLoad = dialog.getData('pageToLoad');
                message.load(pageToLoad);
                return message;
            },
            title: 'New Item',
            data: {
                'pageToLoad': baseUrl + 'barang/new'
            },
            size: BootstrapDialog.SIZE_LARGE
        });
    });

    var addpenjualan = [];

    function print(url) {
        var _this = this;
        var iframeId = 'printFrame';
        var iframe = $('iframe#printFrame');
        iframe.attr('src', url);

        iframe.load(function () {
            callPrint(iframeId);
        });
    }

    function calculateDiscount(percent = true) {
        var discPercent = $('#diskon_persen').val();
        var discount = $('#diskon').val();
        var hpp = $('#hargaBeli').val();
        var ppn = parseInt($('#ppn').val());

        if ((discount !== '' || discPercent !== '') && hpp !== '') {
            if (discPercent !== '') {
                if (percent) {
                    var discRate = (discPercent / 100) * hpp;
                    $('#diskon').val(parseInt(discRate).toFixed(0));
                    $('#diskon-disp').val(parseInt(discRate).toFixed(2));
                }
            }

            if (discount !== '') {
                if (percent == false) {
                    var discPerc = discount / hpp * 100;
                    // console.log('I am here: '+discPerc);
                    $('#diskon_persen').val(parseInt(discPerc).toFixed(0));
                }
            }

        }
        discountRate = $('#diskon').val();
        if (hpp !== '' && ppn !== '') {
            if (ppn == 10) {
                var hsp = ((parseInt(hpp) - parseInt(discountRate)) + ((parseInt(hpp) - parseInt(discountRate)) * ppn / 100)).toFixed(0);
            } else {
                var hsp = (parseInt(hpp - parseInt(discountRate))).toFixed(0);
            }
            $('#harga_kena_pajak').val(hsp);
            $('#harga_kena_pajak-disp').val(parseInt(hsp).toFixed(2));
        }
    }

    function calculateDiscountGlobal(percent = true) {
        var discPercent = $('#diskonGlobalPercent').val();
        var discount = $('#diskonGlobal').val();
        var hpp = $('#hargaTotal').val();
        var ppn = parseInt($('#ppn').val());
        ;

        if ((discount !== '' || discPercent !== '') && hpp !== '') {
            if (discPercent !== '') {
                if (percent) {
                    var discRate = (discPercent / 100) * hpp;
                    $('#diskonGlobal').val(parseInt(discRate).toFixed(0));
                    //update harga setelah pajak
                }
            }

            if (discount !== '') {
                if (percent == false) {
                    var discPerc = discount / hpp * 100;
                    // console.log('I am here GLOBAL : '+discPerc);
                    $('#diskonGlobalPercent').val(parseInt(discPerc).toFixed(0));
                    //update harga setelah pajak
                }
            }
        }
        discountRate = $('#diskonGlobal').val();
        // console.log(hpp+' ---- '+ppn);
        if (hpp !== '' && ppn !== '') {
            if (ppn == 10) {
                var hsp = ((parseInt(hpp) - parseInt(discountRate)) + ((parseInt(hpp) - parseInt(discountRate)) * ppn / 100)).toFixed(0);
                // console.log('HSP '+hsp);
            } else {
                var hsp = (parseInt(hpp - parseInt(discountRate))).toFixed(0);
                // console.log('HP '+hsp);
            }
            $('#hargaTotalPajak').val(hsp);
            $('#hargaTotalFormat').val(formatCurrency(parseFloat($('#hargaTotal').val())));
            $('#hargaTotalPajakFormat').val(formatCurrency(parseFloat($('#hargaTotalPajak').val())));


        }
    }

    function callPrint(iframeId) {
        var PDF = document.getElementById(iframeId);
        PDF.focus();
        PDF.contentWindow.print();
    }

    //$("#addbutton").attr('disabled',true);

    body.on('click', '.remove', function (e) {
        $(this).closest('tr').remove();
        var removeItem = $(this).closest('tr').attr('id');

        $('#grand_total').val(grand_total);
        delete addpenjualan[removeItem];

        //$("#addbutton").fadeIn();
    });

    body.on('keyup', '#hargaBeli', function () {
        calculateDiscount();
    });


    body.on('keyup', '#harga_kena_pajak', function () {
        // var hkp = $('#harga_kena_pajak').val();
        // var hpp = $('#hargaBeli').val();
        // var ppn =  $('#ppn').val();
        // if(hkp !== '' && ppn !== ''){
        // if(ppn === '10'){
        // var hsp = (parseInt(hkp) / 1.1).toFixed(0);
        // } else {
        // var hsp = parseInt(hkp).toFixed(0);
        // }

        // $('#hargaBeli').val(hsp);
        // }
    });


    body.on('change', '#ppn', function () {
        var hkp = $('#harga_kena_pajak').val();
        var hpp = $('#hargaBeli').val();
        var ppn = $('#ppn').val();
        if (ppn !== '0' && ppn !== '10') {
            alert('PPN harus berisi 10 atau 0!');
        }
        calculateDiscount();
    });

    /* 	body.on('change', '#pembelian-harga_beli,#pembelian-persentase', function(){
     var hb = $('#pembelian-harga_beli').val();
     var persen =  $('#pembelian-persentase').val();
     if(hb !== '' && persen !== ''){
     var laba = hb * persen / 100;
     var total = parseFloat(laba) + parseFloat(hb);
     $('#pembelian-harga_jual').val(total.toFixed(2));
     }
     }); */

    $("#addbutton").on('click', function (e) {
        e.preventDefault();
        var idBarang = $('#barangList').val();
        if (idBarang !== '') {
            var form = $('#paymentForm').serialize();
            $.ajax({
                type: "POST",
                url: baseUrl + 'pembelian/editpembelian',
                dataType: 'json',
                cache: false,
                data: form,
                success: function (json) {
                    if (json.status) {
                        $("#tbody").append(json.tr);
                        //$("#addbutton").attr('disabled',true);
                        $("#gudangList").attr('readonly', true);
                        $("#supplierList").attr('readonly', true);
                        $('#pembelian-batch_no').val('');
                        $('#pembelian-expired_date').val('');
                        $('#harga_jual_1').val('');
                        $('#harga_jual_2').val('');
                        $('#harga_jual_3').val('');
                        $('#harga_jual_4').val('');
                        $('#pembelian-jumlah').val('');
                        // $('#pembelian-no_faktur').attr('readonly',true);
                        diskon = parseInt($('#diskon').val());
                        hargaTotal = parseInt($('#hargaTotal').val());
                        hargaTotalPajak = parseInt($('#hargaTotalPajak').val());
                        hargabeli = parseInt($('#hargaBeli').val());
                        hargapajak = parseInt($('#harga_kena_pajak').val());
                        qty = parseInt($('#qty').val());
                        console.log(hargaTotal + ' ==== ' + hargaTotalPajak);
                        hargaDiskon = hargabeli - diskon;
                        $('#hargaTotal').val(hargaDiskon * qty);
                        $('#hargaTotalPajak').val(hargaTotalPajak + (hargapajak * qty));

                        $('#hargaTotalFormat').val(formatCurrency(parseFloat($('#hargaTotal').val())));
                        $('#hargaTotalPajakFormat').val(formatCurrency(parseFloat($('#hargaTotalPajak').val())));

                        // $('#hargaTotalFormat').val(parseInt($('#hargaTotal').val()).toFixed(2));
                        // $('#hargaTotalPajakFormat').val(parseInt($('#hargaTotalPajak').val()).toFixed(2));

                        // $('#hargaBeli').val('');
                        // $('#harga_kena_pajak').val('');
                        // $('#diskon').val('0');
                        // $('#diskon_persen').val('0');
                        $('#qty').val('0');
                    } else {

                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    });

    function loadiFrame(src) {
        $("#iframeplaceholder").html("<iframe id='myiframe' name='myname' src='" + src + "' />");
    }

    $("#save").on('click', function (e) {
        e.preventDefault();
		var idPembelian = $('#idPembelian').val();
        var harga1 = $('#harga1').val();
        var harga2 = $('#harga2').val();
		if(!onXHR) {
			onXHR = true;
			if (harga1==0){
				bootbox.alert("Isikan harga Agen !!");
			}
			else if (harga2==0){
				bootbox.alert("Isikan harga Retail !!");
			}

			else{
				$.ajax({
					type: "POST",
					url: baseUrl + 'pembelian/update?id=' + idPembelian,
					dataType: 'json',
					cache: false,
					data: $('#paymentForm').serialize(),
					success: function (json) {
						if (json.status) {
							onXHR = false;
							bootbox.alert({
								size: "small",
								title: "Success",
								message: "Penerimaan Pembelian Berhasil di Update !!",
								// callback: function(){
									// location.reload();
								// }
							});
							$('#paymentForm').each(function () {
								this.reset();
							});

							// loadiFrame(baseUrl + 'pembelian/printreceipt?id=' + json.nopembelian);
							// $("#myiframe").load(
							//     function () {
							//         window.frames['myname'].focus();
							//         window.frames['myname'].print();
							//     }
							// );

							addpenjualan = [];
							$('#tbody').html('');
						} else {
							onXHR = false;
							bootbox.alert('Penerimaan Pembelian GAGAL di Update !!');
						}
					},
					error: function (xhr, ajaxOptions, thrownError) {
						onXHR = false;
						console.log(xhr.status);
						//console.log(xhr.responseText);
						console.log(thrownError);
					}
				});
			}
		}
    });

    body.on('change', '#poList', function (e) {
        e.preventDefault();
        var curr = $(this);
        $.ajax({
            type: "GET",
            url: baseUrl + 'pembelian/getpo?id=' + curr.val(),
            dataType: 'json',
            cache: false,
            success: function (json) {
                if (json.status) {
                    $("#tbodyorder").html(json.tr);
                    //$("#addbutton").attr('disabled',true);
                    // $('#hargaBeli').val('');
                    // $('#harga_kena_pajak').val('');
                    // $('#namaSupplier').val(json.supplier);
                    // $('#idSupplier').val(json.idsupplier);

                    console.log(json.supplier);

                    $('#supplierWrapper').html(json.supplier);

                    //$('#supplierWrapper').find('span.select2').remove();

                    $('#barangWrapper').html(json.barang);
                    $('#barangWrapper').find('span.select2').remove();

                    var select2setting = {
                        "allowClear": true,
                        "theme": "bootstrap",
                        "width": "100%",
                        "placeholder": "Pilih Barang",
                        "language": "en-US"
                    };
                    var cloneItem = $('#barangList');
                    var dataOption = cloneItem.attr('data-s2-options');
                    if (cloneItem.data('select2')) {
                        cloneItem.select2('destroy').enable(false);
                    }
                    jQuery.when(cloneItem.select2(select2setting)).done(initS2Loading('barangList', dataOption));

                    var select2Supplier = {
                        "allowClear": true,
                        "theme": "bootstrap",
                        "width": "100%",
                        "placeholder": "Pilih Supplier",
                        "language": "en-US"
                    };
                    var cloneSupplier = $('#supplierList');
                    var dataSupplier = cloneSupplier.attr('data-s2-options');
                    if (cloneSupplier.data('select2')) {
                        cloneSupplier.select2('destroy');
                    }
                    jQuery.when(cloneSupplier.select2(select2Supplier)).done(initS2Loading('supplierList', dataSupplier));

                    $('.kv-plugin-loading').hide();
                } else {

                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });

    body.on('keyup', '.jumlahedit', function (e) {
        editDetailItem('qty', $(this));
    });

    body.on('keyup', '.hargabeliedit', function (e) {
        editDetailItem('hargabeli', $(this));
    });

    body.on('keyup', '.diskonpersenedit', function (e) {
        editDetailItem('diskonpersen', $(this));
    });

    body.on('keyup', '.ppnedit', function (e) {
        editDetailItem('ppn', $(this));
    });

    body.on('click', '.havePPN', function (e) {
        editDetailItem('ppn', $(this));
    });

    body.on('keyup', '.harga1edit', function (e) {
        editDetailItem('harga1', $(this));
    });

    body.on('keyup', '.harga2edit', function (e) {
        editDetailItem('harga2', $(this));
    });

   /* function editDetailItem(sender, thisItem) {
        var curr = thisItem;
        if (sender === 'qty') {
            var hargabeli = curr.parent('td').parent('tr').find('.hargabeliedit');
            var diskonpersen = curr.parent('td').parent('tr').find('.diskonpersenedit');
            var qty = thisItem;
            var ppnNew = curr.parent('td').parent('tr').find('.ppnedit');
        }

        if (sender === 'hargabeli') {
            var hargabeli = thisItem;
            var qty = curr.parent('td').parent('tr').find('.jumlahedit');
            var diskonpersen = curr.parent('td').parent('tr').find('.diskonpersenedit');
            var ppnNew = curr.parent('td').parent('tr').find('.ppnedit');
        }

        if (sender === 'diskonpersen') {
            var diskonpersen = thisItem;
            var qty = curr.parent('td').parent('tr').find('.jumlahedit');
            var hargabeli = curr.parent('td').parent('tr').find('.hargabeliedit');
            var ppnNew = curr.parent('td').parent('tr').find('.ppnedit');
        }

        if (sender === 'ppn') {
            var ppnNew = thisItem;
            var qty = curr.parent('td').parent('tr').find('.jumlahedit');
            var hargabeli = curr.parent('td').parent('tr').find('.hargabeliedit');
            var diskonpersen = curr.parent('td').parent('tr').find('.diskonpersenedit');
        }

        var havePPN = curr.parent('td').parent('tr').find('.havePPN');
        var qtyHidden = curr.parent('td').parent('tr').find('.qty');
        var hargaBeliHidden = curr.parent('td').parent('tr').find('.hb');
        var diskonPersenHidden = curr.parent('td').parent('tr').find('.diskon_persen');
        var hargappnedit = curr.parent('td').parent('tr').find('.hkpedit');
        var hargatotaledit = curr.parent('td').parent('tr').find('.hppedit');
        var hargatotaleditHidden = curr.parent('td').parent('tr').find('.hppedit2');

        var ppnedit = curr.parent('td').parent('tr').find('.ppnedit');
        var hbPPNhidden = curr.parent('td').parent('tr').find('.totalHj');
        if (qty.val() !== '' && qty.val() != '0' && hargabeli.val() !== '' && hargabeli.val() != '0') {
            var hargaPcs = parseFloat(hargabeli.val());
            var discHidden = parseFloat($('.diskonpersenedit').val());
            var discRate = (discHidden / 100) * hargaPcs; // from discount percent
            var hrgAfterDisc = hargaPcs - discRate;
            var ppn = 0;
            if(havePPN.is(':checked')){
                ppn = (10 * hrgAfterDisc) / 100;
            }

            var hbppn = parseFloat(hrgAfterDisc + ppn);
            var totalBeli = parseFloat(qty.val()) * hbppn;

            ppnedit.val(parseFloat(ppn));
            hargappnedit.val(hbppn);
            hargatotaledit.val(parseFloat(totalBeli));
            hargatotaleditHidden.val(parseFloat(totalBeli));

            qtyHidden.val(qty.val());
            hargaBeliHidden.val(hargabeli.val());
            diskonPersenHidden.val(discHidden);

            hbPPNhidden.val(parseFloat(totalBeli)); 

            $('#hkpedit').val(hbppn);
            $('#hkpedit-disp').val(hbppn);
            $('#hppeditDisplay').val(parseFloat(totalBeli).toFixed(2)); // total pembelian setelah diskon dan ppn jika ppn ada
            $('#hppedit').val(parseFloat(totalBeli).toFixed(2)); // total pembelian setelah diskon dan ppn jika ppn ada
            $('#hppedit-disp').val(totalBeli.toFixed(2));
            var grandtotalPembelian = 0;
            $(':input.hppedit').each(function (i, val) {
                if ($(this).val() != '') {
                    grandtotalPembelian += parseFloat($(this).val());
                }
            });

            $('#hargaTotalPajakFormat').val(grandtotalPembelian);
        }

    }*/
	
	function editDetailItem(sender, thisItem) {
        var curr = thisItem;
        if (sender === 'qty') {
            var hargabeli = curr.parent('td').parent('tr').find('.hargabeliedit');
            var diskonpersen = curr.parent('td').parent('tr').find('.diskonpersenedit');
            var qty = thisItem;
            var ppnNew = curr.parent('td').parent('tr').find('.ppnedit');
            var harga1 = curr.parent('td').parent('tr').find('.harga1edit');
            var harga2 = curr.parent('td').parent('tr').find('.harga2edit');
        }

        if (sender === 'hargabeli') {
            var hargabeli = thisItem;
            var qty = curr.parent('td').parent('tr').find('.jumlahedit');
            var diskonpersen = curr.parent('td').parent('tr').find('.diskonpersenedit');
            var ppnNew = curr.parent('td').parent('tr').find('.ppnedit');
            var harga1 = curr.parent('td').parent('tr').find('.harga1edit');
            var harga2 = curr.parent('td').parent('tr').find('.harga2edit');
        }

        if (sender === 'diskonpersen') {
            var diskonpersen = thisItem;
            var qty = curr.parent('td').parent('tr').find('.jumlahedit');
            var hargabeli = curr.parent('td').parent('tr').find('.hargabeliedit');
            var ppnNew = curr.parent('td').parent('tr').find('.ppnedit');
            var harga1 = curr.parent('td').parent('tr').find('.harga1edit');
            var harga2 = curr.parent('td').parent('tr').find('.harga2edit');
        }

        if (sender === 'ppn') {
            var ppnNew = thisItem;
            var qty = curr.parent('td').parent('tr').find('.jumlahedit');
            var hargabeli = curr.parent('td').parent('tr').find('.hargabeliedit');
            var diskonpersen = curr.parent('td').parent('tr').find('.diskonpersenedit');
            var harga1 = curr.parent('td').parent('tr').find('.harga1edit');
            var harga2 = curr.parent('td').parent('tr').find('.harga2edit');
        }

        if (sender === 'harga1') {
            var harga1 = thisItem;
            var qty = curr.parent('td').parent('tr').find('.jumlahedit');
            var hargabeli = curr.parent('td').parent('tr').find('.hargabeliedit');
            var diskonpersen = curr.parent('td').parent('tr').find('.diskonpersenedit');
            var ppnNew = curr.parent('td').parent('tr').find('.ppnedit');
            var harga2 = curr.parent('td').parent('tr').find('.harga2edit');
        }

        if (sender === 'harga2') {
            var harga2 = thisItem;
            var qty = curr.parent('td').parent('tr').find('.jumlahedit');
            var hargabeli = curr.parent('td').parent('tr').find('.hargabeliedit');
            var diskonpersen = curr.parent('td').parent('tr').find('.diskonpersenedit');
            var ppnNew = curr.parent('td').parent('tr').find('.ppnedit');
            var harga1 = curr.parent('td').parent('tr').find('.harga1edit');
        }

        var harga1Hidden = curr.parent('td').parent('tr').find('.harga1');
        var harga2Hidden = curr.parent('td').parent('tr').find('.harga2');
        var havePPN = curr.parent('td').parent('tr').find('.havePPN');
        var qtyHidden = curr.parent('td').parent('tr').find('.qty');
        var hargaBeliHidden = curr.parent('td').parent('tr').find('.hb');
        var diskonPersenHidden = curr.parent('td').parent('tr').find('.diskon_persen');
        var hargappnedit = curr.parent('td').parent('tr').find('.hkpedit');
        var hargatotaledit = curr.parent('td').parent('tr').find('.hppedit');
        var hargatotaleditHidden = curr.parent('td').parent('tr').find('.hppedit2');
		// set by ID
		var hkpEditID = curr.parent('td').parent('tr').find('#hkpeditDisplay');
		var hppeditID = curr.parent('td').parent('tr').find('#hppeditDisplay');

        var ppnedit = curr.parent('td').parent('tr').find('.ppnedit');
        var hbPPNhidden = curr.parent('td').parent('tr').find('.totalHj');

        harga1Hidden.val(harga1.val());
        harga2Hidden.val(harga2.val());

        if (qty.val() !== '' && qty.val() != '0' && hargabeli.val() !== '' && hargabeli.val() != '0') {
            var hargaPcs = parseFloat(hargabeli.val());
            var discHidden = parseFloat($('.diskonpersenedit').val());
            var discRate = (discHidden / 100) * hargaPcs; // from discount percent
            var hrgAfterDisc = hargaPcs - discRate;
            var ppn = 0;
            if(havePPN.is(':checked')){
                ppn = (10 * hrgAfterDisc) / 100;
            }

            var hbppn = parseFloat(hrgAfterDisc + ppn);
            var totalBeli = parseFloat(qty.val()) * hbppn;

            ppnedit.val(parseFloat(ppn));
            hargappnedit.val(hbppn);
            hargatotaledit.val(parseFloat(totalBeli));
            hargatotaleditHidden.val(parseFloat(totalBeli));

            qtyHidden.val(qty.val());
            hargaBeliHidden.val(hargabeli.val());
            diskonPersenHidden.val(discHidden);

            hbPPNhidden.val(parseFloat(totalBeli)); 

            hkpEditID.val(hbppn);
            // $('#hkpedit-disp').val(hbppn);
            // $('#hppeditDisplay').val(parseFloat(totalBeli).toFixed(2)); // total pembelian setelah diskon dan ppn jika ppn ada
            hppeditID.val(parseFloat(totalBeli).toFixed(2)); // total pembelian setelah diskon dan ppn jika ppn ada
            // $('#hppedit-disp').val(totalBeli.toFixed(2));
            var grandtotalPembelian = 0;
            $(':input.hppedit').each(function (i, val) {
                if ($(this).val() != '') {
                    grandtotalPembelian += parseFloat($(this).val());
                }
            });

            $('#hargaTotalPajakFormat').val(grandtotalPembelian);
            $('#hargaTotalPajak').val(grandtotalPembelian);
        }

    }
});
