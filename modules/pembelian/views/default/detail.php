<?php
use app\models\DetailPembelian;

$detail = DetailPembelian::find()
    ->where([
        'id_pembelian' => $model->id
    ])
    ->all();
?>
<div class="col-md-10">

    <!-- Your awesome content goes here -->
    <div class="widget invoice">
        <div class="widget-content padding">
            <div class="row">
                <div class="col-sm-4">
                    <!--
                    <div class="company-column">
                    <h4><img src="assets/img/inv-logo.png" alt="Logo"></h4>
                    <address>
                    <br>
                      3049 Anmoore Road<br>
                      Brooklyn, NY 11230 <br>
                      <abbr title="Phone">P:</abbr> (123) 456-7890
                    </address>
                    </div>
                    -->
                </div>
                <div class="col-sm-8 text-right">
                    <h1>No Faktur</h1>
                    <h4>#<?= $model->no_faktur ?></h4>
                    <!--<a href="#" class="btn btn-primary btn-sm invoice-print"><i class="icon-print-2"></i> Print</a>
                    -->
                </div>
            </div>
            <!--
                        <div class="bill-to">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h4><strong>Interesting</strong> Corporation</h4>
                                        <address>
                                          795 Folsom Ave, Suite 600<br>
                                          San Francisco, CA 94107<br>
                                          <abbr title="Phone">P:</abbr> (123) 456-7890
                                        </address>
                                </div>
                                <div class="col-sm-6"><br>
                                    <small class="text-right">
                                    <p><strong>ORDER DATE : </strong> January 15, 2014</p>
                                    <p><strong>ORDER STATUS : </strong> <span class="label label-warning">Pending</span></p>
                                    <p><strong>ORDER ID : </strong> #123456</p>
                                    </small>
                                </div>
                            </div>
                        </div>

                        <br><br>
            -->
            <div class="table-responsive">

                <table class="table table-condensed table-striped">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Item</th>
                        <th>Jumlah</th>
                        <th>Harga Beli</th>
                        <th>Diskon</th>
                        <th>PPN</th>
                        <th>Harga</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 0;
                    foreach ($detail as $row) {
                        $i++;
                        ?>
                        <tr>
                            <td><?= $i ?></td>
                            <td><?= $row->barang->name ?></td>
                            <td><?= $row->jumlah ?></td>
                            <td><?= number_format($row->harga_beli, 2, ',', '.') ?></td>
                            <td><?= number_format($row->diskon, 2, ',', '.') ?></td>
                            <td><?= number_format($row->ppn, 2, ',', '.') ?></td>
                            <td><?= number_format($row->harga_kena_pajak, 2, ',', '.') ?></td>
                            <td><?= number_format($row->harga_kena_pajak * $row->jumlah, 2, ',', '.') ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    <!--
						<tr>
							<td colspan="6" class="text-right"><strong>Subtotal</strong></td>
							<td><?= number_format($model->harga_beli, 2, ',', '.') ?></td>
						</tr>
						<tr>
							<td colspan="6" class="text-right"><strong>Discount</strong></td>
							<td><strong class="text-red-1"><?= number_format($model->diskon, 2, ',', '.') ?></strong></td>
						</tr>
						<tr>
							<td colspan="6" class="text-right"><strong>Tax (10%)</strong></td>
							<td><?= number_format($model->harga_kena_pajak / 1.1 * 10 / 100, 2, ',', '.') ?></td>
						</tr>-->
                    <tr>
                        <td colspan="7" class="text-right"><strong>TOTAL</strong></td>
                        <td>
                            <strong class="text-primary"><?= number_format($model->harga_kena_pajak, 2, ',', '.') ?></strong>
                        </td>
                    </tr>
                    </tbody>

                </table>
                <br><br>
            </div>
        </div>
    </div>
    <!-- End of your awesome content -->
</div>