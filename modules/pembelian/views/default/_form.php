<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use kartik\growl\Growl;
use kartik\money\MaskMoney;
use kartik\select2\Select2;

use app\models\PurchaseOrder;
use app\models\StokGudang;
use app\models\Supplier;
use app\models\FnbStock as Barang;
use app\models\Gudang;
use app\models\DetailPembelian;

$this->title = Yii::t('app', 'Data Pembelian');
$this->params['breadcrumbs'][] = $this->title;

$model->isNewRecord ? $model->ppn = '10' : $model->ppn = $model->ppn;
$setupDefault = \app\models\Merchant::find()->one();
?>
    <div class="card bg-white">
        <div class="portlet mt-element-ribbon light portlet-fit bordered">
            <div class="ribbon ribbon-left ribbon-clip ribbon-shadow ribbon-border-dash-hor ribbon-color-success green-jungle uppercase">
                <div class="ribbon-sub ribbon-clip ribbon-left"></div>
                <i class="icon-notebook font-white"></i> <?= $this->title ?>
            </div>
            <div class="portlet-title">
                <div class="actions">
                    <div class="btn-group btn-group-devided">

                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="detail-penjualan-form">
                    <?php $form = ActiveForm::begin([
                        'id' => 'paymentForm'
                    ]); ?>

                    <div class="row">
                        <div class="col-md-4">
                            <?php
                            $data = ArrayHelper::map(PurchaseOrder::find()->orderBy('tanggal_faktur ASC')->all(), 'id', 'no_po');
                            echo $form->field($model, 'id_po')->widget(Select2::classname(), [
                                'data' => $data,
                                'size' => Select2::SMALL,
                                'options' => ['placeholder' => 'Pilih PO', 'id' => 'poList', 'class' => 'm-t'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ])->label('PO Number');
                            ?>
                        </div>
                    </div>
                    <div id="detailContent" style="display: none;">
                        <div class="row">
                            <div class="col-md-2" id="supplierWrapper">
                                <?php
                                $data = ArrayHelper::map(Supplier::find()->orderBy('nama ASC')->all(), 'id', 'nama');
                                echo $form->field($model, 'id_supplier')->widget(Select2::classname(), [
                                    'data' => $data,
                                    'size' => Select2::SMALL,
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'options' => ['placeholder' => '--Pilih Supplier--', 'id' => 'supplierList', 'value' => $setupDefault->id_supplier],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ])->label('Supplier');
                                ?>

                            </div>
                            <div class="col-md-2">
                                <?= $form->field($model, 'no_faktur')->textInput(['maxlength' => true,'class' => 'form-control input-sm']) ?>
                            </div>
                            <div class="col-md-2">
                                <?= $form->field($model, 'tanggal_faktur')->textInput(['maxlength' => true, 'class' => 'form-control datepicker-input input-sm', 'data-provide' => 'datepicker', 'data-format' => 'yyyy-mm-dd', 'value' => date('m/d/Y')]) ?>
                            </div>
                            <div class="col-md-2">
                                <?= $form->field($model, 'jatuh_tempo')->textInput(['maxlength' => true, 'class' => 'form-control datepicker-input input-sm', 'data-provide' => 'datepicker', 'data-format' => 'yyyy-mm-dd', 'value' => date('m/d/Y')]) ?>
                            </div>
                            <div class="form-group col-md-4">
                                <?php
                                $data = ArrayHelper::map(Gudang::findAll(['type' => 1]), 'id', 'nama');
                                echo $form->field($model, 'id_gudang')->widget(Select2::classname(), [
                                    'data' => $data,
                                    'size' => Select2::SMALL,
                                    'options' => ['placeholder' => '--Pilih Gudang --', 'id' => 'namagudang', 'value' => $setupDefault->id_gudang],
                                    'readonly' => true,
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                    ],
                                ])->label('Gudang');
                                ?>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>
                                                Nama Barang
                                            </th>
                                            <th>
                                                Jumlah Order
                                            </th>
                                            <th>
                                                Jumlah Kirim
                                            </th>
                                            <th>
                                                Sisa
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbodyorder">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="card-header">
                                Tambah Pembelian
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-2" id="barangWrapper">
                                        <?php
                                        $data = ArrayHelper::map(Barang::find()->orderBy('name ASC')->all(), 'id', 'name');
                                        echo $form->field($detailModel, 'id_barang')->widget(Select2::classname(), [
                                            'data' => $data,
                                            'size' => Select2::SMALL,
                                            'theme' => Select2::THEME_BOOTSTRAP,
                                            'options' => ['placeholder' => '--Pilih Barang--', 'id' => 'barangList'],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ])->label('Barang');
                                        ?>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label class="control-label">Satuan</label>
                                            <br/>
                                            <label class="control-label" id="satuanBarang"></label>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <?= $form->field($detailModel, 'jumlah')->textInput(['id' => 'qty','class' => 'form-control input-sm']) ?>
                                    </div>
                                    <div class="col-md-2">
                                        <?= $form->field($detailModel, 'harga_beli')->widget(MaskMoney::classname(),
                                            ['options' =>
                                                [
                                                    'id' => 'hargaBeli','class' => 'form-control input-sm'
                                                ]
                                            ])->label('Harga (/Unit)') ?>
                                    </div>

                                    <div class="col-md-1">
                                        <?= $form->field($detailModel, 'diskon_persen')->textInput(['id' => 'diskon_persen', 'value' => '0','class' => 'form-control input-sm'])->label('Disk (%)') ?>
                                    </div>
                                    <div class="col-md-2">
                                        <?= $form->field($detailModel, 'diskon')->widget(MaskMoney::classname(),
                                            ['options' => ['id' => 'diskon', 'value' => '0','class' => 'form-control input-sm']])->label('Diskon') ?>
                                    </div>
                                    <div class="col-md-1">
                                        <?= $form->field($detailModel, 'ppn')->textInput(['id' => 'ppn', 'value' => '10','class' => 'form-control input-sm'])->label('PPN (%)') ?>
                                    </div>
                                    <div class="col-md-2">
                                        <?= $form->field($detailModel, 'harga_kena_pajak')->widget(MaskMoney::classname(),
                                            ['options' => ['id' => 'harga_kena_pajak', 'value' => '0','class' => 'form-control input-sm'], 'readonly' => true]) ?>
                                    </div>
                                    <!--
                                    <div class="col-md-2">
                                        <?= Html::label('Harga Retail', null, ['class' => 'control-label']) ?>

                                        <div class="input-text">
                                            <?= Html::textInput('harga_retail', null, ['class' => 'form-control', 'id' => 'hargaRetail'])
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <?= Html::label('Harga Agen', null, ['class' => 'control-label']) ?>

                                        <div class="input-text">
                                            <?= Html::textInput('harga_agen', null, ['class' => 'form-control', 'id' => 'hargaAgen'])
                                            ?>
                                        </div>
                                    </div>-->

                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Detail Barang</label>
                                    </div>
                                    <div class="col-md-12" id="detailBarang">

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <?= Html::a('Tambah', ['#'], ['class' => 'btn btn-warning', 'id' => 'addbutton']) ?>

                                    <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-danger']) ?>
                                    <br/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>
                                                Nama Barang
                                            </th>
                                            <th>
                                                Jumlah
                                            </th>
                                            <th>
                                                Harga (/Unit)
                                            </th>

                                            <th>
                                                Diskon
                                            </th>
                                            <th>
                                                PPN
                                            </th>
                                            <th>
                                                Harga (/Unit) + PPN
                                            </th>
                                            <th>
                                                Total Harga + PPN
                                            </th>
<!--                                            <th>-->
<!--                                                Harga Retail-->
<!--                                            </th>-->
<!--                                            <th>-->
<!--                                                Harga Agen-->
<!--                                            </th>-->
                                            <th class="col-md-1">

                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbody">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="pull-left">
                                    <!--
					<div class="col-md-3">
						<?= $form->field($model, 'diskon_persen')->textInput(['id' => 'diskonGlobalPercent', 'class' => 'form-control', 'value' => '0']) ?>
					</div>
					<div class="col-md-6">
						<?= $form->field($model, 'diskon')->textInput(['id' => 'diskonGlobal', 'class' => 'form-control', 'value' => '0']) ?>
					</div>-->
                                </div>
                                <div class="pull-right">
                                    <!--
					<div class="col-md-6">
					<label class="control-label">Total Harga</label>
						<?= Html::textInput('totalharga', '0', ['id' => 'hargaTotalFormat', 'class' => 'form-control', 'readonly' => true, 'value' => '0']) ?>
						<?= $form->field($model, 'harga_beli')->hiddenInput(['id' => 'hargaTotal', 'class' => 'form-control', 'readonly' => true, 'value' => '0'])->label(''); ?>
					</div>
                    -->
                                    <div class="col-md-12">
                                        <label class="control-label">Total Harga + Diskon</label>
                                        <?= Html::textInput('totalhargaPajak', '0', ['id' => 'hargaTotalPajakFormat', 'class' => 'form-control', 'readonly' => true, 'value' => '0']) ?>
                                        <?= $form->field($model, 'harga_kena_pajak')->hiddenInput(['id' => 'hargaTotalPajak', 'class' => 'form-control', 'readonly' => true, 'value' => '0'])->label(''); ?>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="pull-left">
                                        <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Perbaharui', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'id' => 'save']) ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
    <div id="iframeplaceholder" style="display:none;"></div>

<?php

$this->registerCssFile(Yii::$app->getUrlManager()->getBaseUrl() . "/web/theme/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
$this->registerJsFile(Yii::$app->getUrlManager()->getBaseUrl() . "/web/theme/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js");

$this->registerJs($this->render('_create.js'), \yii\web\View::POS_READY);
?>