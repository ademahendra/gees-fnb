<?php

use app\models\FnbStock as Barang;
use app\models\DetailPembelian;
use app\models\Gudang;
use app\models\PurchaseOrder;
use app\models\Supplier;
use kartik\money\MaskMoney;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Data Pembelian');
$this->params['breadcrumbs'][] = $this->title;

$model->isNewRecord ? $model->ppn = '10' : $model->ppn = $model->ppn;
?>
    <div class="card bg-white">
        <div class="portlet mt-element-ribbon light portlet-fit bordered">
            <div class="ribbon ribbon-left ribbon-clip ribbon-shadow ribbon-border-dash-hor ribbon-color-success green-jungle uppercase">
                <div class="ribbon-sub ribbon-clip ribbon-left"></div>
                <i class="icon-notebook font-white"></i> <?= $this->title ?>
            </div>
            <div class="portlet-title">
                <div class="actions">
                    <div class="btn-group btn-group-devided">

                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="detail-penjualan-form">
                        <?php $form = ActiveForm::begin([
                            'id' => 'paymentForm'
                        ]); ?>
                        <div class="col-md-12">
                            <?= $form->field($model, 'id')->hiddenInput(['maxlength' => true, 'value' => $model->id, 'readonly' => true, 'id' => 'idPembelian'])->label('') ?>
                            <div class="row">
                                <div class="col-md-4">
                                    <?php
                                    $data = ArrayHelper::map(PurchaseOrder::find()->orderBy('tanggal_faktur ASC')->all(), 'id', 'no_po');
                                    echo $form->field($model, 'id_po')->widget(Select2::classname(), [
                                        'data' => $data,
                                        'value' => $model->id_po,
                                        'size' => Select2::SMALL,
                                        'options' => ['placeholder' => '--Pilih PO --', 'id' => 'poList', 'class' => 'm-t'],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ])->label('PO Number');
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2" id="supplierWrapper">
                                    <?php
                                    $data = ArrayHelper::map(Supplier::find()->orderBy('nama ASC')->all(), 'id', 'nama');
                                    echo $form->field($model, 'id_supplier')->widget(Select2::classname(), [
                                        'data' => $data,
                                        'theme' => Select2::THEME_BOOTSTRAP,
                                        'size' => Select2::SMALL,
                                        'options' => ['placeholder' => '--Pilih Supplier--', 'id' => 'supplierList'],
                                        'value' => $model->id_supplier,
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ])->label('Supplier');
                                    ?>
                                </div>
                                <div class="col-md-2">
                                    <?= $form->field($model, 'no_faktur')->textInput(['maxlength' => true, 'class' => 'input-sm']) ?>
                                </div>
                                <div class="col-md-2">
                                    <?= $form->field($model, 'tanggal_faktur')->textInput(['maxlength' => true, 'class' => 'form-control datepicker-input input-sm', 'data-provide' => 'datepicker', 'data-format' => 'yyyy-mm-dd',]) ?>
                                </div>
                                <div class="col-md-2">
                                    <?= $form->field($model, 'jatuh_tempo')->textInput(['maxlength' => true, 'class' => 'form-control datepicker-input input-sm', 'data-provide' => 'datepicker', 'data-format' => 'yyyy-mm-dd',]) ?>
                                </div>
                                <div class="form-group col-md-4">
                                    <?php
                                    $data = ArrayHelper::map(Gudang::findAll(['type' => 1]), 'id', 'nama');
                                    echo $form->field($model, 'id_gudang')->widget(Select2::classname(), [
                                        'data' => $data,
                                        'size' => Select2::SMALL,
                                        'options' => ['placeholder' => '--Pilih Gudang --', 'id' => 'namagudang'],
                                        'readonly' => true,
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                        ],
                                    ])->label('Gudang');
                                    ?>

                                </div>
                            </div>
                        </div>

                        <hr>
                        <div class="col-md-12">
                            <div class="form-group">
                                <table class="table table-bordered table-hover shoppingcart">
                                    <thead>
                                    <tr>
                                        <th>
                                            Nama Barang
                                        </th>
                                        <th>
                                            Jumlah Order
                                        </th>
                                        <th>
                                            Jumlah Kirim
                                        </th>
                                        <th>
                                            Sisa
                                        </th>
                                        <!--                                            <th>-->
                                        <!--                                                Harga (/Unit)-->
                                        <!--                                            </th>-->
                                        <!---->
                                        <!--                                            <th>-->
                                        <!--                                                Diskon-->
                                        <!--                                            </th>-->
                                        <!--                                            <th>-->
                                        <!--                                                PPN-->
                                        <!--                                            </th>-->
                                        <!--                                            <th>-->
                                        <!--                                                Harga (/Unit) + PPN-->
                                        <!--                                            </th>-->
                                        <!--                                            <th>-->
                                        <!--                                                Total Harga + PPN-->
                                        <!--                                            </th>-->
                                    </tr>
                                    </thead>
                                    <tbody id="tbodyorder">
                                    <?php
                                    $tr = '';
                                    foreach ($detailPO as $detail) {

                                        $stok = Barang::find()
                                            ->where(['id' => $detail->id_barang])
                                            ->one();

                                        $tr .= "<tr id=\"" . $stok->id . "\" val=\"" . $stok->name . "\">
                                                <td>
                                                    " . $stok->name . "
                                                </td>
                                                <td>
                                                    " . $detail->jumlah . "
                                                </td>
                                                <td>
                                                    " . $detail->jumlah_kirim . "
                                                </td>
                                                <td>
                                                    " . $detail->sisa . "
                                                </td>
                                                
                                            </tr>";

                                    }
                                    echo $tr;
                                    //                                        <td>
                                    //                                        " . number_format($detail->harga_beli, 2, ',', '.') . "
                                    //                                                </td>
                                    //                                                <td>
                                    //                                        " . number_format($detail->diskon, 2, ',', '.') . "
                                    //                                                </td>
                                    //                                                <td>
                                    //                                        " . number_format((($detail->harga_beli - $detail->diskon) * $detail->ppn / 100), 2, ',', '.') . "
                                    //                                                </td>
                                    //                                                <td>
                                    //                                        " . number_format($detail->harga_kena_pajak, 2, ',', '.') . "
                                    //                                                </td>
                                    //                                                <td>
                                    //                                        " . number_format(($detail->jumlah * $detail->harga_kena_pajak), 2, ',', '.') . "
                                    //                                                </td>
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <h4>
                                Tambah Pembelian
                            </h4>
                            <div class="row">
                                <div class="col-md-2" id="barangWrapper">
                                    <?php
                                    echo "<label class='control-label'>Barang</label>" . Select2::widget([
                                            'model' => new DetailPembelian(),
                                            'attribute' => 'id_barang',
                                            'theme' => Select2::THEME_BOOTSTRAP,
                                            'size' => Select2::SMALL,
                                            'data' => ArrayHelper::map($detailPO, 'barang.id', 'barang.name'),
                                            'options' => ['placeholder' => '--Pilih Barang --', 'id' => "barangList"],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                    ?>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label class="control-label">Satuan</label>
                                        <br/>
                                        <label class="control-label" id="satuanBarang"></label>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <?= $form->field($detailModel, 'jumlah')->textInput(['id' => 'qty', 'class' => 'input-sm form-control']) ?>
                                </div>
                                <div class="col-md-2">
                                    <?= $form->field($detailModel, 'harga_beli')->widget(MaskMoney::classname(),
                                        ['options' =>
                                            [
                                                'id' => 'hargaBeli',
                                                'class' => 'input-sm form-control',
                                            ]
                                        ])->label('Harga (/Unit)') ?>
                                </div>

                                <div class="col-md-1">
                                    <?= $form->field($detailModel, 'diskon_persen')->textInput(['id' => 'diskon_persen', 'value' => '0', 'class' => 'input-sm form-control'])->label('Disk (%)') ?>
                                </div>
                                <div class="col-md-2">
                                    <?= $form->field($detailModel, 'diskon')->widget(MaskMoney::classname(),
                                        ['options' => ['id' => 'diskon', 'value' => '0', 'class' => 'input-sm form-control']])->label('Diskon') ?>
                                </div>
                                <div class="col-md-1">
                                    <?= $form->field($detailModel, 'ppn')->textInput(['id' => 'ppn', 'value' => '10', 'class' => 'input-sm form-control'])->label('PPN (%)') ?>
                                </div>
                                <div class="col-md-2">
                                    <?= $form->field($detailModel, 'harga_kena_pajak')->widget(MaskMoney::classname(),
                                        ['options' => ['id' => 'harga_kena_pajak', 'value' => '0', 'class' => 'input-sm form-control'], 'readonly' => true]) ?>
                                </div>
                                <!--
                                <div class="col-md-2">
                                    <?= Html::label('Harga Retail', null, ['class' => 'control-label']) ?>

                                    <div class="input-text">
                                        <?= Html::textInput('harga_retail', null, ['class' => 'input-sm form-control', 'id' => 'hargaRetail'])
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <?= Html::label('Harga Agen', null, ['class' => 'control-label']) ?>

                                    <div class="input-text">
                                        <?= Html::textInput('harga_agen', null, ['class' => 'input-sm form-control', 'id' => 'hargaAgen'])
                                        ?>
                                    </div>
                                </div>
                                -->

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>Detail Barang</h4>
                                </div>
                                <div class="col-md-12" id="detailBarang">

                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <?= Html::a('Tambah', ['#'], ['class' => 'btn btn-warning', 'id' => 'addbutton']) ?>

                                <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-danger']) ?>
                                <br/>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <table class="table table-bordered table-hover shoppingcart">
                                    <thead>
                                    <tr>
                                        <th>
                                            Nama Barang
                                        </th>
                                        <th>
                                            Jumlah
                                        </th>
                                        <th>
                                            Harga (/Unit)
                                        </th>

                                        <th>
                                            Disc(%)
                                        </th>
                                        <th colspan="2">
                                            PPN
                                        </th>
                                        <th>
                                            Harga (/Unit) + PPN
                                        </th>
                                        <th>
                                            Total Harga + PPN
                                        </th>
<!--                                        <th>-->
<!--                                            Harga Retail-->
<!--                                        </th>-->
<!--                                        <th>-->
<!--                                            Harga Agen-->
<!--                                        </th>-->
                                        <th class="col-md-1">

                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbody">
                                    <?php
                                    $tr = '';
                                    $detailPembelian = DetailPembelian::find()
                                        ->where([
                                            'id_pembelian' => $model->id,
                                        ])
                                        ->all();
                                    foreach ($detailPembelian as $detail) {
                                        $stok = Barang::find()
                                            ->where(['id' => $detail->id_barang])
                                            ->one();

                                        $data = array();
                                        // if(count($stok) > 0){
                                        $tr .= "<tr id=\"" . $stok->id . "\" val=\"" . $stok->name . "\">
                                                <td>
                                                    " . $stok->name . "
                                                    <input name=\"namaobat[]\" value=\"" . $stok->name . "\" type=\"hidden\" readonly=\"true\" />
                                                    <input name=\"id[]\" value=\"" . $stok->id . "\" type=\"hidden\" readonly=\"true\" />
                                                    <input name=\"id_gudang[]\" value=\"" . $model->id_gudang . "\" type=\"hidden\" readonly=\"true\" />
                                                    <input class =\"qty jumlahedit\" name=\"qty[]\" value=\"" . $detail->jumlah . "\" type=\"hidden\" readonly=\"true\" />
                                                    <input class =\"hb\" name=\"hb[]\" value=\"" . $detail->harga_beli . "\" type=\"hidden\" readonly=\"true\" />
                                                    <input class =\"harga1\" name=\"harga1[]\" value=\"" . $detail->harga_jual . "\" type=\"hidden\" readonly=\"true\" />
                                                    <input class =\"harga2\" name=\"harga2[]\" value=\"" . $detail->harga_jual_2 . "\" type=\"hidden\" readonly=\"true\" />
                                                    <input class =\"diskon_persen\" name=\"diskon_persen[]\" value=\"" . $detail->diskon_persen . "\" type=\"hidden\" readonly=\"true\" />
                                                    <input class =\"diskon\" name=\"diskon[]\" value=\"" . $detail->diskon . "\" type=\"hidden\" readonly=\"true\" />
                                                    <input class=\"hkpedit\" name=\"hkp[]\" value=\"" . $detail->harga_kena_pajak . "\" type=\"hidden\" readonly=\"true\" />
                                                    <input class =\"ppn\"name=\"ppn[]\" value=\"" . (($detail->harga_beli - $detail->diskon) * $detail->ppn / 100) . "\" type=\"hidden\" readonly=\"true\" />
                                                    <input class=\"hppedit\" id='hppedit' name=\"totalhj[]\" value=\"" . ($detail->jumlah * $detail->harga_kena_pajak) . "\" type=\"hidden\" readonly=\"true\" />
                                                </td>
                                                <td> 
                                                    " . Html::activeTextInput($detail, 'jumlah[]', ['class' => 'form-control jumlahedit input-sm', 'value' => $detail->jumlah]) . "
                                                </td>
                                                <td>
                                                    " . Html::activeTextInput($detail, 'harga_beli[]', ['class' => 'form-control hargabeliedit input-sm', 'value' => $detail->harga_beli]) . "
                                                </td>
                                                <td>
                                                " . Html::activeTextInput($detail, 'diskon[]', ['class' => 'form-control diskonpersenedit input-sm', 'value' => $detail->diskon]) . "
                                                </td>
                                                <td>
                                                " . Html::checkbox('haveppn[]', ($detail->ppn > 0) ? true : false, ['class' => 'havePPN']) . "
                                                </td>
                                                <td>
                                                " . Html::activeTextInput($detail, 'ppn[]', ['class' => 'form-control ppnedit input-sm', 'value' => $detail->ppn]) . "
                                                </td>
                                                <td>
                                                " . Html::activeTextInput($detail, 'harga_kena_pajak[]', ['class' => 'form-control hkpeditDisplay input-sm', 'id' => 'hkpeditDisplay', 'value' => $detail->harga_kena_pajak, 'readonly' => true]) . "
                                                </td>
                                                <td>
                                                " . Html::activeTextInput($detail, 'total[]', ['class' => 'form-control hppeditDisplay input-sm', 'id' => 'hppeditDisplay', 'value' => ($detail->jumlah * $detail->harga_kena_pajak), 'readonly' => true]) . "
                                                </td>
                                              
                                                <td>
                                                    <a class=\"remove btn btn-danger btn-xs\" style='cursor:pointer;'><i class=\"fa fa-minus\"></i></a>
                                                </td>
                                            </tr>";

                                    }
//                                    <td>
//                                    " . Html::activeTextInput($detail, 'harga_jual[]', ['class' => 'harga1edit form-control input-sm', 'value' => $detail->harga_jual, 'id' => 'harga1']) . "
//                                                </td>
//                                                 <td>
//                                    " . Html::activeTextInput($detail, 'harga_jual_2[]', ['class' => 'harga2edit form-control input-sm', 'value' => $detail->harga_jual_2, 'id' => 'harga2']) . "
//                                                </td>
                                    echo $tr;
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="pull-left">
                                <!--
					<div class="col-md-3">
						<?= $form->field($model, 'diskon_persen')->textInput(['id' => 'diskonGlobalPercent', 'class' => 'form-control', 'value' => '0']) ?>
					</div>
					<div class="col-md-6">
						<?= $form->field($model, 'diskon')->textInput(['id' => 'diskonGlobal', 'class' => 'form-control', 'value' => '0']) ?>
					</div>-->
                            </div>
                            <div class="pull-right">
                                <!--
					<div class="col-md-6">
					<label class="control-label">Total Harga</label>
						<?= Html::textInput('totalharga', '0', ['id' => 'hargaTotalFormat', 'class' => 'form-control', 'readonly' => true, 'value' => '0']) ?>
						<?= $form->field($model, 'harga_beli')->hiddenInput(['id' => 'hargaTotal', 'class' => 'form-control', 'readonly' => true, 'value' => '0'])->label(''); ?>
					</div>
                    -->
                                <div class="col-md-12">
                                    <label class="control-label">Total Harga + Diskon</label>
                                    <?= Html::textInput('totalhargaPajak', '0', ['id' => 'hargaTotalPajakFormat', 'class' => 'input-sm form-control', 'readonly' => true, 'value' => '0']) ?>
                                    <?= $form->field($model, 'harga_kena_pajak')->hiddenInput(['id' => 'hargaTotalPajak', 'class' => 'form-control', 'readonly' => true, 'value' => '0'])->label(''); ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <div class="pull-left">
                                    <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Perbaharui', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success', 'id' => 'save']) ?>
                                </div>
                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <div id="iframeplaceholder" style="display:none;"></div>

<?php

$this->registerCssFile(Yii::$app->getUrlManager()->getBaseUrl() . "/web/theme/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
$this->registerJsFile(Yii::$app->getUrlManager()->getBaseUrl() . "/web/theme/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js");

$this->registerJs($this->render('_script.js'));
?>