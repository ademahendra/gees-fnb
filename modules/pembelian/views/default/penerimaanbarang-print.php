<?php
	use app\models\Pembelian;
	use app\models\DetailPembelian;
	$pembelian = Pembelian::find()
		->where(['id' => $nofaktur])
		->one();
	$model = DetailPembelian::find()
		->where([
			'id_pembelian' => $pembelian->id
		])
		->all();
?>
<!--<h3 style="text-align:center;">RSIA IPHI BATU</h3>
<h5 style="text-align:center;">Jln KH Agus Salim No. 35</h5> 
<h6 style="text-align:center;">Telp. (0341) 591480</h6>
--><h4 style="text-align:center;">Bukti Penerimaan Barang</h4>
<h5 style="text-align:center;">No Faktur: <?= $pembelian->no_faktur ?>, Tanggal Faktur: <?= date('d M Y', strtotime($pembelian->tanggal_faktur)) ?></h5>
<table class="table table-bordered" width="100%" border="0" cellspacing="5" cellpadding="5">
	<tr>
		<td>No</td>
		<td>Nama</td>
		<td>Satuan</td>
		<td>Qty</td>
		<td>Harga</td>
		<td>Diskon</td>
		<td>Harga+PPN</td>
		<td>Sub Total</td>
	</tr>
	<?php
	$i = 0;
	$grandtotal = 0;
	foreach($model as $data){
		$i++;
		$grandtotal += ($data->jumlah * $data->harga_kena_pajak);
	?>	
	<tr>
		<td><?= $i ?></td>
		<td><?= $data->barang->nama ?></td>
		<td><?= $data->barang->satuan->nama ?></td>
		<td><?= $data->jumlah ?></td>
		<td style="text-align:right;"><?= number_format($data->harga_beli,'2',',','.') ?></td>
		<td style="text-align:right;"><?= number_format($data->diskon,'2',',','.') ?></td>
		<td style="text-align:right;"><?= number_format($data->harga_kena_pajak,'2',',','.') ?></td>
		<td style="text-align:right;"><?= number_format(($data->jumlah * $data->harga_kena_pajak),'2',',','.') ?></td>
	</tr>
	<?php	
	}
	?>
	
	<tr>
		<td colspan='6'>&nbsp;</td>		
		<td>Grand Total :</td>
		<td style="text-align:right;"><?= number_format($grandtotal,'2',',','.') ?></td>
	</tr>
</table>