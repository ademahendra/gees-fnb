<?php

use app\models\Pembelian;
use kartik\grid\GridView;
use kartik\growl\Growl;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php
if (Yii::$app->session->getFlash('status') == 'true') {
    echo Growl::widget([
        'type' => Growl::TYPE_SUCCESS,
        'icon' => 'glyphicon glyphicon-ok-sign',
        'title' => 'Notifications',
        'showSeparator' => true,
        'body' => 'Data retur pembelian berhasil Ditambahkan',
        'pluginOptions' => [
            'showProgressbar' => false,
            'placement' => [
                'from' => 'bottom',
                'align' => 'right',
            ]
        ]
    ]);
} else if (Yii::$app->session->getFlash('status') == 'false') {
    echo Growl::widget([
        'type' => Growl::TYPE_DANGER,
        'icon' => 'glyphicon glyphicon-ok-sign',
        'title' => 'Notifications',
        'showSeparator' => true,
        'body' => 'Data retur pembelian gagal Ditambahkan',
        'pluginOptions' => [
            'showProgressbar' => false,
            'placement' => [
                'from' => 'bottom',
                'align' => 'right',
            ]
        ]
    ]);
}
?>

    <div class="col-md-12">


        <div class="widget">
            <div class="widget-header transparent">
                <div class="additional-btn">
                    <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                    <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
                </div>
            </div>

            <div class="widget-content padding">
                <div class="retur-pembelian-form">

                    <?php $form = ActiveForm::begin([
                        'id' => 'mainForm'
                    ]); ?>

                    <div class="col-md-6">
                        <!--?= $form->field($model, 'no_faktur_pembelian')->dropDownList(ArrayHelper::map(Pembelian::find()->orderBy('no_faktur ASC')->groupBy('no_faktur')->all(),'no_faktur','no_faktur'),['prompt'=>'-- Pilih Faktur Pembelian --']); ?-->

                        <?php
                        $data = ArrayHelper::map(Pembelian::find()->orderBy('no_faktur ASC')->groupBy('no_faktur')->all(), 'no_faktur', 'no_faktur');
                        echo $form->field($model, 'no_faktur_pembelian')->widget(Select2::classname(), [
                            'data' => $data,
                            'options' => ['placeholder' => '--Pilih Faktur Pembelian--'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <?= Html::submitButton($model->isNewRecord ? 'Tampilkan' : 'Perbaharui', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

                            &nbsp;

                            <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-danger']) ?>

                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>

                    <?php
                    if (isset($_POST['ReturPembelian'])){

                    ?>

                    <div class="col-md-12">
                        <h3>No Faktur: <?= $_POST['ReturPembelian']['no_faktur_pembelian'] ?></h3>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <?php \yii\widgets\Pjax::begin([
                                'enablePushState' => false
                            ]); ?>
                            <?= GridView::widget([
                                'dataProvider' => $dataProvider,
                                /*'filterModel' => $searchModel,*/
                                'columns' => [
                                    ['class' => 'kartik\grid\SerialColumn'],
                                    //'no_faktur',
                                    ['attribute' => 'id_barang',
                                        'label' => 'Kode Produk',
                                        'value' => 'barang.kode'],
                                    ['attribute' => 'id_barang',
                                        'label' => 'Nama Produk',
                                        'value' => 'barang.nama'],
                                    'jumlah',
                                    [
                                        'attribute' => 'harga_beli',
                                        'value' => function ($data) {
                                            return number_format($data->harga_beli, 2, ',', '.');
                                        }
                                    ],
                                    [
                                        'attribute' => 'diskon',
                                        'value' => function ($data) {
                                            return number_format($data->diskon, 2, ',', '.');
                                        }
                                    ],
                                    ['attribute' => 'ppn',
                                        'label' => 'PPN',
                                        'value' => function ($data) {
                                            return number_format($data->ppn, 2, ',', '.');
                                        }],
                                    [
                                        'attribute' => 'harga_kena_pajak',
                                        'label' => 'Harga Total',
                                        'value' => function ($data) {
                                            return number_format($data->harga_kena_pajak, 2, ',', '.');
                                        }
                                    ],
                                    [
                                        'label' => 'Total',
                                        'value' => function ($data) {
                                            return number_format((($data->ppn * $data->jumlah) + ($data->jumlah * ($data->harga_beli - $data->diskon))), 2, ',', '.');
                                        }],

                                    [
                                        'format' => 'raw',
                                        'value' => function ($data) {
                                            return Html::button('Retur', [
                                                'class' => 'btn btn-success retur btn-sm md-trigger',
                                                'data-id' => $data->id,
                                                'data-idPembelian' => $data->id_pembelian,
                                                'data-total' => $data->jumlah,
                                                'data-modal' => 'md-3d-slit']);
                                        }
                                    ],
                                ],
                            ]); ?>

                            <?php \yii\widgets\Pjax::end();
                            }

                            ?>
                        </div>
                    </div>
                    <div class="widget-content padding">
                    </div>
                </div>
            </div>
        </div>
    </div>
	<!--
    <div class="col-md-12">
    <div class="card bg-white">
    <div class="card-block">
	
    <div class="barang-index">
        <div class="widget">
            <div class="widget-header transparent">
                <div class="additional-btn">
                    <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                    <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
                </div>
            </div>
			
            <div class="widget-content padding">
                <?php $form = ActiveForm::begin([
                    'id' => 'returForm'
                ]); ?>

                <div class="form-group">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>
                                SKU
                            </th>
                            <th>
                                Produk
                            </th>
                            <th>
                                Jumlah
                            </th>
                            <th>
                                HPP
                            </th>
                            <th>
                                Diskon
                            </th>
                            <th>
                                PPN
                            </th>
                            <th>
                                Harga
                            </th>
                            <th>
                                Total
                            </th>
                            <th class="col-md-1">

                            </th>
                        </tr>
                        </thead>
                        <tbody id="tbody">
                        </tbody>
                    </table>
                </div>
                <button class="btn btn-success md-close" id="save">Save</button>
                <?php ActiveForm::end(); ?>

            </div>
			
        </div>
    </div>
-->

<?php
$script = <<< JS

	var addpenjualan = [];	
	$("#addbutton").attr('disabled',true);
	
	$("body").on('click','.remove', function(e){
		$(this).closest('tr').remove();
		var removeItem = $(this).closest('tr').attr('id');

		$('#grand_total').val(grand_total);
		delete addpenjualan[removeItem];

		$("#addbutton").fadeIn();
	});
		
	$("#addRetur").on('click', function(e){
		e.preventDefault();
		var form = $('#returForm').serialize();
		$.ajax({
			type: "POST",
			url: baseUrl+'pembelian/retur-pembelian/addretur',
			dataType: 'json',
			cache: false,
			data: form,
			success: function (json) {
				if(json.status){	
					$("#tbody").append(json.tr);	
					$('#md-id').val('');
					$('#md-total').val('');
									
				} else {
					
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log(xhr.status);
				console.log(xhr.responseText);
				console.log(thrownError);
			}
		});
	});	
	
	$("#save").on('click', function(e){
		e.preventDefault();
		var form = $('#returForm').serialize();
		$.ajax({
			type: "POST",
			url: baseUrl+'retur-pembelian/save',
			dataType: 'json',
			cache: false,
			data: form,
			success: function (json) {
				if(json.status){
					$( '#returForm' ).each(function(){
						this.reset();
					});
					addpenjualan = [];	
					$('#tbody').html('');
				} else {
					bootbox.alert("Failed to save!");
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log(xhr.status);
				console.log(xhr.responseText);
				console.log(thrownError);
			}
		});
	});	

	$('body').on('click', '.retur', function(e){
	   e.preventDefault(); 
	   var thisRow = $(this);
	   bootbox.prompt({
            title: "Masukkan jumlah yang di retur",
            inputType: 'number',
            callback: function (result) {
                console.log(result+' - '+thisRow.attr('data-id'));
                $.ajax({
                    type: "POST",
                    url: baseUrl+'retur-pembelian/addretur',
                    dataType: 'json',
                    cache: false,
                    data: {
                        'id':thisRow.attr('data-id'),
                        'amountRetur':result,
                    },
                    success: function (json) {
                        if(json.status){
                            $("#tbody").append(json.tr);
                        } else {
                            
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }
        });
	});

JS;
$this->registerJs($script, \yii\web\View::POS_READY);
?>