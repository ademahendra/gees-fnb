<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ReturPembelian */

$this->title = 'Perbaharui Retur Pembelian: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Retur Pembelian', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<!-- Page Heading Start -->
	<div class="page-heading">
	  <h1><i class='fa fa-check-square'></i> Perbaharui Retur Pembelian</h1>
	</div>
<!-- Page Heading End-->

<div class="widget-content padding">
	<div class="row">
		<div class="col-md-12">
			<?= $this->render('_form', [
			'model' => $model,
		]) ?>

		</div>
	</div>
</div>