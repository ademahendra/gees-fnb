<?php
use app\models\ReturPembelian;
use app\models\Pembelian;

$returPembelian = ReturPembelian::find()
    ->where([
        'id_pembelian' => $id,
        'no_faktur_pembelian' => $faktur,
        'tanggal_retur' => date('Y-m-d', strtotime($tanggal))
    ])
    ->all();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" media="print" href="<?= $this->theme->baseUrl ?>/styles/print.css">
    <link rel="stylesheet" media="print" href="<?= $this->theme->baseUrl ?>/vendor/bootstrap/dist/css/bootstrap.css">
</head>
<body id="nota">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td colspan="3" align="center"><h3>Bukti Retur Pembelian</h3></td>
    </tr>
</table>
<table width="100%" border="1" cellpadding="0" cellspacing="0" class="table table-bordered">
    <thead>
    <tr class="success">
        <th>No</th>
        <th>Tanggal</th>>
        <th>Gudang</th>
        <th>No. Faktur</th>
        <th>Barang</th>
        <th>Jumlah</th>
        <th>Harga Beli</th>
        <th>Diskon</th>
        <th>PPN</th>
        <th>Harga PPN</th>
        <th>Total</th>
    </tr>
    </thead>
    <tbody id="paddingLess">

    <?php
    $i = 0;
    foreach($returPembelian as $row) {
        $i++;
        ?>
        <tr class="success">
            <td style="text-align: center;"><?= $i ?></td>
            <td><?= date('Y-m-d', strtotime($row->tanggal_retur))?></td>
            <td><?= $row->gudang->nama?> </td>
            <td style="text-align: center;"><?= $row->no_faktur_pembelian ?></td>
            <td><?= $row->barang->nama ?></td>
            <td style="text-align: center;"><?= $row->jumlah ?></td>
            <td style="text-align: right;"><?= is_numeric($row->harga_beli)?number_format($row->harga_beli):'0' ?></td>
            <td style="text-align: right;"><?= is_numeric($row->diskon)?number_format($row->diskon):'0' ?></td>
            <td style="text-align: right;"><?= is_numeric($row->ppn)?number_format($row->ppn):'0' ?></td>
            <td style="text-align: right;"><?= is_numeric($row->harga_kena_pajak)?number_format($row->harga_kena_pajak):'0' ?></td>
            <td style="text-align: right;"><?= is_numeric($row->total)?number_format($row->total):'0' ?></td>
        </tr>
    <?php }?>
    </tbody>

</table>
<script src="<?= $this->theme->baseUrl ?>/vendor/jquery/dist/jquery.js"></script>
<script src="<?= $this->theme->baseUrl ?>/vendor/bootstrap/dist/js/bootstrap.js"></script>
</body>
</html>