<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ReturPembelianSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Retur Pembelian';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="page-heading">
	<h1><i class='fa fa-table'></i> Daftar Retur Pembelian</h1>
	<h3>Retur Pembelian Obat</h3>            
</div>

<div class="widget">
	<div class="widget-header transparent">
		<div class="additional-btn">
			<a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
			<a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
			
		</div>
	</div>

	<div class="widget-content padding">

<div class="retur-pembelian-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('New Retur Pembelian', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<!--
	<?php
		Pjax::begin(['timeout'=>10000,'clientOptions'=>['container'=>'Pjax-container']]);
	?>
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'no_faktur_pembelian',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

	<?php
	Pjax::End();
?>
-->
	
</div>
</div>
	</div>
</div>
