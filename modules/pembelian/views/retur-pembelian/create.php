<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ReturPembelian */

$this->title = 'Tambah Retur Pembelian';
$this->params['breadcrumbs'][] = ['label' => 'Retur Pembelian', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Page Heading Start -->
	<div class="page-heading">
	  <h4><i class='fa fa-check-square'></i> Penambahan Retur Pembelian</h4>
	</div>
	<div class="card bg-white">
	<div class="card-header">
	</div>
	<div class="card-block">
<div class="barang-index">
<!-- Page Heading End-->

<div class="widget-content padding">
	<div class="row">
		<?= $this->render('_form', [
			'model' => $model,
			'dataProvider'=>$dataProvider
		]) ?>
	</div>
</div>