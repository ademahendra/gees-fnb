<?php

namespace app\modules\pembelian\controllers;

use Yii;
use app\models\ReturPembelian;
use app\models\Pembelian;
use app\models\DetailPembelian;
use app\models\Mutasi;
use app\models\Barang;
use app\models\StokGudang;
use app\models\ReturPembelianSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

class ReturPembelianController extends Controller
{
	public $enableCsrfValidation = false;
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
			'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create','view','update'],
                'rules' => [
                    [
                        'allow' => false,
                        'actions' => [''],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index','create','view','update','delete'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
		// $privilage = Yii::$app->user->identity->privilage;
	    // if($privilage !== 1 && $privilage !== 3 && $privilage !== 5 ){
		    // throw new ForbiddenHttpException("Forbidden access");
	    // }
        $searchModel = new ReturPembelianSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->pagination->pageSize= 10;
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
		// $privilage = Yii::$app->user->identity->privilage;
	    // if($privilage !== 1 && $privilage !== 3 && $privilage !== 5 ){
		    // throw new ForbiddenHttpException("Forbidden access");
	    // }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
		// $privilage = Yii::$app->user->identity->privilage;
	    // if($privilage !== 1 && $privilage !== 3 && $privilage !== 5 ){
		    // throw new ForbiddenHttpException("Forbidden access");
	    // }
        $model = new ReturPembelian();
		$returList = Pembelian::find()
			->where(['id' => -1]);
		$dataProvider = new ActiveDataProvider([
			'query' => $returList
		]);

        if ($model->load(Yii::$app->request->post())) {

			$noPembelian = Pembelian::find()
				->where(['no_faktur' => $model->no_faktur_pembelian])
				->one();

			$returList = DetailPembelian::find()
				->where(['id_pembelian' => $noPembelian->id]);



			$dataProvider = new ActiveDataProvider([
				'query' => $returList
			]);

			$model = new ReturPembelian();
				return $this->render('create', [
					'model' => $model,
					'dataProvider' => $dataProvider,

				]);


        } else {
			\Yii::$app->getSession()->setFlash('status', '');

            return $this->render('create', [
                'model' => $model,
				'dataProvider' => $dataProvider,

            ]);
        }
    }

    public function actionSave()
    {

        $data = [];
        $model = new ReturPembelian();
        $post = $_POST;
        $data['status'] = false;
		$gudangDefault = \app\models\Merchant::find()->one();
		
        for($i = 0; $i < count($post['id']); $i++){

            $model = new ReturPembelian();
            $model->id_pembelian = $post['id_pembelian'][$i];
            $model->id_gudang = $gudangDefault->id_gudang;
            $model->no_faktur_pembelian = $post['no_faktur'][$i];
            $model->tanggal_retur =date('Y-m-d', strtotime($post['tanggal_retur'])); //new \yii\db\Expression('NOW()');
            $model->id_barang = $post['idbarang'][$i];
            $model->jumlah = $post['jumlah'][$i];
            $model->harga_beli = $post['hb'][$i];
            $model->harga_kena_pajak = $post['hkp'][$i];
            $model->ppn = $post['ppn'][$i];
            $model->diskon = $post['diskon'][$i];
            $model->total = $post['totalhj'][$i];
            $model->tanggal = new \yii\db\Expression('NOW()');
            if($model->save()){
                $data['status'] = true;
				$data['nofaktur'] = $model->id_pembelian;
				$data['faktur'] = $model->no_faktur_pembelian;
				$data['tanggal'] = $post['tanggal_retur'];
                // update pembelian
				$pembelian = Pembelian::find()
					->where([
                       'id' => $post['id_pembelian'][$i],
                       'id_gudang' => $model->id_gudang,
					])
					->one();

				$pembelian->harga_kena_pajak -= $post['totalhj'][$i];
				$pembelian->sisa -= $post['totalhj'][$i];
				$pembelian->admin_id = Yii::$app->user->identity->id;
				$pembelian->save();

                $detail = DetailPembelian::find()
                    ->where([
                        'id_pembelian' => $post['id_pembelian'][$i],
                        'id_barang' => $post['idbarang'][$i]
                    ])
                    ->one();
                $stok = $detail->jumlah - $post['jumlah'][$i];
                $detail->jumlah = $stok;
                $detail->total = $stok * $detail->harga_kena_pajak;
				$detail->admin_id = Yii::$app->user->identity->id;
                $detail->save();

                $produk = StokGudang::find()
                    ->where([
                        'id_barang' => $post['idbarang'][$i],
                        'id_gudang' => $model->id_gudang,
                    ])
                    ->one();
					
				$findStok = Mutasi::find()
                    ->where([
                        'id_barang' => $post['idbarang'][$i],
						'id_gudang' => $pembelian->id_gudang,
                    ])
                    ->orderBy('tanggal desc')
					->orderBy('id desc')
                    ->one();
                $stokAwal = 0;
                if(count($findStok) > 0){
                    $stokAwal = $findStok->stok_akhir;
                }
				
                $mutasi = new Mutasi();
                $mutasi->id_barang = $post['idbarang'][$i];
				$mutasi->id_gudang = $model->id_gudang;
                $mutasi->kredit  = $post['jumlah'][$i];
                $mutasi->tanggal = $model->tanggal;
                $mutasi->stok_awal = $stokAwal;
                $mutasi->stok_akhir = $stokAwal - $post['jumlah'][$i];
                $mutasi->keterangan = 'Return dari No Faktur '.$post['no_faktur'][$i];
				$mutasi->record_date = new \yii\db\Expression('NOW()');
                $mutasi->update_date = new \yii\db\Expression('NOW()');
                $mutasi->date_stamp = new \yii\db\Expression('NOW()');
                $mutasi->record_by = Yii::$app->user->identity->id;
                $mutasi->update_by = Yii::$app->user->identity->id;
                $mutasi->save();
                $query = Yii::$app->db->createCommand("UPDATE stok_gudang SET 
						stok=stok - ".$post['jumlah'][$i]." , date_stamp = ".new \yii\db\Expression('NOW()')."						
						WHERE id_barang='".$post['idbarang'][$i]."' AND id_gudang='".$model->id_gudang."' ")->execute();
            } else {
                $data['pembelian'][$i] = $model->getErrors();
            }
        }
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $data;
    }

    public function actionUpdate($id)
    {
		// $privilage = Yii::$app->user->identity->privilage;
	    // if($privilage !== 1 && $privilage !== 3 && $privilage !== 5 ){
		    // throw new ForbiddenHttpException("Forbidden access");
	    // }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
             \Yii::$app->getSession()->setFlash('status', 'true');
			
				if($model->save()){
				\Yii::$app->getSession()->setFlash('status', 'true');
				} else {
				\Yii::$app->getSession()->setFlash('status', 'false');
				}
				$model = new ReturPembelian();
					return $this->render('create', [
					'model' => $model,
					]);
			
        } else {
			\Yii::$app->getSession()->setFlash('status', '');
			if ($model->load(Yii::$app->request->post())){
				\Yii::$app->getSession()->setFlash('status', 'false');
			}
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
    public function actionAddretur()
    {
			$data = array();
			// $model = $this->findModel($_POST['idPembelian']);

			$model = DetailPembelian::find()
                ->joinWith(['pembelian'])
				->where([
				    'detail_pembelian.id' => $_POST['id']
                ])
                ->one();
			
			$tr = "<tr id=\"".$model->id."\" val=\"".$model->barang->nama."\">						
				<td>
					".$model->barang->kode."
					<input name=\"id[]\" value=\"".$model->id."\" type=\"hidden\" readonly=\"true\" />
					<input name=\"id_gudang[]\" value=\"".$model->pembelian->id_gudang."\" type=\"hidden\" readonly=\"true\" />
					<input name=\"id_pembelian[]\" value=\"".$model->id_pembelian."\" type=\"hidden\" readonly=\"true\" />
					<input name=\"idbarang[]\" value=\"".$model->id_barang."\" type=\"hidden\" readonly=\"true\" />
					<input name=\"namaproduk[]\" value=\"".$model->barang->nama."\" type=\"hidden\" readonly=\"true\" />
					<input name=\"jumlah[]\" value=\"".$_POST['amountRetur']."\" type=\"hidden\" readonly=\"true\" />
					<input name=\"hb[]\" value=\"".$model->harga_beli."\" type=\"hidden\" readonly=\"true\" />
					<input name=\"no_faktur[]\" value=\"".$model->pembelian->no_faktur."\" type=\"hidden\" readonly=\"true\" />
					<input name=\"totalhb[]\" value=\"".($_POST['amountRetur'] * $model->harga_beli)."\" type=\"hidden\" readonly=\"true\" />
					<input name=\"hkp[]\" value=\"".$model->harga_kena_pajak."\" type=\"hidden\" readonly=\"true\" />
					<input name=\"ppn[]\" value=\"".$model->ppn."\" type=\"hidden\" readonly=\"true\" />
					<input name=\"diskon[]\" value=\"".$model->diskon."\" type=\"hidden\" readonly=\"true\" />
					<input name=\"totalhj[]\" value=\"".($_POST['amountRetur'] * $model->harga_kena_pajak)."\" type=\"hidden\" readonly=\"true\" />
				</td>
				<td>
				    ".$model->barang->nama."
				</td>
				<td>
					".$_POST['amountRetur']."
				</td>
				<td>
					".number_format($model->harga_beli, 2, ',', '.')."
				</td>
				<td>
					".number_format($model->diskon, 2, ',', '.')."
				</td>				
				<td>
					".number_format(($model->ppn), 2, ',', '.')."
				</td>
				<td>
					".number_format(($model->harga_kena_pajak), 2, ',', '.')."
				</td>
				<td>
					".number_format(($_POST['amountRetur'] * $model->harga_kena_pajak), 2, ',', '.')."
				</td>
				<td>
					<a class=\"remove\"><i class=\"fa fa-minus\"></i></a>
				</td>
			</tr>";	
			$data['status'] = true;
			$data['tr'] = $tr;
			echo json_encode($data);
    }

    protected function findModel($id)
    {
        if (($model = Pembelian::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionPrintnota($id, $faktur,$tanggal){
        return $this->renderPartial('_nota',[
            'id' => $id,
            'faktur' => $faktur,
            'tanggal' => $tanggal,
        ]);

    }
}
