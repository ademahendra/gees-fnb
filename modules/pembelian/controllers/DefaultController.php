<?php

namespace app\modules\pembelian\controllers;

use Yii;
use app\models\LogPembayaran;
use app\models\FnbStock as Barang;
use app\models\Pembelian;
use app\models\Mutasi;
use app\models\PembelianSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\db\Command;
use yii\db\QueryBuilder;
use kartik\mpdf\Pdf;
use kartik\select2\Select2;
use app\models\StokGudang;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use yii\helpers\ArrayHelper;
use app\models\DetailPembelian;
use app\models\DetailPurchaseOrder;
use app\models\PurchaseOrder;
use app\models\Supplier;

/**
 * Default controller for the `pembelian` module
 */
class DefaultController extends Controller
{
    public $enableCsrfValidation = false;
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create','view','update'],
                'rules' => [
                    [
                        'allow' => false,
                        'actions' => [''],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index','create','view','update','delete'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Pembelian models.
     * @return mixed
     */
    public function actionIndex()
    {
        /* $privilage = Yii::$app->user->identity->privilage;
        if($privilage !== 1 && $privilage !== 3 && $privilage !== 5 ){
            throw new ForbiddenHttpException("Forbidden access");
        } */
        $searchModel = new PembelianSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize= 10;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pembelian model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        /* $privilage = Yii::$app->user->identity->privilage;
        if($privilage !== 1 && $privilage !== 3 && $privilage !== 5 ){
            throw new ForbiddenHttpException("Forbidden access");
        } */
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pembelian model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate()
    {
//		$privilage = Yii::$app->user->identity->privilage;
        // if($privilage !== 1 && $privilage !== 3 && $privilage !== 5 ){
        // throw new ForbiddenHttpException("Forbidden access");
        // }
        $model = new Pembelian();
        $detailModel = new DetailPembelian();
        $barang = new StokGudang();

        $totalhargabeli = 0;
        $totalppn = 0;
        $totalhargapajak = 0;
        $totaljumlah = 0;
        \Yii::$app->getSession()->setFlash('status', '');

        return $this->render('create', [
            'model' => $model,
            'detailModel' => $detailModel,
            'barang'=>$barang
        ]);

    }

    public function actionSave(){
        $model = new Pembelian();
        $detailModel = new DetailPembelian();
        $barang = new StokGudang();
        $data = [];
        $totalhargabeli = 0;
        $totalppn = 0;
        $totalhargapajak = 0;
        $totaljumlah = 0;
        $data['status'] = false;
        if ($model->load(Yii::$app->request->post())) {
            \Yii::$app->getSession()->setFlash('status', 'false');
            $post = Yii::$app->request->post();

            $model->load(Yii::$app->request->post());
            $detailModel->load(Yii::$app->request->post());
            // echo "<pre>";
            // print_r($post);
            // echo "</pre>";
            // exit;
            $pembelian = new Pembelian();
            $pembelian->no_faktur = $model->no_faktur;
            $pembelian->no_invoice = $model->no_invoice;
            $pembelian->no_surat_jalan = $model->no_surat_jalan;
            $pembelian->id_po = $model->id_po;
            $pembelian->id_gudang = $model->id_gudang;
            $pembelian->tanggal_faktur = date('Y-m-d', strtotime($model->tanggal_faktur));
            $pembelian->id_supplier = $model->id_supplier;
            $pembelian->harga_beli = $model->harga_beli;
            $pembelian->harga_kena_pajak = $model->harga_kena_pajak;
            $pembelian->diskon_persen = $model->diskon_persen;
            $pembelian->diskon = $model->diskon;
            $pembelian->jatuh_tempo = date('Y-m-d', strtotime($model->jatuh_tempo));
            $pembelian->tanggal = new \yii\db\Expression('NOW()');
            $pembelian->admin_id = Yii::$app->user->identity->id;


            $pembelian->diskon_persen = $model->diskon_persen;
            $pembelian->diskon = $model->diskon;

            $pembelian->total_bayar = 0;
            $pembelian->sisa = $model->harga_kena_pajak;

            if($pembelian->save()){
                $data['status'] = true;
                $data['nopembelian'] = $pembelian->id;
                for($i = 0; $i < count($post['id']); $i++){

                    $barang = Barang::find()
                        ->where(['id' => $post['id'][$i]])
                        ->one();

                    $detailPembelian = new DetailPembelian();
                    $detailPembelian->id_pembelian = $pembelian->id;
                    $detailPembelian->id_barang = $post['id'][$i];
                    $detailPembelian->harga_beli = $post['hb'][$i];
                    $detailPembelian->jumlah = $post['qty'][$i];
                    $detailPembelian->ppn = $post['ppn'][$i];
                    $detailPembelian->harga_kena_pajak = $post['hkp'][$i];

//                    $detailPembelian->harga_jual = $post['retail'][$i];
//                    $detailPembelian->harga_jual_2 = $post['agen'][$i];

                    $detailPembelian->diskon_persen = $post['diskon_persen'][$i];
                    $detailPembelian->diskon = $post['diskon'][$i];
                    $detailPembelian->total = $post['totalhj'][$i];
					$detailPembelian->admin_id = Yii::$app->user->identity->id;
                    
					//$detailPembelian->tanggal = new \yii\db\Expression('NOW()');
                    $detailPembelian->tanggal = date('Y-m-d', strtotime($pembelian->tanggal_faktur));
                    if(!$detailPembelian->save()){
                        $data['detail'][$i] = $detailPembelian->getErrors();
                    }

//                    $findStok = Mutasi::find()
//                        ->where([
//                            'id_barang' => $post['id'][$i],
//                            'id_gudang' => $model->id_gudang,
//                        ])
//                        ->orderBy('tanggal desc')
//						->orderBy('id desc')
//                        ->one();
//                    $stokAwal = 0;
//                    if(count($findStok) > 0){
//                        $stokAwal = $findStok->stok_akhir;
//                    }
//
//                    $mutasi = new Mutasi();
//                    $mutasi->id_barang = $post['id'][$i];
//                    $mutasi->id_gudang = $model->id_gudang;
//                    $mutasi->debet = $post['qty'][$i];
//                    $mutasi->tanggal = date('Y-m-d', strtotime($pembelian->tanggal_faktur));
//                    $mutasi->stok_awal = $stokAwal;
//                    $mutasi->stok_akhir = $stokAwal + $post['qty'][$i];
//
//                    $mutasi->keterangan = 'Pembelian dari Supplier: '.$model->supplier->nama;
//					$mutasi->record_date = new \yii\db\Expression('NOW()');
//					$mutasi->update_date = new \yii\db\Expression('NOW()');
//					$mutasi->date_stamp = new \yii\db\Expression('NOW()');
//					$mutasi->record_by = Yii::$app->user->identity->id;
//					$mutasi->update_by = Yii::$app->user->identity->id;
//
//                    if(!$mutasi->save()){
//                        $data['mutasi'][$i] = $mutasi->getErrors();
//                    }

                    //update jumlah barang yang di order pada purchase order
                    $findDetailPurchase = DetailPurchaseOrder::find()
                        ->where([
                            'id_purchase_order' => $model->id_po,
                            'id_barang' => $post['id'][$i],
                            //'id' => $post['detail_id'][$i]
                        ])
                        ->one();

                    if ($findDetailPurchase->jumlah_kirim == 0){
                        $findDetailPurchase->jumlah_kirim = $post['qty'][$i];
                        $findDetailPurchase->sisa = $findDetailPurchase->jumlah - $post['qty'][$i];
                        if(!$findDetailPurchase->save()){
                            $data['detail-purchase'][$i] = $findDetailPurchase->getErrors();
                        }
                    }

                    else if ($findDetailPurchase->jumlah_kirim != 0){
                        $findDetailPurchase->jumlah_kirim += $post['qty'][$i];
                        $findDetailPurchase->sisa = $findDetailPurchase->jumlah - $findDetailPurchase->jumlah_kirim;
                        if(!$findDetailPurchase->save()){
                            $data['detail-purchase'][$i] = $findDetailPurchase->getErrors();
                        }
                    }


                    // update global stok
                    $updateBarang = Barang::find()
                        ->where([
                            'id' => $post['id'][$i]
                        ])
                        ->one();
                    if(count($updateBarang) > 0){
                        $updateBarang->qty = $updateBarang->qty + $post['qty'][$i];
                        $updateBarang->save();
                    }

                    //update stok depend on the warehouse
//                    $updateStokGudang = StokGudang::find()
//                        ->where([
//                            'id_barang' => $post['id'][$i],
//                            'id_gudang' => $model->id_gudang // masuk ke gudang pembelian
//                        ])
//                        ->one();
//                    if(count($updateStokGudang) > 0){
//                        $updateStokGudang->stok = $updateStokGudang->stok + $post['qty'][$i];
//                        $updateStokGudang->harga_beli = $post['hb'][$i];
//                        $updateStokGudang->id_pembelian = $pembelian->id;
//                        $updateStokGudang->tgl_pembelian = $pembelian->tanggal_faktur;
//                        $updateStokGudang->ppn = $post['ppn'][$i];
//                        $updateStokGudang->harga_kena_pajak = $post['hkp'][$i];
//						$updateStokGudang->harga_jual1 = $post['retail'][$i];
//						$updateStokGudang->harga_jual2 = $post['agen'][$i];
//						$updateStokGudang->update_by = Yii::$app->user->identity->id;
//						$updateStokGudang->update_date = new \yii\db\Expression('NOW()');
//                        //$updateStokGudang->save();
//						if(!$updateStokGudang->save()){
//							$data['stok-gudang'][$i] = $updateStokGudang->getErrors();
//						}
                    //} else {
                    //new item in the warehouse
                    $updateStokGudang = new StokGudang();
                    $updateStokGudang->id_barang = $post['id'][$i];
                    $updateStokGudang->id_gudang = $model->id_gudang;
                    $updateStokGudang->id_pembelian = $pembelian->id;
                    $updateStokGudang->tgl_pembelian = date('Y-m-d', strtotime($pembelian->tanggal_faktur));
                    $updateStokGudang->harga_beli = $post['hb'][$i];
                    $updateStokGudang->ppn = $post['ppn'][$i];
                    $updateStokGudang->harga_kena_pajak = $post['hkp'][$i];
                    $updateStokGudang->stok = $updateStokGudang->stok + $post['qty'][$i];
//                    $updateStokGudang->harga_jual1 = $post['retail'][$i];
//                    $updateStokGudang->harga_jual2 = $post['agen'][$i];
                    $updateStokGudang->record_by = Yii::$app->user->identity->id;
                    $updateStokGudang->record_date = new \yii\db\Expression('NOW()');
                    $updateStokGudang->update_by = Yii::$app->user->identity->id;
                    $updateStokGudang->update_date = new \yii\db\Expression('NOW()');
                    //$updateStokGudang->save();
                    if(!$updateStokGudang->save()){
                        $data['stok-gudang'][$i] = $updateStokGudang->getErrors();
                    }
                    //}

                    $totalppn += $post['ppn'][$i];
                } //end of loop detail

                $pembelian->ppn = $totalppn;

                if(!$pembelian->save()){
                    $data['status'] = false;
                    $data['msg-pembelian'] = $pembelian->getErrors();
                } else {
                    $data['status'] = true;
                }

            } else {
                $data['status'] = false;
                $data['msg'] = $pembelian->getErrors();
            }

        } else {
            $data['status'] = false;
            $data['msg'] = "Failed to load Post data";
        }
        return json_encode($data);
    }

    /**
     * Updates an existing Pembelian model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $detail = new DetailPembelian();
        $data = [];
        $totalhargabeli = 0;
        $totalppn = 0;
        $totalhargapajak = 0;
        $totaljumlah = 0;
        $data['status'] = false;
        if ($model->load(Yii::$app->request->post())) {

//            print_r( $_POST['DetailPembelian']['harga_jual']);
//            print_r( $_POST['DetailPembelian']['harga_jual_2']);
//            exit;

            $pembelian = Pembelian::find()
                ->where([
                    'id' => $id
                ])
                ->one();

            $pembelian->no_faktur = $model->no_faktur;
            $pembelian->no_invoice = $model->no_invoice;
            $pembelian->no_surat_jalan = $model->no_surat_jalan;
            $pembelian->id_po = $model->id_po;
            $pembelian->id_gudang = $model->id_gudang;
            $pembelian->tanggal_faktur = date('Y-m-d', strtotime($model->tanggal_faktur));
            $pembelian->id_supplier = $model->id_supplier;
            $pembelian->harga_beli = $model->harga_beli;
            $pembelian->harga_kena_pajak = $model->harga_kena_pajak;
            $pembelian->diskon_persen = $model->diskon_persen;
            $pembelian->diskon = $model->diskon;
            $pembelian->jatuh_tempo = date('Y-m-d', strtotime($model->jatuh_tempo));
            $pembelian->tanggal = new \yii\db\Expression('NOW()');
            $pembelian->admin_id = Yii::$app->user->identity->id;
            if ($pembelian->save()) {
                if (count($_POST['id']) > 0) {
                    $valueSelisih = array();
                    for ($x = 0; $x < count($_POST['id']); $x++) {
                        $selisih = DetailPembelian::find()
                            ->where([
                                'id_pembelian' => $id,
                                'id_barang' => $_POST['id'][$x]
                            ])
                            ->one();
                        if (count($selisih) > 0) {
                            $jumlahAwal = $selisih->jumlah;
                            $valueSelisih[$x] = ($jumlahAwal - $_POST['qty'][$x]);
                        } else {
                            $valueSelisih[$x] = 0;
                        }
                    }

                    DetailPembelian::deleteAll(['id_pembelian' => $id]);
                    for ($i = 0; $i < count($_POST['id']); $i++) {
                        $detailPembelian = new DetailPembelian();
                        $detailPembelian->id_pembelian = $pembelian->id;
                        $detailPembelian->id_barang = $_POST['id'][$i];
                        $detailPembelian->harga_beli = $_POST['hb'][$i];
                        $detailPembelian->jumlah = $_POST['qty'][$i];
                        $detailPembelian->ppn = $_POST['ppn'][$i];
                        $detailPembelian->harga_kena_pajak = $_POST['hkp'][$i];

                        $detailPembelian->harga_jual =$_POST['DetailPembelian']['harga_jual'][$i];
                        $detailPembelian->harga_jual_2 = $_POST['DetailPembelian']['harga_jual_2'][$i];

                        $detailPembelian->diskon_persen = $_POST['diskon_persen'][$i];
                        $detailPembelian->diskon = $_POST['diskon'][$i];
                        $detailPembelian->total = $_POST['totalhj'][$i];

                        $detailPembelian->tanggal = new \yii\db\Expression('NOW()');
                        if (!$detailPembelian->save()) {
                            $data['detail'][$i] = $detailPembelian->getErrors();
                        }

                        //update edited artikel stok
                        $findStok = Mutasi::find()
                            ->where([
                                'id_gudang' => $pembelian->id_gudang,
                                'id_barang' => $_POST['id'][$i],
                            ])
                            ->andWhere(['=', 'tanggal', $pembelian->tanggal_faktur])
                            ->orderBy('tanggal desc')
                            ->one();

                        if (count($findStok) > 0) {

                            $valueStokMasuk = $findStok->debet - $valueSelisih[$i];
                            $valueStokAkhir = $findStok->stok_akhir - $valueSelisih[$i];
                            $findStok->debet = $valueStokMasuk;
                            $findStok->stok_akhir = $valueStokAkhir;

                            if ($findStok->save()) {
                                //update mutasi
                                $updatefollowingMutasi = Mutasi::find()
                                    ->where([
                                        'id_gudang' => $pembelian->id_gudang,
                                        'id_barang' => $_POST['id'][$i],
                                    ])
                                    ->andWhere(['>', 'tanggal', $pembelian->tanggal_faktur])
                                    ->orderBy('tanggal desc')
                                    ->all();

                                foreach ($updatefollowingMutasi as $list) {
                                    //$data .= 'Masuk Loop';
                                    $updateItem = Mutasi::find()
                                        ->where([
                                            'id' => $list->id,
                                            'id_barang' => $list->id_barang
                                        ])
                                        ->one();
                                    $updateItem->stok_awal = $updateItem->stok_awal - $valueSelisih[$i];
                                    $updateItem->stok_akhir = $updateItem->stok_akhir - $valueSelisih[$i];
									$updateItem->update_date = new \yii\db\Expression('NOW()');
                                    if (!$updateItem->save()) {
                                        $data .= $updateItem->getErrors();
                                    }
                                }
                            }

                            //update jumlah barang yang di order pada purchase order
                            $findDetailPurchase = DetailPurchaseOrder::find()
                                ->where([
                                    'id_purchase_order' => $model->id_po,
                                    'id_barang' => $_POST['id'][$i],
                                ])
                                ->one();

                            $findDetailPurchase->jumlah_kirim = $findDetailPurchase->jumlah_kirim - $valueSelisih[$i];
                            $findDetailPurchase->sisa = $findDetailPurchase->sisa + $valueSelisih[$i];
                            if(!$findDetailPurchase->save()){
                                $data['detail-purchase'][$i] = $findDetailPurchase->getErrors();
                            }

                            // update global stok
                            $updateBarang = Barang::find()
                                ->where([
                                    'id' => $_POST['id'][$i]
                                ])
                                ->one();
                            if(count($updateBarang) > 0){
                                $updateBarang->stok =$updateBarang->stok - $valueSelisih[$i];
                                $updateBarang->save();
                            }

                            //  update stok depend on the warehouse
                            $updateStokGudang = StokGudang::find()
                                ->where([
                                    'id_barang' => $_POST['id'][$i],
                                    'id_gudang' => $model->id_gudang,
									'id_pembelian' => $id
                                ])
                                ->one();
                            if(count($updateStokGudang) > 0){
								$selisihStok[$i] = $updateStokGudang->stok - $valueSelisih[$i];
                                $updateStokGudang->stok = $selisihStok[$i];
                                $updateStokGudang->update_by = Yii::$app->user->identity->id;
                                $updateStokGudang->update_date = new \yii\db\Expression('NOW()');
                                //$updateStokGudang->save();
                                if(!$updateStokGudang->save()){
                                    $data['stok-gudang'][$i] = $updateStokGudang->getErrors();
                                }
                            }
                            $totalppn += $_POST['ppn'][$i];
                        }
                    }
                    $pembelian->ppn = $totalppn;

                    if(!$pembelian->save()){
                        $data['status'] = false;
                        $data['msg-pembelian'] = $pembelian->getErrors();
                    } else {
                        $data['status'] = true;
                    }
                }
            }
            return json_encode($data);
        }

        $detailModel = DetailPembelian::find()
            ->joinWith('pembelian')
            ->where([
                'id_pembelian' => $id
            ])
            ->one();

        $detailPO = DetailPurchaseOrder::find()
            ->joinWith('barang',true,'LEFT JOIN')
            ->where([
                'id_purchase_order' => $detailModel->pembelian->id_po
            ])
            ->all();

        return $this->render('update', [
            'model' => $model,
            'detailModel' => $detailModel,
            'detailPO' => $detailPO,
        ]);
    }

    /**
     * Deletes an existing Pembelian model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pembelian model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pembelian the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pembelian::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAdditem()
    {
        $model = new Pembelian();
        $detail = new DetailPembelian();

        $model->load(Yii::$app->request->post());
        $detail->load(Yii::$app->request->post());

        $stok = Barang::find()
            // ->joinWith('barang', true, 'left join')
            ->where(['id' => $detail->id_barang])
            ->one();

        $data = array();
        // if(count($stok) > 0){
        $tr = "<tr id=\"".$stok->id."\" val=\"".$stok->name."\">						
				<td>
					".$stok->name."
					<input name=\"nama[]\" value=\"".$stok->name."\" type=\"hidden\" readonly=\"true\" />
					<input class='idReceive' name=\"id[]\" value=\"".$stok->id."\" type=\"hidden\" readonly=\"true\" />
					<input name=\"id_gudang[]\" value=\"".$model->id_gudang."\" type=\"hidden\" readonly=\"true\" />
					<input class='qtyReceive' name=\"qty[]\" value=\"".$detail->jumlah."\" type=\"hidden\" readonly=\"true\" />
					<input name=\"hb[]\" value=\"".$detail->harga_beli."\" type=\"hidden\" readonly=\"true\" />
					<input name=\"diskon_persen[]\" value=\"".$detail->diskon_persen."\" type=\"hidden\" readonly=\"true\" />
					<input name=\"diskon[]\" value=\"".$detail->diskon."\" type=\"hidden\" readonly=\"true\" />
					<input name=\"hkp[]\" value=\"".$detail->harga_kena_pajak."\" type=\"hidden\" readonly=\"true\" />
					<input name=\"ppn[]\" value=\"".(($detail->harga_beli - $detail->diskon) * $detail->ppn/100)."\" type=\"hidden\" readonly=\"true\" />
					<input class='totalHargaItemPPN' name=\"totalhj[]\" value=\"".($detail->jumlah * $detail->harga_kena_pajak)."\" type=\"hidden\" readonly=\"true\" />
				</td>
				<td>
					".$detail->jumlah."
				</td>
				<td>
					".number_format($detail->harga_beli, 2, ',','.')."
				</td>
				<td>
					".number_format($detail->diskon, 2, ',','.')."
				</td>
				<td>
					".number_format((($detail->harga_beli - $detail->diskon) * $detail->ppn/100), 2, ',','.')."
				</td>
				<td>
					".number_format($detail->harga_kena_pajak, 2, ',','.')."
				</td>
				<td>
					".number_format(($detail->jumlah * $detail->harga_kena_pajak), 2, ',','.')."
				</td>
				
				<td>
					<a class=\"remove\" style='cursor:pointer;'><i class=\"fa fa-minus\"></i></a>
				</td>
			</tr>";
        $data['status'] = true;
        $data['tr'] = $tr;
        // } else {
        // $data['status'] = false;
        // }
//        <td>
//    ".$_POST['harga_retail']."
//    </td>
//				<td>
//    ".$_POST['harga_agen']."
//    </td>
        echo json_encode($data);
    }

    public function actionEditpembelian(){
        $model = new Pembelian();
        $detail = new DetailPembelian();

        $model->load(Yii::$app->request->post());
        $detail->load(Yii::$app->request->post());

        $stok = Barang::find()
            // ->joinWith('barang', true, 'left join')
            ->where(['id' => $detail->id_barang])
            ->one();

        $data = array();
        // if(count($stok) > 0){
        $tr = "<tr id=\"".$stok->id."\" val=\"".$stok->nama."\">						
				<td>
					".$stok->nama."
					<input name=\"namaobat[]\" value=\"".$stok->nama."\" type=\"hidden\" readonly=\"true\" />
					<input name=\"id[]\" value=\"".$stok->id."\" type=\"hidden\" readonly=\"true\" />
					<input name=\"id_gudang[]\" value=\"".$model->id_gudang."\" type=\"hidden\" readonly=\"true\" />
					<input name=\"qty[]\" value=\"".$detail->jumlah."\" type=\"hidden\" readonly=\"true\" />
					<input name=\"hb[]\" value=\"".$detail->harga_beli."\" type=\"hidden\" readonly=\"true\" />
					<input name=\"diskon_persen[]\" value=\"".$detail->diskon_persen."\" type=\"hidden\" readonly=\"true\" />
					<input name=\"diskon[]\" value=\"".$detail->diskon."\" type=\"hidden\" readonly=\"true\" />
					<input name=\"hkp[]\" value=\"".$detail->harga_kena_pajak."\" type=\"hidden\" readonly=\"true\" />
					<input name=\"ppn[]\" value=\"".(($detail->harga_beli - $detail->diskon) * $detail->ppn/100)."\" type=\"hidden\" readonly=\"true\" />
					<input name=\"totalhj[]\" value=\"".($detail->jumlah * $detail->harga_kena_pajak)."\" type=\"hidden\" readonly=\"true\" />
					<input name=\"retail[]\" value=\"".$_POST['harga_retail']."\" type=\"hidden\" readonly=\"true\" />
					<input name=\"agen[]\" value=\"".$_POST['harga_agen']."\" type=\"hidden\" readonly=\"true\" />
				</td>
				<td>
					<input type='text' value='".($detail->jumlah)."' class='form-control jumlahedit ' />
				</td>
				<td>
					 <input type='text' value='".($detail->harga_beli)."' class='form-control hargabeliedit ' />
				</td>
				<td>
					<input type='text' value='".($detail->diskon)."' class='form-control diskonedit ' />
				</td>
				<td>
				    <input type='text' value='".(($detail->harga_beli - $detail->diskon) * $detail->ppn/100)."' class='form-control hargappnedit ' />
				</td>
				<td>
					<input type='text' readonly='true' value='".($detail->harga_kena_pajak)."' class='form-control  ' />
				</td>
				<td>
				    <input type='text' readonly='true' value='".($detail->jumlah * $detail->harga_kena_pajak)."' class='form-control  ' />
				</td>
				<td>
    				<input type='text' value='".$_POST['harga_retail']."' class='form-control  ' />
				</td>
				<td>
	    			<input type='text' value='".$_POST['harga_agen']."' class='form-control  ' />
				</td>
				<td>
					<a class=\"remove\" style='cursor:pointer;'><i class=\"fa fa-minus\"></i></a>
				</td>
			</tr>";
        $data['status'] = true;
        $data['tr'] = $tr;
        // } else {
        // $data['status'] = false;
        // }
        echo json_encode($data);
    }

    public function actionDetail() {
        if (isset($_POST['expandRowKey'])) {
            $model = Pembelian::findOne($_POST['expandRowKey']);
            return $this->renderPartial('detail', ['model'=>$model]);
        } else {
            return '<div class="alert alert-danger">No data found</div>';
        }
    }

    public function actionGetpo($id)
    {
        $model = PurchaseOrder::find()
            ->joinWith('supplier',true,'LEFT JOIN')
            ->where([
                'purchase_order.id' => $id
            ])
            ->one();
        $detailPO = DetailPurchaseOrder::find()
            ->joinWith('barang',true,'LEFT JOIN')
            ->where([
                'id_purchase_order' => $model->id
            ])
            ->all();
        $tr = '';
        foreach($detailPO as $detail) {
            $stok = barang::find()
                ->where(['id' => $detail->id_barang])
                ->one();

            $data = array();
            // if(count($stok) > 0){
            $tr .= "<tr id=\"" . $stok->id . "\" val=\"" . $stok->name . "\">
				<td>
					" . $stok->name . " 
					<input class=\"orderQtyItem\"  name=\"orderQtyItem[]\" value=\"" . ($detail->jumlah - ((isset($detail->jumlah_kirim) || ($detail->jumlah_kirim != ''))?$detail->jumlah_kirim:0)). "\" type=\"hidden\" readonly=\"true\" />
					<input class=\"idItem\"  name=\"idItem[]\" value=\"" . $stok->id . "\" type=\"hidden\" readonly=\"true\" />
					
				</td>
				<td>
					" . $detail->jumlah . "
				</td>
				<td>
					" . $detail->jumlah_kirim . "
				</td>
				<td>
					" . $detail->sisa . "
				</td>
			</tr>";
        }
        //Filter Material from Current PO

        $barang = "<label class='control-label'>Barang</label>".Select2::widget([
                'model' => new DetailPembelian(),
                'attribute' => 'id_barang',
                'theme' => Select2::THEME_BOOTSTRAP,
                'data' => ArrayHelper::map($detailPO, 'barang.id', 'barang.name'),
                'options' => ['placeholder' => '--Pilih Barang --', 'id'=> "barangList"],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
        $supplier = "<label class='control-label'>Supplier</label>".Select2::widget([
                'model' => new Pembelian(),
                'data' => ArrayHelper::map(Supplier::find()->where(['id' => $model->id_supplier])->all(), 'id', 'nama'),
                'attribute' => 'id_supplier',
                'size' => Select2::SMALL,
                'theme' => Select2::THEME_BOOTSTRAP,
                'value' => $model->id_supplier,
                'options' => ['placeholder' => '--Pilih Supplier --', 'id'=> "supplierList", 'value' => $model->id_supplier],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);

        $data['status'] = true;
        $data['supplierID'] = $model->id_supplier;
        $data['supplier'] = $supplier;
        $data['supplier-top'] = date('m/d/Y', strtotime('+'.$model->supplier->top.' days'));
        $data['top'] = $model->supplier->top;
        $data['tr'] = $tr;
        $data['barang'] = $barang;
        // } else {
        // $data['status'] = false;
        // }
        echo json_encode($data);
    }

    public function actionPrintreceipt($id){
        return $this->renderPartial('nota',[
            'id' => $id
        ]);

    }
}