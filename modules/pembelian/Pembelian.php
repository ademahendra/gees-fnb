<?php

namespace app\modules\pembelian;

/**
 * pembelian module definition class
 */
class Pembelian extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\pembelian\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
