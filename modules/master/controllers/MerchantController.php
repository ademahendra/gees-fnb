<?php

namespace app\modules\master\controllers;

use app\common\ResizeImage;
use Yii;
use app\models\Merchant;
use app\models\MerchantSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
/**
 * SupplierController implements the CRUD actions for Supplier model.
 */
class MerchantController extends Controller
{

    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];

    }

    /**
     * Lists all Supplier models.
     * @return mixed
     */
    public function actionIndex()
    {
        // $privilage = Yii::$app->user->identity->privilage;
        // if($privilage !== 1 && $privilage !== 3 && $privilage !== 5 ){
        // throw new ForbiddenHttpException("Forbidden access");
        // }
        $searchModel = new MerchantSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize= 10;
        $model = new Merchant();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Supplier model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "Produk # ".$id,
                'content'=>$this->renderAjax('view', [
                    'model' => $this->findModel($id),

                ]),
                'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
            ];
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),

            ]);
        }
    }

    /**
     * Creates a new Supplier model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        // $privilage = Yii::$app->user->identity->privilage;
        // if($privilage !== 1 && $privilage !== 3 && $privilage !== 5 ){
        // throw new ForbiddenHttpException("Forbidden access");
        // }
        $request = Yii::$app->request;
        $model = new Merchant();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new Merchant",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Create new Merchant",
                    'content'=>'<span class="text-success">Create Merchant success</span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Create new Merchant",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing Supplier model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> YIi::t('app', 'Ubah Merchant')." #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button(Yii::t('app','Tutup'),['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button(Yii::t('app','Simpan'),['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post())){
                /**---------------------upload------------------------**/
                extract($_POST);
                $error=array();
                $extension=array("jpeg","jpg","png","gif");
                foreach($_FILES["files"]["tmp_name"] as $key=>$tmp_name) {
                    $file_name = $_FILES["files"]["name"][$key];
                    $file_tmp = $_FILES["files"]["tmp_name"][$key];
                    $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                    $txtGalleryName = 'artikel';
                    if (in_array($ext, $extension)) {
                        $filename = basename($this->random_string(15), $ext);
                        $newFileName = $filename . time() . "." . $ext;
                        if (!file_exists('photo_gallery/')) {
                            mkdir('photo_gallery/', 0777, true);
                        }
                        move_uploaded_file($file_tmp = $_FILES["files"]["tmp_name"][$key], "photo_gallery/" . $newFileName);

                        $resizeObj = new ResizeImage("photo_gallery/" . $newFileName);
                        $resizeObj->resizeImage(512, 512, 'crop');
                        $resizeObj->saveImage("photo_gallery/t-" . $newFileName, 80);
                        $model->logo = "photo_gallery/t-" . $newFileName;
                        $model->record_by = Yii::$app->user->identity->id;
                        $model->update_by = Yii::$app->user->identity->id;
                        $model->record_date = new \yii\db\Expression('NOW()');
                        $model->update_date = new \yii\db\Expression('NOW()');
//                        $model->save();
//                    }
//                    else
//                    {
//                        array_push($error,"$file_name, ");
//                    }
                    }
                }
                /**---------------------------------------------------**/
                if ($model->save()){
                    return [
                        'forceReload'=>'#crud-datatable-pjax',
                        'title'=> "Supplier #".$id,
                        'content'=>$this->renderAjax('view', [
                            'model' => $model,
                        ]),
                        'footer'=> Html::button(Yii::t('app','Tutup'),['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a(Yii::t('app','Ubah'),['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                    ]; }
            }else{
                return [
                    'title'=> YIi::t('app', 'Ubah Merchant')." #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button(Yii::t('app','Tutup'),['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button(Yii::t('app','Simpan'),['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post())){
                extract($_POST);
                $error=array();
                $extension=array("jpeg","jpg","png","gif");
                foreach($_FILES["files"]["tmp_name"] as $key=>$tmp_name) {
                    $file_name = $_FILES["files"]["name"][$key];
                    $file_tmp = $_FILES["files"]["tmp_name"][$key];
                    $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                    $txtGalleryName = 'artikel';
                    if (in_array($ext, $extension)) {
                        $filename = basename($this->random_string(15), $ext);
                        $newFileName = $filename . time() . "." . $ext;
                        if (!file_exists('photo_gallery/')) {
                            mkdir('photo_gallery/', 0777, true);
                        }
                        move_uploaded_file($file_tmp = $_FILES["files"]["tmp_name"][$key], "photo_gallery/" . $newFileName);

                        $resizeObj = new ResizeImage("photo_gallery/" . $newFileName);
                        $resizeObj->resizeImage(512, 512, 'crop');
                        $resizeObj->saveImage("photo_gallery/t-" . $newFileName, 80);
                        $model->logo = "photo_gallery/t-" . $newFileName;
                        $model->record_by = Yii::$app->user->identity->id;
                        $model->update_by = Yii::$app->user->identity->id;
                        $model->record_date = new \yii\db\Expression('NOW()');
                        $model->update_date = new \yii\db\Expression('NOW()');
//                        $model->save();
//                    }
//                    else
//                    {
//                        array_push($error,"$file_name, ");
//                    }
                    }
                }
                if($model->save()){
                    return $this->redirect(['view', 'id' => $model->id]);
                } else {
                    return $this->render('update', [
                        'model' => $model,
                    ]);
                }
            }
            else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }


    private function random_string($length) {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Supplier model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Supplier the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Merchant::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
