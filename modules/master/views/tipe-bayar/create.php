<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TipeBayar */

?>
<div class="tipe-bayar-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
