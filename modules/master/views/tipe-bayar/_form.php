<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\TipeBayar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tipe-bayar-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?php
        echo $form->field($model, 'jenis')->widget(Select2::classname(), [
            'data' => [ 'Debet' => 'Debet', 'Kredit' => 'Kredit', 'Deposit'=>'Deposit'],
            'options' => ['placeholder' => '--Pilih Jenis --', 'id'=>'jenisPopup'],
            'readonly' => true,
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ])->label('Jenis');
    ?>

    <?php //= $form->field($model, 'jenis')->dropDownList([ 'Debet' => 'Debet', 'Kredit' => 'Kredit', 'Deposit'=>'Deposit'], ['prompt' => '-- Pilih Jenis --']) ?>


	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>


    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS
	select2toPopup($('#jenisPopup'),$('#ajaxCrudModal'),'jenisPopup');

JS;

$this->registerJs($script, \yii\web\View::POS_READY);