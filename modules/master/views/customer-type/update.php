<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerType */
?>
<div class="customer-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
