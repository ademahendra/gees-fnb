<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CustomerType */

?>
<div class="widget-content padding">
	<div class="row">
		<div class="col-md-12">
			<?= $this->render('_form', [
			'model' => $model,
		]) ?>

		</div>
	</div>
</div>