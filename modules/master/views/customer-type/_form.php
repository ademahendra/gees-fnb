<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerType */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="widget">
    <div class="widget-header transparent">
        <div class="additional-btn">
            <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
            <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>

        </div>
    </div>
	<div class="widget-content padding">
		<div class="customer-type-form">

			<?php $form = ActiveForm::begin(); ?>

			<?= $form->field($model, 'jenis')->textInput(['maxlength' => true,'class'=>'input-sm form-control']) ?>

			<?= $form->field($model, 'diskon')->textInput(['class'=>'isNumber input-sm form-control']) ?>

			<!--<?= $form->field($model, 'record_by')->textInput() ?>

			<?= $form->field($model, 'update_by')->textInput() ?>

			<?= $form->field($model, 'record_date')->textInput() ?>

			<?= $form->field($model, 'update_date')->textInput() ?>

			<?= $form->field($model, 'date_stamp')->textInput() ?>-->

		  
			<?php if (!Yii::$app->request->isAjax){ ?>
				<div class="form-group">
					<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
				</div>
			<?php } ?>

			<?php ActiveForm::end(); ?>
			
		</div>
	</div> 
</div>

<?php 
$script = <<< JS
$('.isNumber').geesinput();
JS;

$this->registerJsFile(Yii::$app->getUrlManager()->getBaseUrl() . "/js/geesinput.js");
$this->registerJs($script, \yii\web\View::POS_READY);
?>