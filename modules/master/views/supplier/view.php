<?php

use yii\widgets\DetailView;


?>
<div class="jabatan-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nama',
			'top',
            'alamat',
            [
                'attribute'=>'id_provinsi',
                'value'=>$model->provinces->name,
            ],

            [
                'attribute'=>'id_kota',
                'value'=>$model->regencies->name,
            ],

            [
                'attribute'=>'id_daerah',
                'value'=>$model->districts->name,
            ],
            'telepon',
            'contact_person',
            'contact_person_telepone',
        ],
    ]) ?>

</div>