<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Supplier */

$this->title = 'Tambah Supplier';
$this->params['breadcrumbs'][] = ['label' => 'Supplier', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Page Heading Start -->
	<div class="page-heading">
	  <h4><i class='fa fa-check-square'></i> Penambahan Supplier</h4>
	</div>
<!-- Page Heading End-->

<div class="widget-content padding">
	<div class="row">
		<div class="col-md-12">
			<?= $this->render('_form', [
			'model' => $model,
		]) ?>

		</div>
	</div>
</div>
