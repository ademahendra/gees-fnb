<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Supplier */

$this->title = 'Perbaharui Supplier: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Supplier', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<!-- Page Heading Start -->
	<div class="page-heading">
	  <h1><i class='fa fa-check-square'></i> Perbaharui Supplier</h1>
	</div>
<!-- Page Heading End-->

<div class="widget-content padding">
	<div class="row">
		<div class="col-md-12">
			<?= $this->render('_form', [
			'model' => $model,
		]) ?>

		</div>
	</div>
</div>