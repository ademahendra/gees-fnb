<?php
use yii\bootstrap\Html;
use yii\helpers\Url;


return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Logo',
        'format' => 'raw',
        'hiddenFromExport' => true,
        'value'=>function($data){
            if(count($data->logo) > 0){
                // return print_r($data->images);
                return '<img src="'.Yii::$app->params['baseURL'].$data->logo.'" width="100" height="100"/>';
            }
            return "No Image";
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'alamat',
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'telepon',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'website',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'email',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'format' => 'raw',
        'vAlign' => 'middle',
        'value' => function ($data) {
            $view = Html::a('<button class="btn btn-success btn-xs"><span class="fa fa-eye"></span></button>', Url::to(['view', 'id' => $data->id]), [
                'title' => Yii::t('yii', 'View'),
                'data-pjax' => 0,
                'role' => 'modal-remote',
                'data-toggle' => 'tooltip'
            ]);
            $update = Html::a('<button class="btn btn-warning btn-xs"><span class="fa fa-edit"></span></button>', Url::to(['update', 'id' => $data->id]), [
                'title' => Yii::t('yii', 'Update'),
                'data-pjax' => 0,
                'role' => 'modal-remote',
                'data-toggle' => 'tooltip'
            ]);

            return $view . $update;
        }
    ],

];