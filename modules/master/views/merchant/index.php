<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

use yii\widgets\ActiveForm;

$this->title = 'Merchant';
$this->params['breadcrumbs'][] = $this->title;
CrudAsset::register($this);
?>

<div class="page-title">
    <div class="title"><?= $this->title ?></div>
</div>

<div class="card bg-white">
    <div class="card-header">
        Merchant
    </div>
    <div class="card-block">

        <div id="ajaxCrudDatatable">
            <?=GridView::widget([
                'id'=>'crud-datatable',
                'dataProvider' => $dataProvider,
                //'filterModel' => $searchModel,
                'pjax'=>true,
                'columns' => require(__DIR__.'/_columns.php'),
                'toolbar'=> [
                    ['content'=>
                       //''
						Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
						['role'=>'modal-remote','title'=> 'Create new Merchant','class'=>'btn btn-default']).
						Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
						['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
						'{toggleData}'.
						'{export}'
						],
                ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'type' => 'primary',
                    'heading' => '<i class="glyphicon glyphicon-list"></i> Merchant listing',
                    'before'=>'<em>* Resize table columns just like a spreadsheet by dragging the column edges.</em>',
                    'after'=>'',
                ]
            ])?>
        </div>
    </div>
</div>

<?php Modal::begin([
    'options' => [
        "id"=>"ajaxCrudModal",
        "size" => "modal-lg",
        'tabindex' => false // important for Select2 to work properly
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
