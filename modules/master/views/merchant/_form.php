<?php

use app\models\Customer;
use app\models\Gudang;
use app\models\Supplier;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\growl\Growl;

?>

<div class="widget">
    <div class="widget-header transparent">
        <div class="additional-btn">
            <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
            <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>

        </div>
    </div>

    <div class="widget-content padding">
        <div class="supplier-form">

            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>


            <div class="col-md-12">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-12">
                <?= $form->field($model, 'alamat')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-12">
                <?= $form->field($model, 'telepon')->textInput(['maxlength' => true]) ?>
            </div>


            <div class="col-md-6">
                <?= $form->field($model, 'website')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?php
                $data = ArrayHelper::map(Gudang::find()->orderBy('nama ASC')->all(),'id','nama');
                echo $form->field($model, 'id_gudang')->widget(Select2::classname(), [
                    'data' => $data,
                    'options' => ['placeholder' => 'Pilih Gudang','id' => 'gudangPopup'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Default Gudang');
                ?>
            </div>

            <div class="col-md-6">
                <?php
                $data = ArrayHelper::map(Customer::find()->orderBy('nama ASC')->all(),'id','nama');
                echo $form->field($model, 'id_customer')->widget(Select2::classname(), [
                    'data' => $data,
                    'options' => ['placeholder' => 'Pilih Customer','id' => 'customerPopup'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Default Customer');
                ?>
            </div>
            <div class="col-md-6">
                <?php
                $data = ArrayHelper::map(Supplier::find()->orderBy('nama ASC')->all(),'id','nama');
                echo $form->field($model, 'id_supplier')->widget(Select2::classname(), [
                    'data' => $data,
                    'options' => ['placeholder' => 'Pilih Supplier','id' => 'supplierPopup'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Default Supplier');
                ?>
            </div>
            <div class="row">
                <?php if ($model->logo != null){?>
                    <div class="form-group col-md-12">
                        <?= '<img src="'.Yii::$app->params['baseURL'].$model->logo.'" width="100" height="100"/>'?>
                    </div>
                 <?php
                    }
                ?>
                <div class="form-group col-md-12">
                    <label>Note: Supported image format: .jpeg, .jpg, .png, .gif</label>
                    <input type="file" name="files[]" multiple class="form-control" />
                </div>
            </div>

            <?php if (!Yii::$app->request->isAjax){ ?>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            <?php } ?>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>

<?php
$script = <<< JS
	
    select2toPopup($('#customerPopup'), $('#ajaxCrudModal'), 'customerPopup');
    select2toPopup($('#gudangPopup'), $('#ajaxCrudModal'), 'gudangPopup');
    select2toPopup($('#supplierPopup'), $('#ajaxCrudModal'), 'supplierPopup');
 
JS;
$this->registerJs($script, \yii\web\View::POS_READY);
?>