<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Gudang */
?>
<div class="merchant-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            [
                'format' => 'raw',
                'attribute' =>'logo',
                'value'=>function($data){
                    if(count($data->logo) > 0){
                        // return print_r($data->images);
                        return '<img src="'.Yii::$app->params['baseURL'].$data->logo.'" width="100" height="100"/>';
                    }
                    return "No Image";
                },
            ],
            'name',
            'alamat',
            'telepon',
            'website',
            'email',

            [
                'label'=>'Default Gudang',
                'attribute'=>'id_gudang',
                'value'=> function($data){
                    if($data->id_supplier != null){
                        return $data->gudang->nama;
                    }
                    else{
                        return "Not Set";
                    }


                }
            ],
            [
                'label'=>'Default Customer',
                'attribute'=>'id_customer',
                'value'=> function($data){
                    if($data->id_customer != null){
                        return $data->customer->nama;
                    }
                    else{
                        return "Not Set";
                    }


                }
            ],
            [
                'label'=>'Default Supplier',
                'attribute'=>'id_supplier',
                'value'=> function($data){
                    if($data->id_supplier != null){
                        return $data->supplier->nama;
                    }
                    else{
                        return "Not Set";
                    }


                }
            ],

            'record_by',
            'update_by',



            [
                'attribute'=>'record_date',
                'value'=>date('d M Y, h:iA', strtotime($model->record_date)),
            ],

            [
                'attribute'=>'update_date',
                'value'=>date('d M Y, h:iA', strtotime($model->update_date)),
            ],

            [
                'attribute'=>'date_stamp',
                'value'=>date('d M Y, h:iA', strtotime($model->date_stamp)),
            ],
        ],
    ]) ?>

</div>
