<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Supplier */

$this->title = 'Tambah Merchant';
$this->params['breadcrumbs'][] = ['label' => 'Merchant', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Page Heading Start -->
<div class="page-heading">
    <h4><i class='fa fa-check-square'></i> Penambahan Merchant</h4>
</div>
<!-- Page Heading End-->

<div class="widget-content padding">
    <div class="row">
        <div class="col-md-12">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>

        </div>
    </div>
</div>
