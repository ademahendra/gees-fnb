<?php

use yii\widgets\DetailView;
use app\common\User;

/* @var $this yii\web\View */
/* @var $model app\models\Bank */
?>
<div class="bank-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'kode',
            'nama',
            'surcharge',
            [
				'attribute' => 'record_by',
				'value' => function ($data) {
					//check user have name instead username?
					if (isset($data->record_by)) {						
						return User::toUsername($data->record_by);
					}
					return "";
				},
			],

			[
				'attribute' => 'update_by',
				'value' => function ($data) {
					//check user have name instead username?
					if (isset($data->update_by)) {
						return User::toUsername($data->update_by);
					}
					return "";
				},
			],
            [
				'attribute'=>'record_date',
				'value'=>date('d M Y, h:iA', strtotime($model->record_date)),
			],
			
			[
				'attribute'=>'update_date',
				'value'=>date('d M Y, h:iA', strtotime($model->update_date)),
			],
			
			[
				'attribute'=>'date_stamp',
				'value'=>date('d M Y, h:iA', strtotime($model->date_stamp)),
			],
        ],
    ]) ?>

</div>
