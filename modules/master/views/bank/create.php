<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Bank */

?>

<!-- Page Heading Start -->
	<div class="page-heading">
	  <h4><i class='fa fa-check-square'></i> Penambahan Bank</h4>
	</div>
<!-- Page Heading End-->

<div class="widget-content padding">
	<div class="row">
		<div class="col-md-12">
			<?= $this->render('_form', [
				'model' => $model,
			]) ?>

		</div>
	</div>
</div>


