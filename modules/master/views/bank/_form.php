<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\growl\Growl;
	
    if(Yii::$app->session->getFlash('status') == 'true'){
    	echo Growl::widget([
    		'type' => Growl::TYPE_SUCCESS,
    		'icon' => 'glyphicon glyphicon-ok-sign',
    		'title' => 'Notifications',
    		'showSeparator' => true,
    		'body' => 'Data Bank berhasil Ditambahkan',
    		'pluginOptions' => [
    			'showProgressbar' => false,
    			'placement' => [
    				'from' => 'bottom',
    				'align' => 'right', 
    			]
    		]
    	]); 
    } else if(Yii::$app->session->getFlash('status') == 'false'){
    	echo Growl::widget([
    		'type' => Growl::TYPE_DANGER,
    		'icon' => 'glyphicon glyphicon-ok-sign',
    		'title' => 'Notifications',
    		'showSeparator' => true,
    		'body' => 'Data Bank gagal Ditambahkan',
    		'pluginOptions' => [
    			'showProgressbar' => false,
    			'placement' => [
    				'from' => 'bottom',
    				'align' => 'right',
    			]
    		]
    	]); 			
    }
?>

<div class="row">
    <?php $form = ActiveForm::begin(); ?>
    <div class="col-sm-12 ">
        <div class="widget">
            <div class="widget-header transparent">
                <div class="additional-btn">
                    <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                    <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
                </div>
            </div>
            <div class="widget-content padding">
                <div class="col-sm-12 ">
					<?= $form->field($model, 'kode')->textInput(['maxlength' => true]) ?>

					<?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

					<?= $form->field($model, 'surcharge')->textInput() ?>
					<?= $form->field($model, 'discount')->textInput() ?>
				</div>
				
				<!--
				<div class="col-md-12">
					<div class="form-group">
						<?= Html::submitButton($model->isNewRecord ? 'Tambah' : 'Perbaharui', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						&nbsp;
						<?= Html::a('Kembali', ['index'], ['class' => 'btn btn-danger']) ?>
					</div>
				</div>
				-->
                    
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>


