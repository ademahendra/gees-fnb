<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

use app\models\Customer;
use app\models\CustomerType;
use app\models\Provinces;

$lastID =Customer::find()->orderBy('id desc')->one();

$kode = 'CST-' . sprintf("%04s", 1);
if (count($lastID) > 0) {
    $kode = 'CST-' . sprintf("%04s", ($lastID->id + 1));
}

?>
<div class="widget">
    <div class="widget-header transparent">
        <div class="additional-btn">
            <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
            <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>

        </div>
    </div>
	<div class="widget-content padding">
		<div class="customer">

			<?php $form = ActiveForm::begin(); ?>

			
			<div class = "row">
                <div class="col-md-3">
					<?= $form->field($model, 'kode')->textInput(['maxlength' => true, 'enableAjaxValidation' => true,'readonly'=>true,'class'=>'form-control input-sm','value'=>$model->isNewRecord? $kode:$model->kode]) ?>
				</div>
			</div>
			
			<div class = "row">
                <div class="col-md-6">
					<?= $form->field($model, 'nama')->textInput(['maxlength' => true,'class'=>"form-control input-sm"]) ?>
				</div>
				<div class="col-md-6">
					<?php
						$data = ArrayHelper::map(CustomerType::find()->orderBy('jenis ASC')->all(), 'id', 'jenis');
						echo $form->field($model, 'customer_type')->widget(Select2::classname(), [
							'data' => $data,
							'size' => Select2::SMALL,
							'options' => ['placeholder' => '--Pilih Customer Type --', 'id' => 'typePopup'],
							'pluginOptions' => [
								'allowClear' => true
							],
						])->label('Customer Type');
                    ?>
				</div>
			</div>
			<div class = "row">
                <div class="col-md-12">
					<?= $form->field($model, 'alamat')->textarea(['rows' => 6,'class'=>"form-control input-sm"]) ?>
				</div>
			</div>
			<div class = "row">
                <div class="col-md-6">
					<?= $form->field($model, 'contact_person')->textInput(['maxlength' => true,'class'=>"form-control input-sm"]) ?>
				</div>
				<div class="col-md-6">
					<?= $form->field($model, 'phone')->textInput(['maxlength' => true,'class'=>"form-control input-sm"]) ?>
				</div>
			</div>
	
			<div class = "row">
                <div class="col-sm-4 ">
                    <?php
                    $data = ArrayHelper::map(Provinces::find()->orderBy('name ASC')->all(), 'id', 'name');
                    echo $form->field($model, 'id_provinsi')->widget(Select2::classname(), [
                        'data' => $data,
                        'size' => Select2::SMALL,
                        'options' => ['placeholder' => '--Pilih Provinsi --', 'id' => 'provinsiPopup'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label('Provinsi');
                    ?>
                </div>
                <div class="col-sm-4 ">
                    <?php
                    $data = [];
                    echo $form->field($model, 'id_kota')->widget(Select2::classname(), [
                        'data' => $data,
                        'size' => Select2::SMALL,
                        'options' => ['placeholder' => '--Pilih Kota --', 'id' => 'kotaPopup'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label('Kota');
                    ?>
                </div>
                <div class="col-sm-4 ">
                    <?php
                    echo $form->field($model, 'id_daerah')->widget(Select2::classname(), [
                        'data' => $data,
                        'size' => Select2::SMALL,
                        'options' => ['placeholder' => '--Pilih Kecamatan --', 'id' => 'daerahPopup'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label('Daerah');
                    ?>

                </div>
            </div>

			<!--<?= $form->field($model, 'record_by')->textInput() ?>

			<?= $form->field($model, 'update_by')->textInput() ?>

			<?= $form->field($model, 'record_date')->textInput() ?>

			<?= $form->field($model, 'update_date')->textInput() ?>

			<?= $form->field($model, 'date_stamp')->textInput() ?>-->

		  
			<?php if (!Yii::$app->request->isAjax){ ?>
				<div class="form-group">
					<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
				</div>
			<?php } ?>

			<?php ActiveForm::end(); ?>
			
		</div>
	</div> 
</div>
<script type="text/javascript">
    var provinsi = '<?= $model->id_provinsi ?>';
    var kota = '<?= $model->id_kota ?>';
    var daerah = '<?= $model->id_daerah ?>';
</script>
<?php
$script = <<< JS
$(document).ready(function(){
	 $('body').on('change','#provinsiPopup', function(e){
		var curr = $(this);
	    var id = curr.val();
		var onXHR = false;
		if(curr.val() !== ''){
			if(!onXHR){
              onXHR = true;
				$.ajax({
					method: "GET",
					url: baseUrl+"rest/getkota?id="+id,
					dataType: 'json',
					cache: false,
					success: function(data) {
						$("select#kotaPopup").html(data.option);
						$('#kotaPopup').val(kota);
                        $('#kotaPopup').trigger('change');
                        onXHR = false;
					},
					error: function() {
                        onXHR = false;
					},
				});
			}
		}
	});

	$('body').on('change','#kotaPopup', function(e){
		var curr = $(this);
	    var id = curr.val();
		var onXHR = false;
		if(curr.val() !== ''){
			if(!onXHR){
                onXHR = true;
				$.ajax({
					method: "GET",
					url: baseUrl+"rest/getdaerah?id="+id,
					dataType: 'json',
					cache: false,
					success: function(data) {
						$("select#daerahPopup").html(data.option);
						$('#daerahPopup').val(daerah);
                        $('#daerahPopup').trigger('change');
                        onXHR = false;
					},
					error: function() {
                        onXHR = false;
					},
				});
			}

		}
	});
	
	// select2toPopup($('#provinsiPopup'),$('#ajaxCrudModal'),'provinsiPopup');
	// select2toPopup($('#kotaPopup'),$('#ajaxCrudModal'),'kotaPopup');
	// select2toPopup($('#daerahPopup'),$('#ajaxCrudModal'),'daerahPopup');
	$('#provinsiPopup').trigger('change');
}); 
JS;
$this->registerJs($script, \yii\web\View::POS_READY);
?>

