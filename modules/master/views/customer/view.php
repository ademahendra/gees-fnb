<?php

use yii\widgets\DetailView;
use app\common\User;

?>
<div class="customer-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'kode',
            'nama',
			[
				'attribute'=>'customer_type',
				'value'=>isset($model->customerType->jenis)?$model->customerType->jenis:'Umum',
			],	
            'alamat:ntext',
            'contact_person',
            'phone',
           [
				'attribute'=>'id_provinsi',
				'label' => 'Provinsi',
				'value'=>isset($model->provinces->name)?$model->provinces->name:'',
			],
			
			[
				'attribute'=>'id_kota',
				'label' => 'Kota',
				'value'=>isset($model->regencies->name)?$model->regencies->name:'',
			],	
			
			[
				'attribute'=>'id_daerah',
				'label' => 'Daerah',
				'value'=>isset($model->districts->name)?$model->districts->name:'',
			],	
			
            'top',
             [
				'attribute' => 'record_by',
				'value' => function ($data) {
					//check user have name instead username?
					if (isset($data->record_by)) {						
						return User::toUsername($data->record_by);
					}
					return "";
				},
			],

			[
				'attribute' => 'update_by',
				'value' => function ($data) {
					//check user have name instead username?
					if (isset($data->update_by)) {
						return User::toUsername($data->update_by);
					}
					return "";
				},
			],
			[
				'attribute'=>'record_date',
				'value'=>date('d M Y, h:iA', strtotime($model->record_date)),
			],
			[
				'attribute'=>'update_date',
				'value'=>date('d M Y, h:iA', strtotime($model->update_date)),
			],
			[
				'attribute'=>'date_stamp',
				'value'=>date('d M Y, h:iA', strtotime($model->date_stamp)),
			],	
        ],
    ]) ?>

</div>
