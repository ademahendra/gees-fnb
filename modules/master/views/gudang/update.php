<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Gudang */
?>
<div class="gudang-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
