<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\models\Gudang;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model backend\models\ObatSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<hr />

<div class="card bg-white">
    <div class="card-header bg-success text-white">
        <div class="pull-left"><i class="fa fa-search"></i> Advanced Search</div>
<!--        <div class="card-controls">-->
<!--            <a href="javascript:;" class="card-collapse" data-toggle="card-collapse">-->
<!--                <i class="card-icon-collapse"></i>-->
<!--            </a>-->
<!--        </div>-->
    </div>
    <div class="card-block">
        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>

        <div class="row">
            <div class="col-md-2">
                <label>Nama Gudang</label>
                <?= Html::activeTextInput($model,'nama', ['class' => 'form-control input-sm']); ?>
            </div>
            <div class="col-md-3">
                <label>Alamat</label>
                <?= Html::activeTextInput($model,'alamat', ['class' => 'form-control input-sm']); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary btn-sm m-t']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-danger btn-sm m-t']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>