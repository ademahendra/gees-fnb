<?php

use app\models\Barang;
use app\models\PurchaseOrder;
use app\models\Supplier;
use kartik\money\MaskMoney;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

// use yii\helpers\ArrayHelper;
//use kartik\growl\Growl;

/* @var $this yii\web\View */
/* @var $model app\models\Pembelian */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Update Purchase Order');
$lastID = PurchaseOrder::find()->orderBy('id desc')->one();
$noOrder = 'PO-'.sprintf("%06s", 1);
if(count($lastID) > 0){
    $noOrder = 'PO-'.sprintf("%06s", ($lastID->id+1) );
}
?>

<?php
//	if(Yii::$app->session->getFlash('status') == 'true'){
//		echo Growl::widget([
//			'type' => Growl::TYPE_SUCCESS,
//			'icon' => 'glyphicon glyphicon-ok-sign',
//			'title' => 'Notifications',
//			'showSeparator' => true,
//			'body' => 'Data Purchase Order berhasil Ditambahkan',
//			'pluginOptions' => [
//				'showProgressbar' => false,
//				'placement' => [
//					'from' => 'bottom',
//					'align' => 'right',
//				]
//			]
//		]);
//	} else if(Yii::$app->session->getFlash('status') == 'false'){
//		echo Growl::widget([
//			'type' => Growl::TYPE_DANGER,
//			'icon' => 'glyphicon glyphicon-ok-sign',
//			'title' => 'Notifications',
//			'showSeparator' => true,
//			'body' => 'Data Purchase Order gagal Ditambahkan',
//			'pluginOptions' => [
//				'showProgressbar' => false,
//				'placement' => [
//					'from' => 'bottom',
//					'align' => 'right',
//				]
//			]
//		]);
//	}
$model->isNewRecord ? $model->ppn = '10':$model->ppn = $model->ppn;
?>


    <div class="card bg-white">
        <div class="card-block">
            <div class="row">
                <div class="page-title">
                    <div class="title"><?= $this->title ?></div>
                    <!--<div class="sub-title">Page Description</div>-->
                </div>
                <?php $form = ActiveForm::begin([
                    'id' => 'paymentForm'
                ]); ?>
                <div class="col-sm-12 ">
                    <div class="widget">
                        <div class="widget-header transparent">
                            <div class="additional-btn">
                                <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                                <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
                            </div>
                        </div>
                        <div class="widget-content padding">
                            <div class="col-md-12">
                                <div class="row">
                                    <?= $form->field($model, 'id')->hiddenInput(['maxlength' => true, 'value' => $model->id, 'readonly' => true, 'id' => 'idPO']) ?>
                                    <div class="col-md-3">
                                        <?= $form->field($model, 'no_po')->textInput(['maxlength' => true, 'value' => $model->no_po, 'readonly' => true, 'id' => 'noPO']) ?>
                                    </div>
                                    <div class="col-md-2">
                                        <?= $form->field($model, 'tanggal')->textInput(['maxlength' => true, 'class' => 'form-control datepicker-input', 'data-provide'=>'datepicker','data-format' => 'yyyy-mm-dd', 'value' => date('Y-m-d')]) ?>
                                    </div>
                                    <div class="col-md-3">
                                        <?php
                                        $data = yii\helpers\ArrayHelper::map(Supplier::find()->orderBy('nama ASC')->all(),'id','nama');
                                        echo $form->field($model, 'id_supplier')->widget(Select2::classname(), [
                                            'data' => $data,
                                            'theme' => Select2::THEME_BOOTSTRAP,
                                            'options' => ['placeholder' => '--Pilih Supplier--','id' => 'supplierList'],
                                            'value' => $model->id_supplier,
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ])->label('Supplier');
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-3" id="barangWrapper">
                                        <?php
                                        $data = yii\helpers\ArrayHelper::map(Barang::find()->orderBy('nama ASC')->all(),'id','nama');
                                        echo $form->field($formDetail, 'id_barang')->widget(Select2::classname(), [
                                            'data' => $data,
                                            'options' => ['placeholder' => '--Pilih Nama Barang--','id' => 'barangList'],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ])->label('Barang');
                                        ?>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label">Satuan</label>
                                            <br />
                                            <label class="control-label" id="satuanBarang"></label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label">&nbsp;</label>
                                            <br />
                                            <?= Html::a('<i class="fa fa-plus"></i> Barang Baru', ['#'], ['class' => 'btn btn-success' , 'id' => 'newItem']) ?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-1">
                                    <?= $form->field($formDetail, 'jumlah')->textInput(['id' => 'qty']) ?>
                                </div>
                                <div class="col-md-2">
                                    <?= $form->field($formDetail, 'harga_beli')->widget(MaskMoney::classname(),
                                        ['options' =>
                                            [
                                                'id' => 'hargaBeli',
                                            ]
                                        ])->label('Harga (/Unit)') ?>
                                </div>

                                <div class="col-md-1">
                                    <?= $form->field($formDetail, 'diskon_persen')->textInput(['id' => 'diskon_persen','value' => '0'])->label('Disk (%)') ?>
                                </div>
                                <div class="col-md-2">
                                    <?= $form->field($formDetail, 'diskon')->widget(MaskMoney::classname(),
                                        ['options' =>['id' => 'diskon','value' => '0']])->label('Diskon') ?>
                                </div>
                                <div class="col-md-1">
                                    <?= $form->field($formDetail, 'ppn')->textInput(['id' => 'ppn','value' => '10'])->label('PPN (%)') ?>
                                </div>
                                <div class="col-md-2">
                                    <?= $form->field($formDetail, 'harga_kena_pajak')->widget(MaskMoney::classname(),
                                        ['options' =>['id' => 'harga_kena_pajak','value' => '0'],'readonly' => true]) ?>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <?= Html::a('Tambah', ['#'], ['class' => 'btn btn-warning' , 'id' => 'addbutton']) ?>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>
                                                Nama Barang
                                            </th>
                                            <th>
                                                Jumlah
                                            </th>
                                            <th>
                                                Harga (/Unit)
                                            </th>

                                            <th>
                                                Diskon
                                            </th>
                                            <th>
                                                PPN
                                            </th>
                                            <th>
                                                Harga (/Unit) + PPN
                                            </th>
                                            <th>
                                                Total Harga + PPN
                                            </th>
                                            <th class="col-md-1">

                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbody">
                                            <?php
                                            $tr = '';
                                            $totalHarga = 0;
                                            foreach ($detail as $row){
                                                $totalHargaItem = ($row->jumlah * $row-> harga_kena_pajak);
                                                $totalHarga += $totalHargaItem;
                                                $tr = "<tr id=\"" . $row->id_barang . "\" val=\"" . $row->barang->nama . "\">						
                                                    <td>
                                                        " . $row->barang->nama . "
                                                        <input class =\"barang\" name=\"barang[]\" value=\"" . $row->barang->nama . "\" type=\"hidden\" readonly=\"true\" />
                                                        <input class =\"id\" name=\"id[]\" value=\"" . $row->id_barang . "\" type=\"hidden\" readonly=\"true\" />
                                                        <input class =\"id_gudang\" name=\"id_gudang[]\" value=\"" . $model->id_gudang . "\" type=\"hidden\" readonly=\"true\" />
                                                        <input class =\"qty\" name=\"qty[]\" value=\"" . $row->jumlah . "\" type=\"hidden\" readonly=\"true\" />
                                                        <input class =\"hb\" name=\"hb[]\" value=\"" . $row->harga_beli . "\" type=\"hidden\" readonly=\"true\" />
                                                        <input class =\"diskon\" name=\"diskon[]\" value=\"" . $row->diskon . "\" type=\"hidden\" readonly=\"true\" />
                                                        <input class =\"diskon_persen\" name=\"diskon_persen[]\" value=\"" . $row->diskon_persen . "\" type=\"hidden\" readonly=\"true\" />
                                                        <input class =\"hkp\" name=\"hkp[]\" value=\"" . $row->harga_kena_pajak . "\" type=\"hidden\" readonly=\"true\" />
                                                        <input class =\"ppn\" name=\"ppn[]\" value=\"" . (($row->harga_beli - $row->diskon) * $row->ppn / 100) . "\" type=\"hidden\" readonly=\"true\" />
                                                        <input class =\"totalhj\" name=\"totalhj[]\" value=\"" .($row->jumlah * $row-> harga_kena_pajak). "\" type=\"hidden\" readonly=\"true\" />
                                                    </td>
                                                    <td>
                                                        <input type='text' value='".($row->jumlah)."' class='form-control qtyedit ' />
                                                    </td>
                                                    <td>
                                                        <input type='text' value='".($row->harga_beli)."' class='form-control hargaedit' />
                                                    </td>
                                                    <td>
                                                        <input type='text' value='".($row->diskon)."' class='form-control diskonedit' />
                                                    </td>
                                                    <td>
                                                        <input type='text' value='".($row->ppn)."' class='form-control ppnedit' />
                                                    </td>
                                                    <td>
                                                        <input type='text' readonly='true' value='".($row->harga_kena_pajak)."' class='form-control hkpedit' id='hkpedit' />
                                                    </td>
                                                    <td>
                                                        <input type='text' readonly='true' value='" . ($row->jumlah * $row->harga_kena_pajak) ."' class='form-control hppedit' id='hppedit' />
                                                        <input type='hidden' value='" . ($row->jumlah * $row->harga_kena_pajak) ."' class='form-control hppedit2' id='hppedit2''/>
                                                    </td>
                                                    <td>
                                                        <a class=\"remove\" style='cursor:pointer;'><i class=\"fa fa-minus\"></i></a>
                                                    </td>
                                                </tr>";
                                                echo $tr;
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <!--
				<div class="pull-left">
					<div class="col-md-5">
						<?= $form->field($model, 'diskon_persen')->textInput(['id' => 'diskonGlobalPercent', 'class' => 'form-control', 'value' => '0']) ?>
					</div>
					<div class="col-md-6">
						<?= $form->field($model, 'diskon')->textInput(['id' => 'diskonGlobal', 'class' => 'form-control', 'value' => '0']) ?>
					</div>
				</div>-->
                                <div class="pull-right">
                                    <!--
                                    <div class="col-md-6">
                                        <label class="control-label">Total Harga</label>
                                        <?= Html::textInput('totalharga','0',['id' => 'hargaTotalFormat', 'class' => 'form-control','readonly' => true, 'value' => '0']) ?>
                                        <?= $form->field($model, 'harga_beli')->hiddenInput(['id' => 'hargaTotal', 'class' => 'form-control' ,'readonly' => true, 'value' => '0'])->label(''); ?>
                                    </div>-->
                                    <div class="col-md-6">
                                        <label class="control-label">Total Harga + Diskon</label>
                                        <?= Html::textInput('totalhargaPajak',$totalHarga,['id' => 'hargaTotalPajakFormat', 'class' => 'form-control','readonly' => true, 'value' => '0', 'id'=>'totalhargaPajak']) ?>
                                        <?= $form->field($model, 'harga_kena_pajak')->hiddenInput(['id' => 'hargaTotalPajak', 'class' => 'form-control','readonly' => true, 'value' => '0'])->label(''); ?>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="pull-left">
                                        <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Perbaharui', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','id' => 'save']) ?>
                                    </div>
                                    <div class="pull-right">
                                        <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-danger']) ?>
                                        <br />
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="iframeplaceholder"  style="display:none;"></div>
<?php

$this->registerCssFile(Yii::$app->getUrlManager()->getBaseUrl()."/web/theme/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
$this->registerJsFile(Yii::$app->getUrlManager()->getBaseUrl()."/web/theme/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js");


$script = <<< JS
    var editpurchase = [];

	$('body').on('keyup', '#diskon_persen', function(e){
		e.preventDefault();
		var curr = $(this);
		calculateDiscount();
	});
	
	$('body').on('keyup', '#diskon', function(e){
		e.preventDefault();
		var curr = $(this);
		calculateDiscount(false);
	});
	
	$('body').on('keyup', '#diskonGlobalPercent', function(e){
		e.preventDefault();
		var curr = $(this);
		calculateDiscountGlobal();
	});
	
	$('body').on('keyup', '#diskonGlobal', function(e){
		e.preventDefault();
		var curr = $(this);
		calculateDiscountGlobal(false);
	});
	
	
	var newBarangDialog;
	$('body').on('click', '#newItem', function (e) {
		e.preventDefault();
		newBarangDialog = BootstrapDialog.show({
            message: function(dialog) {
                var message = $('<div></div>');
                var pageToLoad = dialog.getData('pageToLoad');
                message.load(pageToLoad);
                return message;
            },
			title:'New Item',
            data: {
                'pageToLoad': baseUrl+'master/barang/new'
            },
			size:BootstrapDialog.SIZE_WIDE
        });
	});

	$('body').on('change', 'select#barangList', function () {
		var barangList = $('select#barangList').val();
		$.get(baseUrl + 'rest/detailbarang?id=' + barangList+'&gudang=0', function (jsonData) {
			var json = jQuery.parseJSON(jsonData);
			if (json.status) {
				$('#satuanBarang').html(json.satuan);
			}
		});
	});
	
	function calculateDiscount(percent = true){
		var discPercent = $('#diskon_persen').val();
		var discount = $('#diskon').val();
		var hpp = $('#hargaBeli').val();
		var ppn =  parseInt($('#ppn').val());
		
		if((discount !== '' || discPercent !== '') && hpp !== ''){
			if(discPercent !== ''){
				if(percent){
					var discRate = (discPercent/100) * hpp;
					$('#diskon').val(parseInt(discRate).toFixed(0));
					$('#diskon-disp').val(parseInt(discRate).toFixed(2));
				}
			} 
			
			if(discount !== ''){
				if(percent == false){
					var discPerc = discount/hpp * 100;
					// console.log('I am here: '+discPerc);
					$('#diskon_persen').val(parseInt(discPerc).toFixed(0));
				}
			}
				
		}
		discountRate = $('#diskon').val();
		if(hpp !== '' && ppn !== ''){
			if(ppn == 10){
				var hsp = ((parseInt(hpp) - parseInt(discountRate)) + ((parseInt(hpp) - parseInt(discountRate)) * ppn / 100)).toFixed(0);
			} else {
				var hsp = (parseInt(hpp - parseInt(discountRate))).toFixed(0);
			}
			$('#harga_kena_pajak').val(hsp);
			$('#harga_kena_pajak-disp').val(parseInt(hsp).toFixed(2));
		}
	}
	
	
	function calculateDiscountGlobal(percent = true){
		var discPercent = $('#diskonGlobalPercent').val();
		var discount = $('#diskonGlobal').val();
		var hpp = $('#hargaTotal').val();
		var ppn =  parseInt($('#ppn').val());;
		console.log(discount+' '+discPercent+' '+hpp+' '+ppn);
		if((discount !== '' || discPercent !== '') && hpp !== ''){
			if(discPercent !== ''){
				if(percent){
					var discRate = (discPercent/100) * hpp;
					$('#diskonGlobal').val(parseInt(discRate).toFixed(0));
					//update harga setelah pajak
				}
			} 
			
			if(discount !== ''){
				if(percent == false){
					var discPerc = discount/hpp * 100;
					// console.log('I am here GLOBAL : '+discPerc);
					$('#diskonGlobalPercent').val(parseInt(discPerc).toFixed(0));
					//update harga setelah pajak
				}
			}	
		}
		discountRate = $('#diskonGlobal').val();
		// console.log(hpp+' ---- '+ppn);
		if(hpp !== '' && ppn !== ''){
			if(ppn == 10){
				var hsp = ((parseInt(hpp) - parseInt(discountRate)) + ((parseInt(hpp) - parseInt(discountRate)) * ppn / 100)).toFixed(0);
				// console.log('HSP '+hsp);
			} else {
				var hsp = (parseInt(hpp - parseInt(discountRate))).toFixed(0);
				// console.log('HP '+hsp);
			}
			$('#hargaTotalPajak').val(hsp);
			$('#hargaTotalFormat').val(parseFloat($('#hargaTotal').val()).formatMoney(2, '.', ','));
			$('#hargaTotalPajakFormat').val(parseFloat($('#hargaTotalPajak').val()).formatMoney(2, '.', ',')) ;
			
			
		}
	}
	
	$('body').on('change', '#ppn', function(){
		var hkp = $('#harga_kena_pajak').val();
		var hpp = $('#hargaBeli').val();
		var ppn =  $('#ppn').val();
		if(ppn !== '0' && ppn !== '10'){
			alert('PPN harus berisi 10 atau 0!');
		}
		calculateDiscount();			
	});
	
	$("body").on('click','.remove', function(e){
			$(this).closest('tr').remove();
			var removeItem = $(this).closest('tr').attr('id');		
			var grandTotal = 0;
			$(":input.hppedit").each(function(i, val) {            
				if($(this).val() !== '' && $(this).val() != 0){
					grandTotal += parseFloat($(this).val());
				}
			});
			$('#totalhargaPajak').val(grandTotal);
			delete editpurchase[removeItem];		
		});
	
	
	$('body').on('keyup', '#hargaBeli', function(){
		calculateDiscount();
	});	
	
	$("body").on('click','#saveButton', function(e){
		e.preventDefault();
		// console.log('Click Oke');

		// return;
		$.ajax({
			type: "POST",
			url: baseUrl+'barang/new',
			dataType: 'json',
			cache: false,
			data: $('#barangForm').serialize(),
			success: function (json) {
				if(json.status){
					bootbox.alert("Barang berhasil ditambahkan!");
					newBarangDialog.close();
					//reload select 2 and close popup
					$('#barangWrapper').html(json.selection);

					var select2setting = {"allowClear":true,"theme":"krajee","width":"100%","placeholder":"--Pilih Nama Barang--","language":"en-US"};
					$('#barangList').select2(select2setting);
					$('.kv-plugin-loading').hide();
					$('#barangList').val(json.newid).trigger('change');
				} else {
					bootbox.alert('Penambahan Barang Gagal');
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log(xhr.status);
				console.log(xhr.responseText);
				console.log(thrownError);
			}
		});
	});
	
	
	$('body').on('change', '#ppn', function(){
		var hkp = $('#harga_kena_pajak').val();
		var hpp = $('#hargaBeli').val();
		var ppn =  $('#ppn').val();
		if(ppn !== '0' && ppn !== '10'){
			alert('PPN harus berisi 10 atau 0!');
		}
		calculateDiscount();			
	});
	
	
	$("#addbutton").on('click', function(e){
		e.preventDefault();
		var idBarang = $('#barangList').val();
		var qtyOrder = $('#qty').val();
		if(idBarang != '' && qtyOrder != '' && qtyOrder != '0'){
			var form = $('#paymentForm').serialize();
			$.ajax({
				type: "POST",
				url: baseUrl+'purchasing/default/editpurchase',
				dataType: 'json',
				cache: false,
				data: form,
				success: function (json) {
					if(json.status){	
						$("#tbody").append(json.tr);
						//$("#addbutton").attr('disabled',true);
						$("#gudangList").attr('readonly',true);
						$("#supplierList").attr('readonly',true);
						$('#pembelian-batch_no').val('');
						$('#pembelian-expired_date').val('');
						$('#harga_jual_1').val('');
						$('#harga_jual_2').val('');
						$('#harga_jual_3').val('');
						$('#harga_jual_4').val('');
						$('#pembelian-jumlah').val('');
						// $('#pembelian-no_faktur').attr('readonly',true); 
						diskon = parseInt($('#diskon').val());
						hargaTotal = parseInt($('#hargaTotal').val());
						hargaTotalPajak = parseInt($('#hargaTotalPajak').val());
						hargabeli = parseInt($('#hargaBeli').val());
						hargapajak = parseInt($('#harga_kena_pajak').val());
						qty = parseInt($('#qty').val());
						console.log(hargaTotal+' ==== '+hargaTotalPajak);
						hargaDiskon = hargabeli - diskon;
						$('#hargaTotal').val(hargaDiskon * qty);
						$('#hargaTotalPajak').val(hargaTotalPajak + (hargapajak * qty));
						
						// $('#hargaTotalFormat').val(parseFloat($('#hargaTotal').val()).formatMoney(2, '.', ','));
						//$('#hargaTotalPajakFormat').val(parseFloat($('#hargaTotalPajak').val()).formatMoney(2, '.', ',')) ;
						
						$('#hargaTotalFormat').val(formatCurrency(parseFloat($('#hargaTotal').val())));
						$('#hargaTotalPajakFormat').val(formatCurrency(parseFloat($('#hargaTotalPajak').val())));
						
						var total = json.totalJual;
					    editpurchase[idBarang] = total;
						
						// $('#hargaBeli').val('');
						// $('#harga_kena_pajak').val('');
						// $('#diskon').val('0');
						// $('#diskon_persen').val('0');
						$('#qty').val('0');
					} else {
						
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log(xhr.status);
					console.log(xhr.responseText);
					console.log(thrownError);
				}
			});
		}
	});	
	
    function loadiFrame(src)
    {
        $("#iframeplaceholder").html("<iframe id='myiframe' name='myname' src='" + src + "' />");
    }
	
	$("#save").on('click', function(e){
		e.preventDefault();
		var form = $('#paymentForm').serialize();
		var idPO = $('#idPO').val();
		$.ajax({
			type: "POST",
			url: baseUrl+'purchasing/default/update?id=' + idPO,
			dataType: 'json',
			cache: false,
			data: form,
			success: function (json) {
				if(json.status){	
					$( '#paymentForm' ).each(function(){
						this.reset();
					});
						
					addpurchasing = [];	
					$('#tbody').html('');
					$('#noPO').val(json.nobaru);
				} else {
				    if(json && typeof json.msg != 'undefined') {
                        bootbox.alert(json.msg);
                    }
					
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log(xhr.status);
				//console.log(xhr.responseText);
				console.log(thrownError);
			}
		});
	});	
	
$('body').on('keyup','.qtyedit', function(e){
	editDetailItem('qty', $(this));
});

$('body').on('keyup','.hargaedit', function(e){
	editDetailItem('harga', $(this));
});

$('body').on('keyup','.diskonedit', function(e){
	editDetailItem('diskon', $(this));
});

$('body').on('keyup','.ppnedit', function(e){
	editDetailItem('ppn', $(this));
});

function editDetailItem(sender, thisItem){
    var curr = thisItem;
	if(sender === 'qty'){
		var harga = curr.parent('td').parent('tr').find('.hargaedit');
		var qty = thisItem;
		var diskon = curr.parent('td').parent('tr').find('.diskonedit');
	}
	if(sender === 'harga'){
		var harga = thisItem;
		var qty = curr.parent('td').parent('tr').find('.qtyedit');
		var diskon = curr.parent('td').parent('tr').find('.diskonedit');
	}
	if(sender === 'diskon'){
	    var diskon = thisItem;
	    var qty = curr.parent('td').parent('tr').find('.qtyedit');
	    var harga = curr.parent('td').parent('tr').find('.hargaedit');
	}


	var ppnedit = curr.parent('td').parent('tr').find('.ppnedit');
	var hargappnedit = curr.parent('td').parent('tr').find('.hkpedit');
	var hargatotaledit = curr.parent('td').parent('tr').find('.hppedit');
	var hargatotaleditHidden = curr.parent('td').parent('tr').find('.hppedit2');

	var qtyHidden = curr.parent('td').parent('tr').find('.qty');
	var HBhidden = curr.parent('td').parent('tr').find('.hb');
	var diskonHidden = curr.parent('td').parent('tr').find('.diskon');
	
	var hbPPNhidden = curr.parent('td').parent('tr').find('.totalHj');
	if(qty.val() !== '' && qty.val() != '0' && harga.val() !== '' && harga.val() != '0'){
		console.log(qty.val()+' '+harga.val());
		var hargaPcs = parseFloat(harga.val());
		var ppn = (10 * hargaPcs)/100;
		var hbppn = hargaPcs+ppn;
		var hb =  parseFloat(qty.val())*hbppn;
		ppnedit.val(parseFloat(ppn));
		hargappnedit.val(parseFloat(hargaPcs + ppn));
		hargatotaledit.val(parseFloat(hb));
		hargatotaleditHidden.val(parseFloat(hb));

		qtyHidden.val(qty.val());
		HBhidden.val(parseFloat(hargaPcs));
		diskonHidden.val(diskon.val());
		
		hbPPNhidden.val(parseFloat(hb));
		
		var discHidden = $('.diskonedit').val();
		console.log("discHidden ---> " + discHidden);
		
		var discRate = (discHidden/100) * harga.val();
		var jumlah = $('.qtyedit').val();
		var hargaBeli = $('.hb').val();
		var ppn = $('.ppnedit').val();
		
		console.log("harga ---> " + harga.val());
		console.log("hargaBeli ---> " + hargaBeli);
		console.log("jumlahedit ---> " + jumlah);
		
		$('#hkpedit').val((parseInt(hargaBeli)+parseInt(ppn)).toFixed(0));
        //$('#hkpedit-disp').val((parseInt(hargaBeli)+parseInt(ppn)).toFixed(0));
		
		if (diskon.val() !=='' && diskon.val() != '0'){
		     $('#hppedit').val(((parseInt(hargaBeli) * parseInt(jumlah))- (parseInt(discRate) + (parseInt(ppn)))).toFixed(0));
             $('#hppedit-disp').val(((parseInt(hargaBeli) * parseInt(jumlah))- (parseInt(discRate)+(parseInt(ppn)))) .toFixed(2));
 
		}
		else{
		     $('#hppedit').val(((parseInt(hargaBeli) * parseInt(jumlah)) + (parseInt(ppn))).toFixed(0));
             $('#hppedit-disp').val(((parseInt(hargaBeli) * parseInt(jumlah)) + (parseInt(ppn))) .toFixed(2));
		}
		
		//var grandTotal = parseInt($('#totalhargaPajak').val());
		var grandTotal = 0
		grandTotal += parseInt($('#hppedit').val());
		
		$('#totalhargaPajak').val(grandTotal)
		
	}
	
    
}
JS;
$this->registerJs($script, \yii\web\View::POS_READY);
?>