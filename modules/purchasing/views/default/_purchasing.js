var body = $("body");
var onXHR = false;
var level1name, level2name, level3name, level4name,
    level1satuan, level2satuan, level3satuan, level4satuan,
    level1value, level2value, level3value, level4value;
var newBarangDialog;

body.on('click', '#newItem', function (e) {
    e.preventDefault();
    newBarangDialog = BootstrapDialog.show({
        message: function (dialog) {
            var message = $('<div></div>');
            var pageToLoad = dialog.getData('pageToLoad');
            message.load(pageToLoad);
            return message;
        },
        title: 'New Item',
        data: {
            'pageToLoad': baseUrl + 'master/barang/new'
        },
        size: BootstrapDialog.SIZE_WIDE,
        type: BootstrapDialog.TYPE_DEFAULT
    });
});

body.on('change', 'select#barangList', function () {
    var barangList = $('select#barangList').val();
    $.get(baseUrl + 'purchasing/default/detailbarang?id=' + barangList, function (jsonData) {
        var json = jQuery.parseJSON(jsonData);
        if (json.status) {
            $('#purchaseUnit').html(json.satuan);
        }
    });
});

body.on('click', '.remove', function (e) {
    e.preventDefault();
    $(this).closest('tr').remove();
    var removeItem = $(this).closest('tr').attr('id');

});

function calculateQTY() {
    if ($('#qty').val() != '') {
        var qty = $('#qty').val();
        var satuan = $('#satuanList').val();
        var selected = $('#satuanList').find(":selected").data("option");
        // console.log(qty + ' ' + satuan + ' ' + selected);
        var level1convert = '';
        var level2convert = '';
        var level3convert = '';
        var level4convert = '';
        switch (true) {
            case (satuan == level1satuan):
                level2convert = parseFloat((qty * level1value)) + ' ' + level2name + ', ';
                level3convert = parseFloat((qty * level1value * level2value)) + ' ' + level3name + ', ';
                level4convert = parseFloat((qty * level1value * level2value * level3value)) + ' ' + level4name;

                break;
            case (satuan == level2satuan):
                level1convert = parseFloat(level2value / (qty * level1value)).toFixed(2) + ' ' + level1name + ', ';
                level3convert = parseFloat((qty * level2value)) + ' ' + level3name + ', ';
                level4convert = parseFloat((qty * level2value * level3value)) + ' ' + level4name;
                break;
            case (satuan == level3satuan):

                break;
            case (satuan == level4satuan):

                break;
            default:
                console.log('None on the above!');
                break;
        }
        $('#detailQTY').html(level1convert + ' ' + level2convert + ' ' + level3convert + ' ' + level4convert);
    }
}

// body.on('change', '#satuanList', function () {
//     calculateQTY();
// });
//
// body.on('keyup', '#qty', function () {
//     calculateQTY();
// });


body.on('click', '#saveButton', function (e) {
    e.preventDefault();
    $.ajax({
        type: "POST",
        url: baseUrl + 'master/barang/new',
        dataType: 'json',
        cache: false,
        data: $('#barangForm').serialize(),
        success: function (json) {
            if (json.status) {
                bootbox.alert("Barang berhasil ditambahkan!");
                newBarangDialog.close();
                //reload select 2 and close popup
                $('#barangWrapper').html(json.selection);

                var select2setting = {
                    "allowClear": true,
                    "theme": "krajee",
                    "width": "100%",
                    "placeholder": "--Pilih Nama Barang--",
                    "language": "en-US"
                };
                $('#barangList').select2(select2setting);
                $('.kv-plugin-loading').hide();
                $('#barangList').val(json.newid).trigger('change');
            } else {
                bootbox.alert('Penambahan Barang Gagal');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
});

$("#addbutton").on('click', function (e) {
    e.preventDefault();
    var idBarang = $('#barangList').val();
    var qtyOrder = $('#qty').val();
    if (idBarang != '' && qtyOrder != '' && qtyOrder != '0') {
        var form = $('#paymentForm').serialize();
        $.ajax({
            type: "POST",
            url: baseUrl + 'purchasing/default/additem',
            dataType: 'json',
            cache: false,
            data: form,
            success: function (json) {
                if (json.status) {
                    $("#tbody").append(json.tr);
                    //$("#addbutton").attr('disabled',true);
                    // $("#gudangList").attr('readonly', true);
                    // $("#supplierList").attr('readonly', true);
                    // $('#pembelian-no_faktur').attr('readonly',true); 
                    $('#qty').val('0');
                    $('#barangList').focus();
                } else {

                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }
});

function loadiFrame(src) {
    $("#iframeplaceholder").html("<iframe id='myiframe' name='myname' src='" + src + "' />");
}

$("#save").on('click', function (e) {
    e.preventDefault();
    if (!onXHR) {
        onXHR = true;
        $.ajax({
            type: "POST",
            url: baseUrl + 'purchasing/default/save',
            dataType: 'json',
            cache: false,
            data: $('#paymentForm').serialize(),
            success: function (json) {
                if (json.status) {
                    onXHR = false;
                    $('#paymentForm').each(function () {
                        this.reset();
                    });

                    loadiFrame(baseUrl + 'purchasing/default/printreceipt?id=' + json.nopo);
                    $("#myiframe").load(
                        function () {
                            window.frames['myname'].focus();
                            window.frames['myname'].print();
                        }
                    );
                    addpenjualan = [];
                    $('#tbody').html('');
                    $('#noPO').val(json.nobaru);
                    bootbox.alert({
                        size: "small",
                        title: "Success",
                        message: "Purchase Order Success to Save!",
                        callback: function () {
                            location.reload();
                        }
                    });


                } else {
                    onXHR = false;
                    if (json && typeof json.msg != 'undefined') {
                        bootbox.alert(json.msg);
                    }

                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                onXHR = false;
                console.log(xhr.status);
                //console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }
});