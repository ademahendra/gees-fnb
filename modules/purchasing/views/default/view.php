<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PurchaseOrder */
?>
<div class="purchase-order-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'id_gudang',
            'no_faktur',
            'no_surat_jalan',
            'no_invoice',
            'tanggal_faktur',
            'id_supplier',
            'diskon_persen',
            'diskon',
            'harga_beli',
            'ppn',
            'harga_kena_pajak',
            'harga_jual',
            'harga_jual_2',
            'harga_jual_3',
            'harga_jual_4',
            'jatuh_tempo',
            'tanggal',
            'admin_id',
        ],
    ]) ?>

</div>
