<?php

use app\models\Barang;
use app\models\PurchaseOrder;
use app\models\Supplier;
use kartik\money\MaskMoney;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

// use yii\helpers\ArrayHelper;
//use kartik\growl\Growl;

/* @var $this yii\web\View */
/* @var $model app\models\Pembelian */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Update Purchase Order');
$lastID = PurchaseOrder::find()->orderBy('id desc')->one();
$noOrder = 'PO-'.sprintf("%06s", 1);
if(count($lastID) > 0){
    $noOrder = 'PO-'.sprintf("%06s", ($lastID->id+1) );
}
?>

<?php
//	if(Yii::$app->session->getFlash('status') == 'true'){
//		echo Growl::widget([
//			'type' => Growl::TYPE_SUCCESS,
//			'icon' => 'glyphicon glyphicon-ok-sign',
//			'title' => 'Notifications',
//			'showSeparator' => true,
//			'body' => 'Data Purchase Order berhasil Ditambahkan',
//			'pluginOptions' => [
//				'showProgressbar' => false,
//				'placement' => [
//					'from' => 'bottom',
//					'align' => 'right',
//				]
//			]
//		]);
//	} else if(Yii::$app->session->getFlash('status') == 'false'){
//		echo Growl::widget([
//			'type' => Growl::TYPE_DANGER,
//			'icon' => 'glyphicon glyphicon-ok-sign',
//			'title' => 'Notifications',
//			'showSeparator' => true,
//			'body' => 'Data Purchase Order gagal Ditambahkan',
//			'pluginOptions' => [
//				'showProgressbar' => false,
//				'placement' => [
//					'from' => 'bottom',
//					'align' => 'right',
//				]
//			]
//		]);
//	}
$model->isNewRecord ? $model->ppn = '10':$model->ppn = $model->ppn;
?>


    <div class="card bg-white">
        <div class="card-block">
            <div class="row">
                <div class="page-title">
                    <div class="title"><?= $this->title ?></div>
                    <!--<div class="sub-title">Page Description</div>-->
                </div>
                <?php $form = ActiveForm::begin([
                    'id' => 'paymentForm'
                ]); ?>
                <div class="col-sm-12 ">
                    <div class="widget">
                        <div class="widget-header transparent">
                            <div class="additional-btn">
                                <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                                <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
                            </div>
                        </div>
                        <div class="widget-content padding">
                            <div class="col-md-12">
                                <div class="row">
                                    <?= $form->field($model, 'id')->hiddenInput(['maxlength' => true, 'value' => $model->id, 'readonly' => true, 'id' => 'idPO']) ?>
                                    <div class="col-md-3">
                                        <?= $form->field($model, 'no_po')->textInput(['maxlength' => true, 'value' => $model->no_po, 'readonly' => true, 'id' => 'noPO']) ?>
                                    </div>
                                    <div class="col-md-2">
                                        <?= $form->field($model, 'tanggal')->textInput(['maxlength' => true, 'class' => 'form-control datepicker-input', 'data-provide'=>'datepicker','data-format' => 'yyyy-mm-dd', 'value' => date('Y-m-d')]) ?>
                                    </div>
                                    <div class="col-md-3">
                                        <?php
                                        $data = yii\helpers\ArrayHelper::map(Supplier::find()->orderBy('nama ASC')->all(),'id','nama');
                                        echo $form->field($model, 'id_supplier')->widget(Select2::classname(), [
                                            'data' => $data,
                                            'theme' => Select2::THEME_BOOTSTRAP,
                                            'options' => ['placeholder' => '--Pilih Supplier--','id' => 'supplierList'],
                                            'value' => $model->id_supplier,
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ])->label('Supplier');
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-3" id="barangWrapper">
                                        <?php
                                        $data = yii\helpers\ArrayHelper::map(Barang::find()->orderBy('nama ASC')->all(),'id','nama');
                                        echo $form->field($formDetail, 'id_barang')->widget(Select2::classname(), [
                                            'data' => $data,
                                            'options' => ['placeholder' => '--Pilih Nama Barang--','id' => 'barangList'],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ])->label('Barang');
                                        ?>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label">Satuan</label>
                                            <br />
                                            <label class="control-label" id="satuanBarang"></label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label">&nbsp;</label>
                                            <br />
                                            <?= Html::a('<i class="fa fa-plus"></i> Barang Baru', ['#'], ['class' => 'btn btn-success' , 'id' => 'newItem']) ?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-1">
                                    <?= $form->field($formDetail, 'jumlah')->textInput(['id' => 'qty']) ?>
                                </div>
                                <div class="col-md-2">
                                    <?= $form->field($formDetail, 'harga_beli')->widget(MaskMoney::classname(),
                                        ['options' =>
                                            [
                                                'id' => 'hargaBeli',
                                            ]
                                        ])->label('Harga (/Unit)') ?>
                                </div>

                                <div class="col-md-1">
                                    <?= $form->field($formDetail, 'diskon_persen')->textInput(['id' => 'diskon_persen','value' => '0'])->label('Disk (%)') ?>
                                </div>
                                <div class="col-md-2">
                                    <?= $form->field($formDetail, 'diskon')->widget(MaskMoney::classname(),
                                        ['options' =>['id' => 'diskon','value' => '0']])->label('Diskon') ?>
                                </div>
                                <div class="col-md-1">
                                    <?= $form->field($formDetail, 'ppn')->textInput(['id' => 'ppn','value' => '10'])->label('PPN (%)') ?>
                                </div>
                                <div class="col-md-2">
                                    <?= $form->field($formDetail, 'harga_kena_pajak')->widget(MaskMoney::classname(),
                                        ['options' =>['id' => 'harga_kena_pajak','value' => '0'],'readonly' => true]) ?>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <?= Html::a('Tambah', ['#'], ['class' => 'btn btn-warning' , 'id' => 'addbutton']) ?>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>
                                                Nama Barang
                                            </th>
                                            <th>
                                                Jumlah
                                            </th>
                                            <th>
                                                Harga (/Unit)
                                            </th>

                                            <th>
                                                Diskon
                                            </th>
                                            <th colspan="2">
                                                PPN
                                            </th>
                                            <th>
                                                Harga (/Unit) + PPN
                                            </th>
                                            <th>
                                                Total Harga + PPN
                                            </th>
                                            <th class="col-md-1">

                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbody">
                                            <?php
                                            $tr = '';
                                            $totalHarga = 0;
                                            foreach ($detail as $row){
                                                $totalHargaItem = ($row->jumlah * $row-> harga_kena_pajak);
                                                $totalHarga += $totalHargaItem;
                                                $tr = "<tr id=\"" . $row->id_barang . "\" val=\"" . $row->barang->nama . "\">						
                                                    <td>
                                                        " . $row->barang->nama . "
                                                        <input class =\"barang\" name=\"barang[]\" value=\"" . $row->barang->nama . "\" type=\"hidden\" readonly=\"true\" />
                                                        <input class =\"id\" name=\"id[]\" value=\"" . $row->id_barang . "\" type=\"hidden\" readonly=\"true\" />
                                                        <input class =\"id_gudang\" name=\"id_gudang[]\" value=\"" . $model->id_gudang . "\" type=\"hidden\" readonly=\"true\" />
                                                        <input class =\"qty\" name=\"qty[]\" value=\"" . $row->jumlah . "\" type=\"hidden\" readonly=\"true\" />
                                                        <input class =\"hb\" name=\"hb[]\" value=\"" . $row->harga_beli . "\" type=\"hidden\" readonly=\"true\" />
                                                        <input class =\"diskon\" name=\"diskon[]\" value=\"" . $row->diskon . "\" type=\"hidden\" readonly=\"true\" />
                                                        <input class =\"diskon_persen\" name=\"diskon_persen[]\" value=\"" . $row->diskon_persen . "\" type=\"hidden\" readonly=\"true\" />
                                                        <input class =\"hkp\" name=\"hkp[]\" value=\"" . $row->harga_kena_pajak . "\" type=\"hidden\" readonly=\"true\" />
                                                        <input class =\"ppn\" name=\"ppn[]\" value=\"" . (($row->harga_beli - $row->diskon) * $row->ppn / 100) . "\" type=\"hidden\" readonly=\"true\" />
                                                        <input class=\"hpp\" name=\"totalhj[]\" value=\"" .($row->jumlah * $row-> harga_kena_pajak). "\" type=\"hidden\" readonly=\"true\" />
                                                    </td>
                                                    <td>
														" . Html::activeTextInput($row, 'jumlah[]', ['class' => 'form-control qtyedit input-sm', 'value' => $row->jumlah]) . "
                                                    </td>
                                                    <td>
														" . Html::activeTextInput($row, 'harga_beli[]', ['class' => 'form-control hargaedit input-sm', 'value' => $row->harga_beli]) . "
                                                    </td>
                                                    <td>
														" . Html::activeTextInput($row, 'diskon[]', ['class' => 'form-control diskonedit input-sm', 'value' => $row->diskon]) . "
                                                    </td>
													<td>
													" . Html::checkbox('haveppn[]', ($row->ppn > 0)?true:false, ['class' => 'havePPN']) . "
													</td>
                                                    <td>
														 " . Html::activeTextInput($row, 'ppn[]', ['class' => 'form-control ppnedit input-sm', 'value' => $row->ppn]) . "
                                                    </td>
                                                    <td>
														" . Html::activeTextInput($row, 'harga_kena_pajak[]', ['class' => 'form-control hkpedit input-sm', 'id' => 'hkpedit', 'value' => $row->harga_kena_pajak, 'readonly' => true]) . "
                                                    </td>
                                                    <td>
														" . Html::activeTextInput($row, 'total[]', ['class' => 'form-control hppedit input-sm', 'id' => 'hppedit', 'value' => ($row->jumlah * $row->harga_kena_pajak), 'readonly' => true]) . "
														
														" . Html::activeHiddenInput($row, 'total[]', ['class' => 'form-control hppedit2 input-sm', 'id' => 'hppedit2', 'value' => ($row->jumlah * $row->harga_kena_pajak), 'readonly' => true]) . "
                                                    </td>
                                                    <td>
                                                        <a class=\"remove\" style='cursor:pointer;'><i class=\"fa fa-minus\"></i></a>
                                                    </td>
                                                </tr>";
                                                echo $tr;
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <!--
				<div class="pull-left">
					<div class="col-md-5">
						<?= $form->field($model, 'diskon_persen')->textInput(['id' => 'diskonGlobalPercent', 'class' => 'form-control', 'value' => '0']) ?>
					</div>
					<div class="col-md-6">
						<?= $form->field($model, 'diskon')->textInput(['id' => 'diskonGlobal', 'class' => 'form-control', 'value' => '0']) ?>
					</div>
				</div>-->
                                <div class="pull-right">
                                    <!--
                                    <div class="col-md-6">
                                        <label class="control-label">Total Harga</label>
                                        <?= Html::textInput('totalharga','0',['id' => 'hargaTotalFormat', 'class' => 'form-control','readonly' => true, 'value' => '0']) ?>
                                        <?= $form->field($model, 'harga_beli')->hiddenInput(['id' => 'hargaTotal', 'class' => 'form-control' ,'readonly' => true, 'value' => '0'])->label(''); ?>
                                    </div>-->
                                    <div class="col-md-6">
                                        <label class="control-label">Total Harga + Diskon</label>
                                        <?= Html::textInput('totalhargaPajak',$totalHarga,['id' => 'hargaTotalPajakFormat', 'class' => 'form-control','readonly' => true, 'value' => '0', 'id'=>'totalhargaPajak']) ?>
                                        <?= $form->field($model, 'harga_kena_pajak')->hiddenInput(['id' => 'hargaTotalPajak', 'class' => 'form-control','readonly' => true, 'value' => '0'])->label(''); ?>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="pull-left">
                                        <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Perbaharui', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','id' => 'save']) ?>
                                    </div>
                                    <div class="pull-right">
                                        <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-danger']) ?>
                                        <br />
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="iframeplaceholder"  style="display:none;"></div>
<?php

$this->registerCssFile(Yii::$app->getUrlManager()->getBaseUrl()."/web/theme/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
$this->registerJsFile(Yii::$app->getUrlManager()->getBaseUrl()."/web/theme/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js");

$this->registerJs($this->render('_update.js'));
?>