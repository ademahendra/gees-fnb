<?php
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PurchaseOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Purchase Order';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card bg-white">
    <div class="portlet mt-element-ribbon light portlet-fit bordered">
        <div class="ribbon ribbon-left ribbon-clip ribbon-shadow ribbon-border-dash-hor ribbon-color-danger green-jungle uppercase">
            <div class="ribbon-sub ribbon-clip ribbon-left"></div>
            <i class='fa fa-table'></i>  <?= $this->title ?>
        </div>
        <div class="portlet-title">
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    <?= Html::a('Buat Purchase Order (PO)', ['create'], ['role' => 'modal-remote', 'title' => 'Create new Purchase Order', 'class' => 'btn green-turquoise btn-sm']) ?>
                </div>
            </div>
        </div>
        <div class="portlet-body">

            <?php
            $columns = [
                ['class' => 'kartik\grid\SerialColumn', 'order' => DynaGrid::ORDER_FIX_LEFT],
                [
                    'class' => 'kartik\grid\ExpandRowColumn',
                    'expandAllTitle' => 'Expand All',
                    'collapseTitle' => 'Collapse All',
                    'expandTitle' => 'Expand',
                    'expandIcon' => '<span class="fa fa-plus"></span>',
                    'collapseIcon' => '<span class="fa fa-minus"></span>',
                    'value' => function ($model, $key, $index) {
                        return GridView::ROW_COLLAPSED;
                    },
                    'detailRowCssClass' => GridView::TYPE_DEFAULT,
                    'pageSummary' => false,
                    'detailUrl' => Yii::$app->params['baseURL'].'purchasing/default/detail',//Url::to('/gees-resto/purchasing/default/detail'),
                    'detailAnimationDuration' => 100,
                    'expandOneOnly' => true,
                    'enableCache' => false
                ],

                'no_po',
                [
                    'attribute' => 'id_supplier',
                    'format' => 'raw',
                    'value' => 'supplier.nama',
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{edit} {delete}',
                    'buttons' => [
                        'edit' => function ($url, $model, $key) {     // render your custom button
                            return Html::a('<span class="fa fa-pencil"></span>', ['update', 'id' => $model->id], ['class' => 'btn btn-success btn-xs', 'id' => 'editButton']);
                        },
                        'delete' => function ($url, $model, $key) {     // render your custom button
                            return Html::a('<span class="fa fa-trash"></span>', ['delete', 'id' => $model->id], ['class' => 'btn btn-primary btn-xs', 'id' => 'deleteButton', 'data-method' => 'post']);
                        }
                    ],
                ],
            ];

            echo DynaGrid::widget([
                'columns' => $columns,
                'storage' => DynaGrid::TYPE_COOKIE,
                'theme' => 'panel-success',
                'gridOptions' => [
                    'dataProvider' => $dataProvider,
//				'filterModel'=>$searchModel,
                    'panel' => ['heading' => '<h3 class="panel-title">Purchase Order</h3>'],
                    // 'enableRowClick' => true,
                ],
                'options' => [
                    'id' => 'dynagrid-1',
                    'storage' => DynaGrid::TYPE_SESSION
                ],

            ]);
            ?>

        </div>
    </div>
</div>
