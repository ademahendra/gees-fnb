<?php
/**
 * Created by PhpStorm.
 * User: adema
 * Date: 29/04/2017
 * Time: 11.12
 */
use app\models\DetailPurchaseOrder;
use app\models\PurchaseOrder;
use app\models\Merchant;

setlocale(LC_ALL , 'id');

$penerimaan = PurchaseOrder::find()
    ->where([
        'id' => $id
    ])
    ->one();
	$merchantInfo = Merchant::find()->one();
?>
<!DOCTYPE html>
<html lang="en">
<head>

    <link rel="stylesheet" media="print" href="<?= $this->theme->baseUrl ?>/vendor/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" media="print" href="<?= $this->theme->baseUrl ?>/styles/print.css">
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="2" align="left"><img src="<?=Yii::$app->params['baseURL'].$merchantInfo->logo?>" width="100" height="100"/></td>
	</tr>
	<tr>
        <td colspan="2" align="left"><h3><?= $merchantInfo->name?></h3></td>
		<td colspan="3" align="right"><h2>PURCHASE ORDER</h2></td>
    </tr>
	<tr>
		<td colspan="2" align="left"><small><?= $merchantInfo->website?></small></td>
	</tr>
	<tr>
        <td colspan="2" align="left"><small><?= $merchantInfo->alamat?><br /> Telp. <?= $merchantInfo->telepon?></small></td>
    </tr>
	<tr>
        <td width="18%">No. PO</td>
        <td width="3%" align="center">:</td>
        <td width="74%"><?= $penerimaan->no_po ?></td>
    </tr>
	<tr>
        <td width="18%">Tanggal PO</td>
        <td width="3%" align="center">:</td>
        <td width="74%"><?= strftime('%A, %d %B %G', strtotime($penerimaan->tanggal)) ?></td>
    </tr>
	<tr>
        <td width="18%">Nama Supplier</td>
        <td width="3%" align="center">:</td>
        <td width="74%"><?= $penerimaan->supplier->nama ?></td>
    </tr>
</table>
<table width="100%" border="1" cellpadding="0" cellspacing="0" class="table table-bordered">
    <thead>
    <tr class="success">
        <th>No</th>
        <th>SKU</th>
        <th>Produk</th>
        <th>Qty</th>
    </tr>
    </thead>
    <tbody id="paddingLess">
    <!--loop-->
    <?php
    $detail = DetailPurchaseOrder::find()
        ->where([
            'id_purchase_order' => $id
        ])
        ->all();
    $i = 0;
    $qytTotal = 0;
    foreach($detail as $row) {
        $i++;
        $qytTotal += $row->jumlah;
        ?>
        <tr class="success">
            <td><?= $i ?></td>
            <td><?= $row->barang->id ?></td>
            <td style="text-align:left;"><?= $row->barang->name ?></td>
            <td style="text-align:right;"><?= number_format($row->jumlah,2,',','.'). ' '. $row->barang->bigUnit->nama ?></td>
        </tr>
        <?php
    }
    ?>
    </tbody>

</table>
<script src="<?= $this->theme->baseUrl ?>/vendor/jquery/dist/jquery.js"></script>
<script src="<?= $this->theme->baseUrl ?>/vendor/bootstrap/dist/js/bootstrap.js"></script>
</body>
</html>