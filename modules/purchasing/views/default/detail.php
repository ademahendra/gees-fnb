<?php
use app\models\DetailPurchaseOrder;

$detail = DetailPurchaseOrder::find()
    ->joinWith('purchase')
    ->where([
        'id_purchase_order' => $model->id
    ])
    ->all();
?>

<div class="col-md-10">

    <!-- Your awesome content goes here -->
    <div class="widget invoice">
        <div class="widget-content padding">
            <div class="row">
                <div class="col-sm-4">
                </div>
                <div class="col-sm-8 text-right">
                    <h3>Detail Purchase Order</h3>
                    <h4>#<?= $model->no_po ?></h4>
                </div>
            </div>

            <div class="table-responsive">

                <table class="table table-condensed table-striped">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Item</th>
                        <th>Jumlah</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 0;
                    $totalHarga = 0;

                    foreach ($detail as $row) {
                        $i++;
                        $totalHargaItem = ($row->jumlah * $row->harga_kena_pajak);
                        $totalHarga += $totalHargaItem;
                        ?>
                        <tr>
                            <td><?= $i ?></td>
                            <td><?= $row->barang->name ?></td>
                            <td><?= $row->jumlah.' '.$row->barang->bigUnit->nama ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
                <br><br>
            </div>
        </div>
    </div>
    <!-- End of your awesome content -->
</div>