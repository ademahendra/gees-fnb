/**
 * Created by SERVER on 23/09/2017.
 */
var body = $('body');

body.on('keyup', '#diskon_persen', function (e) {
    e.preventDefault();
    var curr = $(this);
    calculateDiscount();
});

body.on('keyup', '#diskon', function (e) {
    e.preventDefault();
    var curr = $(this);
    calculateDiscount(false);
});

body.on('keyup', '#diskonGlobalPercent', function (e) {
    e.preventDefault();
    var curr = $(this);
    calculateDiscountGlobal();
});

body.on('keyup', '#diskonGlobal', function (e) {
    e.preventDefault();
    var curr = $(this);
    calculateDiscountGlobal(false);
});


var newBarangDialog;
body.on('click', '#newItem', function (e) {
    e.preventDefault();
    newBarangDialog = BootstrapDialog.show({
        message: function (dialog) {
            var message = $('<div></div>');
            var pageToLoad = dialog.getData('pageToLoad');
            message.load(pageToLoad);
            return message;
        },
        title: 'New Item',
        data: {
            'pageToLoad': baseUrl + 'master/barang/new'
        },
        size: BootstrapDialog.SIZE_WIDE
    });
});

body.on('change', 'select#barangList', function () {
    var barangList = $('select#barangList').val();
    $.get(baseUrl + 'rest/detailbarang?id=' + barangList + '&gudang=0', function (jsonData) {
        var json = jQuery.parseJSON(jsonData);
        if (json.status) {
            $('#satuanBarang').html(json.satuan);
        }
    });
});

function calculateDiscount(percent = true) {
    var discPercent = $('#diskon_persen').val();
    var discount = $('#diskon').val();
    var hpp = $('#hargaBeli').val();
    var ppn = parseInt($('#ppn').val());

    if ((discount !== '' || discPercent !== '') && hpp !== '') {
        if (discPercent !== '') {
            if (percent) {
                var discRate = (discPercent / 100) * hpp;
                $('#diskon').val(parseInt(discRate).toFixed(0));
                $('#diskon-disp').val(parseInt(discRate).toFixed(2));
            }
        }

        if (discount !== '') {
            if (percent == false) {
                var discPerc = discount / hpp * 100;
                // console.log('I am here: '+discPerc);
                $('#diskon_persen').val(parseInt(discPerc).toFixed(0));
            }
        }

    }
    discountRate = $('#diskon').val();
    if (hpp !== '' && ppn !== '') {
        if (ppn == 10) {
            var hsp = ((parseInt(hpp) - parseInt(discountRate)) + ((parseInt(hpp) - parseInt(discountRate)) * ppn / 100)).toFixed(0);
        } else {
            var hsp = (parseInt(hpp - parseInt(discountRate))).toFixed(0);
        }
        $('#harga_kena_pajak').val(hsp);
        $('#harga_kena_pajak-disp').val(parseInt(hsp).toFixed(2));
    }
}


function calculateDiscountGlobal(percent = true) {
    //noinspection JSJQueryEfficiency
    var discPercent = $('#diskonGlobalPercent').val();
    var discount = $('#diskonGlobal').val();
    var hpp = $('#hargaTotal').val();
    var ppn = parseInt($('#ppn').val());

    console.log(discount + ' ' + discPercent + ' ' + hpp + ' ' + ppn);
    if ((discount !== '' || discPercent !== '') && hpp !== '') {
        if (discPercent !== '') {
            if (percent) {
                var discRate = (discPercent / 100) * hpp;
                $('#diskonGlobal').val(parseInt(discRate).toFixed(0));
                //update harga setelah pajak
            }
        }

        if (discount !== '') {
            if (percent == false) {
                var discPerc = discount / hpp * 100;
                // console.log('I am here GLOBAL : '+discPerc);
                $('#diskonGlobalPercent').val(parseInt(discPerc).toFixed(0));
                //update harga setelah pajak
            }
        }
    }
    discountRate = $('#diskonGlobal').val();
    var hsp = 0;
    $('.totalHargaItemPPN').each(function (i, val) {
        hsp += parseFloat($(this).val());
    });
    $('#hargaTotalPajak').val(hsp);
    $('#hargaTotalPajakFormat').val(humanizeNumber(hsp));
    // console.log(hpp+' ---- '+ppn);
    // if (hpp !== '' && ppn !== '') {
    //     if (ppn == 10) {
    //         var hsp = ((parseInt(hpp) - parseInt(discountRate)) + ((parseInt(hpp) - parseInt(discountRate)) * ppn / 100)).toFixed(0);
    //         // console.log('HSP '+hsp);
    //     } else {
    //         var hsp = (parseInt(hpp - parseInt(discountRate))).toFixed(0);
    //         // console.log('HP '+hsp);
    //     }
    //     $('#hargaTotalPajak').val(hsp);
    //     $('#hargaTotalFormat').val(parseFloat($('#hargaTotal').val()).formatMoney(2, '.', ','));
    //     $('#hargaTotalPajakFormat').val(parseFloat($('#hargaTotalPajak').val()).formatMoney(2, '.', ','));
    // }
}

body.on('change', '#ppn', function () {
    var hkp = $('#harga_kena_pajak').val();
    var hpp = $('#hargaBeli').val();
    var ppn = $('#ppn').val();
    if (ppn !== '0' && ppn !== '10') {
        alert('PPN harus berisi 10 atau 0!');
    }
    calculateDiscount();
});

$("body").on('click', '.remove', function (e) {
    e.preventDefault();
    $(this).closest('tr').remove();
    var removeItem = $(this).closest('tr').attr('id');

    calculateDiscountGlobal();
    delete addpenjualan[removeItem];


    //$("#addbutton").fadeIn();
});

body.on('keyup', '#hargaBeli', function () {
    calculateDiscount();
});


$("body").on('click', '#saveButton', function (e) {
    e.preventDefault();
    // console.log('Click Oke');

    // return;
    $.ajax({
        type: "POST",
        url: baseUrl + 'master/barang/new',
        dataType: 'json',
        cache: false,
        data: $('#barangForm').serialize(),
        success: function (json) {
            if (json.status) {
                bootbox.alert("Barang berhasil ditambahkan!");
                newBarangDialog.close();
                //reload select 2 and close popup
                $('#barangWrapper').html(json.selection);

                var select2setting = {
                    "allowClear": true,
                    "theme": "krajee",
                    "width": "100%",
                    "placeholder": "--Pilih Nama Barang--",
                    "language": "en-US"
                };
                $('#barangList').select2(select2setting);
                $('.kv-plugin-loading').hide();
                $('#barangList').val(json.newid).trigger('change');
            } else {
                bootbox.alert('Penambahan Barang Gagal');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
});

body.on('change', '#ppn', function () {
    var hkp = $('#harga_kena_pajak').val();
    var hpp = $('#hargaBeli').val();
    var ppn = $('#ppn').val();
    if (ppn !== '0' && ppn !== '10') {
        alert('PPN harus berisi 10 atau 0!');
    }
    calculateDiscount();
});

$("#addbutton").on('click', function (e) {
    e.preventDefault();
    var idBarang = $('#barangList').val();
    var qtyOrder = $('#qty').val();
    if (idBarang != '' && qtyOrder != '' && qtyOrder != '0') {
        var form = $('#paymentForm').serialize();
        $.ajax({
            type: "POST",
            url: baseUrl + 'purchasing/default/additem',
            dataType: 'json',
            cache: false,
            data: form,
            success: function (json) {
                if (json.status) {
                    $("#tbody").append(json.tr);
                    //$("#addbutton").attr('disabled',true);
                    $("#gudangList").attr('readonly', true);
                    $("#supplierList").attr('readonly', true);
                    $('#pembelian-batch_no').val('');
                    $('#pembelian-expired_date').val('');
                    $('#harga_jual_1').val('');
                    $('#harga_jual_2').val('');
                    $('#harga_jual_3').val('');
                    $('#harga_jual_4').val('');
                    $('#pembelian-jumlah').val('');
                    // $('#pembelian-no_faktur').attr('readonly',true); 
                    diskon = parseInt($('#diskon').val());
                    hargaTotal = parseInt($('#hargaTotal').val());
                    hargaTotalPajak = parseInt($('#hargaTotalPajak').val());
                    hargabeli = parseInt($('#hargaBeli').val());
                    hargapajak = parseInt($('#harga_kena_pajak').val());
                    qty = parseInt($('#qty').val());
                    console.log(hargaTotal + ' ==== ' + hargaTotalPajak);
                    hargaDiskon = hargabeli - diskon;
                    $('#hargaTotal').val(hargaDiskon * qty);
                    $('#hargaTotalPajak').val(hargaTotalPajak + (hargapajak * qty));

                    // $('#hargaTotalFormat').val(parseFloat($('#hargaTotal').val()).formatMoney(2, '.', ','));
                    //$('#hargaTotalPajakFormat').val(parseFloat($('#hargaTotalPajak').val()).formatMoney(2, '.', ',')) ;

                    $('#hargaTotalFormat').val(formatCurrency(parseFloat($('#hargaTotal').val())));
                    $('#hargaTotalPajakFormat').val(formatCurrency(parseFloat($('#hargaTotalPajak').val())));

                    // $('#hargaBeli').val('');
                    // $('#harga_kena_pajak').val('');
                    // $('#diskon').val('0');
                    // $('#diskon_persen').val('0');
                    $('#qty').val('0');
                    $('#barangList').focus();

                } else {

                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }
});

function loadiFrame(src) {
    $("#iframeplaceholder").html("<iframe id='myiframe' name='myname' src='" + src + "' />");
}

$("#save").on('click', function (e) {
    e.preventDefault();
    var form = $('#paymentForm').serialize();
    $.ajax({
        type: "POST",
        url: baseUrl + 'purchasing/default/save',
        dataType: 'json',
        cache: false,
        data: form,
        success: function (json) {
            if (json.status) {
                $('#paymentForm').each(function () {
                    this.reset();
                });

                loadiFrame(baseUrl + 'purchasing/default/printreceipt?id=' + json.nopo);
                $("#myiframe").load(
                    function () {
                        window.frames['myname'].focus();
                        window.frames['myname'].print();
                    }
                );
                addpenjualan = [];
                $('#tbody').html('');
                $('#noPO').val(json.nobaru);
                bootbox.alert({
                    size: "small",
                    title: "Success",
                    message: "Purchase Order Success to Save!",
                    callback: function(){
                        location.reload();
                    }
                });


            } else {
                if (json && typeof json.msg != 'undefined') {
                    bootbox.alert(json.msg);
                }

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
});