<?php

use app\models\Barang;
use app\models\FnbIngredients;
use app\models\FnbStock;
use app\models\PurchaseOrder;
use app\models\Satuan;
use app\models\Supplier;
use kartik\money\MaskMoney;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

// use yii\helpers\ArrayHelper;
//use kartik\growl\Growl;

/* @var $this yii\web\View */
/* @var $model app\models\Pembelian */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Purchase Order');
$lastID = PurchaseOrder::find()->orderBy('id desc')->one();
$noOrder = 'PO-' . sprintf("%06s", 1);
if (count($lastID) > 0) {
    $noOrder = 'PO-' . sprintf("%06s", ($lastID->id + 1));
}

$setupDefault = \app\models\Merchant::find()->one();

$model->isNewRecord ? $model->ppn = '10' : $model->ppn = $model->ppn;

?>
    <div class="card bg-white">
        <div class="portlet mt-element-ribbon light portlet-fit bordered">
            <div class="ribbon ribbon-left ribbon-clip ribbon-shadow ribbon-border-dash-hor ribbon-color-success green-jungle uppercase">
                <div class="ribbon-sub ribbon-clip ribbon-left"></div>
                <i class="icon-notebook font-white"></i> <?= $this->title ?>
            </div>
            <div class="portlet-title">
                <div class="actions">
                    <div class="btn-group btn-group-devided">

                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <?php $form = ActiveForm::begin([
                    'id' => 'paymentForm'
                ]); ?>
                <div class="row">
                    <div class="widget col-md-12">
                        <div class="widget-content">
                            <div class="row">

                                <div class="col-md-3">
                                    <?= $form->field($model, 'no_po')->textInput(['maxlength' => true, 'value' => $noOrder, 'readonly' => true, 'id' => 'noPO', 'class' => 'form-control input-sm']) ?>
                                </div>
                                <div class="col-md-2">
                                    <?= $form->field($model, 'tanggal')->textInput(['maxlength' => true, 'class' => 'form-control datepicker-input input-sm', 'data-provide' => 'datepicker', 'data-format' => 'yyyy-mm-dd', 'value' => date('m/d/Y')]) ?>
                                </div>
                                <div class="col-md-3">
                                    <?php
                                    $data = yii\helpers\ArrayHelper::map(Supplier::find()->orderBy('nama ASC')->all(), 'id', 'nama');
                                    echo $form->field($model, 'id_supplier')->widget(Select2::classname(), [
                                        'data' => $data,
                                        'theme' => Select2::THEME_BOOTSTRAP,
                                        'size' => Select2::SMALL,
                                        'options' => ['placeholder' => '--Pilih Supplier--', 'id' => 'supplierList', 'value' => $setupDefault->id_supplier],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ])->label('Supplier');
                                    ?>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-3" id="barangWrapper">
                                    <?php
                                    $data = yii\helpers\ArrayHelper::map(FnbStock::find()->orderBy('name ASC')->all(), 'id', 'name');
                                    echo $form->field($detailModel, 'id_barang')->widget(Select2::classname(), [
                                        'data' => $data,
                                        'size' => Select2::SMALL,
                                        'options' => ['placeholder' => '--Pilih Nama Barang--', 'id' => 'barangList'],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ])->label('Barang');
                                    ?>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label">&nbsp;</label>
                                        <br/>
                                        <?= Html::a('<i class="fa fa-plus"></i> Barang Baru', ['#'], ['class' => 'btn purple-studio btn-xs', 'id' => 'newItem']) ?>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">
                                    <?= $form->field($detailModel, 'jumlah')->textInput(['id' => 'qty', 'class' => 'form-control input-sm']) ?>
                                </div>
                                <div class="col-md-3" id="satuanWrapper">
                                    <label>&nbsp;</label><br/>
                                    <label id="purchaseUnit"></label>
                                </div>
                                <div class="col-md-3" id="levelsatuanWrapper">
                                    <label id="detailQTY"></label>
                                </div>
                            </div>

                            <div class="row ">
                                <div class="form-group col-md-12">
                                    <?= Html::a('Tambah', ['#'], ['class' => 'btn btn-sm green-jungle', 'id' => 'addbutton']) ?>

                                </div>
                            </div>
                            <hr/>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>
                                                Item
                                            </th>
                                            <th>
                                                Jumlah
                                            </th>
                                            <th class="col-md-1">

                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbody">
                                        </tbody>
                                    </table>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="pull-left">
                                            <?= Html::a('Kembali', ['index'], ['class' => 'btn red-flamingo btn-sm']) ?>
                                        </div>
                                        <div class="pull-right">
                                            <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn green-sharp btn-sm' : 'btn blue-madison btn-sm', 'id' => 'save']) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="iframeplaceholder" style="display:none;"></div>
<?php

$this->registerCssFile(Yii::$app->getUrlManager()->getBaseUrl() . "/web/theme/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
$this->registerJsFile(Yii::$app->getUrlManager()->getBaseUrl() . "/web/theme/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js");
$this->registerJs($this->render('_purchasing.js'), \yii\web\View::POS_READY);
?>