<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Bank */

?>

<?= $this->render('_form', [
    'model' => $model,
    'detailModel' => $detailModel,
]) ?>


