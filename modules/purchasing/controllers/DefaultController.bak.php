<?php

namespace app\modules\purchasing\controllers;

use Yii;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

use app\models\PurchaseOrder;
use app\models\PurchaseOrderSearch;
use app\models\Barang;
use yii\db\Command;
use yii\db\QueryBuilder;
use kartik\mpdf\Pdf;
use app\models\DetailPurchaseOrder;

/**
 * Default controller for the `purchasing` module
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PurchaseOrder models.
     * @return mixed
     */


    public function actionAdditem()
    {
        $model = new PurchaseOrder();
        $detail = new DetailPurchaseOrder();

        if($model->load(Yii::$app->request->post()) && $detail->load(Yii::$app->request->post())) {
            $stok = Barang::find()
                ->where(['id' => $detail->id_barang])
                ->one();

            $data = array();
            if(count($stok) > 0){
                $tr = "<tr id=\"" . $stok->id . "\" val=\"" . $stok->nama . "\">						
				<td>
					" . $stok->nama . "
					<input name=\"namaobat[]\" value=\"" . $stok->nama . "\" type=\"hidden\" readonly=\"true\" />
					<input name=\"id[]\" value=\"" . $stok->id . "\" type=\"hidden\" readonly=\"true\" />
					<input name=\"id_gudang[]\" value=\"" . $model->id_gudang . "\" type=\"hidden\" readonly=\"true\" />
					<input name=\"qty[]\" value=\"" . $detail->jumlah . "\" type=\"hidden\" readonly=\"true\" />
					<input name=\"hb[]\" value=\"" . $detail->harga_beli . "\" type=\"hidden\" readonly=\"true\" />
					<input name=\"diskon_persen[]\" value=\"" . $detail->diskon_persen . "\" type=\"hidden\" readonly=\"true\" />
					<input name=\"diskon[]\" value=\"" . $detail->diskon . "\" type=\"hidden\" readonly=\"true\" />
					<input name=\"hkp[]\" value=\"" . $detail->harga_kena_pajak . "\" type=\"hidden\" readonly=\"true\" />
					<input name=\"ppn[]\" value=\"" . (($detail->harga_beli - $detail->diskon) * $detail->ppn / 100) . "\" type=\"hidden\" readonly=\"true\" />
					<input name=\"totalhj[]\" value=\"" .($detail->jumlah *$detail-> harga_kena_pajak). "\" type=\"hidden\" readonly=\"true\" />
				</td>
				<td>
					" . $detail->jumlah . "
				</td>
				<td>
					" . number_format($detail->harga_beli, 2, ',', '.') . "
				</td>
				<td>
					" . number_format($detail->diskon, 2, ',', '.') . "
				</td>
				<td>
					" . number_format((($detail->harga_beli - $detail->diskon) * $detail->ppn / 100), 2, ',', '.') . "
				</td>
				<td>
					" . number_format($detail->harga_kena_pajak, 2, ',', '.') . "
				</td>
				<td>
					" . number_format(($detail->jumlah * $detail->harga_kena_pajak), 2, ',', '.') . "
				</td>
				<td>
					<a class=\"remove\" style='cursor:pointer;'><i class=\"fa fa-minus\"></i></a>
				</td>
			</tr>";
                $data['status'] = true;
                $data['tr'] = $tr;
            } else {
                $data['status'] = false;
                $data['msg'] = "Produk tidak ditemukan!";
            }
        }
        echo json_encode($data);
    }

    public function actionDetail() {
        if (isset($_POST['expandRowKey'])) {
            $model = PurchaseOrder::findOne($_POST['expandRowKey']);
            return $this->renderPartial('detail', ['model'=>$model]);
        } else {
            return '<div class="alert alert-danger">No data found</div>';
        }
    }


    public function actionIndex()
    {
        $searchModel = new PurchaseOrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single PurchaseOrder model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "PurchaseOrder #".$id,
                'content'=>$this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
            ];
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new PurchaseOrder model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PurchaseOrder();
        $detailModel = new DetailPurchaseOrder();
        return $this->render('create', [
            'model' => $model,
            'detailModel' => $detailModel,
        ]);
    }

    public function actionSave(){
        $data = [];
        $model = new PurchaseOrder();
        $detailModel = new DetailPurchaseOrder();
        $totalhargabeli = 0;
        $totalppn = 0;
        $totalhargapajak = 0;
        $totaljumlah = 0;
        $total = 0;
        $data['status'] = false;
        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post();
            $detailModel->load(Yii::$app->request->post());
            $lastID = PurchaseOrder::find()->orderBy('id desc')->one();
            $noOrder = 'PO-'.sprintf("%06s", 1);
            if(count($lastID) > 0){
                $noOrder = 'PO-'.sprintf("%06s", ($lastID->id+1) );
            }
            $purchaseOrder = new PurchaseOrder();
            $purchaseOrder->no_po = $noOrder;
            $purchaseOrder->tanggal= date('Y-m-d', strtotime($model->tanggal));
            $purchaseOrder->id_supplier = $model->id_supplier;
            $purchaseOrder->jatuh_tempo = date('Y-m-d', strtotime($model->jatuh_tempo));

            $purchaseOrder->diskon_persen = $model->diskon_persen;
            $purchaseOrder->diskon = $model->diskon;
            if($purchaseOrder->save()){
                $data['status'] = true;
                $data['nopo'] = $purchaseOrder->id;
                for($i = 0; $i < count($post['id']); $i++){
                    $detailPurchaseOrder = new DetailPurchaseOrder();
                    $detailPurchaseOrder->id_purchase_order = $purchaseOrder->id;
                    $detailPurchaseOrder->id_barang = $post['id'][$i];
                    $detailPurchaseOrder->harga_beli = $post['hb'][$i];

                    $detailPurchaseOrder->diskon_persen = $post['diskon_persen'][$i];
                    $detailPurchaseOrder->diskon = $post['diskon'][$i];

                    $detailPurchaseOrder->jumlah = $post['qty'][$i];
                    $detailPurchaseOrder->ppn = $post['ppn'][$i];
                    $detailPurchaseOrder->harga_kena_pajak = $post['hkp'][$i];
                    $detailPurchaseOrder->total = $post['totalhj'][$i];
                    $detailPurchaseOrder->tanggal = new \yii\db\Expression('NOW()');
                    $detailPurchaseOrder->last_update = new \yii\db\Expression('NOW()');
                    $detailPurchaseOrder->date_added = new \yii\db\Expression('NOW()');
                    if(!$detailPurchaseOrder->save()){
                        $data['status'] = false;
                        $data['po'][$i] = $detailPurchaseOrder->getErrors();
                    }

                    $totalhargabeli += $post['hb'][$i];
                    $totalppn += $post['ppn'][$i];
                    $totalhargapajak += $post['hkp'][$i];
                    $totaljumlah += $post['qty'][$i];
                    $total += ($post['totalhj'][$i]);

                }
                $purchaseOrder->jumlah = $totaljumlah;
                $purchaseOrder->harga_kena_pajak = $totalhargapajak;
                $purchaseOrder->harga_beli = $totalhargabeli;
                $purchaseOrder->ppn = $totalppn;
                $purchaseOrder->total = $total;
                $purchaseOrder->save();

                $lastID = PurchaseOrder::find()->orderBy('id desc')->one();
                $noOrder = 'PO-'.sprintf("%06s", 1);
                if(count($lastID) > 0){
                    $noOrder = 'PO-'.sprintf("%06s", ($lastID->id+1) );
                }
                $data['nobaru'] = $noOrder;
            } else {
                $data['status'] = false;
                $data['msg'] = "PO gagal di simpan!";
                $data['error'] = $purchaseOrder->getErrors();
            }
        } else {
            $data['status'] = false;
            $data['msg'] = "Failed to load Post Data!";
        }
        return json_encode($data);
    }

    /**
     * Updates an existing PurchaseOrder model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update PurchaseOrder #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "PurchaseOrder #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
            }else{
                return [
                    'title'=> "Update PurchaseOrder #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing PurchaseOrder model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing PurchaseOrder model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the PurchaseOrder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PurchaseOrder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PurchaseOrder::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionPrintreceipt($id){
        return $this->renderPartial('nota',[
            'id' => $id
        ]);
    }
}
