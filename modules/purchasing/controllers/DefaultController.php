<?php

namespace app\modules\purchasing\controllers;

use app\models\StokGudang;
use Yii;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

use app\models\PurchaseOrder;
use app\models\PurchaseOrderSearch;
use app\models\FnbStock as Barang;
use yii\db\Command;
use yii\db\QueryBuilder;
use kartik\mpdf\Pdf;
use app\models\DetailPurchaseOrder;

/**
 * Default controller for the `purchasing` module
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors ()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionEditpurchase ()
    {
        $model = new PurchaseOrder();
        $detail = new DetailPurchaseOrder();

        if ($model->load(Yii::$app->request->post()) && $detail->load(Yii::$app->request->post())) {
            $stok = Barang::find()
                ->where(['id' => $detail->id_barang])
                ->one();

            $data = array();
            if (count($stok) > 0) {
                $tr = "<tr id=\"" . $stok->id . "\" val=\"" . $stok->nama . "\">						
				<td>
					" . $stok->nama . "
					<input name=\"namaobat[]\" value=\"" . $stok->nama . "\" type=\"hidden\" readonly=\"true\" />
					<input name=\"id[]\" value=\"" . $stok->id . "\" type=\"hidden\" readonly=\"true\" />
					<input name=\"id_gudang[]\" value=\"" . $model->id_gudang . "\" type=\"hidden\" readonly=\"true\" />
					<input name=\"qty[]\" value=\"" . $detail->jumlah . "\" type=\"hidden\" readonly=\"true\" />
					<input name=\"hb[]\" value=\"" . $detail->harga_beli . "\" type=\"hidden\" readonly=\"true\" />
					<input name=\"diskon_persen[]\" value=\"" . $detail->diskon_persen . "\" type=\"hidden\" readonly=\"true\" />
					<input name=\"diskon[]\" value=\"" . $detail->diskon . "\" type=\"hidden\" readonly=\"true\" />
					<input name=\"hkp[]\" value=\"" . $detail->harga_kena_pajak . "\" type=\"hidden\" readonly=\"true\" />
					<input name=\"ppn[]\" value=\"" . (($detail->harga_beli - $detail->diskon) * $detail->ppn / 100) . "\" type=\"hidden\" readonly=\"true\" />
					<input name=\"totalhj[]\" value=\"" . ($detail->jumlah * $detail->harga_kena_pajak) . "\" type=\"hidden\" readonly=\"true\" />
				</td>
				<td>
                    <input type='text' value='" . ($detail->jumlah) . "' class='form-control qtyedit ' />
                </td>
                <td>
                    <input type='text' value='" . ($detail->harga_beli) . "' class='form-control hargaedit' />
                </td>
                <td>
                    <input type='text' value='" . ($detail->diskon) . "' class='form-control diskonedit' />
                </td>
                <td>
                    <input type='text' value='" . (($detail->harga_beli - $detail->diskon) * $detail->ppn / 100) . "' class='form-control ppnedit' />
                </td>
                <td>
                    <input type='text' value='" . ($detail->harga_kena_pajak) . "' class='form-control hkpedit' />
                </td>
                <td>
                    <input type='text' value='" . ($detail->jumlah * $detail->harga_kena_pajak) . "' class='form-control hppedit' />
                    <input type='hidden' value='" . ($detail->jumlah * $detail->harga_kena_pajak) . "' class='form-control hppedit2' />
                </td>
				<td>
					<a class=\"remove\" style='cursor:pointer;'><i class=\"fa fa-minus\"></i></a>
				</td>
			</tr>";
                $data['status'] = true;
                $data['totalJual'] = ($detail->jumlah * $detail->harga_kena_pajak);
                $data['tr'] = $tr;
            } else {
                $data['status'] = false;
                $data['msg'] = "Produk tidak ditemukan!";
            }
        }
        echo json_encode($data);
    }

    public function actionAdditem ()
    {
        $model = new PurchaseOrder();
        $detail = new DetailPurchaseOrder();
        $data = array();
        if ($model->load(Yii::$app->request->post()) && $detail->load(Yii::$app->request->post())) {
            $stok = Barang::find()
                ->where(['id' => $detail->id_barang])
                ->one();


            if (count($stok) > 0) {
                $tr = "<tr id=\"" . $stok->id . "\" val=\"" . $stok->name . "\">						
				<td>
					" . $stok->name . "
					<input name=\"namaobat[]\" value=\"" . $stok->name . "\" type=\"hidden\" readonly=\"true\" />
					<input name=\"id[]\" value=\"" . $stok->id . "\" type=\"hidden\" readonly=\"true\" />
					
					<input name=\"qty[]\" value=\"" . $detail->jumlah . "\" type=\"hidden\" readonly=\"true\" />
					
				</td>
				<td>
					" . $detail->jumlah . "
				</td>
				<td>
					<a class=\"remove\" style='cursor:pointer;'><i class=\"fa fa-minus\"></i></a>
				</td>
			</tr>";
                $data['status'] = true;
                $data['tr'] = $tr;
            } else {
                $data['status'] = false;
                $data['msg'] = "Produk tidak ditemukan!";
            }
        }
        echo json_encode($data);
    }

    public function actionDetailbarang ($id)
    {
        $stok = Barang::find()
            ->where(['id' => $id])
            ->one();

        $stokGudang = StokGudang::find()
            ->where(['id_barang' => $id])
            ->one();

        $data = array();
        $data['satuan'] = '';
        if (count($stok) > 0) {
            $data['stok'] = isset($stokGudang->stok) ? $stokGudang->stok : 0;
            $data['satuan'] = $stok->bigUnit->nama;
            $data['status'] = true;
        } else {
            $data['status'] = false;
        }
        echo json_encode($data);

    }

    public function actionDetail ()
    {
        if (isset($_POST['expandRowKey'])) {
            $model = PurchaseOrder::findOne($_POST['expandRowKey']);
            return $this->renderPartial('detail', ['model' => $model]);
        } else {
            return '<div class="alert alert-danger">No data found</div>';
        }
    }


    public function actionIndex ()
    {
        $searchModel = new PurchaseOrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single PurchaseOrder model.
     * @param integer $id
     * @return mixed
     */
    public function actionView ($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "PurchaseOrder #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Edit', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new PurchaseOrder model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate ()
    {
        $model = new PurchaseOrder();
        $detailModel = new DetailPurchaseOrder();
        return $this->render('create', [
            'model' => $model,
            'detailModel' => $detailModel,
        ]);
    }

    public function actionSave ()
    {
        $data = [];
        $model = new PurchaseOrder();
        $detailModel = new DetailPurchaseOrder();
        $totalhargabeli = 0;
        $totalppn = 0;
        $totalhargapajak = 0;
        $totaljumlah = 0;
        $total = 0;
        $data['status'] = false;
        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post();
            $detailModel->load(Yii::$app->request->post());
            $lastID = PurchaseOrder::find()->orderBy('id desc')->one();
            $noOrder = 'PO-' . sprintf("%06s", 1);
            if (count($lastID) > 0) {
                $noOrder = 'PO-' . sprintf("%06s", ($lastID->id + 1));
            }
            $purchaseOrder = new PurchaseOrder();
            $purchaseOrder->no_po = $noOrder;
            $purchaseOrder->tanggal = date('Y-m-d', strtotime($model->tanggal));
            $purchaseOrder->id_supplier = $model->id_supplier;

            $purchaseOrder->admin_id = Yii::$app->user->identity->id;
            if ($purchaseOrder->save()) {
                $data['status'] = true;
                $data['nopo'] = $purchaseOrder->id;
                for ($i = 0; $i < count($post['id']); $i++) {
                    $detailPurchaseOrder = new DetailPurchaseOrder();
                    $detailPurchaseOrder->id_purchase_order = $purchaseOrder->id;
                    $detailPurchaseOrder->id_barang = $post['id'][$i];

                    $detailPurchaseOrder->jumlah = $post['qty'][$i];
                    $detailPurchaseOrder->sisa = $post['qty'][$i];
                    $detailPurchaseOrder->tanggal = new \yii\db\Expression('NOW()');
                    $detailPurchaseOrder->last_update = new \yii\db\Expression('NOW()');
                    $detailPurchaseOrder->date_added = new \yii\db\Expression('NOW()');
                    $detailPurchaseOrder->update_by = Yii::$app->user->identity->id;
                    if (!$detailPurchaseOrder->save()) {
                        $data['status'] = false;
                        $data['po'][$i] = $detailPurchaseOrder->getErrors();
                    }
                }
                $lastID = PurchaseOrder::find()->orderBy('id desc')->one();
                $noOrder = 'PO-' . sprintf("%06s", 1);
                if (count($lastID) > 0) {
                    $noOrder = 'PO-' . sprintf("%06s", ($lastID->id + 1));
                }
                $data['nobaru'] = $noOrder;
            } else {
                $data['status'] = false;
                $data['msg'] = "PO gagal di simpan!";
                $data['error'] = $purchaseOrder->getErrors();
            }
        } else {
            $data['status'] = false;
            $data['msg'] = "Failed to load Post Data!";
        }
        return json_encode($data);
    }


    public function actionUpdate ($id)
    {
        $data = [];
        $model = new PurchaseOrder();
        $detail = new DetailPurchaseOrder();
        $totalhargabeli = 0;
        $totalppn = 0;
        $totalhargapajak = 0;
        $totaljumlah = 0;
        $total = 0;
        $error = '';
        $data['status'] = false;
        if ($model->load(Yii::$app->request->post())) {
            $detail->load(Yii::$app->request->post());

            $Purchase = PurchaseOrder::find()
                ->where([
                    'id' => $id //id artikel
                ])
                ->one();

            $Purchase->no_po = $model->no_po;
            $Purchase->tanggal = date('Y-m-d', strtotime($model->tanggal));
            $Purchase->id_supplier = $model->id_supplier;
            $Purchase->jatuh_tempo = date('Y-m-d', strtotime($model->jatuh_tempo));

            $Purchase->diskon_persen = $model->diskon_persen;
            $Purchase->diskon = $model->diskon;
            $Purchase->admin_id = Yii::$app->user->identity->id;

            if ($Purchase->save()) {
                $data['status'] = true;
                if (count($detail->id_barang) > 0) {
                    $valueSelisih = array();
                    for ($x = 0; $x < count($_POST['id']); $x++) {

                        $findBefore = DetailPurchaseOrder::find()
                            ->where([
                                'id_purchase_order' => $id,
                                'id_barang' => $_POST['id'][$x]
                            ])
                            ->one();

                        $selisih = DetailPurchaseOrder::find()
                            ->where([
                                'id_purchase_order' => $id,
                                'id_barang' => $_POST['id'][$x]
                            ])
                            ->one();
                        if (count($selisih) > 0) {
                            $jumlahAwal = $selisih->jumlah;
                            $valueSelisih[$x] = ($jumlahAwal - $_POST['qty'][$x]);
                        } else {
                            $valueSelisih[$x] = 0;
                        }
                    }
                    $detail->deleteAll(['id_purchase_order' => $Purchase->id]);
                    for ($i = 0; $i < count($_POST['id']); $i++) {
                        $detailPurchase = new DetailPurchaseOrder();
                        $detailPurchase->id_purchase_order = $Purchase->id;
                        $detailPurchase->id_barang = $_POST['id'][$i];

                        $detailPurchase->harga_beli = $_POST['hb'][$i];

                        $detailPurchase->diskon_persen = $_POST['diskon_persen'][$i];
                        $detailPurchase->diskon = $_POST['diskon'][$i];

                        $detailPurchase->jumlah = $_POST['qty'][$i];
                        $detailPurchase->jumlah_kirim = $findBefore->jumlah_kirim;
                        $detailPurchase->sisa = $findBefore->sisa - $valueSelisih[$i];
                        $detailPurchase->ppn = $_POST['ppn'][$i];
                        $detailPurchase->harga_kena_pajak = $_POST['hkp'][$i];
                        $detailPurchase->total = $_POST['totalhj'][$i];
                        $detailPurchase->tanggal = new \yii\db\Expression('NOW()');
                        $detailPurchase->last_update = new \yii\db\Expression('NOW()');
                        $detailPurchase->date_added = new \yii\db\Expression('NOW()');
                        $detailPurchase->update_by = Yii::$app->user->identity->id;
                        if (!$detailPurchase->save()) {
                            $data['status'] = false;
                            $data['po'][$i] = $detailPurchase->getErrors();
                        }

                        $totalhargabeli += $_POST['hb'][$i];
                        $totalppn += $_POST['ppn'][$i];
                        $totalhargapajak += $_POST['hkp'][$i];
                        $totaljumlah += $_POST['qty'][$i];
                        $total += ($_POST['totalhj'][$i]);

                    }

                    $Purchase->jumlah = $totaljumlah;
                    $Purchase->harga_kena_pajak = $totalhargapajak;
                    $Purchase->harga_beli = $totalhargabeli;
                    $Purchase->ppn = $totalppn;
                    $Purchase->total = $total;
                    //$Purchase->save();

                    if (!$Purchase->save()) {
                        $data['status'] = false;
                        $data['msg-Purchase'] = $Purchase->getErrors();
                    } else {
                        $data['status'] = true;
                    }

                }
            }
            return json_encode($data);
        }

        $model = PurchaseOrder::find()
            ->where([
                'id' => $id
            ])
            ->one();

        $detail = DetailPurchaseOrder::find()
            ->where([
                'id_purchase_order' => $id
            ])
            ->all();

        $formDetail = new DetailPurchaseOrder();
        return $this->render('update', [
            'model' => $model,
            'detail' => $detail,
            'formDetail' => $formDetail,
        ]);
    }

    public function actionDelete ($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing PurchaseOrder model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete ()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the PurchaseOrder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PurchaseOrder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel ($id)
    {
        if (($model = PurchaseOrder::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionPrintreceipt ($id)
    {
        return $this->renderPartial('nota', [
            'id' => $id
        ]);
    }
}
