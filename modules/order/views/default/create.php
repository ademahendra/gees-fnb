<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FnbSale */

$this->title = 'New Order';

echo $this->render('_form', [
    'model' => $model,
    'menu' => $menu,
]) ?>
