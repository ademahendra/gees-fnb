<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\FnbMenu;
use app\models\Room;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\models\FnbIngredients;
use app\models\FnbStock;
use app\models\Guest;
use kartik\touchspin\TouchSpin;
/* @var $this yii\web\View */
/* @var $model app\models\FnbSale */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
	'id' => 'menuForm'
]); ?>
<div class="card bg-white">
    <div class="col-md-12">
        <!-- BEGIN BORDERED TABLE PORTLET-->
        <div class="portlet light portlet-fit bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-notebook font-red"></i>
                    <span class="caption-subject font-red sbold uppercase">New Order</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    

                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= Html::label('Table', null, ['class' => 'control-label']) ?>
                                    <?php
                                        $datatable = [];
                                        for($i = 1; $i < 11; $i++){
                                            $datatable[$i] = $i;
                                        }
                                        echo Select2::widget([
                                            'name' => 'table_no',
                                            'data' => $datatable,
                                            'options' => ['placeholder' => 'Select Table','id' => 'table_no'],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <?php
                                    echo $form->field($model, 'no_of_guest')->widget(TouchSpin::classname(), [
                                        'options'=>['placeholder'=>'Enter No of Guest'],
                                        'pluginOptions' => [
                                            'min' => 1,
                                            'max' => 50,
                                            'verticalupclass' => 'glyphicon glyphicon-plus',
                                            'verticaldownclass' => 'glyphicon glyphicon-minus',
                                        ]
                                    ]);
                                ?>
                            </div>
							 <div class="col-md-2">
								<?= $form->field($model, 'date_sale')->textInput(['maxlength' => true, 'class' => 'form-control datetimepicker input-sm', 'data-format' => 'yyyy-mm-dd', 'value' => date('m/d/Y H:i:s')]) ?>
							</div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <small>Click on Menu to Select</small>
                            </div>
                            <?php
                                $menuQuery = FnbMenu::find()
//                                    ->where(['hotel_id' => Yii::$app->user->identity->hotel_id])
                                    ->orderBy('name ASC')
                                    ->all();


                                foreach($menuQuery as $menu){
                                    $ingredients = FnbIngredients::find()->where(['menu_id' => $menu->id])->all();
                                    $net_price = 0;
                                    for($i = 0; $i < count($ingredients); $i++){
                                        $stock = FnbStock::find()->where(['id' => $ingredients[$i]->stock_id])->one();
                                        $net_price += ($stock->unit_price * $ingredients[$i]->qty);
                                    }
                            ?>
                                <button class="col-md-1 menu_id menubox" data-id="<?= $menu->id ?>" data-price="<?= $menu->selling_price ?>" data-name="<?= $menu->name ?>" data-netto="<?= $net_price ?>">
                                    <label><?= $menu->name ?></label>
                                </button>
                            <?php
                                }
                            ?>
                        </div>

                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row">
				    <div class="form-group col-sm-3">
						<?= $form->field($model, 'total_sale')->textInput(['id' => 'grandtotal']) ?>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <table class="table table-hover table-light">
                        <thead>
                            <tr>
                                <th>
                                    Menu Name
                                </th>
                                <th>
                                    Qty
                                </th>
                                <th>
                                    Price
                                </th>
                                <th>
                                    Sub Total
                                </th>
                                <th colspan="3">
                                Action
                                </th>
                            </tr>
                        </thead>
                        <tbody id="tbody">
                        </tbody>
                    </table>
                </div>
	
                <div class="row">
                    <div class="form-group col-md-6">
                        <button class="btn btn-success" id="putOrder">Put Order</button>&nbsp;
                        <button class="btn btn-danger" id="reset">Reset</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>


<script>
var posturl = "<?= Yii::$app->urlManager->createUrl('order') ?>";
</script>
<?php
$script = <<< JS
$(document).ready(function(){
	$('.datetimepicker').datetimepicker();
	var orderList = [];	
	var grand_total = 0;

	
	$("#addMenu").attr('disabled',true);
	$("#putOrder").attr('disabled',true);
	
	$("body").on('click','.remove', function(e){
		e.preventDefault();
		$(this).closest('tr').remove();
		var removeItem = $(this).closest('tr').attr('id');
		
		grand_total = grand_total - orderList[removeItem];
		$('#grandtotal').val(grand_total.toFixed(2));
		delete orderList[removeItem];

	});
	
    $(".menubox").on('click', function(e){
		e.preventDefault();	
		var id_menu = $(this).attr('data-id');
		var menuBox = $(this);
		if(typeof(orderList[id_menu]) === 'undefined'){
			
			var tbody = "<tr id=\""+id_menu+"\">";
				tbody += "<td id=\"td_main\">"+menuBox.attr('data-name');
				tbody += "<input type=\"hidden\" name=\"id_menu[]\" value=\""+id_menu+"\">";
				tbody += "<input type=\"hidden\" name=\"qty_order[]\" value=\"1\">";
				tbody += "<input type=\"hidden\" name=\"price_val[]\" value=\""+menuBox.attr('data-price')+"\">";
				tbody += "<input type=\"hidden\" name=\"subtotal_val[]\" value=\""+menuBox.attr('data-price')+"\">";
				tbody += "<input type=\"hidden\" name=\"net_price[]\" value=\""+menuBox.attr('data-netto')+"\">";
				tbody += "</td>";
				tbody += "<td id=\"td_qty\">1</td>";
				tbody += "<td id=\"td_price\">"+menuBox.attr('data-price')+"</td>";
				tbody += "<td id=\"td_subtotal\">"+menuBox.attr('data-price')+"</td>";
				tbody += "<td style=\"text-align:center;\"><button class='remove btn btn-sm btn-danger'><i class=\"fa fa-trash\"></i></button>";
				tbody += "<button class='minus btn btn-sm btn-warning'><i class=\" fa fa-minus\"></i></button>";
				tbody += "<button class='plus btn btn-sm btn-success'><i class=\" fa fa-plus\"></i></button></td>";
				tbody += "</tr>";
			grand_total += parseInt(menuBox.attr('data-price'));
			$('#grandtotal').val(grand_total.toFixed(2));
			$('#tbody').append(tbody);
			$('#subtotal').val('');
			$('#subtotal_val').val('');
			$('#price').val('');
			$('#price_val').val('');
			$('#qty_order').val('');
			$("#addMenu").attr('disabled',true);
			$("#putOrder").attr('disabled',false);
			orderList[id_menu] = menuBox.attr('data-price');
		} else if(orderList[id_menu] > 0){
		console.log('id menu : '+orderList);			
			console.log('Search item '+$('#tbody').find('tr[id='+id_menu+']').find('input[name^="subtotal_val[]"]').val());
			var qty = $('#tbody').find('tr[id='+id_menu+']').find('td[id=td_qty]').html();
			var sub_total = $('#tbody').find('tr[id='+id_menu+']').find('td[id=td_subtotal]').html();
			var newsubtotal = menuBox.attr('data-price') * (parseInt(qty) + 1);
			$('#tbody').find('tr[id='+id_menu+']').find('input[name^="qty_order[]"]').val(parseInt(qty) + 1);
			$('#tbody').find('tr[id='+id_menu+']').find('input[name^="subtotal_val[]"]').val(newsubtotal);
			$('#tbody').find('tr[id='+id_menu+']').find('td[id=td_qty]').html(parseInt(qty) + 1);
			$('#tbody').find('tr[id='+id_menu+']').find('td[id=td_subtotal]').html(newsubtotal);
			grand_total += parseInt(menuBox.attr('data-price'));
			$('#grandtotal').val(grand_total.toFixed(2));
			orderList[id_menu] = newsubtotal;
		}
		
	});
	
	$("body").on('click','.minus', function(e){
		e.preventDefault();

		var removeItem = $(this).closest('tr').attr('id');
		var qty = $('#tbody').find('tr[id='+removeItem+']').find('td[id=td_qty]').html();
		var sub_total = $('#tbody').find('tr[id='+removeItem+']').find('td[id=td_subtotal]').html();
		if((parseInt(qty) - 1) <= 0){
			$(this).closest('tr').remove();
			grand_total = grand_total - orderList[removeItem];
			$('#grandtotal').val(grand_total.toFixed(2));
			delete orderList[removeItem];
		} else {
			var price = $('#tbody').find('tr[id='+removeItem+']').find('td[id=td_price]').html();
			var newsubtotal = price * (parseInt(qty) - 1);
			$('#tbody').find('tr[id='+removeItem+']').find('input[name^="qty_order[]"]').val(parseInt(qty) - 1);
			$('#tbody').find('tr[id='+removeItem+']').find('input[name^="subtotal_val[]"]').val(newsubtotal);
			$('#tbody').find('tr[id='+removeItem+']').find('td[id=td_qty]').html(parseInt(qty) - 1);
			$('#tbody').find('tr[id='+removeItem+']').find('td[id=td_subtotal]').html(newsubtotal);
			grand_total -= parseInt(price);
			$('#grandtotal').val(grand_total.toFixed(2));	
			orderList[removeItem] = newsubtotal;		
		}

	});
	
	$("body").on('click','.plus', function(e){
		e.preventDefault();

		var removeItem = $(this).closest('tr').attr('id');
		var qty = $('#tbody').find('tr[id='+removeItem+']').find('td[id=td_qty]').html();
		var sub_total = $('#tbody').find('tr[id='+removeItem+']').find('td[id=td_subtotal]').html();

		var price = $('#tbody').find('tr[id='+removeItem+']').find('td[id=td_price]').html();
		var newsubtotal = price * (parseInt(qty) + 1);
		$('#tbody').find('tr[id='+removeItem+']').find('input[name^="qty_order[]"]').val(parseInt(qty) + 1);
			$('#tbody').find('tr[id='+removeItem+']').find('input[name^="subtotal_val[]"]').val(newsubtotal);
		$('#tbody').find('tr[id='+removeItem+']').find('td[id=td_qty]').html(parseInt(qty) + 1);
		$('#tbody').find('tr[id='+removeItem+']').find('td[id=td_subtotal]').html(newsubtotal);
		grand_total += parseInt(price);
		$('#grandtotal').val(grand_total.toFixed(2));
		orderList[removeItem] = newsubtotal;		
	});
	
	$('body').on('change', 'select#menu_name', function(e){
		e.preventDefault();	
		var menuid = $('select#menu_name').val();
		if(menuid !== ''){
			$.post(posturl+'/detailmenu?id='+menuid, function( jsonData ) {
				var json = jQuery.parseJSON(jsonData);
				if(json.status){			
					$('#name_menu').val(json.name);
					$('#id_menu').val(json.id);
					$('#price').val(json.selling_price);
					$('#price_val').val(json.selling_price);
					$('#net_price').val(json.net_price);
				} else {
					alert("Something wrong with System, Please Try Again!");	
				}
			});
		}
	});
	
	$('body').on('click', '#putOrder', function(e){
		e.preventDefault();
		var form = $('#menuForm').serialize();
		$.ajax({
			type: "POST",
			url: posturl+'/create',
			dataType: 'json',
			cache: false,
			data: form,
			success: function (json) {
				if(json.status){	
					/*
					$( '#menuForm' ).reset();
					
					$( '#menuForm' ).each(function(){
						this.reset();
					});
					
					document.getElementById('printFrame').src = '../recipe/recipe'+json.id+'.pdf';
					print('../recipe/recipe'+json.id+'.pdf'); 
					*/
					orderList = [];	
					grand_total = 0;

					$('#tbody').html('');
					$("#addMenu").attr('disabled',true);
					$("#putOrder").attr('disabled',true);
				} else {
					
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log(xhr.status);
				console.log(xhr.responseText);
				console.log(thrownError);
			}
		});
	});	
	
	$("body").on('click','#reset', function(e){
		e.preventDefault();
		/*
		$( '#menuForm' ).reset();
		
		$( '#menuForm' ).each(function(){
			this.reset();
		});
		*/
		$('#tbody').html('');
		$("#addMenu").attr('disabled',true);
		$("#putOrder").attr('disabled',true);

	});
	


}); 
JS;
$this->registerJsFile("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/moment.min.js");
$this->registerCssFile(Yii::$app->getUrlManager()->getBaseUrl() . "/web/theme/vendor/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css");
$this->registerJsFile(Yii::$app->getUrlManager()->getBaseUrl() . "/web/theme/vendor/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js");
$this->registerJs($script, \yii\web\View::POS_READY);
?>
