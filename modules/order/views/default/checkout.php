<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

use app\models\FnbMenu;
use app\models\FnbSaleDetail;
use app\models\TipeBayar;
use app\models\Bank;
use app\models\Customer;
use app\models\Merchant;

$default = Merchant::find()->one();

$this->title = 'Check Out';

?>
<div class="card bg-white">
    <div class="col-md-12">
        <div class="portlet light portlet-fit bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-basket font-red"></i>
                    <span class="caption-subject font-red sbold"><?= Html::encode($this->title) ?> <small>**Please review the Order before Check out</small></span>
                </div>
<!--                <div class="actions">-->
<!--                    <div class="btn-group btn-group-devided">-->
<!--                        <a href="--><?php //= Yii::$app->params['baseURL'] ?><!--/daily/journal/new" class="btn green-turquoise btn-sm"><i class="icon-plus"></i> --><?//= Yii::t('app', 'New Transaction')?><!--</a>-->
<!--                    </div>-->
<!--                </div>-->
            </div>
            <div class="portlet-body">

                <?php $form = ActiveForm::begin([
                            'id' => 'menuForm'
                        ]); ?>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?= $form->field($model, 'table_no')->hiddenInput() ?>
									<?= $form->field($model, 'id')->hiddenInput(['value'=>$model->id,'id'=>'saleId']) ?>
                                    <?= $model->table_no ?>
                                </div>
                            </div>
							<div class="col-md-4" id="customerWrapper">
                                <?php
                                $data2 = ArrayHelper::map(Customer::find()
									->select(['customer.*, CONCAT(kode," - ",nama) as item'])
									->orderBy('kode ASC')->all(), 'id', 'item');
                                echo $form->field($model, 'id_guest')->widget(Select2::classname(), [
                                    'data' => $data2,
                                    'value' => 1,
                                    'size' => Select2::SMALL,
                                    'options' => ['placeholder' => 'Pilih Customer', 'id' => 'customerList', 'value' => $default->id_customer],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ])->label('Customer '.'<small> **Customer untuk menyesuaikan Disc(%)</small>');
                                ?>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>
                                        Menu Name
                                    </th>
                                    <th>
                                        Qty
                                    </th>
                                    <th>
                                        Price
                                    </th>
                                    <th>
                                        Sub Total
                                    </th>
                                    <th class="col-md-1">

                                    </th>
                                </tr>
                            </thead>
                            <tbody id="tbody">
                                <?php
                                    $orderQuery = FnbSaleDetail::find()->where(['id_sale' => $model->id])->all();
									$totalOrder = 0;
                                    foreach($orderQuery as $order){
										$totalOrder += $order->total_price;
										
                                    echo "<tr id=\"".$order->id_menu."\">
                                    <td>".$order->menu->name."
                                    <input type=\"hidden\" name=\"id_menu[]\" value=\"".$order->id_menu."\">
                                    <input type=\"hidden\" name=\"qty_order[]\" value=\"".$order->qty."\">
                                    <input type=\"hidden\" name=\"price_val[]\" value=\"".$order->price."\">
                                    <input type=\"hidden\" name=\"subtotal_val[]\" value=\"".$order->total_price."\">
                                    <input type=\"hidden\" name=\"net_price[]\" value=\"".$order->production_price."\">
                                    </td>
                                    <td>".$order->qty."</td>
                                    <td class = \"text-right\">".number_format($order->price,2,",",".")."</td>
                                    <td class = \"text-right\">".number_format($order->total_price,2,",",".")."</td>
                                    <td><button class=\"remove btn btn-sm btn-danger\"><i class=\"fa fa-trash\"></i></button></td>
                                    </tr>";
                                ?>

                                <?php
                                    }
									$ppn = $totalOrder*10/100;
									$service =$totalOrder * $default->service_charge/100;
									$grandTotal = $ppn+$service + $totalOrder;
                                ?>
								<tr>
									<td colspan='3' class = 'text-right'><strong>Total</strong></td>
									<td class = 'text-right' class='totalOrder'>
										<label class="total"><?=number_format($totalOrder,2,",",".")?></label>
                                            <?= Html::activeHiddenInput($model, 'total_sale', ['class' => 'form-control', 'id' => 'totalOrder']) ?>
											
									<!--?= number_format($totalOrder,2,",",".")?-->
									
									</td>
									<td></td>
								</tr>
								<tr>
									<td colspan="3" class = "text-right"><strong>Disc Member</strong></td>
									<td class = "text-right">
										<label class="discMember">0</label>
										<?= Html::hiddenInput('discMember', null, ['class' => 'form-control', 'id' => 'discMember']) ?>
									</td>
									</td>
								</tr>
								<tr>
									<td colspan='3' class = 'text-right'><strong>PPN</strong></td>
									<td class = "text-right">
										<label class="tax"><?= number_format($ppn,2,",",".")?></label>
										<?= Html::activeHiddenInput($model, 'tax', ['class' => 'form-control', 'id' => 'tax','value' => $ppn]) ?>
									</td>
									<td></td>
								</tr>
								<tr>
									<td colspan='3' class = 'text-right'><strong>Service Charge</strong></td>
									<td class = "text-right">
										<label class="service"><?= number_format($service,2,",",".")?></label>
										<?= Html::activeHiddenInput($model, 'service', ['class' => 'form-control', 'id' => 'service','value'=>$service]) ?>
									</td>
									<td></td>
								</tr>
								<tr>
									<td colspan='3' class = 'text-right'><strong>Grand Total</strong></td>
									<td class = 'text-right'><strong><?= number_format($grandTotal,2,",",".")?></strong></td>
									<td></td>
								</tr>
								<tr>
									<td colspan="3" class = "text-right"><strong>Tipe Bayar</strong></td>
									<td class = "text-right">
										<?=
										Html::activeRadioList($model, 'id_tipe_bayar', ArrayHelper::map(TipeBayar::find()->orderBy('nama')->all(), 'id', 'nama'), ['id' => 'tipeBayar', 'value' => 3]);
										?>
										
										<div class="row">
											<div class="form-group col-md-4 haveBank">
												<label>Bank</label>
												<?= Html::activeRadioList($model, 'id_bank', ArrayHelper::map(Bank::find()->orderBy('nama')->all(), 'id', 'nama'), ['id' => 'tipeBank']); ?>
											</div>
											<div class="form-group col-md-4 haveBank">
												<label>Card Number</label>
												<?= Html::activeTextInput($model, 'card_number', ['class' => 'form-control input-sm',]) ?>
											</div>
										</div>
										
									</td>
									<td></td>
								</tr>
								
								<tr>
									<td colspan="3" class = "text-right"><strong>Bayar</strong></td>
									<td class ="geesmasking text-right"><input name="jumlahBayar[]" type="text" value="" type="text" class="isNumber jumlahBayar"></td>
									<td></td>
								</tr>
								 <tr>
									<td colspan="3" class = "text-right"><strong>Kembalian</strong></td>
									<td class = "text-right">
										<label class="kembalian">0</label>
										<?= Html::hiddenInput('kembalian', null, ['class' => 'form-control', 'id' => 'kembalian']) ?>
									</td>
								</tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="form-group">
                    <button class="btn btn-success" id="checkout">Check Out</button>&nbsp;
                    <a href="<?= Yii::$app->getUrlManager()->getBaseUrl()."/order/default/editorder/?id=".$model->id ?>" class="btn btn-warning">Edit Order</a>&nbsp;
                    <a href="<?= Yii::$app->getUrlManager()->getBaseUrl()."/order/default/index" ?>" class="btn btn-danger">Back</a>&nbsp;
                </div>

                <?php ActiveForm::end(); ?>
            </div>
		</div>

	</div>
</div>

<script>
var orderList = [];	
<?php
	$orderQuery = FnbSaleDetail::find()->where(['id_sale' => $model->id])->all();
	foreach($orderQuery as $order){	
?>
	orderList[<?= $order->id_menu?>] = <?= $order->total_price ?>;
<?php
	}
?>	
var posturl = "<?= Yii::$app->urlManager->createUrl('order') ?>";
var currentGrandtotal = <?= $model->total_sale ?>;
</script>
<?php
$script = <<< JS
$(document).ready(function(){
var onXHR = false;
	$('.haveBank').hide();
	
	$('body').on('click', "input[name='FnbSale[id_tipe_bayar]']", function () {
        var value = $(this).val();
        // var haveSurcharge = $('.haveSurcharge');
        var haveBank = $('.haveBank');
        tipebayar = value;
        if (value == 1 || value == 2) {
            haveBank.show();
            // haveSurcharge.hide();
            if (value == 1) {
                // haveSurcharge.show();
            }
        } else {
            // haveSurcharge.hide();
            haveBank.hide();
        }
    });

	$("body").on('change', '#customerList', function () {
		var id = $(this).val();
		var total = parseInt($('#totalOrder').val());
		var ppn = parseInt($('#tax').val());
		var serviceCharge = parseInt($('#service').val());
		console.log('serviceCharge : '+serviceCharge);
		if (id !== '') {
			if (!onXHR) {
				onXHR = true;
				$.ajax({
					method: "GET",
					url: baseUrl + "order/getdiskon?id=" + id,
					dataType: 'json',
					cache: false,
					success: function (data) {
						diskon = data.diskon;
						var disc = total * diskon/100;
						var grandTotal = parseInt(total-disc+ppn+serviceCharge);
						console.log('grandTotal : '+ grandTotal);
						$('.discMember').html(disc);
						$('#discMember').val(disc);
						onXHR = false;
					},
					error: function () {
						onXHR = false;
					}
				});
			}
		}
	});
	
	$("body").on('click','.remove', function(e){
		e.preventDefault();
		$(this).closest('tr').remove();
		var removeItem = $(this).closest('tr').attr('id');
		
		grand_total = grand_total - orderList[removeItem];
		$('#grandtotal').val(grand_total.toFixed(2));
		delete orderList[removeItem];
		$("#putOrder").attr('disabled',false);
		$("#addMenu").fadeIn();
	});
	
	$('body').on('keyup', '#qty_order', function(){
		var qty = parseInt($('#qty_order').val());
		var price =  $('#price').val();			
		if(qty !== '' &&  qty !== '0'){
			$('#subtotal').val((price * qty).toFixed(2));
			$('#subtotal_val').val((price * qty).toFixed(2));
			$("#addMenu").attr('disabled',false);
		}
	});	
	
    $("#addMenu").on('click', function(e){
		e.preventDefault();	
		var id_menu = $('#id_menu').val();
		orderList[id_menu] = $('#subtotal_val').val();
		var tbody = "<tr id=\""+id_menu+"\">";
			tbody += "<td>"+$('#name_menu').val();
			tbody += "<input type=\"hidden\" name=\"id_menu[]\" value=\""+$('#id_menu').val()+"\">";
			tbody += "<input type=\"hidden\" name=\"qty_order[]\" value=\""+$('#qty_order').val()+"\">";
			tbody += "<input type=\"hidden\" name=\"price_val[]\" value=\""+$('#price_val').val()+"\">";
			tbody += "<input type=\"hidden\" name=\"subtotal_val[]\" value=\""+$('#subtotal_val').val()+"\">";
			tbody += "<input type=\"hidden\" name=\"net_price[]\" value=\""+$('#net_price').val()+"\">";
			tbody += "</td>";
			tbody += "<td>"+$('#qty_order').val()+"</td>";
			tbody += "<td>"+$('#price').val()+"</td>";
			tbody += "<td>"+$('#subtotal_val').val()+"</td>";
			tbody += "<td><a href=\"#\"><i class=\"remove fa fa-trash\"></i></a></td>";
			tbody += "</tr>";
		grand_total += parseInt($('#subtotal_val').val());
		$('#grandtotal').val(grand_total.toFixed(2));
		$('#tbody').append(tbody);
		$('#subtotal').val('');
		$('#subtotal_val').val('');
		$('#price').val('');
		$('#price_val').val('');
		$('#qty_order').val('');
		$("#addMenu").attr('disabled',true);
		$("#putOrder").attr('disabled',false);
	});
	
	$('body').on('change', 'select#menu_name', function(e){
		e.preventDefault();	
		var menuid = $('select#menu_name').val();
		if(menuid !== ''){
			$.post(posturl+'/detailmenu?id='+menuid, function( jsonData ) {
				var json = jQuery.parseJSON(jsonData);
				if(json.status){			
					$('#name_menu').val(json.name);
					$('#id_menu').val(json.id);
					$('#price').val(json.selling_price);
					$('#price_val').val(json.selling_price);
					$('#net_price').val(json.net_price);
				} else {
					alert("Something wrong with System, Please Try Again!");	
				}
			});
		}
	});
	
	$('body').on('click', '#checkout', function(e){
		e.preventDefault();
		var form = $('#menuForm').serialize();
		var id = $('#saleId').val();
		$.ajax({
			type: "POST",
			url: posturl+'/checkout?id='+id,
			dataType: 'json',
			cache: false,
			data: form,
			success: function (json) {
				if(json.status){	
					orderList = [];	
					grand_total = 0;

					$('#tbody').html('');
					$("#addMenu").attr('disabled',true);
					$("#putOrder").attr('disabled',true);
				} else {
					
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log(xhr.status);
				console.log(xhr.responseText);
				console.log(thrownError);
			}
		});
	});	
	
	$('body').on('click', '#putOrder', function(e){
		e.preventDefault();
		var form = $('#menuForm').serialize();
		$.ajax({
			type: "POST",
			//url: posturl,
			dataType: 'json',
			cache: false,
			data: form,
			success: function (json) {
				if(json.status){	
					/*
					$( '#menuForm' ).reset();
					
					$( '#menuForm' ).each(function(){
						this.reset();
					});
					
					document.getElementById('printFrame').src = '../recipe/recipe'+json.id+'.pdf';
					print('../recipe/recipe'+json.id+'.pdf'); 
					*/
					orderList = [];	
					grand_total = 0;

					$('#tbody').html('');
					$("#addMenu").attr('disabled',true);
					$("#putOrder").attr('disabled',true);
				} else {
					
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log(xhr.status);
				console.log(xhr.responseText);
				console.log(thrownError);
			}
		});
	});
}); 
JS;
$this->registerJs($script, \yii\web\View::POS_READY);
?>

