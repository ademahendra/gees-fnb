<?php

namespace app\modules\order\controllers;

use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\VerbFilter;

use Yii;

use app\models\FnbSale;
use app\models\FnbSaleDetail;
use app\models\FnbIngredients;
use app\models\FnbStock;
use app\models\FnbMenu;
use app\models\FnbSaleSearch;
use app\models\Customer;

class DefaultController extends Controller
{
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
			'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all FnbSale models.
     * @return mixed
     */
    public function actionIndex()
    {
        
		$query = FnbSale::find()
			->where(['status' => 0]);
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => false,
		]);
		$dataProvider->pagination->pageSize= 40;
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FnbSale model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FnbSale model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FnbSale();
        $menu = new FnbMenu();

        if ($model->load(Yii::$app->request->post())) {           
			$post = $_POST;
			$sale = new FnbSale();
			//$sale->id_guest = $post['FnbSale']['id_guest'];
			$sale->no_of_guest = $post['FnbSale']['no_of_guest'];			
			$sale->table_no = $post['table_no'];
			$sale->date_sale = new \yii\db\Expression('NOW()');
			$sale->total_sale = $post['FnbSale']['total_sale'];
			//$sale->hotel_id = Yii::$app->user->identity->hotel_id;
			//$sale->default_currency
			//$sale->currency
			//$sale->currency_rate
			$sale->tax = 10/100 * $post['FnbSale']['total_sale'];
			$sale->service = 11/100 * $post['FnbSale']['total_sale'];
			$sale->updateby = Yii::$app->user->identity->id;
			$sale->lastupdate = new \yii\db\Expression('NOW()');
			$data = [];
			
			if($sale->save()){
				for($i = 0; $i < count($post['id_menu']); $i++){
					$saleDetail = new FnbSaleDetail();	
					$saleDetail->id_sale = $sale->id;
					$saleDetail->id_menu = $post['id_menu'][$i];
					$saleDetail->qty = $post['qty_order'][$i];
					$saleDetail->price = $post['price_val'][$i];
					$saleDetail->production_price = $post['net_price'][$i];
					$saleDetail->total_price = $post['subtotal_val'][$i];
					//$saleDetail->hotel_id = Yii::$app->user->identity->hotel_id;
					//$saleDetail->default_currenct = 
					//$saleDetail->currency =
					//$saleDetail->currency_rate =
					$saleDetail->updateby = Yii::$app->user->identity->id;
					$saleDetail->lastupdate = new \yii\db\Expression('NOW()');
					$saleDetail->save();
				}	
				$data['status'] = true;
			} else {
				$data['status'] = false;
			}
			echo json_encode($data);
        } else {
            return $this->render('create', [
                'model' => $model,
                'menu' => $menu,
            ]);
        }
    }

    /**
     * Updates an existing FnbSale model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }    
	
    public function actionEditorder($id)
    {
        $model = $this->findModel($id);
		$menu = new FnbMenu();
        if ($model->load(Yii::$app->request->post())) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'menu' => $menu,
            ]);
        }
    }	
	
    public function actionCheckout($id)
    {
	    $model = $this->findModel($id);
		$menu = new FnbMenu();
		// $sale = FnbSale::find()
			// ->where([
				// 'id' =>$id,
			// ])
			// ->one();
		// $sale->status = 1;
		// $sale->save();
	    return $this->render('checkout', [
            'model' => $model,
            'menu' => $menu,
        ]);   
    }
	
	

    /**
     * Deletes an existing FnbSale model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionGetdiskon($id)
	{
		$customer = Customer::find()
			->where([
				'id' => $id
			])
			->one();
		$data = [];
		$data['diskon'] = isset($customer->customerType->diskon) ? $customer->customerType->diskon : 0;
        return json_encode($data);
	}
	
    protected function findModel($id)
    {
        if (($model = FnbSale::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
    protected function findMenu($id)
    {
        if (($model = FnbMenu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
