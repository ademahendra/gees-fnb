<?php

use johnitvn\ajaxcrud\BulkButtonWidget;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\grid\GridView;
use app\models\FnbMenu;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FnbIngredientsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Menu';
$this->params['breadcrumbs'][] = $this->title;
CrudAsset::register($this);
?>
<div class="panel panel-danger fnb-stock-create">

    <div class="panel-heading"><?= Html::encode($this->title) ?></div>
	<div class="panel-body">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('New Menu', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'pjax'=>true,
		'striped'=>false,
		'hover'=>true,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
			[
				'class' => 'kartik\grid\ExpandRowColumn',
				'expandAllTitle' => 'Expand All',
				'collapseTitle' => 'Collapse All',
				'expandTitle' => 'Expand',
				'expandIcon' => '<span class="fa fa-angle-up"></span>',
				'collapseIcon' => '<span class="fa fa-angle-down"></span>',
				'value' =>     function ($model, $key, $index) { 
					return GridView::ROW_COLLAPSED;
				},
				'detailRowCssClass' => GridView::TYPE_DEFAULT,
				'pageSummary' => false,
				'detailUrl' => Url::to('menudetail'),
				'detailAnimationDuration' => 100,
				'expandOneOnly' => true
			],
            //'id',
            [
				'attribute' => 'name',
				'label' => 'Menu',
				'value' => 'name',
			],
            [
				'attribute' => 'category.name',
				'label' => 'Category',
				'value' => function($model){
                    return $model->categories->name;
                },
			],
            //'stock_id',
            //'qty',
            [
				'attribute' => 'selling_price',
				'label' => 'Harga',
				'value' => 'selling_price',
			],
            // 'updateby',
            // 'lastupdate',

            [
                'class' => '\kartik\grid\DataColumn',
                'format' => 'raw',
                'vAlign' => 'middle',
                'value' => function ($data) {
                    $view = Html::a('<button class="btn btn-success btn-xs"><span class="fa fa-eye"></span></button> ', Url::to(['view', 'id' => $data->id]), [
                        'title' => Yii::t('yii', 'View'),
                        'data-pjax' => 0,
                        'role' => 'modal-remote',
                        'data-toggle' => 'tooltip'
                    ]);
                    $update = Html::a('<button class="btn btn-warning btn-xs"><span class="fa fa-edit"></span></button> ', Url::to(['update', 'id' => $data->id]), [
                        'title' => Yii::t('yii', 'Update'),
                        'data-pjax' => 0,
//                        'role' => 'modal-remote',
                        'data-toggle' => 'tooltip'
                    ]);
                    $delete = Html::a('<button class="btn btn-danger btn-xs"><span class="fa fa-trash"></span></button> ', Url::to(['delete', 'id' => $data->id]), [
                        'title' => Yii::t('yii', 'Delete'),
                        'data-pjax' => 0,
                        'role' => 'modal-remote',
                        'data-toggle' => 'tooltip',
                        'data-request-method' => 'post',
                        'data-confirm-title' => 'Are you sure?',
                        'data-confirm-message' => 'Are you sure want to delete this item'
                    ]);
                    return $view.$update.$delete;
                }
            ],
        ],
    ]); ?>

</div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>