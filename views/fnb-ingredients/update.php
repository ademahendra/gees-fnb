<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FnbIngredients */

$this->title = 'Update Fnb Ingredients: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Fnb Ingredients', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
    <?= $this->render('_form', [
        'model' => $model,
        'menu' => $menu,
    ]) ?>
