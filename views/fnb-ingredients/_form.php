<?php

use app\models\FnbCategory;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\FnbMenu;
use app\models\FnbStock;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\FnbIngredients */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="card bg-white">
    <div class="col-md-12">
        <!-- BEGIN BORDERED TABLE PORTLET-->
        <div class="portlet light portlet-fit bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-notebook font-red"></i>
                    <span class="caption-subject font-red sbold uppercase"><?= $this->title ?></span>
                </div>
                <div class="actions">
                    <div class="btn-group btn-group-devided">
                        <a href="<?= Yii::$app->params['baseURL'] ?>/daily/journal/new" class="btn green-turquoise btn-sm"><i class="icon-plus"></i> <?= Yii::t('app', 'New Transaction')?></a>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
	<?php $form = ActiveForm::begin([
		'id' => 'menuForm'
	]); ?>
	<div class="panel panel-color panel-warning">
		<div class="panel-heading">
				<h4 class="panel-title">Menu Information</h4>
			</div>
		<div class="panel-body">
			<div class="form-group row">
				<div class="col-md-2">
					<?= Html::label('Menu Name', null, ['class' => 'control-label']) ?>
				</div>
				<div class="col-md-6">
					<?= Html::activeTextInput($menu, 'name', ['class' => 'form-control input-sm']) ?>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-md-2"><?= Html::label('Category', null, ['class' => 'control-label']) ?></div>
				<div class="col-md-6">
					<?= Html::activeDropDownList($menu, 'category_id', ArrayHelper::map(FnbCategory::find()->orderBy('name ASC')->all(), 'id', 'name'), ['class' => 'form-control','id' => 'category_id','prompt'=>'-Select Category-']) ?>
					<?= Html::hiddenInput('category_id', NULL, ['class' => 'form-control input-sm','id' => 'category_id']) ?>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-md-2"><?= Html::label('Ingredients', null, ['class' => 'control-label']) ?></div>
				<div class="col-md-6">
					<?= Html::activeDropDownList($model, 'stock_id', ArrayHelper::map(FnbStock::find()->orderBy('name ASC')->all(), 'id', 'name'), ['class' => 'form-control','id' => 'stock_id','prompt'=>'-Select Ingredient-']) ?>
					<?= Html::hiddenInput('stock_name', NULL, ['class' => 'form-control input-sm','id' => 'stock_name']) ?>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-md-2">
					<?= Html::label('Price / Unit', null, ['class' => 'control-label']) ?>
				</div>
				<div class="col-md-3">
					<?= Html::activeTextInput($model, 'price', ['class' => 'form-control input-sm','disabled' => 'true','id' => 'stock_price2']) ?>
				</div>
				<div class="col-md-3">
					<?= Html::TextInput('unit', NULL, ['class' => 'form-control input-sm','disabled' => 'true', 'id' => 'stock_unit' ]) ?>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-md-2">
					<?= Html::label('Amount', null, ['class' => 'control-label']) ?>
				</div>
				<div class="col-md-3">
					<?= Html::activeTextInput($model, 'qty', ['class' => 'form-control input-sm']) ?>
					<?= Html::activeHiddenInput($model, 'price', ['class' => 'form-control','id' => 'stock_price']) ?>
				</div>
				<div class="col-md-3">
					<?= Html::label('', null, ['class' => 'control-label', 'id' => 'stock_unit2']) ?>
				</div>
			</div>

			<div class="form-group row">
				<div class="col-md-12">
					<div class="form-group">
						<?= Html::a('Add', ['#'], ['class' => 'btn btn-warning btn-sm' , 'id' => 'addbutton']) ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-color panel-info">
		<div class="panel-heading">			
				<h4 class="panel-title">Ingredients List</h4>
			</div>
		<div class="panel-body">
			<div class="row">
				<div class="form-group">
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>
									Ingredient Name 
								</th>
								<th>
									Qty
								</th>
								<th>
									Price
								</th>
								<th>
									Sub Total
								</th>
								<th class="col-md-1">
								
								</th>
							</tr>
						</thead>
						<tbody id="tbody">
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-color panel-success">
		<div class="panel-heading">			
				<h4 class="panel-title">Selling Price</h4>
			</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<div class="row">				
							<div class="col-md-2">
								<?= Html::label('Production Price', null, ['class' => 'control-label']) ?>
							</div>
							<div class="col-md-4">
								<?= Html::textInput('netCost', null, ['class' => 'form-control','id' => 'netcost','readonly' => 'true']) ?>
							</div>				
							<div class="col-md-2">
								<?= Html::label('Selling Price', null, ['class' => 'control-label']) ?>
							</div>
							<div class="col-md-4">
								<?= Html::textInput('sellingprice', null, ['class' => 'form-control']) ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
    <div class="form-group">
        <?= Html::button('Save' , ['class' => 'btn btn-success', 'id' => 'save']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
	var posturl = "<?= Yii::$app->urlManager->createUrl('fnb%2Dingredients') ?>";
</script>

<?php
$script = <<< JS
	var netcost = 0;
	$("body").on('change','#stock_id', function(e){		
		e.preventDefault();
		var id = $('#stock_id').val();
		$.ajax({
			type: "POST",
			url: posturl+'/detail',
			data: {'id':id},
			cache: false,
			dataType: 'json',
			success: function (json) {
				if(json.status){	
					$('#stock_unit').val(json.unit);
					$('#stock_unit2').html(json.unit);
					$('#stock_price').val(json.price);
					$('#stock_price2').val(json.price);
					$('#stock_name').val(json.name);
				} else {
					alert('Something wrong when retriving stock data!');
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log(xhr.status);
				console.log(xhr.responseText);
				console.log(thrownError);
			}
		});
	});	
	
	$("body").on('click','#save', function(e){		
		e.preventDefault();
		var form = $('#menuForm').serialize();
		
		$.ajax({
			type: "POST",			
			data: form,
			cache: false,
			dataType: 'json',
			success: function (json) {
				if(json.status){	
					netcost = 0;
					$( '#menuForm' ).each(function(){
						this.reset();
						$('#tbody').html('');
					});
				} else {
					alert('Something wrong when saving a new menu!');
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log(xhr.status);
				console.log(xhr.responseText);
				console.log(thrownError);
			}
		});
	});	
	
	$("#addbutton").on('click', function(e){
		e.preventDefault();
		var tr = "<tr>";
			tr += "<td>"+$('#stock_name').val();
			tr += "<input type=\"hidden\" value=\""+$('#stock_id').val()+"\" name=\"id[]\">";	
			tr += "<input type=\"hidden\" value=\""+$('#fnbingredients-qty').val()+"\" name=\"qty[]\">";	
			tr += "<input type=\"hidden\" value=\""+$('#stock_price2').val()+"\" name=\"price[]\">";	
			tr += "<input type=\"hidden\" value=\""+($('#fnbingredients-qty').val() * $('#stock_price2').val())+"\" name=\"subtotal[]\">";				
			tr += "</td>";
			tr += "<td>"+$('#fnbingredients-qty').val()+" "+$('#stock_unit').val()+"</td>";
			tr += "<td>"+$('#stock_price2').val()+"</td>";
			tr += "<td>"+($('#fnbingredients-qty').val() * $('#stock_price2').val())+"</td>";
			tr += "<td><i class=\"fa fa-trash\" class=\"remove\"></i></td>";
		tr += "</tr>";
		netcost += ($('#fnbingredients-qty').val() * $('#stock_price2').val());
		$('#netcost').val(netcost);
		
		$('#tbody').append(tr);
		$('#fnbingredients-qty').val('');
		$('#stock_price2').val('');
	});	
JS;
$this->registerJs($script, \yii\web\View::POS_READY);

?>
