<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;

?>
<div class="fnb-ingredients-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
		'pjax'=>true,
		'striped'=>false,
		
		'hover'=>true,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],		
            //'id',
           
            [
				'attribute' => 'stock_id',
				'label' => 'Ingredients Name',
				'value' => 'stock.name',
			],
            [
				'attribute' => 'qty',
				'label' => 'Qty',
				'value' => function($data){
					return $data->qty." ".$data->stock->unit;
				},
			],
            'price',
            // 'updateby',
            // 'lastupdate',

            //['class' => 'kartik\grid\ActionColumn'],
        ],
    ]); ?>

</div>
