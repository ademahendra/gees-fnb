<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FnbStockSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Stocks';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card bg-white fnb-category-index">
    <div class="col-md-12">
        <div class="portlet light portlet-fit bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-layers font-red"></i>
                    <span class="caption-subject font-red sbold uppercase"><?= Html::encode($this->title) ?></span>
                </div>
                <div class="actions">
                    <div class="btn-group btn-group-devided">
                        <?= Html::a('<i class=\"icon-plus\"></i> Add Stock', ['create'], ['class' => 'btn green-turquoise btn-sm']) ?>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                <div class="table-scrollable table-scrollable-borderless">
                    <div id="ajaxCrudDatatable">

                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            // 'filterModel' => $searchModel,
                            'columns' => [
                                [
                                    'class' => 'kartik\grid\SerialColumn',
                                    'width' => '30px',
                                ],
                                //'id',
                                'name',
                                'unit',
                                'qty',
                                'unit_price',
                                'shelf_no',
                                // 'updateby',
                                // 'lastupdate',
                                [
                                    'class' => '\kartik\grid\DataColumn',
                                    'format' => 'raw',
                                    'vAlign' => 'middle',
                                    'value' => function ($data) {
                                        $view = Html::a('<button class="btn green-jungle btn-xs"><span class="fa fa-eye"></span></button> ', Url::to(['view', 'id' => $data->id]), [
                                            'title' => Yii::t('yii', 'View'),
                                            'data-pjax' => 0,
//                                                'role' => 'modal-remote',
                                            'data-toggle' => 'tooltip'
                                        ]);
                                        $update = Html::a('<button class="btn btn-warning btn-xs"><span class="fa fa-edit"></span></button> ', Url::to(['update', 'id' => $data->id]), [
                                            'title' => Yii::t('yii', 'Update'),
                                            'data-pjax' => 0,
//                                         'role' => 'modal-remote',
                                            'data-toggle' => 'tooltip'
                                        ]);
                                        $delete = Html::a('<button class="btn btn-danger btn-xs"><span class="fa fa-trash"></span></button> ', Url::to(['delete', 'id' => $data->id]), [
                                            'title' => Yii::t('yii', 'Delete'),
                                            'data-pjax' => 0,
                                            'role' => 'modal-remote',
                                            'data-toggle' => 'tooltip',
                                            'data-request-method' => 'post',
                                            'data-confirm-title' => 'Are you sure?',
                                            'data-confirm-message' => 'Are you sure want to delete this item'
                                        ]);
                                        return $view . $update;
                                    }
                                ],
                            ],
                        ]); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>