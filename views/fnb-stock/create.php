<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FnbStock */

$this->title = 'New Stock';
?>
<div class="panel panel-danger fnb-stock-create">

    <div class="panel-heading"><?= Html::encode($this->title) ?></div>
	<div class="panel-body">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
	</div>
</div>
