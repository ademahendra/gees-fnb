<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FnbCategory */

$this->title = 'New Food Category';
$this->params['breadcrumbs'][] = ['label' => 'Add Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

echo $this->render('_form', [
    'model' => $model,
]) ?>
