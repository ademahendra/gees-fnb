<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FnbCategory */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Fnb Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card bg-white fnb-category-index">
    <div class="col-md-12">
        <div class="portlet light portlet-fit bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-cup font-red"></i>
                    <span class="caption-subject font-red sbold uppercase"><?= Html::encode($this->title) ?></span>
                </div>
                <div class="actions">
                    <div class="btn-group btn-group-devided">
                        <?= Html::a('<i class=\"icon-layer\"></i> List Food Category', ['index'], ['class' => 'btn green-jungle btn-sm']) ?>
                        <?= Html::a('<i class=\"icon-plus\"></i> New Food Category', ['create'], ['class' => 'btn green-turquoise btn-sm']) ?>
                        <?= Html::a('<i class=\"icon-pencil\"></i> Edit', ['update', 'id' => $model->id], ['class' => 'btn yellow-gold btn-sm']) ?>
                        <?= Html::a('<i class=\"icon-trash\"></i> Delete', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-sm red',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="portlet-body">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
//                        'hotel_id',
//                        'parent',
                        'name',
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => function($model){
                                return ($model->status == 1)?'<span class="label label-success">Active</span>':'<span class="label label-danger">Non Active</span>';
                            }
                        ],
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>