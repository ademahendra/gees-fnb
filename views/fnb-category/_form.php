<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FnbCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card bg-white">
    <div class="col-md-12">
        <!-- BEGIN BORDERED TABLE PORTLET-->
        <div class="portlet light portlet-fit bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-notebook font-red"></i>
                    <span class="caption-subject font-red sbold uppercase"><?= $this->title ?></span>
                </div>
                <div class="actions">
                    <div class="btn-group btn-group-devided">
                        <?= Html::a('<i class="icon-layer"></i> List Food Category', ['index'], ['class' => 'btn green-turquoise btn-sm']) ?>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <?php $form = ActiveForm::begin(); ?>
                <?= $form->errorSummary($model) ?>
                <div style="display: none;">
                    <?= $form->field($model, 'hotel_id')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'status')->hiddenInput(['value' => 1])->label('') ?>
                    <?= $form->field($model, 'parent')->hiddenInput(['value' => 0])->label('') ?>
                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <label class="control-label">Category Name</label>
                        <?= Html::activeTextInput($model, 'name', ['maxlength' => true, 'class' => 'form-control']) ?>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary']) ?>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
