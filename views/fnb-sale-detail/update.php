<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FnbSaleDetail */

$this->title = 'Update Fnb Sale Detail: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Fnb Sale Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="fnb-sale-detail-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
