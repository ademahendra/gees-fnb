<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FnbSaleDetail */

$this->title = 'Create Fnb Sale Detail';
$this->params['breadcrumbs'][] = ['label' => 'Fnb Sale Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fnb-sale-detail-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
