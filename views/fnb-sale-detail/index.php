<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FnbSaleDetailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Fnb Sale Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fnb-sale-detail-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Fnb Sale Detail', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_sale',
            'id_menu',
            'qty',
            'price',
            // 'production_price',
            // 'total_price',
            // 'default_currenct',
            // 'currency',
            // 'currency_rate',
            // 'updateby',
            // 'lastupdate',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
