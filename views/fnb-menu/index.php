<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FnbMenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Menu';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-danger fnb-stock-create">

    <div class="panel-heading"><?= Html::encode($this->title) ?></div>
	<div class="panel-body">

	    <h1><?= Html::encode($this->title) ?></h1>
	    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	
	    <p>
	        <?= Html::a('Create Fnb Menu', ['create'], ['class' => 'btn btn-success']) ?>
	    </p>
	
	    <?= GridView::widget([
	        'dataProvider' => $dataProvider,
	        'filterModel' => $searchModel,
	        'columns' => [
	            ['class' => 'yii\grid\SerialColumn'],
	
	            'id',
	            'name',
	            'selling_price',
	            'updateby',
	            'lastupdate',
	
	            ['class' => 'yii\grid\ActionColumn'],
	        ],
	    ]); ?>

	</div>
</div>
