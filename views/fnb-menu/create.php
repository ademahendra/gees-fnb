<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FnbMenu */

$this->title = 'Create Fnb Menu';
$this->params['breadcrumbs'][] = ['label' => 'Fnb Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fnb-menu-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
