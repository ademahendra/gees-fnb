<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FnbSaleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Order List';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-danger fnb-stock-create">

    <div class="panel-heading"><?= Html::encode($this->title) ?></div>
	<div class="panel-body">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
//            'room.name',
            //'id_guest',
            'table_no',
            //'date_sale',
            // 'total_sale',
            // 'default_currency',
            // 'currency',
            // 'currency_rate',
            // 'tax',
             [
				'attribute' => 'date_sale',
				'label' => 'Date',
				'value' => function($data){
					return date('d M Y', strtotime($data->date_sale));
				},
			 ],
             [
				'attribute' => 'status',
				'label' => 'Status Order',
				'value' => function($data){
					return $data->status == 0?'Unpaid Order':'Paid';
				},
			 ],
             [
				'label' => 'Action',
				'format' => 'raw',
				'value' => function($data){
					if($data->status == 0){
						return "<a href=\"".Yii::$app->getUrlManager()->getBaseUrl()."/fnb-sale/checkout/?id=".$data->id."\" class=\"btn 		btn-success\">Paid</a>
					
							<a href=\"".Yii::$app->getUrlManager()->getBaseUrl()."/fnb-sale/editorder/?id=".$data->id."\" class=\"btn btn-warning\">Edit Order</a>";	
					}
					return "";					
				},
			 ],
            // 'updateby',
            // 'lastupdate',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

	</div>
</div>