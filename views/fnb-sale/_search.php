<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FnbSaleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fnb-sale-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_room') ?>

    <?= $form->field($model, 'id_guest') ?>

    <?= $form->field($model, 'table_no') ?>

    <?= $form->field($model, 'date_sale') ?>

    <?php // echo $form->field($model, 'total_sale') ?>

    <?php // echo $form->field($model, 'default_currency') ?>

    <?php // echo $form->field($model, 'currency') ?>

    <?php // echo $form->field($model, 'currency_rate') ?>

    <?php // echo $form->field($model, 'tax') ?>

    <?php // echo $form->field($model, 'service') ?>

    <?php // echo $form->field($model, 'updateby') ?>

    <?php // echo $form->field($model, 'lastupdate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
