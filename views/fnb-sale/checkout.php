<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\FnbMenu;
use app\models\FnbSaleDetail;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\FnbSale */

$this->title = 'Check Out';

?>
<div class="card bg-white">
    <div class="col-md-12">
        <div class="portlet light portlet-fit bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-basket font-red"></i>
                    <span class="caption-subject font-red sbold uppercase"><?= Html::encode($this->title) ?> <small>Please review the Order before Check out</small></span>
                </div>
<!--                <div class="actions">-->
<!--                    <div class="btn-group btn-group-devided">-->
<!--                        <a href="--><?php //= Yii::$app->params['baseURL'] ?><!--/daily/journal/new" class="btn green-turquoise btn-sm"><i class="icon-plus"></i> --><?//= Yii::t('app', 'New Transaction')?><!--</a>-->
<!--                    </div>-->
<!--                </div>-->
            </div>
            <div class="portlet-body">

                <?php $form = ActiveForm::begin([
                            'id' => 'menuForm'
                        ]); ?>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?= $form->field($model, 'table_no')->hiddenInput() ?>
                                    <?= $model->table_no ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 col-md-3">
                        <?= $form->field($model, 'total_sale')->textInput(['id' => 'grandtotal','readonly' => true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>
                                        Menu Name
                                    </th>
                                    <th>
                                        Qty
                                    </th>
                                    <th>
                                        Price
                                    </th>
                                    <th>
                                        Sub Total
                                    </th>
                                    <th class="col-md-1">

                                    </th>
                                </tr>
                            </thead>
                            <tbody id="tbody">
                                <?php
                                    $orderQuery = FnbSaleDetail::find()->where(['id_sale' => $model->id])->all();
                                    foreach($orderQuery as $order){

                                    echo "<tr id=\"".$order->id_menu."\">
                                    <th>".$order->menu->name."
                                    <input type=\"hidden\" name=\"id_menu[]\" value=\"".$order->id_menu."\">
                                    <input type=\"hidden\" name=\"qty_order[]\" value=\"".$order->qty."\">
                                    <input type=\"hidden\" name=\"price_val[]\" value=\"".$order->price."\">
                                    <input type=\"hidden\" name=\"subtotal_val[]\" value=\"".$order->total_price."\">
                                    <input type=\"hidden\" name=\"net_price[]\" value=\"".$order->production_price."\">
                                    </th>
                                    <th>".$order->qty."</th>
                                    <th>".$order->price."</th>
                                    <th>".$order->total_price."</th>
                                    <th><button class=\"remove btn btn-sm btn-danger\"><i class=\"fa fa-trash\"></i></button></th>
                                    </tr>";
                                ?>

                                <?php
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="form-group">
                    <button class="btn btn-success" id="checkout">Check Out</button>&nbsp;
                    <a href="<?= Yii::$app->getUrlManager()->getBaseUrl()."/fnb-sale/editorder/?id=".$model->id ?>" class="btn btn-warning">Edit Order</a>&nbsp;
                    <a href="<?= Yii::$app->getUrlManager()->getBaseUrl()."/fnb-sale/index" ?>" class="btn btn-danger">Back</a>&nbsp;
                </div>

                <?php ActiveForm::end(); ?>
            </div>
		</div>

	</div>
</div>

<script>
var orderList = [];	
<?php
	$orderQuery = FnbSaleDetail::find()->where(['id_sale' => $model->id])->all();
	foreach($orderQuery as $order){	
?>
	orderList[<?= $order->id_menu?>] = <?= $order->total_price ?>;
<?php
	}
?>	
var posturl = "<?= Yii::$app->urlManager->createUrl('pos') ?>";
var currentGrandtotal = <?= $model->total_sale ?>;
</script>
<?php
$script = <<< JS
$(document).ready(function(){
	
	var grand_total = currentGrandtotal;
	$('#grandtotal').val(grand_total);
	
	$("#addMenu").attr('disabled',true);
	$("#putOrder").attr('disabled',true);
	
	$("body").on('click','.remove', function(e){
		e.preventDefault();
		$(this).closest('tr').remove();
		var removeItem = $(this).closest('tr').attr('id');
		
		grand_total = grand_total - orderList[removeItem];
		$('#grandtotal').val(grand_total.toFixed(2));
		delete orderList[removeItem];
		$("#putOrder").attr('disabled',false);
		$("#addMenu").fadeIn();
	});
	
	$('body').on('keyup', '#qty_order', function(){
		var qty = parseInt($('#qty_order').val());
		var price =  $('#price').val();			
		if(qty !== '' &&  qty !== '0'){
			$('#subtotal').val((price * qty).toFixed(2));
			$('#subtotal_val').val((price * qty).toFixed(2));
			$("#addMenu").attr('disabled',false);
		}
	});	
	
    $("#addMenu").on('click', function(e){
		e.preventDefault();	
		var id_menu = $('#id_menu').val();
		orderList[id_menu] = $('#subtotal_val').val();
		var tbody = "<tr id=\""+id_menu+"\">";
			tbody += "<td>"+$('#name_menu').val();
			tbody += "<input type=\"hidden\" name=\"id_menu[]\" value=\""+$('#id_menu').val()+"\">";
			tbody += "<input type=\"hidden\" name=\"qty_order[]\" value=\""+$('#qty_order').val()+"\">";
			tbody += "<input type=\"hidden\" name=\"price_val[]\" value=\""+$('#price_val').val()+"\">";
			tbody += "<input type=\"hidden\" name=\"subtotal_val[]\" value=\""+$('#subtotal_val').val()+"\">";
			tbody += "<input type=\"hidden\" name=\"net_price[]\" value=\""+$('#net_price').val()+"\">";
			tbody += "</td>";
			tbody += "<td>"+$('#qty_order').val()+"</td>";
			tbody += "<td>"+$('#price').val()+"</td>";
			tbody += "<td>"+$('#subtotal_val').val()+"</td>";
			tbody += "<td><a href=\"#\"><i class=\"remove fa fa-trash\"></i></a></td>";
			tbody += "</tr>";
		grand_total += parseInt($('#subtotal_val').val());
		$('#grandtotal').val(grand_total.toFixed(2));
		$('#tbody').append(tbody);
		$('#subtotal').val('');
		$('#subtotal_val').val('');
		$('#price').val('');
		$('#price_val').val('');
		$('#qty_order').val('');
		$("#addMenu").attr('disabled',true);
		$("#putOrder").attr('disabled',false);
	});
	
	$('body').on('change', 'select#menu_name', function(e){
		e.preventDefault();	
		var menuid = $('select#menu_name').val();
		if(menuid !== ''){
			$.post(posturl+'/detailmenu?id='+menuid, function( jsonData ) {
				var json = jQuery.parseJSON(jsonData);
				if(json.status){			
					$('#name_menu').val(json.name);
					$('#id_menu').val(json.id);
					$('#price').val(json.selling_price);
					$('#price_val').val(json.selling_price);
					$('#net_price').val(json.net_price);
				} else {
					alert("Something wrong with System, Please Try Again!");	
				}
			});
		}
	});
	
	$('body').on('click', '#putOrder', function(e){
		e.preventDefault();
		var form = $('#menuForm').serialize();
		$.ajax({
			type: "POST",
			//url: posturl,
			dataType: 'json',
			cache: false,
			data: form,
			success: function (json) {
				if(json.status){	
					/*
					$( '#menuForm' ).reset();
					
					$( '#menuForm' ).each(function(){
						this.reset();
					});
					
					document.getElementById('printFrame').src = '../recipe/recipe'+json.id+'.pdf';
					print('../recipe/recipe'+json.id+'.pdf'); 
					*/
					orderList = [];	
					grand_total = 0;

					$('#tbody').html('');
					$("#addMenu").attr('disabled',true);
					$("#putOrder").attr('disabled',true);
				} else {
					
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log(xhr.status);
				console.log(xhr.responseText);
				console.log(thrownError);
			}
		});
	});	
}); 
JS;
$this->registerJs($script, \yii\web\View::POS_READY);
?>

