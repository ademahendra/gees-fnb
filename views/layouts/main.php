<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\widgets\Menu;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="<?= Yii::$app->language ?>"> <!--<![endif]-->
<head>
	<meta charset="<?= Yii::$app->charset ?>">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

	<!-- CSS
  ================================================== -->
  	<link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
  	<link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/template/styles/style.css" />
    <link rel="stylesheet" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/template/styles/inner.css" />
    <link rel="stylesheet" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/template/styles/layout.css" />
    <link rel="stylesheet" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/template/styles/flexslider.css" />
    <link rel="stylesheet" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/template/styles/color.css" />
    <link rel="stylesheet" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/template/styles/prettyPhoto.css"  media="screen" />
    <link rel="stylesheet" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/template/vendor/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/template/vendor/fontawesome/css/font-awesome.min.css"/>
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script type="text/javascript" src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/template/js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/template/js/jquery-migrate-1.2.1.min.js"></script>
	<script type="text/javascript" src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/template/vendor/bootstrap/js/bootstrap.min.js"></script>
	
	 <?php $this->head() ?>
</head>

<body>
<?php $this->beginBody() ?>
	
	<div id="bodychild">
		<div id="outercontainer">
			<div id="outerheader">
				<div class="container">            
					<header id="top">
						<div class="row">
							<div class="three columns"></div>
							<div class="six columns">
								<div class="logo-icon-l"></div>
								<div id="logo">
									<h1>Athena</h1>
									<span class="desc">Yummy</span>
								</div>
								<div class="logo-icon-r"></div>
							</div>
							<div class="three columns"></div>
							<div class="twelve columns">
								<section id="navigation">     
									<div class="nav-left"></div>
										<nav id="nav-wrap">
											<?php
											
											echo Menu::widget([
												'options' => ['id' => 'topnav', 'class' => 'sf-menu'],
												'items' => [
													['label' => 'Home', 'url' => ['/site/index']],
													['label' => 'Stock', 'url' => ['/stock/index']],
													['label' => 'Menu', 'url' => ['/menu/index']],
													['label' => 'Order', 'url' => ['/pos/create']],
													['label' => 'POS', 'url' => ['/pos/index']],
													
													Yii::$app->user->isGuest ?
														['label' => 'Login', 'url' => ['/user/security/login']] :
														[
															'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
															'url' => ['/user/security/logout'],
															'linkOptions' => ['data-method' => 'post']
														],
												],
											]);
											
											?>
										</nav>
									</section>
								</div>
							<div class="clear"></div>
							</div>
						</header>
					</div>
				</div>
				<div id="outermain">
					<div class="container">
						<?= $content ?>
					</div>
				</div>
		

			<footer  id="footer">
				<div id="outercopyright">
					<div class="container">
						<div class="row">
							<div id="copyright" class="six columns">
								&copy; Gees Studio <?= date('Y') ?>
							</div>	
						</div>	
					</div>
				</div>
			</footer>
        <div class="clear"></div><!-- clear float --> 
	</div><!-- end outercontainer -->
</div><!-- end bodychild -->

<div class="modal fade" id="modal"  role="dialog" >
	<div class="modal-dialog"  role="document">
		<div class="modal-content">			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			
			<div class="modal-body">
			
				Content is loading...
				
			</div>				
		</div>
	</div>
</div>	



<script type="text/javascript" src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/template/js/hoverIntent.js"></script> 
<script type="text/javascript" src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/template/js/superfish.js"></script> 
<script type="text/javascript" src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/template/js/supersubs.js"></script>
<script type="text/javascript" src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/template/js/tinynav.min.js"></script>

<script type="text/javascript" src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/template/js/custom.js"></script>





<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
