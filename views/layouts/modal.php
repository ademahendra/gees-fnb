<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\widgets\Menu;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="<?= Yii::$app->language ?>"> <!--<![endif]-->
<head>
	<meta charset="<?= Yii::$app->charset ?>">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

	<!-- CSS
  ================================================== -->
  	<link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
  	<link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/styles/style.css" />
    <link rel="stylesheet" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/styles/inner.css" />
    <link rel="stylesheet" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/styles/layout.css" />
    <link rel="stylesheet" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/styles/flexslider.css" />
    <link rel="stylesheet" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/styles/color.css" />
    <link rel="stylesheet" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/styles/prettyPhoto.css"  media="screen" />
    <link rel="stylesheet" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/vendor/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/vendor/fontawesome/css/font-awesome.min.css"/>
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script type="text/javascript" src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/js/jquery-migrate-1.2.1.min.js"></script>
	<script type="text/javascript" src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/vendor/bootstrap/js/bootstrap.min.js"></script>
	
	 <?php $this->head() ?>
</head>

<body>
<?php $this->beginBody() ?>
	
<?= $content ?>
	

		


<script type="text/javascript" src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/js/hoverIntent.js"></script> 
<script type="text/javascript" src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/js/superfish.js"></script> 
<script type="text/javascript" src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/js/supersubs.js"></script>
<script type="text/javascript" src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/js/tinynav.min.js"></script>

<script type="text/javascript" src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/js/custom.js"></script>





<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
