<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\KonversiSatuan */
?>
<div class="konversi-satuan-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'id_satuan',
            'child_satuan',
            'konversi',
            'record_by',
            'update_by',
            'record_date',
            'update_date',
            'date_stamp',
        ],
    ]) ?>

</div>
