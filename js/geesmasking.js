//used for masking money
(function ($) {
    "use strict"

    $.fn.geesmasking = function(options) {
        var formatCurrency = function(num) {
            var p = num.toFixed(2).split(".");
            return p[0].split("").reverse().reduce(function(acc, num, i, orig) {
                    return  num=="-" ? acc : num + (i && !(i % 3) ? "," : "") + acc;
                }, "") + "." + p[1];
        }

        this.each(function(e){
            var thisValue = parseFloat(this.value);
            var thisID = this.id;
            var thisClass = this.className;
            var thisName = this.name;
            //copy value to new object
            var input = document.createElement("input");
            input.type = "text";
            input.className = thisClass+" masked";
            input.name = thisName+"-masked";
            input.id = thisID+"-masked";
            input.value = formatCurrency(thisValue);
            this.parentNode.insertBefore(input, this.nextSibling);
            this.style.display = "none";
        });
        return this;
    };


}( jQuery ));