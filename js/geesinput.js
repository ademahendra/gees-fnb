/**
 * Created by DON VINCENT on 10/25/2017.
 */
//  used for masking money
(function ($) {
    "use strict"
    $.fn.geesinput = function(options) {
        this.each(function(e){
            this.onkeypress  = function(event){
                return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57;
            };
        });
        return this;
    };

}( jQuery ));