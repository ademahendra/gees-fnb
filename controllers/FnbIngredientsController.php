<?php

namespace app\controllers;

use app\models\FnbMenuSearch;
use Yii;
use app\models\FnbIngredients;
use app\models\FnbMenu;
use app\models\FnbStock;
use app\models\FnbIngredientsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
// use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

/**
 * FnbIngredientsController implements the CRUD actions for FnbIngredients model.
 */
class FnbIngredientsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create','view','update'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [''],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index','create','view','update'],
                        'roles' => ['@'],
                    ],
                ],
            ],
			'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all FnbIngredients models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FnbMenuSearch();
		$model = new FnbIngredients();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FnbIngredients model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
	
    public function actionMenudetail()
    {
        if (isset($_POST['expandRowKey'])) {
			$model = FnbMenu::findOne($_POST['expandRowKey']);
			$query = FnbIngredients::find()
				->where(['menu_id' => $model->id]);
			$dataProvider = new ActiveDataProvider([
				'query' => $query,
				'sort' => false,
			]);
			$dataProvider->pagination->pageSize= 40;
			return $this->renderPartial('detail', ['dataProvider'=>$dataProvider]);
		} else {
			return '<div class="alert alert-danger">No data found</div>';
		}
    }

    /**
     * Creates a new FnbIngredients model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FnbIngredients();
        $menu = new FnbMenu();

        if ($model->load(Yii::$app->request->post())) {
			$post = $_POST;
			$data = [];
			$data['status'] = false;
			$menu = new FnbMenu();
			$menu->name = $post['FnbMenu']['name'];
			$menu->selling_price = $post['sellingprice'];
			$menu->hotel_id = Yii::$app->user->identity->hotel_id;
			if($menu->save()){
				$data['status'] = true;
				for($i = 0; $i < count($post['id']); $i++){
					$ingredients = new FnbIngredients();	
					$ingredients->menu_id = $menu->id;
					$ingredients->stock_id = $post['id'][$i];
					$ingredients->qty = $post['qty'][$i];
					$ingredients->price = $post['price'][$i];
					$ingredients->hotel_id = Yii::$app->user->identity->hotel_id;
					if($ingredients->save()){
						$data['status'] = true;
					} else {
						$data['status'] = false;
					}
				}
			}
			echo json_encode($data);
            
        } else {
            return $this->render('create', [
                'model' => $model,
                'menu' => $menu,
            ]);
        }
    }
	
	public function actionDetail()
    {
        $item = FnbStock::find()
			->where(['id' => $_POST['id']])
			->one();
		$data = [];
		$data['status'] = false;
		if(count($item) > 0){
			$data['price']	= $item->unit_price;
			$data['unit']	= $item->unit;	
			$data['name']	= $item->name;	
			$data['status'] = true;
		}
		echo json_encode($data);
    }

    /**
     * Updates an existing FnbIngredients model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $menu = new FnbMenu();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'menu' => $menu,
            ]);
        }
    }

    /**
     * Deletes an existing FnbIngredients model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FnbIngredients model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FnbIngredients the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FnbIngredients::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
    protected function findMenu($id)
    {
        if (($model = FnbMenu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }	
}
