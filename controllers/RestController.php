<?php

namespace app\controllers;

use app\models\Bank;
use app\models\DetailPembelian;
use app\models\DetailPurchaseOrder;
use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use app\models\Provinces;
use app\models\Districts;
use app\models\Regencies;

class RestController extends Controller
{
    public function actionGetkota ($id)
    {
        $request = Yii::$app->request;
        // if($request->isAjax) {
        $findKota = Regencies::find()->where([
            'province_id' => $id
        ])
            ->all();
        $data = [];
        $data['option'] = '';
        if (count($findKota) > 0) {
            foreach ($findKota as $row) {
                $data['option'] .= '<option value="' . $row->id . '">' . $row->name . '</option>';
            }
        } else {
            $data['option'] .= '<option>Kota Tidak Ditemukan</option>';
        }
        echo json_encode($data);
        // } else {
        // throw new ForbiddenHttpException('Direct access isn\'t allowed');
        // }
    }

    public function actionGetdaerah ($id)
    {
        $request = Yii::$app->request;
        // if($request->isAjax) {
        $findDaerah = Districts::find()->where([
            'regency_id' => $id
        ])
            ->all();
        $data = [];
        $data['option'] = '';
        if (count($findDaerah) > 0) {
            foreach ($findDaerah as $row) {
                $data['option'] .= '<option value="' . $row->id . '">' . $row->name . '</option>';
            }
        } else {
            $data['option'] .= '<option>Daerah Tidak Ditemukan</option>';
        }
        echo json_encode($data);
        // } else {
        // throw new ForbiddenHttpException('Direct access isn\'t allowed');
        // }
    }
}