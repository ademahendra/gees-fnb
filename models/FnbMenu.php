<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fnb_menu".
 *
 * @property integer $id
 * @property string $name
 * @property double $selling_price
 * @property integer $updateby
 * @property integer $category_id
 * @property string $lastupdate
 *
 * @property FnbIngredients[] $fnbIngredients
 * @property FnbSaleDetail[] $fnbSaleDetails
 */
class FnbMenu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fnb_menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['selling_price'], 'number'],
            [['updateby','category_id'], 'integer'],
            [['lastupdate'], 'safe'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'selling_price' => 'Selling Price',
            'category_id' => 'Category',
            'updateby' => 'Updateby',
            'lastupdate' => 'Lastupdate',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFnbIngredients()
    {
        return $this->hasMany(FnbIngredients::className(), ['menu_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFnbSaleDetails()
    {
        return $this->hasMany(FnbSaleDetail::className(), ['id_menu' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasOne(FnbCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @inheritdoc
     * @return FnbMenuQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FnbMenuQuery(get_called_class());
    }
}
