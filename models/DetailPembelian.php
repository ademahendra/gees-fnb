<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "detail_pembelian".
 *
 * @property integer $id
 * @property integer $id_pembelian
 * @property integer $id_barang
 * @property integer $id_supplier
 * @property double $jumlah
 * @property string $batch_no
 * @property string $expired_date
 * @property double $harga_beli
 * @property double $ppn
 * @property double $harga_kena_pajak
 * @property double $harga_jual
 * @property double $harga_jual_2
 * @property double $harga_jual_3
 * @property double $harga_jual_4
 * @property string $tanggal
 * @property integer $admin_id
 */
class DetailPembelian extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'detail_pembelian';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pembelian', 'id_barang', 'admin_id'], 'integer'],
            [['jumlah', 'harga_beli', 'ppn', 'harga_kena_pajak', 'total', 'harga_jual', 'harga_jual_2', 'harga_jual_3', 'harga_jual_4','diskon_persen','diskon'], 'number'],
            [['tanggal'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_pembelian' => 'Id Pembelian',
            'id_barang' => 'Id Barang',
            'jumlah' => 'Jumlah',
            'harga_beli' => 'Harga Beli', 
            'ppn' => 'PPN',
            'total' => 'Total',
            'harga_kena_pajak' => 'Harga Setelah Pajak',
            'harga_jual' => 'Harga Jual',
            'harga_jual_2' => 'Harga Jual 2',
            'harga_jual_3' => 'Harga Jual 3',
            'harga_jual_4' => 'Harga Jual 4',
            'tanggal' => 'Tanggal',
            'admin_id' => 'Admin ID',
            'diskon_persen' => 'Diskon (%)',
            'diskon' => 'Diskon',
        ];
    }

    public function getBarang()
    {
        return $this->hasOne(FnbStock::className(), ['id' => 'id_barang']);
    }

    public function getPembelian()
    {
        return $this->hasOne(Pembelian::className(), ['id' => 'id_pembelian']);
    }
}
