<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FnbStock;

/**
 * FnbStockSearch represents the model behind the search form about `app\models\FnbStock`.
 */
class FnbStockSearch extends FnbStock
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'updateby'], 'integer'],
            [['name', 'unit', 'shelf_no', 'lastupdate'], 'safe'],
            [['qty', 'unit_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FnbStock::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'qty' => $this->qty,
            'unit_price' => $this->unit_price,
            'updateby' => $this->updateby,
            'lastupdate' => $this->lastupdate,
//			'hotel_id' => Yii::$app->user->identity->hotel_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'unit', $this->unit])
            ->andFilterWhere(['like', 'shelf_no', $this->shelf_no]);

        return $dataProvider;
    }
}
