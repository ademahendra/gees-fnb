<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fnb_ingredients".
 *
 * @property integer $id
 * @property integer $menu_id
 * @property integer $stock_id
 * @property double $qty
 * @property double $price
 * @property integer $updateby
 * @property string $lastupdate
 *
 * @property FnbMenu $menu
 * @property FnbStock $stock
 */
class FnbIngredients extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fnb_ingredients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_id'], 'required'],
            [['menu_id', 'stock_id', 'updateby'], 'integer'],
            [['qty', 'price'], 'number'],
            [['lastupdate'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'menu_id' => 'Menu ID',
            'stock_id' => 'Stock ID',
            'qty' => 'Qty',
            'price' => 'Price',
            'updateby' => 'Updateby',
            'lastupdate' => 'Lastupdate',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(FnbMenu::className(), ['id' => 'menu_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStock()
    {
        return $this->hasOne(FnbStock::className(), ['id' => 'stock_id']);
    }

    /**
     * @inheritdoc
     * @return FnbIngredientsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FnbIngredientsQuery(get_called_class());
    }
}
