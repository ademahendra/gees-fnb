<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[FnbSaleDetail]].
 *
 * @see FnbSaleDetail
 */
class FnbSaleDetailQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return FnbSaleDetail[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return FnbSaleDetail|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}