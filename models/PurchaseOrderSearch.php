<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PurchaseOrder;

/**
 * PurchaseOrderSearch represents the model behind the search form about `app\models\PurchaseOrder`.
 */
class PurchaseOrderSearch extends PurchaseOrder
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_gudang', 'id_supplier', 'ppn', 'admin_id'], 'integer'],
            [['no_faktur', 'no_surat_jalan', 'no_invoice', 'tanggal_faktur', 'jatuh_tempo', 'tanggal'], 'safe'],
            [['diskon_persen', 'diskon', 'harga_beli', 'harga_kena_pajak', 'harga_jual', 'harga_jual_2', 'harga_jual_3', 'harga_jual_4'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PurchaseOrder::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_gudang' => $this->id_gudang,
            'tanggal_faktur' => $this->tanggal_faktur,
            'id_supplier' => $this->id_supplier,
            'diskon_persen' => $this->diskon_persen,
            'diskon' => $this->diskon,
            'harga_beli' => $this->harga_beli,
            'ppn' => $this->ppn,
            'harga_kena_pajak' => $this->harga_kena_pajak,
            'harga_jual' => $this->harga_jual,
            'harga_jual_2' => $this->harga_jual_2,
            'harga_jual_3' => $this->harga_jual_3,
            'harga_jual_4' => $this->harga_jual_4,
            'jatuh_tempo' => $this->jatuh_tempo,
            'tanggal' => $this->tanggal,
            'admin_id' => $this->admin_id,
        ]);

        $query->andFilterWhere(['like', 'no_faktur', $this->no_faktur])
            ->andFilterWhere(['like', 'no_surat_jalan', $this->no_surat_jalan])
            ->andFilterWhere(['like', 'no_invoice', $this->no_invoice]);

        return $dataProvider;
    }
}
