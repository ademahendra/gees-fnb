<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fnb_category".
 *
 * @property integer $id
 * @property integer $hotel_id
 * @property integer $parent
 * @property string $name
 * @property integer $status
 */
class FnbCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fnb_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hotel_id', 'parent', 'status'], 'integer'],
            [['name'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hotel_id' => 'Hotel ID',
            'parent' => 'Parent',
            'name' => 'Name',
            'status' => 'Status',
        ];
    }
}
