<?php

namespace app\models;

use Yii;


class Customer extends \yii\db\ActiveRecord
{
    public $item;
	
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alamat'], 'string'],
            [['id_provinsi', 'id_kota', 'id_daerah', 'customer_type', 'record_by', 'update_by'], 'integer'],
            [['record_date', 'update_date', 'date_stamp'], 'safe'],
            [['nama', 'contact_person', 'phone','kode'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kode' => 'Kode',
            'nama' => 'Nama',
            'alamat' => 'Alamat',
            'contact_person' => 'Contact Person',
            'phone' => 'Phone',
            'id_provinsi' => 'Provinsi',
            'id_kota' => 'Kota',
            'id_daerah' => 'Daerah',
            'customer_type' => 'Tipe Customer',
            'record_by' => 'Record By',
            'update_by' => 'Update By',
            'record_date' => 'Record Date',
            'update_date' => 'Update Date',
            'date_stamp' => 'Date Stamp',
        ];
    }
	public function getCustomerType()
    {
        return $this->hasOne(CustomerType::className(), ['id' => 'customer_type']);
    }
	public function getProvinces()
    {
        return $this->hasOne(Provinces::className(), ['id' => 'id_provinsi']);
    }
	
	public function getRegencies()
    {
        return $this->hasOne(Regencies::className(), ['id' => 'id_kota']);
    }
	
	public function getDistricts()
    {
        return $this->hasOne(Districts::className(), ['id' => 'id_daerah']);
    }
	 public function getUsers()
    {
        return $this->hasOne(Users::className(), ['id' => 'update_by']);
    }

}
