<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "stok_gudang".
 *
 * @property integer $id
 * @property string $nama
 * @property string $no_gudang
 * @property string $id_satuan
 * @property integer $id_supplier
 * @property string $no_rak
 * @property double $harga_beli
 * @property integer $ppn
 * @property double $harga_kena_pajak
 * @property double $stok
 * @property integer $minimum_stok
 * @property string $id_kategori
 *
 * @property DetailPenjualan[] $detailPenjualans
 * @property Pembelian[] $pembelians
 * @property Supplier $idSupplier
 */
class StokGudang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stok_bahan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           
            [[ 'ppn', 'minimum_stok','id_gudang','record_by', 'update_by','id_pembelian'], 'integer'],
            [['harga_beli', 'harga_kena_pajak', 'stok','harga_jual1','harga_jual2'], 'number'],
            [['no_rak'], 'string', 'max' => 100],
			[['record_date', 'update_date', 'date_stamp','tgl_pembelian'], 'safe'],
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_gudang' => 'Gudang',
            'id_barang' => 'Barang',
            'id_pembelian' =>'Pembelian',
            'tgl_pembelian' =>'Tanggal Pembelian',
            'no_rak' => 'No Rak',
            'harga_beli' => 'Harga Beli',
            'ppn' => 'Ppn',
            'harga_kena_pajak' => 'Harga Kena Pajak',
            'stok' => 'Stok',
            'minimum_stok' => 'Minimum Stok',
			'harga_jual1' => 'Harga Jual Retail',
			'harga_jual2' => 'Harga Jual Agen',
			'harga_jual3' => 'Harga Jual 3',
			'harga_jual4' => 'Harga Jual 4',
			'record_by' => 'Record By',
            'update_by' => 'Update By',
            'record_date' => 'Record Date',
            'update_date' => 'Update Date',
            'date_stamp' => 'Date Stamp',
        ];
    }

    public function getDetailPenjualan()
    {
        return $this->hasMany(DetailPenjualan::className(), ['id_barang' => 'id']);
    }

    public function getPembelian()
    {
        return $this->hasMany(Pembelian::className(), ['id_barang' => 'id']);
    }
	
    public function getBarang()
    {
        return $this->hasOne(Barang::className(), ['id' => 'id_barang']);
    }
	
    public function getGudang()
    {
        return $this->hasOne(Gudang::className(), ['id' => 'id_gudang']);
    }
}
