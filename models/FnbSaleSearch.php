<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FnbSale;

/**
 * FnbSaleSearch represents the model behind the search form about `app\models\FnbSale`.
 */
class FnbSaleSearch extends FnbSale
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_room', 'id_guest', 'table_no', 'updateby'], 'integer'],
            [['date_sale', 'default_currency', 'currency', 'lastupdate'], 'safe'],
            [['total_sale', 'currency_rate', 'tax', 'service'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FnbSale::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_room' => $this->id_room,
            'id_guest' => $this->id_guest,
            'table_no' => $this->table_no,
            'date_sale' => $this->date_sale,
            'total_sale' => $this->total_sale,
            'currency_rate' => $this->currency_rate,
            'tax' => $this->tax,
            'service' => $this->service,
            'updateby' => $this->updateby,
            'lastupdate' => $this->lastupdate,
			'hotel_id' => Yii::$app->user->identity->hotel_id,
        ]);

        $query->andFilterWhere(['like', 'default_currency', $this->default_currency])
            ->andFilterWhere(['like', 'currency', $this->currency]);

        return $dataProvider;
    }
}
