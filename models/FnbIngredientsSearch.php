<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FnbIngredients;
use app\models\FnbMenu;

/**
 * FnbIngredientsSearch represents the model behind the search form about `app\models\FnbIngredients`.
 */
class FnbIngredientsSearch extends FnbIngredients
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'menu_id', 'stock_id', 'updateby'], 'integer'],
            [['qty', 'price'], 'number'],
            [['lastupdate','menu.name','menu.selling_price'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
	
	public function attributes()
	{
		return array_merge (parent::attributes(),['menu.name', 'menu.selling_price']);
	}

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FnbIngredients::find();

		$dataProvider->sort->attributes['menu.name'] = [
			'asc' => ['menu.name' => SORT_ASC],
			'desc' => ['menu.name' => SORT_DESC],
		];
		$dataProvider->sort->attributes['menu.selling_price'] = [
			'asc' => ['menu.selling_price' => SORT_ASC],
			'desc' => ['menu.selling_price' => SORT_DESC],
		];
		
		$query->joinWith(['menu']);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'menu_id' => $this->menu_id,
            'stock_id' => $this->stock_id,
            'qty' => $this->qty,
            'price' => $this->price,
            'updateby' => $this->updateby,
            'lastupdate' => $this->lastupdate,
			'fnb_menu.hotel_id' => Yii::$app->user->identity->hotel_id,			
        ])
		->andFilterWhere(['like', 'fnb_menu.name', $this->getAttribute('menu.name')])
		->andFilterWhere(['like', 'fnb_menu.selling_price', $this->getAttribute('menu.selling_price')]);
		$query->groupBy('menu_id');

        return $dataProvider;
    }
}
