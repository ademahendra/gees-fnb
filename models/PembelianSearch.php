<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pembelian;

/**
 * PembelianSearch represents the model behind the search form about `app\models\Pembelian`.
 */
class PembelianSearch extends Pembelian
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             [['id_gudang', 'id_supplier', 'ppn', 'admin_id'], 'integer'],
            [['tanggal_faktur', 'jatuh_tempo', 'tanggal'], 'safe'],
            [['harga_beli', 'harga_kena_pajak', 'harga_jual', 'harga_jual_2', 'harga_jual_3', 'harga_jual_4','diskon_persen','diskon'], 'number'],
            [['no_faktur','no_invoice','no_surat_jalan'], 'string', 'max' => 255],
        ];
    }
	
	// public function attributes()
	// {
		// return array_merge (parent::attributes(),[ 'barang.nama']);
	// }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pembelian::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort'=> ['defaultOrder' => ['tanggal_faktur'=>SORT_ASC]]
        ]);

		// $dataProvider->sort->attributes['barang.nama'] = [
			// 'asc' => ['barang.nama' => SORT_ASC],
			// 'desc' => ['barang.nama' => SORT_DESC],
		// ];
		
		// $query->joinWith(['barang']);
		
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            // 'id_barang' => $this->id_barang,
            // 'jumlah' => $this->jumlah,
            'harga_beli' => $this->harga_beli,
            'harga_jual' => $this->harga_jual,
            'tanggal' => $this->tanggal,
            'admin_id' => $this->admin_id,
        ]);

        $query->andFilterWhere(['like', 'no_faktur', $this->no_faktur]);
			 
        return $dataProvider;
    }
}
