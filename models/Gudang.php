<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gudang".
 *
 * @property integer $id
 * @property string $nama
 * @property string $alamat
 * @property integer $status
 * @property string $lastupdate
 */
class Gudang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gudang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['record_by', 'update_by'], 'integer'],
            [['record_date', 'update_date', 'date_stamp'], 'safe'],
            [['nama', 'alamat'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'alamat' => 'Alamat',
            'status' => 'Status',
            'record_by' => 'Record By',
            'update_by' => 'Update By',
            'record_date' => 'Record Date',
            'update_date' => 'Update Date',
            'date_stamp' => 'Date Stamp',
        ];
    }
	
	public function getUsers()
    {
        return $this->hasOne(Users::className(), ['id' => 'update_by']);
    }
}
