<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fnb_stock".
 *
 * @property integer $id
 * @property string $name
 * @property string $unit
 * @property double $qty
 * @property double $unit_price
 * @property string $shelf_no
 * @property integer $updateby
 * @property string $lastupdate
 *
 * @property FnbIngredients[] $fnbIngredients
 */
class FnbStock extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fnb_stock';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['qty', 'unit_price'], 'number'],
            [['updateby', 'unit_id', 'bigunit_id'], 'integer'],
            [['lastupdate'], 'safe'],
            [['name', 'unit'], 'string', 'max' => 255],
            [['shelf_no'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'unit' => 'Unit',
            'unit_id' => 'Recipe Unit',
            'bigunit_id' => 'Purchase Unit',
            'qty' => 'Qty',
            'unit_price' => 'Unit Price',
            'shelf_no' => 'Shelf No',
            'updateby' => 'Updateby',
            'lastupdate' => 'Lastupdate',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFnbIngredients()
    {
        return $this->hasMany(FnbIngredients::className(), ['stock_id' => 'id']);
    }

    public function getUnit()
    {
        return $this->hasOne(Satuan::className(), ['id' => 'unit_id']);
    }

    public function getBigUnit()
    {
        return $this->hasOne(Satuan::className(), ['id' => 'bigunit_id']);
    }

    /**
     * @inheritdoc
     * @return FnbStockQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FnbStockQuery(get_called_class());
    }
}
