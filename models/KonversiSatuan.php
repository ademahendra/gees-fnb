<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "konversi_satuan".
 *
 * @property integer $id
 * @property integer $id_satuan
 * @property string $child_satuan
 * @property double $konversi
 * @property integer $record_by
 * @property integer $update_by
 * @property string $record_date
 * @property string $update_date
 * @property string $date_stamp
 */
class KonversiSatuan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'konversi_satuan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_satuan'], 'required'],
            [['id_satuan', 'record_by', 'update_by'], 'integer'],
            [['konversi'], 'number'],
            [['record_date', 'update_date', 'date_stamp'], 'safe'],
            [['child_satuan'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_satuan' => 'Id Satuan',
            'child_satuan' => 'Child Satuan',
            'konversi' => 'Konversi',
            'record_by' => 'Record By',
            'update_by' => 'Update By',
            'record_date' => 'Record Date',
            'update_date' => 'Update Date',
            'date_stamp' => 'Date Stamp',
        ];
    }
}
