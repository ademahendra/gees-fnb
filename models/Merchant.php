<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "merchant".
 *
 * @property int $id
 * @property int $id_gudang
 * @property int $id_customer
 * @property string $logo
 * @property string $name
 * @property string $alamat
 * @property string $telepon
 * @property string $website
 * @property string $email
 * @property string $record_date
 * @property string $update_date
 * @property int $record_by
 * @property int $update_by
 * @property string $date_stamp
 */
class Merchant extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'merchant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_gudang', 'id_customer','id_supplier', 'record_by', 'update_by'], 'integer'],
            [['record_date', 'update_date', 'date_stamp','service_charge'], 'safe'],
            [['logo', 'name', 'alamat', 'telepon', 'website', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_gudang' => 'Id Gudang',
            'id_customer' => 'Id Customer',
            'id_supplier' => 'Supplier',
            'logo' => 'Logo',
            'name' => 'Name',
            'alamat' => 'Alamat',
            'telepon' => 'Telepon',
            'website' => 'Website',
            'email' => 'Email',
            'record_date' => 'Record Date',
            'update_date' => 'Update Date',
            'record_by' => 'Record By',
            'update_by' => 'Update By',
            'date_stamp' => 'Date Stamp',
            'service_charge' => 'Service Charge',
        ];
    }

    public function getGudang(){
        return $this->hasOne(Gudang::className(), ['id' => 'id_gudang']);
    }

    public function getCustomer(){
        return $this->hasOne(Customer::className(), ['id' => 'id_customer']);
    }

    public function getSupplier(){
        return $this->hasOne(Supplier::className(), ['id' => 'id_supplier']);
    }

}
