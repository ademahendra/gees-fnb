<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[FnbSale]].
 *
 * @see FnbSale
 */
class FnbSaleQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return FnbSale[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return FnbSale|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}