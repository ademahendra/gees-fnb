<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StokGudang;

/**
 * StokGudangSearch represents the model behind the search form about `app\models\StokGudang`.
 */
class StokGudangSearch extends StokGudang
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ppn', 'minimum_stok'], 'integer'],
            [['id_barang', 'id_gudang', 'no_rak','barang.nama'], 'safe'],
            [['harga_beli', 'harga_kena_pajak', 'stok'], 'number'],
        ];
    }
	
	public function attributes()
	{
		return array_merge (parent::attributes(),['barang.nama']);
	}
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StokGudang::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		$dataProvider->sort->attributes['barang.nama'] = [
			'asc' => ['barang.nama' => SORT_ASC],
			'desc' => ['barang.nama' => SORT_DESC],
		];
		
		$query->joinWith(['barang']);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'harga_beli' => $this->harga_beli,
            'ppn' => $this->ppn,
            'harga_kena_pajak' => $this->harga_kena_pajak,
            'stok' => $this->stok,
            'minimum_stok' => $this->minimum_stok,
        ]);

        $query->andFilterWhere(['like', 'barang.nama', $this->getAttribute('barang.nama')])
            ->andFilterWhere(['like', 'id_gudang', $this->id_gudang]);

        return $dataProvider;
    }
}
