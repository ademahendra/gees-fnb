<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "purchase_order".
 *
 * @property integer $id
 * @property integer $id_gudang
 * @property string $no_faktur
 * @property string $no_surat_jalan
 * @property string $no_invoice
 * @property string $tanggal_faktur
 * @property integer $id_supplier
 * @property double $diskon_persen
 * @property double $diskon
 * @property double $harga_beli
 * @property integer $ppn
 * @property double $harga_kena_pajak
 * @property double $harga_jual
 * @property double $harga_jual_2
 * @property double $harga_jual_3
 * @property double $harga_jual_4
 * @property string $jatuh_tempo
 * @property string $tanggal
 * @property integer $admin_id
 */
class PurchaseOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'purchase_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_gudang', 'id_supplier', 'ppn', 'admin_id'], 'integer'],
            [['id_supplier', ], 'required'],
            [['tanggal_faktur', 'jatuh_tempo', 'tanggal'], 'safe'],
            [['total','diskon_persen', 'diskon', 'harga_beli', 'harga_kena_pajak', 'harga_jual', 'harga_jual_2', 'harga_jual_3', 'harga_jual_4'], 'number'],
            [['no_faktur','no_po'], 'string', 'max' => 255],
            [['no_surat_jalan', 'no_invoice'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_gudang' => 'Id Gudang',
            'no_faktur' => 'No Faktur',
            'no_po' => 'No Purchase Order',
            'no_surat_jalan' => 'No Surat Jalan',
            'no_invoice' => 'No Invoice',
            'tanggal_faktur' => 'Tanggal Faktur',
            'id_supplier' => 'Id Supplier',
            'diskon_persen' => 'Diskon Persen',
            'diskon' => 'Diskon',
            'harga_beli' => 'Harga Beli',
            'ppn' => 'Ppn',
            'harga_kena_pajak' => 'Harga Kena Pajak',
            'harga_jual' => 'Harga Jual',
            'harga_jual_2' => 'Harga Jual 2',
            'harga_jual_3' => 'Harga Jual 3',
            'harga_jual_4' => 'Harga Jual 4',
            'jatuh_tempo' => 'Jatuh Tempo',
            'tanggal' => 'Tanggal',
            'admin_id' => 'Admin ID',
        ];
    }
	
	public function getStokGudang()
    {
        return $this->hasOne(StokGudang::className(), ['id' => 'id_barang']);
    }
	
    public function getBarang()
    {
        return $this->hasOne(FnbStock::className(), ['id' => 'id_barang']);
    }
	
    public function getSupplier()
    {
        return $this->hasOne(Supplier::className(), ['id' => 'id_supplier']);
    }
}
