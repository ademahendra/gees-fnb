<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\KonversiSatuan;

/**
 * KonversiSatuanSearch represents the model behind the search form about `app\models\KonversiSatuan`.
 */
class KonversiSatuanSearch extends KonversiSatuan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_satuan', 'record_by', 'update_by'], 'integer'],
            [['child_satuan', 'record_date', 'update_date', 'date_stamp'], 'safe'],
            [['konversi'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KonversiSatuan::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_satuan' => $this->id_satuan,
            'konversi' => $this->konversi,
            'record_by' => $this->record_by,
            'update_by' => $this->update_by,
            'record_date' => $this->record_date,
            'update_date' => $this->update_date,
            'date_stamp' => $this->date_stamp,
        ]);

        $query->andFilterWhere(['like', 'child_satuan', $this->child_satuan]);

        return $dataProvider;
    }
}
