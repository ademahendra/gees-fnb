<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "room".
 *
 * @property integer $id
 * @property string $name
 * @property integer $id_room_type
 * @property integer $id_bed_type
 * @property integer $phone_extension
 * @property integer $smooking
 * @property string $image1
 * @property string $image2
 * @property string $image3
 * @property string $image4
 * @property integer $status
 * @property integer $updateby
 * @property string $lastupdate
 *
 * @property Reservation[] $reservations
 * @property ReservationDate[] $reservationDates
 * @property BedType $idBedType
 * @property RoomType $idRoomType
 */
class Room extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	public function behaviors()
    {
        return [
            //'bedezign\yii2\audit\AuditTrailBehavior'
        ];
    }	
	
    public static function tableName()
    {
        return 'room';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'id_room_type'], 'required'],
            [['id_room_type', 'id_bed_type', 'phone_extension', 'smooking', 'status', 'updateby'], 'integer'],
            [['lastupdate'], 'safe'],
            [['name'], 'string', 'max' => 200],
            [['image1', 'image2', 'image3', 'image4'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Room Name',
            'id_room_type' => 'Room Type',
            'id_bed_type' => 'Bed Type',
            'phone_extension' => 'Phone Extension',
            'smooking' => 'Smooking',
            'image1' => 'Image1',
            'image2' => 'Image2',
            'image3' => 'Image3',
            'image4' => 'Image4',
            'status' => 'Status',
            'updateby' => 'Updateby',
            'lastupdate' => 'Lastupdate',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReservations()
    {
        return $this->hasMany(Reservation::className(), ['id_room' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReservationDates()
    {
        return $this->hasMany(ReservationDate::className(), ['id_room' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdBedType()
    {
        return $this->hasOne(BedType::className(), ['id' => 'id_bed_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRoomType()
    {
        return $this->hasOne(RoomType::className(), ['id' => 'id_room_type']);
    }

    /**
     * @inheritdoc
     * @return RoomQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RoomQuery(get_called_class());
    }
}
