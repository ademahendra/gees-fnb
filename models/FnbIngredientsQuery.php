<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[FnbIngredients]].
 *
 * @see FnbIngredients
 */
class FnbIngredientsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return FnbIngredients[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return FnbIngredients|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}