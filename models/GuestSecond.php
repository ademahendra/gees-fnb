<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "guest_second".
 *
 * @property integer $id
 * @property integer $id_guest
 * @property string $title
 * @property string $name
 * @property string $relation
 * @property string $address
 * @property string $phone
 * @property integer $id_nationality
 * @property integer $updateby
 * @property string $lastupdate
 *
 * @property Guest $idGuest
 */
class GuestSecond extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	public function behaviors()
    {
        return [
            'bedezign\yii2\audit\AuditTrailBehavior'
        ];
    }		 
	 
    public static function tableName()
    {
        return 'guest_second';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_guest', 'id_nationality', 'updateby'], 'integer'],
            [['title', 'relation', 'address'], 'string'],
            [['lastupdate'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_guest' => 'Id Guest',
            'title' => 'Title',
            'name' => 'Name',
            'relation' => 'Relation',
            'address' => 'Address',
            'phone' => 'Phone',
            'id_nationality' => 'Id Nationality',
            'updateby' => 'Updateby',
            'lastupdate' => 'Lastupdate',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdGuest()
    {
        return $this->hasOne(Guest::className(), ['id' => 'id_guest']);
    }

    /**
     * @inheritdoc
     * @return GuestSecondQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GuestSecondQuery(get_called_class());
    }
}
