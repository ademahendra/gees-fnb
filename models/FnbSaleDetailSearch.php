<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FnbSaleDetail;

/**
 * FnbSaleDetailSearch represents the model behind the search form about `app\models\FnbSaleDetail`.
 */
class FnbSaleDetailSearch extends FnbSaleDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_sale', 'id_menu', 'qty', 'updateby'], 'integer'],
            [['price', 'production_price', 'total_price', 'currency_rate'], 'number'],
            [['default_currenct', 'currency', 'lastupdate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FnbSaleDetail::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_sale' => $this->id_sale,
            'id_menu' => $this->id_menu,
            'qty' => $this->qty,
            'price' => $this->price,
            'production_price' => $this->production_price,
            'total_price' => $this->total_price,
            'currency_rate' => $this->currency_rate,
            'updateby' => $this->updateby,
            'lastupdate' => $this->lastupdate,
			'hotel_id' => Yii::$app->user->identity->hotel_id,
        ]);

        $query->andFilterWhere(['like', 'default_currenct', $this->default_currenct])
            ->andFilterWhere(['like', 'currency', $this->currency]);

        return $dataProvider;
    }
}
