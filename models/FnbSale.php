<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fnb_sale".
 *
 * @property integer $id
 * @property integer $id_room
 * @property integer $id_guest
 * @property integer $table_no
 * @property string $date_sale
 * @property double $total_sale
 * @property string $default_currency
 * @property string $currency
 * @property double $currency_rate
 * @property double $tax
 * @property double $service
 * @property integer $updateby
 * @property string $lastupdate
 *
 * @property FnbSaleDetail[] $fnbSaleDetails
 */
class FnbSale extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fnb_sale';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_room', 'id_guest', 'table_no', 'updateby','id_bank'], 'integer'],
            [['date_sale', 'lastupdate','card_number'], 'safe'],
            [['total_sale', 'currency_rate', 'tax', 'service'], 'number'],
            [['default_currency', 'currency'], 'string', 'max' => 3]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_room' => 'Room No.',
            'id_guest' => 'Guest',
            'table_no' => 'Table No',
            'date_sale' => 'Date Sale',
            'total_sale' => 'Total Sale',
            'default_currency' => 'Default Currency',
            'currency' => 'Currency',
            'currency_rate' => 'Currency Rate',
            'tax' => 'Tax',
            'service' => 'Service',
            'updateby' => 'Updateby',
            'lastupdate' => 'Lastupdate',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFnbSaleDetails()
    {
        return $this->hasMany(FnbSaleDetail::className(), ['id_sale' => 'id']);
    }

    public function getRoom()
    {
        return $this->hasOne(Room::className(), ['id' => 'id_room']);
    }

    /**
     * @inheritdoc
     * @return FnbSaleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FnbSaleQuery(get_called_class());
    }
}
