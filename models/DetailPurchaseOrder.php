<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "detail_purchase_order".
 *
 * @property integer $id
 * @property integer $id_purchase_order
 * @property integer $id_barang
 * @property double $jumlah
 * @property double $harga
 * @property double $ppn
 * @property double $harga_kena_pajak
 * @property double $diskon
 * @property double $diskon_persen
 * @property integer $update_by
 * @property string $last_update
 * @property string $date_added
 */
class DetailPurchaseOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'purchase_order_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_purchase_order', 'id_barang', 'update_by','id_supplier'], 'integer'],
            [['jumlah', 'harga_beli', 'total', 'ppn', 'harga_kena_pajak', 'diskon', 'diskon_persen','jumlah_kirim','sisa'], 'number'],
            [['last_update', 'date_added','tanggal'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_purchase_order' => 'Purchase Order',
            'id_barang' => 'Barang',
            'id_supplier' => 'Supplier',
            'jumlah' => 'Jumlah',
            'jumlah_kirim' => 'Jumlah Kirim',
            'sisa' => 'Sisa',
            'harga' => 'Harga',
            'ppn' => 'PPN',
            'harga_kena_pajak' => 'Harga Kena Pajak',
            'diskon' => 'Diskon',
            'diskon_persen' => 'Diskon Persen',
            'update_by' => 'Update By',
            'last_update' => 'Last Update',
            'date_added' => 'Date Added',
        ];
    }
	
		public function getStokGudang()
    {
        return $this->hasOne(StokGudang::className(), ['id' => 'id_barang']);
    }
	
    public function getBarang()
    {
        return $this->hasOne(FnbStock::className(), ['id' => 'id_barang']);
    }
	
    public function getSupplier()
    {
        return $this->hasOne(Supplier::className(), ['id' => 'id_supplier']);
    }

    public function getPurchase()
    {
        return $this->hasOne(PurchaseOrder::className(), ['id' => 'id_purchase_order']);
    }
}
