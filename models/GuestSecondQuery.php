<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[GuestSecond]].
 *
 * @see GuestSecond
 */
class GuestSecondQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return GuestSecond[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return GuestSecond|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}