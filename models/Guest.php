<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "guest".
 *
 * @property integer $id
 * @property integer $hotel_id
 * @property string $title
 * @property string $name
 * @property string $address
 * @property string $city
 * @property integer $id_country
 * @property string $zipcode
 * @property string $email
 * @property string $phone
 * @property string $mobile
 * @property string $fax
 * @property string $identity
 * @property string $identity_no
 * @property string $dob
 * @property string $nationality_registration_no
 * @property integer $id_nationality
 * @property string $gender
 * @property integer $status
 * @property integer $rate
 * @property integer $updateby
 * @property string $lastupdate
 *
 * @property GuestSecond[] $guestSeconds
 * @property Reservation[] $reservations
 */
class Guest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	public function behaviors()
    {
        return [
            'bedezign\yii2\audit\AuditTrailBehavior'
        ];
    }		 
	 
    public static function tableName()
    {
        return 'guest';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hotel_id', 'id_country', 'id_nationality', 'status', 'rate', 'updateby'], 'integer'],
            [['title', 'address', 'identity', 'gender'], 'string'],
            [['title', 'identity', 'name'], 'required'],
            [['dob', 'lastupdate'], 'safe'],
            [['name', 'email'], 'string', 'max' => 255],
            [['city'], 'string', 'max' => 50],
            [['zipcode'], 'string', 'max' => 8],
            [['phone', 'mobile', 'fax', 'identity_no', 'nationality_registration_no'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hotel_id' => 'Hotel ID',
            'title' => 'Title',
            'name' => 'Name',
            'address' => 'Address',
            'city' => 'City',
            'id_country' => 'Country',
            'zipcode' => 'Zip Code',
            'email' => 'Email',
            'phone' => 'Phone',
            'mobile' => 'Mobile',
            'fax' => 'Fax',
            'identity' => 'Identity',
            'identity_no' => 'Identity No',
            'dob' => 'Dob',
            'nationality_registration_no' => 'Nationality Registration No',
            'id_nationality' => 'Id Nationality',
            'gender' => 'Gender',
            'status' => 'Status',
            'rate' => 'Rate',
            'updateby' => 'Updateby',
            'lastupdate' => 'Lastupdate',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuestSeconds()
    {
        return $this->hasMany(GuestSecond::className(), ['id_guest' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReservations()
    {
        return $this->hasMany(Reservation::className(), ['id_guest' => 'id']);
    }
    
    public function getCountry()
    {
        return $this->hasOne(Countries::className(), ['id' => 'id_country']);
    }
    
    public function getNationality()
    {
        return $this->hasOne(Countries::className(), ['id' => 'id_nationality']);
    }

    /**
     * @inheritdoc
     * @return GuestQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GuestQuery(get_called_class());
    }
}
