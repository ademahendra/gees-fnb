<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipe_bayar".
 *
 * @property integer $id
 * @property string $nama
 * @property string $jenis
 * @property integer $record_by
 * @property integer $update_by
 * @property string $record_date
 * @property string $update_date
 * @property string $date_stamp
 */
class TipeBayar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipe_bayar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'jenis'], 'required'],
            [['jenis'], 'string'],
            [['record_by', 'update_by'], 'integer'],
            [['record_date', 'update_date', 'date_stamp'], 'safe'],
            [['nama'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'jenis' => 'Jenis',
            'record_by' => 'Record By',
            'update_by' => 'Update By',
            'record_date' => 'Record Date',
            'update_date' => 'Update Date',
            'date_stamp' => 'Date Stamp',
        ];
    }
}
