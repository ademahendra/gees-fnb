<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "satuan".
 *
 * @property integer $id
 * @property integer $parent
 * @property string $nama
 * @property double $parent_conversion
 * @property integer $record_by
 * @property integer $update_by
 * @property string $record_date
 * @property string $update_date
 * @property string $date_stamp
 */
class Satuan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'satuan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent', 'record_by', 'update_by'], 'integer'],
            [['parent_conversion'], 'number'],
            [['record_date', 'update_date', 'date_stamp'], 'safe'],
            [['nama'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent' => 'Parent',
            'nama' => 'Nama',
            'parent_conversion' => 'Parent Conversion',
            'record_by' => 'Record By',
            'update_by' => 'Update By',
            'record_date' => 'Record Date',
            'update_date' => 'Update Date',
            'date_stamp' => 'Date Stamp',
        ];
    }
	
	public function getUsers()
    {
        return $this->hasOne(Users::className(), ['id' => 'record_by']);
    }
}
