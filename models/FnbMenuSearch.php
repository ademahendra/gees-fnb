<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FnbMenu;

/**
 * FnbMenuSearch represents the model behind the search form about `app\models\FnbMenu`.
 */
class FnbMenuSearch extends FnbMenu
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'updateby'], 'integer'],
            [['name', 'lastupdate', 'category.name'], 'safe'],
            [['selling_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return array_merge (parent::attributes(),['category.name']);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FnbMenu::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['category.name'] = [
            'asc' => ['fnb_category.name' => SORT_ASC],
            'desc' => ['fnb_category.name' => SORT_DESC],
        ];

        $query->joinWith(['categories']);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'selling_price' => $this->selling_price,
            'updateby' => $this->updateby,
            'lastupdate' => $this->lastupdate,
//			'hotel_id' => Yii::$app->user->identity->hotel_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'fnb_category.name', $this->getAttribute('category.name')]);

        return $dataProvider;
    }
}
