<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bank".
 *
 * @property integer $id
 * @property string $kode
 * @property string $nama
 * @property double $surcharge
 * @property double $discount
 * @property integer $record_by
 * @property integer $update_by
 * @property string $record_date
 * @property string $update_date
 * @property string $date_stamp
 */
class Bank extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bank';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode', 'nama'], 'required'],
            [['surcharge', 'discount'], 'number'],
            [['record_by', 'update_by'], 'integer'],
            [['record_date', 'update_date', 'date_stamp'], 'safe'],
            [['kode'], 'string', 'max' => 10],
            [['nama'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kode' => 'Kode',
            'nama' => 'Nama',
            'surcharge' => 'Surcharge',
            'discount' => 'Discount',
            'record_by' => 'Record By',
            'update_by' => 'Update By',
            'record_date' => 'Record Date',
            'update_date' => 'Update Date',
            'date_stamp' => 'Date Stamp',
        ];
    }
}
