<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fnb_sale_detail".
 *
 * @property integer $id
 * @property integer $id_sale
 * @property integer $id_menu
 * @property integer $qty
 * @property double $price
 * @property double $production_price
 * @property double $total_price
 * @property string $default_currenct
 * @property string $currency
 * @property double $currency_rate
 * @property integer $updateby
 * @property string $lastupdate
 *
 * @property FnbSale $idSale
 * @property FnbMenu $idMenu
 */
class FnbSaleDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fnb_sale_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_sale', 'id_menu', 'qty', 'updateby'], 'integer'],
            [['price', 'production_price', 'total_price', 'currency_rate'], 'number'],
            [['lastupdate'], 'safe'],
            [['default_currenct', 'currency'], 'string', 'max' => 3]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_sale' => 'Id Sale',
            'id_menu' => 'Id Menu',
            'qty' => 'Qty',
            'price' => 'Price',
            'production_price' => 'Production Price',
            'total_price' => 'Total Price',
            'default_currenct' => 'Default Currenct',
            'currency' => 'Currency',
            'currency_rate' => 'Currency Rate',
            'updateby' => 'Updateby',
            'lastupdate' => 'Lastupdate',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSale()
    {
        return $this->hasOne(FnbSale::className(), ['id' => 'id_sale']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(FnbMenu::className(), ['id' => 'id_menu']);
    }

    /**
     * @inheritdoc
     * @return FnbSaleDetailQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FnbSaleDetailQuery(get_called_class());
    }
}
