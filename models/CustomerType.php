<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customer_type".
 *
 * @property integer $id
 * @property string $jenis
 * @property double $diskon
 * @property integer $record_by
 * @property integer $update_by
 * @property string $record_date
 * @property string $update_date
 * @property string $date_stamp
 */
class CustomerType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['diskon'], 'number'],
            [['record_by', 'update_by'], 'integer'],
            [['record_date', 'update_date', 'date_stamp'], 'safe'],
            [['jenis'], 'string', 'max' => 255],
			[['jenis'],'unique'],
			[['jenis','diskon'],'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenis' => 'Jenis',
            'diskon' => 'Diskon',
            'record_by' => 'Record By',
            'update_by' => 'Update By',
            'record_date' => 'Record Date',
            'update_date' => 'Update Date',
            'date_stamp' => 'Date Stamp',
        ];
    }
}
