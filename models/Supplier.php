<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "supplier".
 *
 * @property integer $id
 * @property string $nama
 * @property string $alamat
 * @property string $telepon
 * @property string $contact_person
 * @property string $contact_person_telepone
 *
 * @property Obat[] $obats
 */
class Supplier extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'supplier';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'alamat', 'telepon', 'contact_person', 'contact_person_telepone'], 'string', 'max' => 255],
            [['top','id_kota','id_provinsi','id_daerah'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'alamat' => 'Alamat',
            'id_kota' => 'Kota',
            'id_provinsi' => 'Provinsi',
            'id_daerah' => 'Daerah',
            'telepon' => 'Telepon',
            'contact_person' => 'Contact Person',
            'contact_person_telepone' => 'Contact Person Telepone',
            'top' => 'Term Of Payment (Hari)'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvinces()
    {
        return $this->hasOne(Provinces::className(), ['id' => 'id_provinsi']);
    }

    public function getRegencies()
    {
        return $this->hasOne(Regencies::className(), ['id' => 'id_kota']);
    }

    public function getDistricts()
    {
        return $this->hasOne(Districts::className(), ['id' => 'id_daerah']);
    }
}
