<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pembelian".
 *
 * @property integer $id
 * @property integer $id_gudang
 * @property string $no_faktur
 * @property string $tanggal_faktur
 * @property integer $id_supplier
 * @property double $harga_beli
 * @property integer $ppn
 * @property double $harga_kena_pajak
 * @property double $harga_jual
 * @property double $harga_jual_2
 * @property double $harga_jual_3
 * @property double $harga_jual_4
 * @property string $jatuh_tempo
 * @property string $tanggal
 * @property integer $admin_id
 */
class Pembelian extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pembelian';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_gudang','id_po', 'id_supplier', 'ppn', 'admin_id','id_tipe_bayar','id_bank'], 'integer'],
            [['tanggal_faktur', 'jatuh_tempo', 'tanggal'], 'safe'],
            [['harga_beli', 'harga_kena_pajak', 'harga_jual', 'harga_jual_2', 'harga_jual_3', 'harga_jual_4','diskon_persen','diskon','total_bayar','sisa','surcharge'], 'number'],
            [['no_faktur','no_invoice','no_surat_jalan','card_number'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_gudang' => 'Id Gudang',
            'no_faktur' => 'No Faktur Pajak',
            'no_invoice' => 'No Invoice',
            'no_surat_jalan' => 'No Surat Jalan',
            'tanggal_faktur' => 'Tanggal Faktur',
            'id_supplier' => 'Supplier',
            'harga_beli' => 'Harga (/Unit)',
            'ppn' => 'Ppn',
            'harga_kena_pajak' => 'Harga Setelah Pajak',
            'harga_jual' => 'Harga Jual',
            'harga_jual_2' => 'Harga Jual 2',
            'harga_jual_3' => 'Harga Jual 3',
            'harga_jual_4' => 'Harga Jual 4',
            'jatuh_tempo' => 'Jatuh Tempo',
            'tanggal' => 'Tanggal',
            'admin_id' => 'Admin ID',
			
            'diskon_persen' => 'Diskon (%)',
            'diskon' => 'Diskon',
			
			'total_bayar' => 'Total Bayar',
			'sisa' => 'Sisa',
        ];
    }
	
	// no surat jalan, diskon, no faktur pajak
	
	public function getStokGudang()
    {
        return $this->hasOne(StokGudang::className(), ['id' => 'id_barang']);
    }
	
    public function getBarang()
    {
        return $this->hasOne(Barang::className(), ['id' => 'id_barang']);
    }
	
    public function getSupplier()
    {
        return $this->hasOne(Supplier::className(), ['id' => 'id_supplier']);
    }

    public function getPurchase()
    {
        return $this->hasOne(PurchaseOrder::className(), ['id' => 'id_po']);
    }
}
