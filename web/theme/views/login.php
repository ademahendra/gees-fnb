<?php

/* @var $this yii\web\View */
use dektrium\user\models\LoginForm;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

use backend\components\Dashboard;
$this->title = Yii::$app->params['appName'].' | Login';
?>
<div class="login-box col-md-10 col-md-offset-1">
	<h3>
		<img src="<?= $this->theme->baseUrl ?>/images/logo-big.png" class="img-responsive" />
	</h3>
	<p class="text-center m-t-md">Please login into your account.</p>
	<?php
	$form = ActiveForm::begin([
		'id'                     => 'login-form',
		'class'                  => 'm-t-md',
		'enableAjaxValidation'   => true,
		'enableClientValidation' => false,
		'validateOnBlur'         => false,
		'validateOnType'         => false,
		'validateOnChange'       => false,
	]) ?>
	<?= $form->field(
		$model,
		'login',
		['inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control rounded input-lg text-center no-border', 'tabindex' => '1']]
	) ?>

	<?= $form
		->field(
			$model,
			'password',
			['inputOptions' => ['class' => 'form-control rounded input-lg text-center no-border', 'tabindex' => '2']]
		)
		->passwordInput()
		->label(
			'Password'
			.($module->enablePasswordRecovery ?
				' (' . Html::a(
					'Forgot password?',
					['/user/recovery/request'],
					['tabindex' => '5']
				)
				. ')' : '')
		) ?>

	<?= $form->field($model, 'rememberMe')->checkbox(['tabindex' => '4']) ?>

	<?= Html::submitButton(
		'Login',
		['class' => 'btn btn-success btn-block', 'tabindex' => '3']
	) ?>
<!--
	<?= Html::a(
		'Register',\yii\helpers\Url::to(['/user/register']),
		['class' => 'btn btn-info btn-block', 'tabindex' => '3']
	) ?> -->
	<?php ActiveForm::end(); ?>
	<br />
	<p class="text-center m-t-xs text-sm"><?= date('Y') ?> &copy; <a href="http://www.geesstudios.com/" target="_blank">Gees Studio</a></p>
</div>