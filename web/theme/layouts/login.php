<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
  <meta charset="<?= Yii::$app->charset ?>">
  
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
  <!-- build:css({.tmp,app}) styles/app.min.css -->
  <link rel="stylesheet" href="<?= $this->theme->baseUrl ?>/styles/webfont.css">
  <link rel="stylesheet" href="<?= $this->theme->baseUrl ?>/styles/climacons-font.css">
  <link rel="stylesheet" href="<?= $this->theme->baseUrl ?>/vendor/bootstrap/dist/css/bootstrap.css">
  <link rel="stylesheet" href="<?= $this->theme->baseUrl ?>/styles/font-awesome.css">
  <link rel="stylesheet" href="<?= $this->theme->baseUrl ?>/styles/card.css">
  <link rel="stylesheet" href="<?= $this->theme->baseUrl ?>/styles/sli.css">
  <link rel="stylesheet" href="<?= $this->theme->baseUrl ?>/styles/animate.css">
  <link rel="stylesheet" href="<?= $this->theme->baseUrl ?>/styles/app.css">
  <link rel="stylesheet" href="<?= $this->theme->baseUrl ?>/styles/app.skins.css">
  <link rel="stylesheet" href="<?= $this->theme->baseUrl ?>/styles/custom.css">
  <!-- endbuild -->
   <!--favicon -->
  <link rel="apple-touch-icon" sizes="57x57" href="<?= Yii::$app->params['baseURL'] ?>images/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?= Yii::$app->params['baseURL'] ?>images/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?= Yii::$app->params['baseURL'] ?>images/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?= Yii::$app->params['baseURL'] ?>images/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?= Yii::$app->params['baseURL'] ?>images/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?= Yii::$app->params['baseURL'] ?>images/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?= Yii::$app->params['baseURL'] ?>images/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?= Yii::$app->params['baseURL'] ?>images/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?= Yii::$app->params['baseURL'] ?>images/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?= Yii::$app->params['baseURL'] ?>images/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?= Yii::$app->params['baseURL'] ?>images/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?= Yii::$app->params['baseURL'] ?>images/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?= Yii::$app->params['baseURL'] ?>images/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?= Yii::$app->params['baseURL'] ?>images/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?= Yii::$app->params['baseURL'] ?>images/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
 
  <!--end of favicon -->
  <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
	<script>
		var baseUrl = '<?= Yii::$app->params['baseURL'] ?>';
	</script>
    <?php $this->head() ?>
</head>

<body class="page-loading">
<?php $this->beginBody() ?>
  <!-- page loading spinner -->
  <div class="pageload">
    <div class="pageload-inner">
      <div class="sk-rotating-plane"></div>
    </div>
  </div>
  <!-- /page loading spinner -->
  <div class="app signin v2 usersession">
    <div class="session-wrapper">
      <div class="session-carousel slide" data-ride="carousel" data-interval="3000">
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          <div class="item active" style="background-image:url(../web/theme/images/slide-home-1.jpeg);background-size:cover;background-repeat: no-repeat;background-position: 50% 50%;">
          </div>
        </div>
      </div>
      <div class="card bg-white no-border">
        <div class="card-block">
			<?= $content ?>             
        </div>
      </div>
      <div class="push"></div>
    </div>
  </div>
  <!-- build:js({.tmp,app}) scripts/app.min.js -->
  <script src="<?= $this->theme->baseUrl ?>/scripts/helpers/modernizr.js"></script>
  <script src="<?= $this->theme->baseUrl ?>/vendor/jquery/dist/jquery.js"></script>
  <script src="<?= $this->theme->baseUrl ?>/vendor/bootstrap/dist/js/bootstrap.js"></script>
  <script src="<?= $this->theme->baseUrl ?>/vendor/fastclick/lib/fastclick.js"></script>
  <script src="<?= $this->theme->baseUrl ?>/vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
  <script src="<?= $this->theme->baseUrl ?>/scripts/helpers/smartresize.js"></script>
  <script src="<?= $this->theme->baseUrl ?>/scripts/constants.js"></script>
  <script src="<?= $this->theme->baseUrl ?>/scripts/main.js"></script>
  <!-- endbuild -->

<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>