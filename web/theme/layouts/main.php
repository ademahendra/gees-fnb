<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
if(isset($_COOKIE['lang']) && !empty($_COOKIE['lang'])){
	Yii::$app->language = $_COOKIE['lang'];
} else {
	Yii::$app->language	= 'en-US';
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
	  <!-- build:css({.tmp,app}) styles/app.min.css -->
	  <link rel="stylesheet" href="<?= $this->theme->baseUrl ?>/styles/webfont.css">
	  <link rel="stylesheet" href="<?= $this->theme->baseUrl ?>/styles/climacons-font.css">
	  <link rel="stylesheet" href="<?= $this->theme->baseUrl ?>/vendor/bootstrap/dist/css/bootstrap.css">
	  <link rel="stylesheet" href="<?= $this->theme->baseUrl ?>/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css">
	  <link rel="stylesheet" href="<?= $this->theme->baseUrl ?>/styles/font-awesome.css">
	  <link rel="stylesheet" href="<?= $this->theme->baseUrl ?>/styles/card.css">
	  <link rel="stylesheet" href="<?= $this->theme->baseUrl ?>/styles/sli.css">
	  <link rel="stylesheet" href="<?= $this->theme->baseUrl ?>/styles/animate.css">
	  <link rel="stylesheet" href="<?= $this->theme->baseUrl ?>/styles/app.css">
	  <link rel="stylesheet" href="<?= $this->theme->baseUrl ?>/styles/app.skins.css">
	  <link rel="stylesheet" href="<?= $this->theme->baseUrl ?>/styles/custom.css">
	   <!--favicon -->
  <link rel="apple-touch-icon" sizes="57x57" href="<?= Yii::$app->params['baseURL'] ?>images/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?= Yii::$app->params['baseURL'] ?>images/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?= Yii::$app->params['baseURL'] ?>images/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?= Yii::$app->params['baseURL'] ?>images/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?= Yii::$app->params['baseURL'] ?>images/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?= Yii::$app->params['baseURL'] ?>images/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?= Yii::$app->params['baseURL'] ?>images/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?= Yii::$app->params['baseURL'] ?>images/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?= Yii::$app->params['baseURL'] ?>images/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?= Yii::$app->params['baseURL'] ?>images/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?= Yii::$app->params['baseURL'] ?>images/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?= Yii::$app->params['baseURL'] ?>images/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?= Yii::$app->params['baseURL'] ?>images/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?= Yii::$app->params['baseURL'] ?>images/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?= Yii::$app->params['baseURL'] ?>images/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
 
  <!--end of favicon -->
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
	<script>
		var baseUrl = '<?= Yii::$app->params['baseURL'] ?>';
	</script>
    <?php $this->head() ?>
</head>
<body class="page-loading">
<?php $this->beginBody() ?>
<!-- page loading spinner -->
  <div class="pageload">
    <div class="pageload-inner">
      <div class="sk-rotating-plane"></div>
    </div>
  </div>
  <!-- /page loading spinner -->
  <div class="app layout-fixed-header">
	<?= $this->render('nav') ?>
    <!-- content panel -->
    <div class="main-panel">	
		<?= $this->render('headernav') ?>
		<div class="main-content">
		<?= $content ?>    
		</div>
	</div>
    <!-- /content panel -->
   <?= $this->render('footer') ?>
  </div>


<!-- build:js({.tmp,app}) scripts/app.min.js -->
<script src="<?= $this->theme->baseUrl ?>/scripts/helpers/modernizr.js"></script>
<script src="<?= $this->theme->baseUrl ?>/vendor/jquery/dist/jquery.js"></script>
<script src="<?= $this->theme->baseUrl ?>/scripts/noconflict.min.js"></script>
<script src="<?= $this->theme->baseUrl ?>/vendor/bootstrap/dist/js/bootstrap.js"></script>
<script src="<?= $this->theme->baseUrl ?>/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?= $this->theme->baseUrl ?>/vendor/fastclick/lib/fastclick.js"></script>
<script src="<?= $this->theme->baseUrl ?>/vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
<script src="<?= $this->theme->baseUrl ?>/scripts/helpers/smartresize.js"></script>
<script src="<?= $this->theme->baseUrl ?>/scripts/constants.js"></script>
<script src="<?= $this->theme->baseUrl ?>/scripts/main.js"></script>
<script src="<?= $this->theme->baseUrl ?>/scripts/custom.js"></script>
<!-- endbuild -->

<?php $this->endBody() ?>
<script>
	$(document).ready(function(){
				// set lang
		$('a.lang').on('click', function(e){
			e.preventDefault();
			createCookie('lang', $(this).attr('data-id'));
			window.location.reload();
		});

		function guid() {
			function s4() {
				return Math.floor((1 + Math.random()) * 0x10000)
						.toString(16)
						.substring(1);
			}
			return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
					s4() + '-' + s4() + s4() + s4();
		}
	});
</script>
</body>
</html>
<?php $this->endPage() ?>
