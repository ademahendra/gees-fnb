<?php
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Menu;
?>   
   <!-- sidebar panel -->
    <div class="sidebar-panel offscreen-left">
      <div class="brand">
      
        <!-- logo -->
        <a class="brand-logo">
          <span><img src="<?= $this->theme->baseUrl ?>/images/logo.png" class="img-responsive" /></span>
        </a>
        <a href="#" class="small-menu-visible brand-logo">F</a>
        <!-- /logo -->
      </div>

	<!-- main navigation -->
	<nav role="navigation">
	<?php
//	 echo \Yii::$app->controller->id." ".Yii::$app->controller->action->id;
		echo Menu::widget([
			'options' => ['class' => 'nav'],
			'items' => [
				['label' => '<i class="icon-home text-info"></i> <span>Dashboard</span>', 'url' => ['/index']],
				['label' => '<i class="icon-notebook text-danger"></i> <span>Menu</span>', 'url' => ['/menu/index'], 'active'=> \Yii::$app->controller->id == 'menu'],
				['label' => '<i class="fa fa-shopping-cart text-danger"></i> <span>Stock</span>', 'url' => ['/stock/index'], 'active'=> \Yii::$app->controller->id == 'stock'],
				['label' => '<i class="icon-layers text-danger"></i> <span>Order</span>', 'url' => ['/pos/create'], 'active'=> \Yii::$app->controller->id == 'pos'],
				['label' => '<i class="icon-layers text-danger"></i> <span>Cashier</span>', 'url' => ['/pos/index'], 'active'=> \Yii::$app->controller->id == 'pos'],
				// [
					// 'label' => '<i class="icon-basket-loaded text-warning"></i> <span>Produksi</span>',
					// 'options' => ['class' => 'menu-accordion'],
					// 'url' => 'javascript:;',
					// 'items' => [
						// ['label' => ' <span>Produksi Harian</span>', 'url' => ['/produksi'], 'active'=> (\Yii::$app->controller->id == 'produksi'  && \Yii::$app->controller->action->id == 'index')],
						// ['label' => ' <span>Record Produksi</span>', 'url' => ['/produksi/record'], 'active'=> (\Yii::$app->controller->id == 'produksi' && \Yii::$app->controller->action->id == 'record')],
						
					// ],
				// ],
				// [
						// 'label' => '<i class="icon-book-open text-success"></i> <span>Report</span>',
						// 'options' => ['class' => 'menu-accordion'],
						// 'url' => 'javascript:;',
						// 'items' => [
								// ['label' => ' <span>Produksi Harian</span>', 'url' => ['/report/laporan-harian'], 'active'=> \Yii::$app->controller->id == 'report/laporan-harian'],

						// ],
				// ],
			],
			'submenuTemplate' => "\n<ul class='sub-menu'>\n{items}\n</ul>\n",
			'encodeLabels' => false,
			'activateParents' => true,
			'activeCssClass' => 'open'
		]);
	?>
	</nav>
    </div>
    <!-- /sidebar panel -->
