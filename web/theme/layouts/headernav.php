<?php
$theme = $this->theme;
switch(Yii::$app->language){
	case 'en-US':
		$activeLanguage = 'EN';	
		break;
	case 'id-ID':
		$activeLanguage = 'ID';	
		break;
	default:
		$activeLanguage = 'EN';
		break;
}
?>
  <!-- top header -->
      <div class="header navbar">
        <div class="brand visible-xs">
          <!-- toggle offscreen menu -->
          <div class="toggle-offscreen">
            <a href="javascript:;" class="hamburger-icon visible-xs" data-toggle="offscreen" data-move="ltr">
              <span></span>
              <span></span>
              <span></span>
            </a>
          </div>
          <!-- /toggle offscreen menu -->
          <!-- logo -->
          <a class="brand-logo">
            <span><?= Yii::$app->params['appName'] ?></span>
          </a>
          <!-- /logo -->
        </div>
        <ul class="nav navbar-nav navbar-right hidden-xs">

        <li>
          <a href="javascript:;" class="ripple" data-toggle="dropdown">
            <i class="icon-bell"></i>
          </a>
          <ul class="dropdown-menu notifications">
            <li class="notifications-header">
              <p class="text-muted small">You have 3 new notification</p>
            </li>
            <li>
              <ul class="notifications-list">
                <li>
                  <a href="javascript:;">
                    <div class="notification-icon">
                      <div class="circle-icon bg-success text-white">
                        <i class="icon-bulb"></i>
                      </div>
                    </div>
                    <span class="notification-message"><b>Sean</b> launched a new application</span>
                    <span class="time">2s</span>
                  </a>
                </li>
                <li>
                  <a href="javascript:;">
                    <div class="notification-icon">
                      <div class="circle-icon bg-danger text-white">
                        <i class="icon-cursor"></i>
                      </div>
                    </div>
                    <span class="notification-message"><b>Removed calendar</b> from app list</span>
                    <span class="time">4h</span>
                  </a>
                </li>
                <li>
                  <a href="javascript:;">
                    <div class="notification-icon">
                      <div class="circle-icon bg-primary text-white">
                        <i class="icon-basket"></i>
                      </div>
                    </div>
                    <span class="notification-message"><b>Denise</b> bought <b>Urban Admin Kit</b></span>
                    <span class="time">2d</span>
                  </a>
                </li>
                <li>
                  <a href="javascript:;">
                    <div class="notification-icon">
                      <div class="circle-icon bg-info text-white">
                        <i class="icon-bubble"></i>
                      </div>
                    </div>
                    <span class="notification-message"><b>Vincent commented</b> on an item</span>
                    <span class="time">2s</span>
                  </a>
                </li>
                <li>
                  <a href="javascript:;">
                      <span class="notification-icon">
                      <img src="<?= $theme->getUrl('images/face3.jpg') ?>" class="avatar img-circle" alt="">
                      </span>
                    <span class="notification-message"><b>Jack Hunt</b> has <b>joined</b> mailing list</span>
                    <span class="time">9d</span>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="javascript:;" class="ripple" data-toggle="dropdown">
            <img src="<?= $theme->getUrl('images/avatar.jpg') ?>" class="header-avatar img-circle" alt="user" title="user">
            <span></span>
            <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            <li>
              <a href="<?= Yii::$app->params['baseURL'].'user/settings' ?>">Settings</a>
            </li>
            <li role="separator" class="divider"></li>
            <li>
              <a href="<?= Yii::$app->params['baseURL'].'user/security/logout' ?>" data-method="post">Logout</a>
            </li>
          </ul>
        </li>
        </ul>
      </div>
      <!-- /top header -->
<?php
$pageScript = <<<JS
	$('a.lang').on('click', function(e){
		e.preventDefault();
		createCookie('lang', $(this).attr('data-id'));
		window.location.reload();
	});
JS;
$this->registerJs($pageScript, \yii\web\View::POS_READY);
?>