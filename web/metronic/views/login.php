<?php

/* @var $this yii\web\View */
use dektrium\user\models\LoginForm;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

use backend\components\Dashboard;
$this->title = Yii::$app->params['appName'].' | Login';
?>

<?php
$form = ActiveForm::begin([
	'id'                     => 'login-form',
	'class'                  => 'login-form',
	'enableAjaxValidation'   => true,
	'enableClientValidation' => false,
	'validateOnBlur'         => false,
	'validateOnType'         => false,
	'validateOnChange'       => false,
]) ?>
<h3 class="form-title">Login to your account</h3>
<div class="form-group">
	<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
	<label class="control-label visible-ie8 visible-ie9">Username</label>
	<div class="input-icon">
		<i class="fa fa-user"></i>
		<?= Html::activeTextinput($model, 'login', ['class'=>"form-control placeholder-no-fix", 'autocomplete' => "off",  'placeholder' => "Username", 'tabindex' => '1', 'autofocus' => 'autofocus']) ?>
	</div>
</div>
<div class="form-group">
	<label class="control-label visible-ie8 visible-ie9">Password</label>
	<div class="input-icon">
		<i class="fa fa-lock"></i>
		<?= Html::activePasswordinput($model, 'password', ['class'=>"form-control placeholder-no-fix", 'placeholder' => "Password", 'tabindex' => '2', 'autofocus' => 'autofocus']) ?>
	</div>
</div>
<div class="form-actions">
	<label class="rememberme mt-checkbox mt-checkbox-outline">
		<?= Html::activeTextinput($model, 'rememberMe', ['type' => 'checkbox', 'tabindex' => '4', 'value' => '1']) ?> Remember me
		<span></span>
	</label>
	<?= Html::submitButton(
	'Login',
	['class' => 'btn green pull-right', 'tabindex' => '3']
) ?>
</div>


<?php if($module->enablePasswordRecovery){ ?>
<div class="forget-password">
	<h4>Forgot your password ?</h4>
	<p> no worries, click
		<?= Html::a(
				'Here',
				['/user/recovery/request'],
				['tabindex' => '5', 'id' => 'forget-password']
			) ?> to reset your password. </p>
</div>
<?php } ?>
<div class="create-account">
	<p> Don't have an account yet ?&nbsp;
		<?= Html::a(
	'Create an Account',\yii\helpers\Url::to(['/user/register']),
	['id' => 'register-btn', 'tabindex' => '3']
) ?>
	</p>
</div>
<?php ActiveForm::end(); ?>
