<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
if(isset($_COOKIE['lang']) && !empty($_COOKIE['lang'])){
    Yii::$app->language = $_COOKIE['lang'];
} else {
    Yii::$app->language	= 'en-US';
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <!-- build:css({.tmp,app}) styles/app.min.css -->
    <meta content="Preview page of Metronic Admin Theme #1 for statistics, charts, recent events and reports" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="<?= $this->theme->baseUrl ?>/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= $this->theme->baseUrl ?>/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= $this->theme->baseUrl ?>/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= $this->theme->baseUrl ?>/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="<?= $this->theme->baseUrl ?>/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= $this->theme->baseUrl ?>/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <link href="<?= $this->theme->baseUrl ?>/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= $this->theme->baseUrl ?>/assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="<?= $this->theme->baseUrl ?>/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="<?= $this->theme->baseUrl ?>/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="<?= $this->theme->baseUrl ?>/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= $this->theme->baseUrl ?>/assets/layouts/layout/css/themes/light2.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="<?= $this->theme->baseUrl ?>/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />

    <!--end of favicon -->
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <script>
        var baseUrl = '<?= Yii::$app->params['baseURL'] ?>';
    </script>
    <?php $this->head() ?>
</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md">
<?php $this->beginBody() ?>
<div class="page-wrapper">
    <!-- page loading spinner -->
    <?= $this->render('headernav') ?>

    <!-- content panel -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar-wrapper">
            <?= $this->render('nav') ?>
        </div>

        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <?= $content ?>
            </div>
        </div>

        <a href="javascript:;" class="page-quick-sidebar-toggler">
            <i class="icon-login"></i>
        </a>
        <?= $this->render('footer') ?>
    </div>
</div>


<!-- build:js({.tmp,app}) scripts/app.min.js -->
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/moment.min.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/horizontal-timeline/horizontal-timeline.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?= $this->theme->baseUrl ?>/assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?= $this->theme->baseUrl ?>/assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?= $this->theme->baseUrl ?>/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<script src="<?= $this->theme->baseUrl ?>/assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>

<script src="<?= $this->theme->baseUrl ?>/assets/global/scripts/custom.js" type="text/javascript"></script>
<!-- endbuild -->

<?php $this->endBody() ?>
<script>
    $(document).ready(function(){
        // set lang
        $('a.lang').on('click', function(e){
            e.preventDefault();
            createCookie('lang', $(this).attr('data-id'));
            window.location.reload();
        });

        function guid() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        }
    });
</script>
</body>
</html>
<?php $this->endPage() ?>
