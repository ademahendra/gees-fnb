<!-- bottom footer -->
<div class="page-footer">
	<div class="page-footer-inner"> <?= date('Y') ?> &copy; Gees restoPOS By
		<a target="_blank" href="http://geesstudios.com">Gees Studio</a> &nbsp;|&nbsp;
		<a href="javascript:;" title="Subscribe Gees restoPOS" target="_blank">Subscribe Gees restoPOS</a>
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- /bottom footer -->