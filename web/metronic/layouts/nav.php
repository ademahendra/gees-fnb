<?php
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Menu;

?>

<div class="page-sidebar navbar-collapse collapse">
    <?php
    $controller = \Yii::$app->controller->id;
    $action = \Yii::$app->controller->action->id;
    $module = \Yii::$app->controller->module->id;
    //	 echo \Yii::$app->controller->id." ".\Yii::$app->controller->action->id;
    echo Menu::widget([
        'options' => ['class' => 'page-sidebar-menu  page-header-fixed'],
        // 'options' => ['class' => 'page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu'],
        'items' => [
            [
                'label' => '<i class="fa fa-home font-green-haze"></i> <span class="title">' . Yii::t('app', 'Dashboard') . '</span>',
                'url' => ['/index']
            ],
            [
                'label' => '<i class="fa fa-cog font-green-haze"></i> <span class="title">Setup</span>',
                'options' => ['class' => 'nav-item'],
                'url' => 'javascript:;',
                'template' => '<a href="{url}" class="nav-link nav-toggle">{label}<span class="arrow"></span></a>',
                'items' => [
                    ['label' => ' <span>Food Category</span>', 'url' => ['/fnb-category'], 'active' => \Yii::$app->controller->id == 'fnb-category'],
					['label' => ' <span>Supplier</span>', 'url' => ['/master/supplier'], 'active' => \Yii::$app->controller->id == 'supplier'],
					['label' => ' <span>Satuan</span>', 'url' => ['/master/satuan'], 'active' => \Yii::$app->controller->id == 'satuan'],
					[
                        'label' => '<span>Customer</span>',
                        'options' => ['class' => 'nav-item'],
                        'url' => 'javascript:;',
						'template' => '<a href="{url}" class="nav-link nav-toggle">{label}<span class="arrow"></span></a>',
                        'items' => [
                            [
								'label' => ' <span>Customer Type</span>', 
								'url' => ['/master/customer-type'], 
								'active' => \Yii::$app->controller->id == 'customer-type'
							],
							[
								'label' => ' <span>Customer</span>', 
								'url' => ['/master/customer'], 
								'active' => \Yii::$app->controller->id == 'customer'
							],
                        ],
                    ],
                ],
            ],
            [
                'label' => '<i class="icon-credit-card"></i> <span class="title">Purchasing</span>',
                'options' => ['class' => 'nav-item'],
                'url' => 'javascript:;',
                'template' => '<a href="{url}" class="nav-link nav-toggle">{label}<span class="arrow"></span></a>',
                'items' => [
                    [
                        'label' => '<span>Order Pembelian (PO)</span>',
                        'url' => ['/purchasing'],
                        'active' => ($module == 'purchasing' && $controller == 'default')
                    ],
                    [
                        'label' => ' <span>Pembelian</span>',
                        'url' => ['/pembelian/create'],
                        'active' => ($module == 'pembelian' && $controller == 'default' && $action == 'create')
                    ],
                    [
                        'label' => ' <span>Daftar Pembelian</span>',
                        'url' => ['/pembelian'],
                        'active' => ($module == 'pembelian' && $controller == 'default' && $action != 'create')
                    ],
                    [
                        'label' => ' <span>Retur Pembelian</span>',
                        'url' => ['/pembelian/retur-pembelian/create'],
                        'active' => ($module == 'pembelian' && $controller == 'retur-pembelian' && $action == 'create')
                    ],
                ],
            ],
            ['label' => '<i class="fa fa-book font-green-haze"></i> <span class="title">Menu</span>', 'url' => ['/menu/index'], 'active' => \Yii::$app->controller->id == 'fnb-ingredients'],
            ['label' => '<i class="fa fa-shopping-cart font-green-haze"></i> <span class="title">Stock</span>', 'url' => ['/stock/index'], 'active' => \Yii::$app->controller->id == 'fnb-stock'],
            ['label' => '<i class="fa fa-edit font-green-haze"></i> <span class="title">Order</span>', 'url' => ['/order/create'], 'active' => (\Yii::$app->controller->id == 'default' && \Yii::$app->controller->action->id == 'create')],
            ['label' => '<i class="fa fa-money font-green-haze"></i> <span class="title">Cashier</span>', 'url' => ['/order/index'], 'active' => (\Yii::$app->controller->id == 'default' && \Yii::$app->controller->action->id != 'create')],

        ],
        'submenuTemplate' => "\n<ul class='sub-menu'>\n{items}\n</ul>\n",
        'encodeLabels' => false,
        'activateParents' => true,
        'activeCssClass' => 'active'
    ]);
    ?>
</div>