<?php
use yii\helpers\Html;
$theme = $this->theme;
switch(Yii::$app->language){
	case 'en-US':
		$activeLanguage = 'EN';	
		$flag = 'us';
		break;
	case 'id-ID':
		$activeLanguage = 'ID';	
		$flag = 'id';
		break;
	default:
		$activeLanguage = 'EN';
		$flag = 'us';
		break;
}
$avatar = '//gravatar.com/avatar/'.Yii::$app->user->identity->profile->gravatar_id.'?s=200';

?>
<div class="page-header navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner ">
		<!-- BEGIN LOGO -->
		<div class="page-logo">
			<a href="<?= Yii::$app->params['baseURL'] ?>">
				<img src="<?= $this->theme->baseUrl ?>/images/logo.png" alt="logo" class="img-responsive logo-default" style="margin:0;"/> </a>
			<div class="menu-toggler sidebar-toggler">
				<span></span>
			</div>
		</div>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
			<span></span>
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
				<!-- BEGIN NOTIFICATION DROPDOWN -->
				<!-- DOC: Apply "dropdown-dark" class after "dropdown-extended" to change the dropdown styte -->
				<!-- DOC: Apply "dropdown-hoverable" class after below "dropdown" and remove data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to enable hover dropdown mode -->
				<!-- DOC: Remove "dropdown-hoverable" and add data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to the below A element with dropdown-toggle class -->
				<li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<i class="icon-bell"></i>
						<span class="badge badge-default"> 7 </span>
					</a>
					<ul class="dropdown-menu">
						<li class="external">
							<h3>
								<span class="bold">12 pending</span> notifications</h3>
							<a href="page_user_profile_1.html">view all</a>
						</li>
						<li>
							<ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
								<li>
									<a href="javascript:;">
										<span class="time">just now</span>
										<span class="details">
											<span class="label label-sm label-icon label-success">
												<i class="fa fa-plus"></i>
											</span> New user registered. </span>
									</a>
								</li>
								<li>
									<a href="javascript:;">
										<span class="time">3 mins</span>
										<span class="details">
											<span class="label label-sm label-icon label-danger">
												<i class="fa fa-bolt"></i>
											</span> Server #12 overloaded. </span>
									</a>
								</li>
								<li>
									<a href="javascript:;">
										<span class="time">10 mins</span>
										<span class="details">
											<span class="label label-sm label-icon label-warning">
												<i class="fa fa-bell-o"></i>
											</span> Server #2 not responding. </span>
									</a>
								</li>
							</ul>
						</li>
					</ul>
				</li>
				<!-- END NOTIFICATION DROPDOWN 
				<li class="dropdown dropdown-language">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<img alt="" src="<?= $this->theme->baseUrl ?>/assets/global/img/flags/<?= $flag ?>.png">
						<span class="langname"> <?= $activeLanguage ?> </span>
						<i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-default">
						<li>
							<a class="lang" data-id="en-US" href="javascript:;">
								<img alt="" src="<?= $this->theme->baseUrl ?>/assets/global/img/flags/us.png"> EN </a>
						</li>
						<li>
							<a class="lang" data-id="id-ID" href="javascript:;">
								<img alt="" src="<?= $this->theme->baseUrl ?>/assets/global/img/flags/id.png"> ID </a>
						</li>
					</ul>
				</li>-->
				<!-- BEGIN USER LOGIN DROPDOWN -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				<li class="dropdown dropdown-user">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						 <?= Html::img($avatar, [
							'class' => 'img-circle',
							'alt' => Yii::$app->user->identity->username,
						]) ?>
						<span class="username username-hide-on-mobile"> <?= Yii::$app->user->identity->username ?> </span>
						<i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-default">
						<li>
							<a href="<?= Yii::$app->params['baseURL'].'user/settings/profile' ?>">
								<i class="icon-user"></i> My Profile </a>
						</li>
						<li role="separator" class="divider"></li>
						<li>
						  <a href="<?= Yii::$app->params['baseURL'].'user/settings/account' ?>"><i class="icon-wrench"></i> Settings</a>
						</li>
					</ul>
				</li>
				<!-- END USER LOGIN DROPDOWN -->
				<!-- BEGIN QUICK SIDEBAR TOGGLER -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				<li class="dropdown dropdown-quick-sidebar-toggler">
					<a href="<?= Yii::$app->params['baseURL'].'user/security/logout' ?>" data-method="post" class="dropdown-toggle">
						<i class="icon-logout"></i>
					</a>
				</li>
				<!-- END QUICK SIDEBAR TOGGLER -->
			</ul>
		</div>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
  <!-- top header 
      
        <ul class="nav navbar-nav navbar-right hidden-xs">
          <li>
            <a href="javascript:;" class="ripple" data-toggle="dropdown">
              <span><?= $activeLanguage ?></span>
              <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
              <li>
                <a class="lang" data-id="en-US" href="javascript:;">English</a>
              </li>
              <li>
                <a class="lang" data-id="id-ID" href="javascript:;">Indonesia</a>
              </li>
            </ul>
          </li>
          
      <!-- /top header -->
<?php
$pageScript = <<<JS
	$('a.lang').on('click', function(e){
		e.preventDefault();
		createCookie('lang', $(this).attr('data-id'));
		window.location.reload();
	});
JS;
$this->registerJs($pageScript, \yii\web\View::POS_READY);
?>