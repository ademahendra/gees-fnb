<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
if(isset($_COOKIE['lang']) && !empty($_COOKIE['lang'])){
	Yii::$app->language = $_COOKIE['lang'];
} else {
	Yii::$app->language	= 'en-US';
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
	  <!-- build:css({.tmp,app}) styles/app.min.css -->
	<meta content="Preview page of Metronic Admin Theme #1 for statistics, charts, recent events and reports" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
	<link href="<?= $this->theme->baseUrl ?>/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="<?= $this->theme->baseUrl ?>/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
	<link href="<?= $this->theme->baseUrl ?>/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="<?= $this->theme->baseUrl ?>/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<link href="<?= $this->theme->baseUrl ?>/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
	<link href="<?= $this->theme->baseUrl ?>/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />

	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL STYLES -->
	<link href="<?= $this->theme->baseUrl ?>/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
	<link href="<?= $this->theme->baseUrl ?>/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<!-- END THEME GLOBAL STYLES -->
	<!-- BEGIN THEME LAYOUT STYLES -->
	<link href="<?= $this->theme->baseUrl ?>/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
	<link href="<?= $this->theme->baseUrl ?>/assets/layouts/layout/css/themes/light2.min.css" rel="stylesheet" type="text/css" id="style_color" />
	<link href="<?= $this->theme->baseUrl ?>/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
	<link href="<?= $this->theme->baseUrl ?>/assets/apps/css/custom.css" rel="stylesheet" type="text/css" />
   <!--begin favicon -->
	<link rel="apple-touch-icon" sizes="57x57" href="<?= $this->theme->baseUrl ?>/icon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?= $this->theme->baseUrl ?>/icon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?= $this->theme->baseUrl ?>/icon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?= $this->theme->baseUrl ?>/icon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?= $this->theme->baseUrl ?>/icon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?= $this->theme->baseUrl ?>/icon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?= $this->theme->baseUrl ?>/icon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?= $this->theme->baseUrl ?>/icon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?= $this->theme->baseUrl ?>/icon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?= $this->theme->baseUrl ?>/icon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?= $this->theme->baseUrl ?>/icon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?= $this->theme->baseUrl ?>/icon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?= $this->theme->baseUrl ?>/icon/favicon-16x16.png">
	<link rel="manifest" href="<?= $this->theme->baseUrl ?>/icon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?= $this->theme->baseUrl ?>/icon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">  
  
  <!--end of favicon -->
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
	<script>
		var baseUrl = '<?= Yii::$app->params['baseURL'] ?>';
	</script>
    <?php $this->head() ?>
</head>
<body class="page-header-fixed page-sidebar-fixed page-sidebar-closed-hide-logo page-content-white page-md">
	<?php $this->beginBody() ?>
	<div class="page-wrapper">
	<!-- page loading spinner -->
	<?= $this->render('headernav') ?>
		
		<!-- content panel -->
		<div class="page-container">
			<!-- BEGIN SIDEBAR -->
			<div class="page-sidebar-wrapper">
			<?= $this->render('nav') ?>
			</div>
		
			<div class="page-content-wrapper">
			<!-- BEGIN CONTENT BODY -->
				<div class="page-content">
					<?= $content ?>    
				</div>
			</div>
		
			<a href="javascript:;" class="page-quick-sidebar-toggler">
				<i class="icon-login"></i>
			</a>
		   <?= $this->render('footer') ?>
	  </div>
  </div>


<!-- build:js({.tmp,app}) scripts/app.min.js -->
	<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
	<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
	<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
	<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
	<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/moment.min.js" type="text/javascript"></script>
	<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
	<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
	<script src="<?= $this->theme->baseUrl ?>/assets/global/scripts/app.min.js" type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script src="<?= $this->theme->baseUrl ?>/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
	<script src="<?= $this->theme->baseUrl ?>/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
	<script src="<?= $this->theme->baseUrl ?>/assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
	
	<script src="<?= $this->theme->baseUrl ?>/assets/global/scripts/custom.js" type="text/javascript"></script>
<!-- endbuild -->

<?php $this->endBody() ?>
<script>
	$(document).ready(function(){
		// set lang
		$('a.lang').on('click', function(e){
			e.preventDefault();
			createCookie('lang', $(this).attr('data-id'));
			window.location.reload();
		});

		function guid() {
			function s4() {
				return Math.floor((1 + Math.random()) * 0x10000)
						.toString(16)
						.substring(1);
			}
			return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
					s4() + '-' + s4() + s4() + s4();
		}
	});
</script>
</body>
</html>
<?php $this->endPage() ?>
