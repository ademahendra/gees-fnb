<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
  <meta charset="<?= Yii::$app->charset ?>">
  
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
  
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
	<link href="<?= $this->theme->baseUrl ?>/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="<?= $this->theme->baseUrl ?>/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
	<link href="<?= $this->theme->baseUrl ?>/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="<?= $this->theme->baseUrl ?>/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<link href="<?= $this->theme->baseUrl ?>/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
	<link href="<?= $this->theme->baseUrl ?>/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL STYLES -->
	<link href="<?= $this->theme->baseUrl ?>/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
	<link href="<?= $this->theme->baseUrl ?>/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<!-- END THEME GLOBAL STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link href="<?= $this->theme->baseUrl ?>/assets/pages/css/login-4.css" rel="stylesheet" type="text/css" />
	<!-- END PAGE LEVEL STYLES -->
	<!-- BEGIN THEME LAYOUT STYLES -->
	<!-- END THEME LAYOUT STYLES -->
  <!-- endbuild -->
     <!--begin favicon -->
	<link rel="apple-touch-icon" sizes="57x57" href="<?= $this->theme->baseUrl ?>/icon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?= $this->theme->baseUrl ?>/icon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?= $this->theme->baseUrl ?>/icon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?= $this->theme->baseUrl ?>/icon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?= $this->theme->baseUrl ?>/icon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?= $this->theme->baseUrl ?>/icon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?= $this->theme->baseUrl ?>/icon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?= $this->theme->baseUrl ?>/icon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?= $this->theme->baseUrl ?>/icon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?= $this->theme->baseUrl ?>/icon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?= $this->theme->baseUrl ?>/icon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?= $this->theme->baseUrl ?>/icon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?= $this->theme->baseUrl ?>/icon/favicon-16x16.png">
	<link rel="manifest" href="<?= $this->theme->baseUrl ?>/icon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?= $this->theme->baseUrl ?>/icon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">  
  <!--end of favicon -->
  <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
	<script>
		var baseUrl = '<?= Yii::$app->params['baseURL'] ?>';
	</script>
    <?php $this->head() ?>
</head>

<body class="login ">
<?php $this->beginBody() ?>
  <!-- page loading spinner -->
	<div class="logo">
		<a href="<?= Yii::$app->params['baseURL'] ?>"><img src="<?= $this->theme->baseUrl ?>/images/logo.png" alt="Gees Accounting" /></a>
	</div>
	<!-- END LOGO -->
	<!-- BEGIN LOGIN -->
	<div class="content">
		<?= $content ?>             
	</div>
	<!-- END LOGIN -->
	<!-- BEGIN COPYRIGHT -->
	<div class="copyright"> <?= date('Y') ?> &copy; Gees Studio. </div>
  <!-- build:js({.tmp,app}) scripts/app.min.js -->
	<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
	<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
	<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
	<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
	<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
	<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
	<script src="<?= $this->theme->baseUrl ?>/assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="<?= $this->theme->baseUrl ?>/assets/global/scripts/app.js" type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?= $this->theme->baseUrl ?>/assets/pages/scripts/login-4.js" type="text/javascript"></script>
	<script src="<?= $this->theme->baseUrl ?>/assets/global/scripts/custom.js" type="text/javascript"></script>
  <!-- endbuild -->

<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>