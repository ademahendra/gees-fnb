<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();

echo "<?php\n";
?>

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
/* use <?= $generator->indexWidgetType === 'grid' ? "yii\\grid\\GridView" : "yii\\widgets\\ListView" ?>; */
<?= $generator->enablePjax ? 'use yii\widgets\Pjax;' : '' ?>

/* @var $this yii\web\View */
<?= !empty($generator->searchModelClass) ? "/* @var \$searchModel " . ltrim($generator->searchModelClass, '\\') . " */\n" : '' ?>
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>;
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);
?>
<div class="card bg-white <?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-index">
    <div class="col-md-12">
        <div class="portlet light portlet-fit bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-notebook font-red"></i>
                    <span class="caption-subject font-red sbold uppercase"><?= "<?= " ?>Html::encode($this->title) ?></span>
                </div>
                <div class="actions">
                    <div class="btn-group btn-group-devided">
                        <?= "<?= " ?>Html::a(<?= $generator->generateString('<i class="icon-plus"></i> New ' . Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>, ['create'], ['class' => 'btn green-turquoise btn-sm']) ?>
                    </div>
                </div>
            </div>
        <div class="portlet-body">
<?php if(!empty($generator->searchModelClass)): ?>
<?= "    <?php " . ($generator->indexWidgetType === 'grid' ? "// " : "") ?>echo $this->render('_search', ['model' => $searchModel]); ?>
<?php endif; ?>
            <div class="table-scrollable table-scrollable-borderless">
                <div id="ajaxCrudDatatable">
            <?= $generator->enablePjax ? '<?php Pjax::begin(); ?>' : '' ?>
            <?php if ($generator->indexWidgetType === 'grid'): ?>
        <?= "<?= " ?>GridView::widget([
                    'dataProvider' => $dataProvider,
                    'pjax'=>true,
                    <?= !empty($generator->searchModelClass) ? "'filterModel' => \$searchModel,\n        'columns' => [\n" : "'columns' => [\n"; ?>
                [
                'class' => 'kartik\grid\SerialColumn',
                'width' => '30px',
                ],

            <?php
            $count = 0;
            if (($tableSchema = $generator->getTableSchema()) === false) {
                foreach ($generator->getColumnNames() as $name) {
                    if (++$count < 6) {
                        echo "            '" . $name . "',\n";
                    } else {
                        echo "            // '" . $name . "',\n";
                    }
                }
            } else {
                foreach ($tableSchema->columns as $column) {
                    $format = $generator->generateColumnFormat($column);
                    if (++$count < 6) {
                        echo "            '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
                    } else {
                        echo "            // '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
                    }
                }
            }
            ?>

                        [
                            'class' => '\kartik\grid\DataColumn',
                            'format' => 'raw',
                            'vAlign' => 'middle',
                            'value' => function ($data) {
                                $view = Html::a('<button class="btn btn-success btn-xs"><span class="fa fa-eye"></span></button> ', Url::to(['view', 'id' => $data->id]), [
                                     'title' => Yii::t('yii', 'View'),
                                     'data-pjax' => 0,
                                     'role' => 'modal-remote',
                                     'data-toggle' => 'tooltip'
                                     ]);
                                     $update = Html::a('<button class="btn btn-warning btn-xs"><span class="fa fa-edit"></span></button> ', Url::to(['update', 'id' => $data->id]), [
                                         'title' => Yii::t('yii', 'Update'),
                                         'data-pjax' => 0,
                                         'role' => 'modal-remote',
                                         'data-toggle' => 'tooltip'
                                         ]);
                                     $delete = Html::a('<button class="btn btn-danger btn-xs"><span class="fa fa-trash"></span></button> ', Url::to(['delete', 'id' => $data->id]), [
                                        'title' => Yii::t('yii', 'Delete'),
                                        'data-pjax' => 0,
                                        'role' => 'modal-remote',
                                        'data-toggle' => 'tooltip',
                                        'data-request-method' => 'post',
                                        'data-confirm-title' => 'Are you sure?',
                                        'data-confirm-message' => 'Are you sure want to delete this item'
                                      ]);
                                    return $view.$update.$delete;
                                }
                            ],
                        ],
                ]); ?>
            <?php else: ?>
                <?= "<?= " ?>ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemOptions' => ['class' => 'item'],
                    'itemView' => function ($model, $key, $index, $widget) {
                        return Html::a(Html::encode($model-><?= $nameAttribute ?>), ['view', <?= $urlParams ?>]);
                    },
                ]) ?>
            <?php endif; ?>
            <?= $generator->enablePjax ? '<?php Pjax::end(); ?>' : '' ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
echo '<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>';
?>