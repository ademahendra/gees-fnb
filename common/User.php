<?php
namespace app\common;
use app\models\MasterCoa2;
use Yii;

use app\models\Users;

class User{
	public static function toUsername($id){
		$user = Users::find()->where(['id' => $id])->one();
		if(isset($user->profile->name) && $user->profile->name !== '' && !empty($user->profile->name) && $user->profile->name !== null){
			return $user->profile->name;
		}
		return isset($user->username)?$user->username:'Unknown';
	}

	public static function getConfig($id){
		$user = MasterCoa2::find()->where(['InternalID' => $id])->one();
		if(isset($user->ACC2Name)){
			return $user->ACC2ID.' - '.$user->ACC2Name;
		}
		return 'Unknown';
	}
	
	public static function readableDate($strDate){
		return date('d M Y h:i:s', strtotime($strDate));
	}
}