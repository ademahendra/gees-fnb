<?php
/**
 * Created by PhpStorm.
 * User: adema
 * Date: 09/05/2017
 * Time: 10.15
 */

namespace app\common\barcode;
use yii\web\AssetBundle;

class BarcodeAssets extends AssetBundle {

    /**
     * @inherit doc
     */
    public function init() {
        $this->sourcePath = __DIR__ . '/assets';
        $this->js = ['jquery-barcode.min.js'];
        $this->depends = ['yii\web\YiiAsset' ];
        parent::init();
    }

}