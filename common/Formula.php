<?php
/**
 * Created by PhpStorm.
 * User: adema
 * Date: 02/03/2017
 * Time: 15:31
 */

namespace app\common;
use Yii;


class Formula
{
    public static function currentAge($startAge, $dateIn){
        $dateDiff = date_diff(new \DateTime(),new \DateTime($dateIn));
        $weekAdded = floor($dateDiff->format('%a')  / 7);
        return $startAge+$weekAdded;
    }
}